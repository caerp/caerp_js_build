/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/base/models/ActionButtonCollection.js":
/*!***************************************************!*\
  !*** ./src/base/models/ActionButtonCollection.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ButtonCollection_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ButtonCollection.js */ "./src/base/models/ButtonCollection.js");
/*
 * File Name :  ActionButtonCollection.js
 * Kept for compatibility use ButtonCollection instead
 */


var ActionButtonCollection = _ButtonCollection_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ActionButtonCollection);

/***/ }),

/***/ "./src/sale_product/components/App.js":
/*!********************************************!*\
  !*** ./src/sale_product/components/App.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Router_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Router.js */ "./src/sale_product/components/Router.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../views/RootComponent.js */ "./src/sale_product/views/RootComponent.js");
/* harmony import */ var _Controller_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Controller.js */ "./src/sale_product/components/Controller.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");






var AppClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().Application.extend({
  channelName: 'app',
  radioEvents: {
    "navigate": 'onNavigate',
    'product:delete': 'onProductDelete',
    'product:duplicate': 'onProductDuplicate',
    "show:modal": "onShowModal",
    "products:export": 'onProductsExport',
    "product:archive": 'onProductArchive'
  },
  region: '#target_content',
  onBeforeStart: function onBeforeStart(app, options) {
    console.log("AppClass.onBeforeStart");
    this.rootView = new _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__.default();
    this.controller = new _Controller_js__WEBPACK_IMPORTED_MODULE_4__.default({
      rootView: this.rootView
    });
    this.router = new _Router_js__WEBPACK_IMPORTED_MODULE_1__.default({
      controller: this.controller
    });
  },
  onStart: function onStart(app, options) {
    this.showView(this.rootView);
    console.log("Starting the history");
    (0,_tools_js__WEBPACK_IMPORTED_MODULE_2__.hideLoader)();
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.start();
  },
  onNavigate: function onNavigate(route_name, parameters) {
    console.log("App.onNavigate");
    var dest_route = route_name;

    if (!_.isUndefined(parameters)) {
      dest_route += "/" + parameters;
    }

    window.location.hash = dest_route;
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.loadUrl(dest_route);
  },
  onShowModal: function onShowModal(view) {
    this.controller.showModal(view);
  },
  onProductDelete: function onProductDelete(view) {
    this.controller.productDelete(view);
  },
  onProductDuplicate: function onProductDuplicate(view) {
    this.controller.productDuplicate(view);
  },
  onProductsExport: function onProductsExport(view) {
    this.controller.productsExport(view);
  },
  onProductArchive: function onProductArchive(view) {
    this.controller.productArchive(view);
  }
});
var App = new AppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (App);

/***/ }),

/***/ "./src/sale_product/components/Controller.js":
/*!***************************************************!*\
  !*** ./src/sale_product/components/Controller.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _models_BaseProductModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/BaseProductModel.js */ "./src/sale_product/models/BaseProductModel.js");



var Controller = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().Object.extend({
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    console.log("Controller initialize");
    this.rootView = options['rootView'];
  },
  index: function index() {
    console.log("Controller.index");
    this.rootView.index();
  },
  showModal: function showModal(view) {
    this.rootView.showModal(view);
  },
  productDuplicate: function productDuplicate(childView) {
    var _this = this;

    console.log("Controller.productDuplicate");
    var model = childView.model;
    var collection = model.collection; // On duplique, la collection est paginée et ne contient donc pas 
    // forcément le duplicata donc on fetch le duplicata séparément

    var request = model.duplicate({}, false);
    request = request.then(function (resp) {
      return collection.fetchSingle(resp['id']);
    });
    request.done(function (model) {
      var dest_route = "/products/" + model.get('id');
      window.location.hash = dest_route;

      _this.rootView.showEditProductForm(model);
    });
  },
  // Product related views
  addProduct: function addProduct() {
    var collection = this.facade.request('get:collection', 'products');
    var model = new _models_BaseProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default({}, {
      collection: collection
    });
    this.rootView.showAddProductForm(model, collection);
  },
  editProduct: function editProduct(modelId) {
    var _this2 = this;

    var collection = this.facade.request('get:collection', 'products');
    var request = collection.fetchSingle(modelId);
    request.then(function (model) {
      return _this2.rootView.showEditProductForm(model);
    });
  },
  // Common model views
  _onModelDeleteSuccess: function _onModelDeleteSuccess() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('success', this, "Vos données ont bien été supprimées");
    this.rootView.index();
  },
  _onModelDeleteError: function _onModelDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  productDelete: function productDelete(childView) {
    console.log("Controller.modelDelete");
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");

    if (result) {
      childView.model.destroy({
        success: this._onModelDeleteSuccess.bind(this),
        error: this._onModelDeleteError.bind(this),
        wait: true
      });
    }
  },
  productsExport: function productsExport() {
    var collection = this.facade.request('get:collection', 'products');
    var model = new _models_BaseProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default({}, {
      collection: collection
    });
    return window.open(model.url() + '?action=export', '_blank');
  },
  productArchive: function productArchive(childView) {
    console.log("Controller.productArchive");
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    var rootView = this.rootView;
    var result = window.confirm("Êtes-vous sûr de vouloir archiver cet élément ? " + "Vous n'y aurez plus accès.");

    if (result) {
      childView.model.save({
        'archived': true
      }, {
        success: function success() {
          messagebus.trigger('success', this, "Vos données ont bien été archivées");
          rootView.index();
        },
        error: function error() {
          messagebus.trigger('error', this, "Une erreur a été rencontrée lors de l' " + "archivage de cet élément");
        },
        patch: true,
        wait: true
      });
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Controller);

/***/ }),

/***/ "./src/sale_product/components/Facade.js":
/*!***********************************************!*\
  !*** ./src/sale_product/components/Facade.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/components/FacadeModelApiMixin.js */ "./src/base/components/FacadeModelApiMixin.js");
/* harmony import */ var _models_CategoryCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/CategoryCollection.js */ "./src/sale_product/models/CategoryCollection.js");
/* harmony import */ var _models_ProductCollection_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/ProductCollection.js */ "./src/sale_product/models/ProductCollection.js");
/* harmony import */ var _models_FilterModel_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/FilterModel.js */ "./src/sale_product/models/FilterModel.js");
/*
Global Api, handling all the model and collection fetch

facade = Radio.channel('facade');
facade.request('get:collection', 'products');
*/








var FacadeClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().Object.extend(_base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_3__.default).extend({
  radioRequests: {
    'get:collection': 'getCollectionRequest',
    'get:collection:filter': 'getCollectionFilterRequest',
    'get:collection:firstpage': 'getCollectionFirstPageRequest',
    'get:collection:lastpage': 'getCollectionLastPageRequest',
    'get:collection:nextpage': 'getCollectionNextPageRequest',
    'get:collection:page': 'getCollectionPageRequest',
    'get:collection:previouspage': 'getCollectionPreviousPageRequest',
    'get:collection:refresh': 'getCollectionRefresh',
    'get:model': 'getModelRequest',
    'load:collection': 'loadCollection',
    'load:model': 'loadModel',
    'set:collection:itemsperpage': 'setCollectionItemsPerPage'
  },
  initialize: function initialize(options) {
    this.models = {};
    this.models['ui_list_filter'] = new _models_FilterModel_js__WEBPACK_IMPORTED_MODULE_6__.default();
    this.collections = {};
    var collection;
    collection = new _models_ProductCollection_js__WEBPACK_IMPORTED_MODULE_5__.default();
    this.collections['products'] = collection;
    collection = new _models_CategoryCollection_js__WEBPACK_IMPORTED_MODULE_4__.default();
    this.collections['categories'] = collection;
  },
  setup: function setup(options) {
    this.setCollectionUrl('categories', options['category_url']);
    this.setCollectionUrl('products', options['context_url']);
  },
  start: function start() {
    /*
     * Fires initial One Page application Load
     */
    return this.loadCollection('categories');
  },
  sendAjaxError: function sendAjaxError(result) {
    var messageBus = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('message');
    messageBus.trigger('error:ajax', result);
  },
  getCollectionFilterRequest: function getCollectionFilterRequest(label, query_params) {
    /*
     * Apply a filter on the given collection
     */
    console.log("Facade.getCollectionFilterRequest");
    console.log(query_params);
    this.collections[label].updateParams(query_params);
    var this_ = this;
    return this.collections[label].getFirstPage().fail(this.sendAjaxError);
  },
  getCollectionFirstPageRequest: function getCollectionFirstPageRequest(label) {
    console.log("Facade.getCollectionFirstPageRequest");
    return this.collections[label].getFirstPage().fail(this.sendAjaxError);
  },
  getCollectionLastPageRequest: function getCollectionLastPageRequest(label) {
    return this.collections[label].getLastPage().fail(this.sendAjaxError);
  },
  getCollectionPageRequest: function getCollectionPageRequest(label, page) {
    return this.collections[label].getPage(page).fail(this.sendAjaxError);
  },
  getCollectionNextPageRequest: function getCollectionNextPageRequest(label) {
    return this.collections[label].getNextPage().fail(this.sendAjaxError);
  },
  getCollectionPreviousPageRequest: function getCollectionPreviousPageRequest(label) {
    return this.collections[label].getPreviousPage().fail(this.sendAjaxError);
  },
  getCollectionRefresh: function getCollectionRefresh(label) {
    var collection = this.collections[label];
    return collection.getPage(collection.currentPage).fail(this.sendAjaxError);
  },
  loadModel: function loadModel(collectionName, modelId) {
    var collection = this.collections[collectionName];
    return collection.fetchSingle(modelId);
  },
  setCollectionItemsPerPage: function setCollectionItemsPerPage(label, items_per_page) {
    this.collections[label].updatePageSize(items_per_page);
    return this.collections[label].getFirstPage().fail(this.sendAjaxError);
  }
});
var Facade = new FacadeClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Facade);

/***/ }),

/***/ "./src/sale_product/components/Router.js":
/*!***********************************************!*\
  !*** ./src/sale_product/components/Router.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var marionette_approuter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! marionette.approuter */ "./node_modules/marionette.approuter/lib/marionette.approuter.esm.js");

var Router = marionette_approuter__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  appRoutes: {
    'index': 'index',
    '': 'index',
    'addproduct': 'addProduct',
    'products/:id': 'editProduct'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Router);

/***/ }),

/***/ "./src/sale_product/models/BaseProductModel.js":
/*!*****************************************************!*\
  !*** ./src/sale_product/models/BaseProductModel.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! base/models/DuplicableMixin.js */ "./src/base/models/DuplicableMixin.js");
/* harmony import */ var _StockOperationCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./StockOperationCollection.js */ "./src/sale_product/models/StockOperationCollection.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name : BaseProductModel.js
 *
 */





var BaseProductModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend(base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_3__.default).extend({
  props: ['id', 'type_', 'label', 'description', 'ht', 'ttc', 'unity', 'tva_id', 'product_id', 'supplier_id', 'supplier_ref', 'supplier_unity_amount', 'supplier_ht', 'purchase_type_id', 'margin_rate', 'category_id', 'category_label', 'ref', 'current_stock', 'stock_operations', 'notes', "locked", 'mode', 'updated_at', 'archived'],
  validation: {
    'label': {
      required: true,
      msg: "Veuillez saisir un nom"
    },
    ht: {
      required: false,
      pattern: "amount",
      msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
    },
    ttc: {
      required: false,
      pattern: "amount",
      msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
    },
    supplier_ht: {
      required: false,
      pattern: "amount",
      msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule"
    },
    margin_rate: [{
      required: false,
      pattern: 'amount',
      msg: "Le coefficient de marge doit être un nombre entre 0 et 1, dans la limite de 5 chiffres après la virgule"
    }, function (value) {
      value = parseInt(value.replace(',', '.'));

      if (value < 0 || value >= 1) {
        return "Le coefficient de marge doit être un nombre entre 0 et 1";
      }
    }]
  },
  icons: {
    sale_product_product: 'box',
    sale_product_material: 'box',
    sale_product_composite: 'product-composite',
    sale_product_training: 'chalkboard-teacher',
    sale_product_work_force: "user",
    sale_product_service_delivery: "hands-helping"
  },
  initialize: function initialize() {
    BaseProductModel.__super__.initialize.apply(this, arguments);

    if ("collection" in arguments) {
      this.collection = arguments.collection;
    }

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.supplier_options = this.config.request('get:options', 'suppliers');
    this.is_complex = false;
    this.populate_stock_operations();
  },
  setupSyncEvents: function setupSyncEvents() {
    console.log("BaseProduct.onSetId");
    this.stopListening(this.stock_operations);
    this.listenTo(this.stock_operations, 'add', this.syncStockOperations);
    this.listenTo(this.stock_operations, 'sync', this.syncStockOperations);
    this.listenTo(this.stock_operations, 'remove', this.syncStockOperations);
    this.listenTo(this.stock_operations, 'change', this.syncStockOperations);
  },
  populate_stock_operations: function populate_stock_operations() {
    if (this.has('stock_operations')) {
      this.stock_operations = new _StockOperationCollection_js__WEBPACK_IMPORTED_MODULE_4__.default(this.get('stock_operations'));
    } else {
      this.stock_operations = new _StockOperationCollection_js__WEBPACK_IMPORTED_MODULE_4__.default([]);
    }

    var url = this.url();

    this.stock_operations.url = function () {
      return url + '/' + "stock_operations";
    };
  },
  syncStockOperations: function syncStockOperations() {
    if (!this.stock_operations) {
      this.populate_stock_operations();
    }

    this.set('stock_operations', this.stock_operations.toJSON());
  },
  category_label: function category_label() {
    return this.get('category_label');
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  ht_label: function ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('ht'), true);
  },
  ttc_label: function ttc_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('ttc'), true);
  },
  supplier_ht_label: function supplier_ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('supplier_ht'), false, false);
  },
  supplier_label: function supplier_label() {
    return this.findLabelFromId('supplier_id', 'label', this.supplier_options);
  },
  matchPattern: function matchPattern(search) {
    if (this.get('label').indexOf(search) !== -1 || this.get('description').indexOf(search) !== -1 || this.category_label().indexOf(search) !== -1) {
      return true;
    }

    return false;
  },
  getIcon: function getIcon() {
    if (_.has(this.icons, this.get('type_'))) {
      return this.icons[this.get('type_')];
    } else {
      return false;
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BaseProductModel);

/***/ }),

/***/ "./src/sale_product/models/CategoryCollection.js":
/*!*******************************************************!*\
  !*** ./src/sale_product/models/CategoryCollection.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CategoryModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CategoryModel.js */ "./src/sale_product/models/CategoryModel.js");
/* harmony import */ var _tools__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../tools */ "./src/tools.js");
/*
 * File Name :  CategoryCollection
 */



var CategoryCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  model: _CategoryModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize() {
    (0,_tools__WEBPACK_IMPORTED_MODULE_2__.sortCollection)(this, 'title', 'asc');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CategoryCollection);

/***/ }),

/***/ "./src/sale_product/models/CategoryModel.js":
/*!**************************************************!*\
  !*** ./src/sale_product/models/CategoryModel.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/*
 * File Name :  CategoryModel
 */


var CategoryModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'title', 'description'],
  validation: {
    title: {
      required: true,
      msg: "Le titre est requis"
    }
  },
  "default": {
    description: ""
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CategoryModel);

/***/ }),

/***/ "./src/sale_product/models/FilterModel.js":
/*!************************************************!*\
  !*** ./src/sale_product/models/FilterModel.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/*
 * File Name :  FilterModel
 */

var FilterModel = backbone__WEBPACK_IMPORTED_MODULE_0___default().Model.extend({
  defaults: {
    currentPage: 0
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FilterModel);

/***/ }),

/***/ "./src/sale_product/models/ProductCollection.js":
/*!******************************************************!*\
  !*** ./src/sale_product/models/ProductCollection.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_paginator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.paginator */ "./node_modules/backbone.paginator/lib/backbone.paginator.js");
/* harmony import */ var backbone_paginator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_paginator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _BaseProductModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BaseProductModel.js */ "./src/sale_product/models/BaseProductModel.js");
/* harmony import */ var _ProductWorkModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductWorkModel.js */ "./src/sale_product/models/ProductWorkModel.js");
/* harmony import */ var _TrainingModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TrainingModel.js */ "./src/sale_product/models/TrainingModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
 * File Name : ProductCollection.js
 */




var ProductCollection = backbone_paginator__WEBPACK_IMPORTED_MODULE_0___default().extend({
  model: function model(modeldict, options) {
    if (modeldict.type_ == 'sale_product_work') {
      return new _ProductWorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default(modeldict, options);
    } else if (modeldict.type_ == 'sale_product_training') {
      return new _TrainingModel_js__WEBPACK_IMPORTED_MODULE_3__.default(modeldict, options);
    } else {
      return new _BaseProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default(modeldict, options);
    }
  },
  state: {
    firstPage: 0,
    pageSize: 10
  },
  queryParams: {
    currentPage: "page",
    pageSize: "items_per_page"
  },
  updatePageSize: function updatePageSize(items_per_page) {
    this.setPageSize(items_per_page);
  },
  updateParams: function updateParams(query_params) {
    _.extend(this.queryParams, query_params);
  },
  buildModel: function buildModel(modelDict, response) {
    this.add([modelDict]);
    return this.get(modelDict['id']);
  },
  fetchSingle: function fetchSingle(modelId) {
    /*
    Fetch a single model and returns a promise resolving the model
     collection.fetchSingle(id).then(
        function(model){console.log(model.get('name'))}
    );
    */
    var result;
    var model = this.get(modelId);

    if (_.isUndefined(model)) {
      // On fetche le modèle en utilisant un BaseProductModel et on recrée 
      // un modèle en utilisant la collection qui va créer le bon type de produit
      model = new _BaseProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
        id: modelId
      }, {
        collection: this
      });
      model.url = this.url + '/' + modelId;
      result = model.fetch();
      result = result.then(this.buildModel.bind(this));
    } else {
      result = $.Deferred();
      result.resolve(model, null, null);
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCollection);

/***/ }),

/***/ "./src/sale_product/models/ProductWorkModel.js":
/*!*****************************************************!*\
  !*** ./src/sale_product/models/ProductWorkModel.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./WorkItemCollection.js */ "./src/sale_product/models/WorkItemCollection.js");
/* harmony import */ var _BaseProductModel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./BaseProductModel.js */ "./src/sale_product/models/BaseProductModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/*
 * File Name :  ProductWorkModel
 */





var ProductWorkModel = _BaseProductModel_js__WEBPACK_IMPORTED_MODULE_4__.default.extend({
  /* props spécifique à ce modèle */
  props: ['title', 'items', 'flat_cost'],
  defaults: function defaults() {
    return {};
  },
  validation: {
    title: {
      required: true,
      msg: "Veuillez saisir un titre"
    },
    items: function items(value) {
      if (value.length === 0) {
        return "Veuillez saisir au moins un produit";
      }
    }
  },
  initialize: function initialize() {
    ProductWorkModel.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.is_complex = true;
    this.populate();
  },
  setupSyncEvents: function setupSyncEvents() {
    /* Launched when id is set */
    console.log("ProductWorkModel.onSetId");

    ProductWorkModel.__super__.setupSyncEvents.apply(this, arguments);

    this.stopListening(this.items);
    this.listenTo(this.items, 'saved', this.syncWithItems);
    this.listenTo(this.items, 'destroyed', this.syncWithItems);
  },
  populate: function populate() {
    console.log("ProductWorkModel.populate");

    if (this.has('items')) {
      this.items = new _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_3__.default(this.get('items'));
    } else {
      this.items = new _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_3__.default([]);
    }

    this.items.stopListening(this);
    this.items.listenTo(this, 'saved', this.items.syncAll.bind(this.items));
    this.items._parent = this;
    var this_ = this;

    this.items.url = function () {
      return this_.url() + '/' + "work_items";
    };

    this.items.syncAll();
  },
  syncWithItems: function syncWithItems() {
    this.set('items', this.items.toJSON());
    this.fetch();
  },
  supplier_ht_label: function supplier_ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('flat_cost'));
  },
  supplier_label: function supplier_label() {
    return "-";
  },
  ht_label: function ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('ht'));
  },
  getIcons: function getIcons() {
    var icons = [];
    this.items.models.forEach(function (model) {
      var icon = model.getIcon();

      if (icon) {
        icons.push(icon);
      }
    });
    return _toConsumableArray(new Set(icons));
  },
  refreshWorkItems: function refreshWorkItems() {
    this.items.syncAll();
  }
});
/*
 * On complète les 'props' du BaseProductModel avec celle du ProductWorkModel
 */

ProductWorkModel.prototype.props = ProductWorkModel.prototype.props.concat(_BaseProductModel_js__WEBPACK_IMPORTED_MODULE_4__.default.prototype.props);

_.extend(ProductWorkModel.prototype.validation, _BaseProductModel_js__WEBPACK_IMPORTED_MODULE_4__.default.prototype.validation);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductWorkModel);

/***/ }),

/***/ "./src/sale_product/models/StockOperationCollection.js":
/*!*************************************************************!*\
  !*** ./src/sale_product/models/StockOperationCollection.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _StockOperationModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StockOperationModel.js */ "./src/sale_product/models/StockOperationModel.js");
/*
 * File Name :  StockOperationCollection
 */


var StockOperationCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _StockOperationModel_js__WEBPACK_IMPORTED_MODULE_1__.default
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StockOperationCollection);

/***/ }),

/***/ "./src/sale_product/models/StockOperationModel.js":
/*!********************************************************!*\
  !*** ./src/sale_product/models/StockOperationModel.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/DuplicableMixin.js */ "./src/base/models/DuplicableMixin.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/*
 * File Name : StockOperationModel.js
 *
 */




var StockOperationModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend(_base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_2__.default).extend({
  props: ['id', 'date', 'description', 'stock_variation', 'base_sale_product_id'],
  defaults: {
    date: (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.dateToIso)(new Date()),
    stock_variation: 0
  },
  validation: {
    date: {
      required: true,
      pattern: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
      msg: "Veuillez saisir une date valide"
    },
    stock_variation: {
      required: true,
      pattern: 'amount',
      msg: "Veuillez saisir la quantité de variation du stock"
    }
  },
  initialize: function initialize() {
    StockOperationModel.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StockOperationModel);

/***/ }),

/***/ "./src/sale_product/models/TrainingModel.js":
/*!**************************************************!*\
  !*** ./src/sale_product/models/TrainingModel.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ProductWorkModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductWorkModel.js */ "./src/sale_product/models/ProductWorkModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name :  TrainingModel
 */



var TrainingModel = _ProductWorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  /* props spécifique à ce modèle */
  props: ["goals", "prerequisites", "for_who", "duration", "content", "teaching_method", "logistics_means", "more_stuff", "evaluation", "place", "modality_one", "modality_two", "types", "date", "price", "free_1", "free_2", "free_3"],
  validation: {},
  defaults: {
    types: []
  },
  initialize: function initialize() {
    TrainingModel.__super__.initialize.apply(this, arguments);

    this.on('change:types', this.ensureTypesIsList, this);
  },
  ensureTypesIsList: function ensureTypesIsList() {
    var types = this.get('types');

    if (!_.isArray(types)) {
      this.attributes['types'] = [types];
    }
  },
  getIcons: function getIcons() {
    return "chalkboard-teacher";
  }
});
/*
 * On complète les 'props' du ProductWorkModel avec celle du TrainingModel
 */

TrainingModel.prototype.props = TrainingModel.prototype.props.concat(_ProductWorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default.prototype.props);

_.extend(TrainingModel.prototype.validation, _ProductWorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default.prototype.validation);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TrainingModel);

/***/ }),

/***/ "./src/sale_product/models/WorkItemCollection.js":
/*!*******************************************************!*\
  !*** ./src/sale_product/models/WorkItemCollection.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _WorkItemModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemModel.js */ "./src/sale_product/models/WorkItemModel.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
 * File Name :  WorkItemCollection
 */




var WorkItemCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _WorkItemModel_js__WEBPACK_IMPORTED_MODULE_2__.default,
  load_from_catalog: function load_from_catalog(models) {
    var base_sale_product_ids = _.pluck(models, 'id');

    var serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.url() + '?action=load_from_catalog', {
      base_sale_product_ids: base_sale_product_ids
    }, 'POST');
    return serverRequest.then(this.fetch.bind(this));
  },
  syncAll: function syncAll(models) {
    var _$;

    console.log("WorkItemCollection Syncing all models");
    var promises = [];
    this.each(function (model) {
      promises.push(model.fetch());
    });

    var resulting_deferred = (_$ = $).when.apply(_$, promises);

    return resulting_deferred;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollection);

/***/ }),

/***/ "./src/sale_product/models/WorkItemModel.js":
/*!**************************************************!*\
  !*** ./src/sale_product/models/WorkItemModel.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name :  WorkItemModel
 */



var WorkItemModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  props: ["id", "type_", "label", "description", "ht", "supplier_ht", "quantity", "unity", 'base_sale_product_id', "sync_catalog", 'locked', 'mode', "total_ht"],
  catalog_attributes: ['ht', 'supplier_ht', 'mode', 'unity'],
  defaults: function defaults() {
    return {
      quantity: 1,
      locked: true,
      mode: "supplier_ht"
    };
  },
  validation: {
    'label': {
      required: true,
      msg: "Veuillez saisir un nom"
    },
    description: {
      required: true,
      msg: "Veuillez saisir une description"
    },
    type_: {
      required: true,
      msg: "Veuillez choisir un type de produit"
    }
  },
  icons: {
    sale_product_product: 'box',
    sale_product_material: 'box',
    sale_product_composite: 'product-composite',
    sale_product_training: 'chalkboard-teacher',
    sale_product_work_force: "user",
    sale_product_service_delivery: "hands-helping"
  },
  initialize: function initialize() {
    base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
  },
  supplier_ht_label: function supplier_ht_label() {
    if (this.get('mode') == 'supplier_ht') {
      return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('supplier_ht'), false, false);
    } else {
      return null;
    }
  },
  ht_label: function ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('ht'), false, false);
  },
  total_ht_label: function total_ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.get('total_ht'), false, false);
  },
  getIcon: function getIcon() {
    if (_.has(this.icons, this.get('type_'))) {
      return this.icons[this.get('type_')];
    } else {
      return false;
    }
  },
  isFromCatalog: function isFromCatalog(attribute) {
    /*
     * Check if the given attribute comes from the catalog
     * Not editable
     */
    if (!this.get('id')) {
      // Add mode
      return false;
    }

    if (!this.get('locked')) {
      return false;
    }

    return this.catalog_attributes.indexOf(attribute) >= 0;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemModel);

/***/ }),

/***/ "./src/sale_product/sale_product.js":
/*!******************************************!*\
  !*** ./src/sale_product/sale_product.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _backbone_tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var _components_App_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/App.js */ "./src/sale_product/components/App.js");
/* harmony import */ var _components_Facade_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Facade.js */ "./src/sale_product/components/Facade.js");
/* global AppOption; */






jquery__WEBPACK_IMPORTED_MODULE_0___default()(function () {
  (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_2__.applicationStartup)(AppOption, _components_App_js__WEBPACK_IMPORTED_MODULE_3__.default, _components_Facade_js__WEBPACK_IMPORTED_MODULE_4__.default);
});

/***/ }),

/***/ "./src/sale_product/views/RootComponent.js":
/*!*************************************************!*\
  !*** ./src/sale_product/views/RootComponent.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _list_ProductListComponent_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./list/ProductListComponent.js */ "./src/sale_product/views/list/ProductListComponent.js");
/* harmony import */ var _product_form_ProductForm_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product_form/ProductForm.js */ "./src/sale_product/views/product_form/ProductForm.js");
/* harmony import */ var _product_form_AddProductForm_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product_form/AddProductForm.js */ "./src/sale_product/views/product_form/AddProductForm.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * Module name : RootComponent
 */






var template = __webpack_require__(/*! ./templates/RootComponent.mustache */ "./src/sale_product/views/templates/RootComponent.mustache");

var RootComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  regions: {
    main: '.main',
    modalRegion: '.modal-container'
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'list:filter': 'onListFilter',
    'list:navigate': 'onListNavigate'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.filter_model = this.facade.request('get:model', 'ui_list_filter');
  },
  index: function index() {
    /*
     * Show the List view
     */
    var collection = this.facade.request('get:collection', 'products');
    this.showChildView('main', new _list_ProductListComponent_js__WEBPACK_IMPORTED_MODULE_1__.default({
      collection: collection
    }));
  },
  templateContext: function templateContext() {
    return {};
  },
  isLoaded: function isLoaded() {
    /*
     * Check if datas has already been loaded (can not be the case in case
     * of other route income)
     */
    return !_.isUndefined(this.collection);
  },
  showAddProductForm: function showAddProductForm(model, collection) {
    /*
     * Launched when an add button is clicked, build a temporary model and
     * shows the add form
     */
    var view = new _product_form_AddProductForm_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: model,
      destCollection: collection
    });
    this.showChildView('main', view);
  },
  showEditProductForm: function showEditProductForm(model) {
    var view = new _product_form_ProductForm_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model,
      destCollection: model.collection
    });
    this.showChildView('main', view);
  },
  showModal: function showModal(view) {
    this.showChildView('modalRegion', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RootComponent);

/***/ }),

/***/ "./src/sale_product/views/categories/CategoriesComponent.js":
/*!******************************************************************!*\
  !*** ./src/sale_product/views/categories/CategoriesComponent.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _models_CategoryModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/CategoryModel.js */ "./src/sale_product/models/CategoryModel.js");
/* harmony import */ var _CategoryForm_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CategoryForm.js */ "./src/sale_product/views/categories/CategoryForm.js");
/*
 * Module name : CategoriesComponent
 */





var template = __webpack_require__(/*! ./templates/CategoriesComponent.mustache */ "./src/sale_product/views/categories/templates/CategoriesComponent.mustache");

var CategoryView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  tagName: 'tr',
  className: 'clickable-row',
  template: __webpack_require__(/*! ./templates/CategoryView.mustache */ "./src/sale_product/views/categories/templates/CategoryView.mustache"),
  ui: {
    edit: "button.edit",
    "delete": "button.delete",
    clickableTd: "td:not(.col_actions)"
  },
  events: {
    'click @ui.edit': "onEdit",
    'click @ui.delete': "onDelete",
    "click @ui.clickableTd": "onEdit"
  },
  modelEvents: {
    'change': 'render'
  },
  onEdit: function onEdit() {
    this.triggerMethod('category:edit', this.model, this);
  },
  onDelete: function onDelete() {
    this.triggerMethod('category:delete', this.model, this);
  }
});
var NoCategoryView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  tagName: 'td',
  template: __webpack_require__(/*! ./templates/CategoryEmptyView.mustache */ "./src/sale_product/views/categories/templates/CategoryEmptyView.mustache")
});
var CategoryCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  template: __webpack_require__(/*! ./templates/CategoryCollectionView.mustache */ "./src/sale_product/views/categories/templates/CategoryCollectionView.mustache"),
  childView: CategoryView,
  childViewContainer: 'tbody',
  childViewTriggers: {
    'category:edit': 'category:edit',
    'category:delete': 'category:delete'
  },
  emptyView: NoCategoryView,
  ui: {
    add_button: "button.add"
  },
  events: {
    'click @ui.add_button': "onAddClicked"
  },
  onAddClicked: function onAddClicked() {
    this.triggerMethod('category:add');
  }
});
var CategoriesComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  regions: {
    list: '.category-list',
    modal: '.modal-container'
  },
  // Listen to child view events
  childViewEvents: {
    'category:add': "onAdd",
    'category:edit': "onEdit",
    'category:delete': "onDelete"
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {
    this.showChildView('list', new CategoryCollectionView({
      collection: this.collection
    }));
  },
  onDeleteSuccess: function onDeleteSuccess() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('success', this, "Vos données ont bien été supprimées");
  },
  onDeleteError: function onDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  showForm: function showForm(model, edit) {
    var view = new _CategoryForm_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model,
      destCollection: this.collection,
      edit: edit
    });
    this.showChildView('modal', view);
  },
  onAdd: function onAdd() {
    var model = new _models_CategoryModel_js__WEBPACK_IMPORTED_MODULE_1__.default();
    this.showForm(model, false);
  },
  onEdit: function onEdit(model, childView) {
    this.showForm(model, true);
  },
  onDelete: function onDelete(model, childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette catégorie ?");

    if (result) {
      model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError,
        wait: true
      });
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CategoriesComponent);

/***/ }),

/***/ "./src/sale_product/views/categories/CategoryForm.js":
/*!***********************************************************!*\
  !*** ./src/sale_product/views/categories/CategoryForm.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_behaviors_ModalFormBehavior__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/behaviors/ModalFormBehavior */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/*
 * Module name : CategoryForm
 */






var template = __webpack_require__(/*! ./templates/CategoryForm.mustache */ "./src/sale_product/views/categories/templates/CategoryForm.mustache");

var CategoryForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  id: "category-modal",
  behaviors: [_base_behaviors_ModalFormBehavior__WEBPACK_IMPORTED_MODULE_1__.default],
  regions: {
    title: ".field-title",
    description: ".field-description"
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onSyncSuccess: function onSyncSuccess() {},
  onRender: function onRender() {
    this.showChildView('title', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'title',
      value: this.model.get('title'),
      label: "Titre de la catégorie",
      required: true
    }));
    this.showChildView('description', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      field_name: 'description',
      value: this.model.get('description'),
      label: "Description"
    }));
  },
  templateContext: function templateContext() {
    var title = "Ajouter";

    if (this.getOption('edit')) {
      title = "Modifier";
    }

    return {
      edit: this.getOption('edit'),
      title: title
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CategoryForm);

/***/ }),

/***/ "./src/sale_product/views/list/ProductEmptyView.js":
/*!*********************************************************!*\
  !*** ./src/sale_product/views/list/ProductEmptyView.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : ProductEmptyView
 */



var template = __webpack_require__(/*! ./templates/ProductEmptyView.mustache */ "./src/sale_product/views/list/templates/ProductEmptyView.mustache");

var ProductEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  template: template,
  regions: {},
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {},
  templateContext: function templateContext() {
    return {};
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductEmptyView);

/***/ }),

/***/ "./src/sale_product/views/list/ProductFilterForm.js":
/*!**********************************************************!*\
  !*** ./src/sale_product/views/list/ProductFilterForm.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../tools.js */ "./src/tools.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/*
 * Module name : ProductFilterForm
 */






var template = __webpack_require__(/*! ./templates/ProductFilterForm.mustache */ "./src/sale_product/views/list/templates/ProductFilterForm.mustache");

var DeleteFiltersView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: __webpack_require__(/*! ./templates/DeleteFiltersView.mustache */ "./src/sale_product/views/list/templates/DeleteFiltersView.mustache")
});
var ProductFilterForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  childView: DeleteFiltersView,
  regions: {
    'delete_filters': '#delete_filters',
    type_: '.field-type_',
    'name': '.field-name',
    'description': '.field-description',
    'category_id': '.field-category-id',
    'ref': '.field-ref',
    'supplier_id': '.field-supplier-id',
    'supplier_ref': '.field-supplier-ref',
    'mode': '.field-mode'
  },
  ui: {
    submit_button: 'button[type=submit]',
    form: 'form',
    "delete": ".handle_delete_filters"
  },
  events: {
    'click @ui.submit_button': 'onSubmitClicked',
    'click @ui.delete': "onDeleteFilters"
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.model = this.getOption('model');
    this.categories = this.facade.request('get:collection', 'categories');
    this.listenTo(this.categories, 'add', this.render);
    this.listenTo(this.categories, 'sync', this.render);
    this.listenTo(this.categories, 'remove', this.render);
  },
  onRender: function onRender() {
    var product_types = this.config.request('get:options', "product_types");
    console.log(product_types);
    this.showChildView('type_', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      field_name: 'type_',
      label: 'Type de produit',
      options: product_types,
      id_key: 'value',
      label_key: 'label',
      placeholder: "Choisir un type de produit",
      value: this.model.get('type_')
    }));
    this.showChildView('name', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'search',
      placeholder: 'Nom',
      label: 'Nom interne',
      dataList: this.config.request('get:options', 'product_labels'),
      value: this.model.get('search')
    }));
    this.showChildView('description', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'description',
      placeholder: 'Description',
      label: 'Description et notes',
      dataList: this.config.request('get:options', 'product_descriptions'),
      value: this.model.get('description')
    }));
    this.showChildView('supplier_ref', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'supplier_ref',
      placeholder: 'Référence fournisseur',
      label: 'Référence fournisseur',
      dataList: this.config.request('get:options', 'product_suppliers_refs'),
      value: this.model.get('supplier_ref')
    }));
    var categories = this.categories.toJSON();

    if (categories.length > 0) {
      this.showChildView('category_id', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        field_name: 'category_id',
        label: 'Catégorie',
        options: categories,
        id_key: 'id',
        label_key: 'title',
        placeholder: 'Choisir une catégorie',
        value: this.model.get('category_id')
      }));
    }

    this.showChildView('ref', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'ref',
      placeholder: 'Référence',
      label: 'Référence',
      dataList: this.config.request('get:options', 'references'),
      value: this.model.get('ref')
    }));
    var suppliers = this.config.request('get:options', 'suppliers');

    if (suppliers.length > 0) {
      this.showChildView('supplier_id', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        field_name: 'supplier_id',
        label: 'Fournisseur',
        options: suppliers,
        id_key: 'id',
        label_key: 'label',
        placeholder: 'Choisir un fournisseur',
        value: this.model.get('supplier_id')
      }));
    }

    var modes = this.config.request('get:options', 'modes');

    if (modes.length > 1) {
      this.showChildView('mode', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        field_name: 'mode',
        label: "Mode de saisie",
        options: [{
          value: 'ht',
          label: "HT"
        }, {
          value: "ttc",
          label: "TTC"
        }, {
          value: 'supplier_ht',
          label: "Coût d'achat"
        }],
        id_key: "value",
        label_key: "label",
        placeholder: "Choisir un mode de saisie",
        value: this.model.get('mode')
      }));
    }
  },
  templateContext: function templateContext() {
    return {
      productReferences: this.config.request('get:options', 'references')
    };
  },
  toggleDeleteFilters: function toggleDeleteFilters(show) {
    if (show) {
      this.showChildView('delete_filters', new DeleteFiltersView({
        collection: this.collection
      }));
    } else {
      this.getChildView('delete_filters') && this.getChildView('delete_filters').destroy();
    }
  },
  resetForm: function resetForm() {
    this.ui.form[0].reset();
    this.ui.form.find('select').prop("selectedIndex", 0);
  },
  getFiltersValues: function getFiltersValues() {
    var data = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.serializeForm)(this.ui.form);
    var empty = Object.values(data).some(function (val) {
      return val !== '';
    });
    this.toggleDeleteFilters(empty);
    this.model.set(data);
    return data;
  },
  onDeleteFilters: function onDeleteFilters(event) {
    this.resetForm();
    this.toggleDeleteFilters(false);
    var data = this.getFiltersValues();
    this.triggerMethod('list:filter', this, data);
  },
  onSubmitClicked: function onSubmitClicked(event) {
    event.preventDefault();
    var data = this.getFiltersValues();
    this.triggerMethod('filter:submit', this, data);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductFilterForm);

/***/ }),

/***/ "./src/sale_product/views/list/ProductListComponent.js":
/*!*************************************************************!*\
  !*** ./src/sale_product/views/list/ProductListComponent.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _base_views_MessageView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../base/views/MessageView.js */ "./src/base/views/MessageView.js");
/* harmony import */ var _categories_CategoriesComponent_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../categories/CategoriesComponent.js */ "./src/sale_product/views/categories/CategoriesComponent.js");
/* harmony import */ var _ProductTable_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ProductTable.js */ "./src/sale_product/views/list/ProductTable.js");
/* harmony import */ var _ProductFilterForm_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ProductFilterForm.js */ "./src/sale_product/views/list/ProductFilterForm.js");
/* harmony import */ var _widgets_PagerWidget_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../widgets/PagerWidget.js */ "./src/widgets/PagerWidget.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
 * File Name : ProductListComponent.js
 *
 */











var template = __webpack_require__(/*! ./templates/ProductListComponent.mustache */ "./src/sale_product/views/list/templates/ProductListComponent.mustache");

var ProductListComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default().View.extend({
  /*
   * A list view : filter + list + category component
   *
   * takes 2 parameters:
   *
   *  collection
   *  filter_model
   */
  template: template,
  regions: {
    categoryContainer: '.category-container',
    filters: '.search_filters',
    messageContainer: {
      el: ".message-container",
      replaceElement: true
    },
    pager_widget_bottom: {
      el: '.pager_widget_bottom',
      replaceElement: true
    },
    pager_widget_top: {
      el: '.pager_widget_top',
      replaceElement: true
    },
    table: ".table_container"
  },
  ui: {
    add_button: 'button[value=add]',
    export_button: 'button[value=export]'
  },
  events: {
    'click @ui.add_button': 'onAddButtonClicked',
    'click @ui.export_button': 'onExportButtonClicked'
  },
  // Listen to child view events
  childViewEvents: {
    'filter:submit': 'onListFilter',
    'list:filter': 'onListFilter',
    'list:navigate': 'onListNavigate',
    'model:delete': 'onModelDelete',
    'model:duplicate': 'onModelDuplicate',
    'model:edit': 'onModelEdit',
    'model:archive': 'onModelArchive',
    'navigate:itemsperpage': 'onNavigateItemsPerPage',
    'navigate:page': 'onNavigatePage'
  },
  initialize: function initialize() {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.filter_model = this.facade.request('get:model', 'ui_list_filter');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('app');
  },
  showFilters: function showFilters() {
    this.filterView = new _ProductFilterForm_js__WEBPACK_IMPORTED_MODULE_7__.default({
      model: this.filter_model
    });
    this.showChildView('filters', this.filterView);
  },
  loadTable: function loadTable() {
    var serverCall = this.facade.request('get:collection:page', 'products', this.collection.state.currentPage);
    serverCall.done(this.showTable.bind(this));
    serverCall.done(this.showPagerWidgets.bind(this));
  },
  showPagerWidgets: function showPagerWidgets() {
    if (this.collection.length > 0) {
      this.showChildView('pager_widget_top', new _widgets_PagerWidget_js__WEBPACK_IMPORTED_MODULE_8__.default({
        collection: this.collection,
        position: 'top'
      }));
      this.showChildView('pager_widget_bottom', new _widgets_PagerWidget_js__WEBPACK_IMPORTED_MODULE_8__.default({
        collection: this.collection,
        position: 'bottom'
      }));
    }
  },
  showTable: function showTable() {
    this.filter_model.set('currentPage', this.collection.state['currentPage']);
    var view = new _ProductTable_js__WEBPACK_IMPORTED_MODULE_6__.default({
      collection: this.collection
    });
    this.showChildView('table', view);
  },
  showCategories: function showCategories() {
    var collection = this.facade.request('get:collection', 'categories');
    var view = new _categories_CategoriesComponent_js__WEBPACK_IMPORTED_MODULE_5__.default({
      collection: collection
    });
    this.showChildView('categoryContainer', view);
  },
  showMessageView: function showMessageView() {
    var model = new (backbone__WEBPACK_IMPORTED_MODULE_0___default().Model)();
    var view = new _base_views_MessageView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      model: model
    });
    this.showChildView('messageContainer', view);
  },
  onRender: function onRender() {
    this.showMessageView();
    this.showFilters();
    this.showCategories();
    this.loadTable();
  },

  /* List related events */
  onNavigate: function onNavigate(event) {
    /*
     * The event target has an event-type (getPreviousPage, getNextPage
     * ...) attached that is used for the facade request
     * */
    var event_type = $(event.target).data('event-type');
    this.triggerMethod('list:navigate', event_type);
  },
  onListFilter: function onListFilter(childView, filters) {
    var serverCall = this.facade.request('get:collection:filter', 'products', filters);
    serverCall.done(this.showTable.bind(this));
    serverCall.done(this.showPagerWidgets.bind(this));
  },
  onListNavigate: function onListNavigate(event_type) {
    /*
     * Launched when navigating in the list's pages
     */
    var serverCall = this.facade.request('get:collection:' + event_type, 'products');
    serverCall.done(this.showTable.bind(this));
    serverCall.done(this.showPagerWidgets.bind(this));
  },
  onNavigateItemsPerPage: function onNavigateItemsPerPage(items_per_page) {
    var serverCall = this.facade.request('set:collection:itemsperpage', 'products', items_per_page);
    serverCall.done(this.showTable.bind(this));
    serverCall.done(this.showPagerWidgets.bind(this));
  },
  onNavigatePage: function onNavigatePage(page) {
    var serverCall = this.facade.request('get:collection:page', 'products', page);
    serverCall.done(this.showTable.bind(this));
    serverCall.done(this.showPagerWidgets.bind(this));
  },

  /* Add product action */
  onAddButtonClicked: function onAddButtonClicked() {
    this.app.trigger('navigate', 'addproduct');
  },
  onModelEdit: function onModelEdit(childView) {
    var modelId = childView.model.get('id');
    this.app.trigger('navigate', 'products/' + modelId);
  },
  onModelDelete: function onModelDelete(childView) {
    this.app.trigger('product:delete', childView);
  },
  onModelDuplicate: function onModelDuplicate(childView) {
    this.app.trigger('product:duplicate', childView);
  },
  onModelArchive: function onModelArchive(childView) {
    this.app.trigger('product:archive', childView);
  },
  onExportButtonClicked: function onExportButtonClicked(childView) {
    this.app.trigger('products:export', childView);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductListComponent);

/***/ }),

/***/ "./src/sale_product/views/list/ProductTable.js":
/*!*****************************************************!*\
  !*** ./src/sale_product/views/list/ProductTable.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ProductView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductView.js */ "./src/sale_product/views/list/ProductView.js");
/* harmony import */ var _ProductEmptyView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductEmptyView.js */ "./src/sale_product/views/list/ProductEmptyView.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
 * Module name : ProductTable
 */





var template = __webpack_require__(/*! ./templates/ProductTable.mustache */ "./src/sale_product/views/list/templates/ProductTable.mustache");

var ProductCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  tagName: 'tbody',
  childView: _ProductView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  emptyView: _ProductEmptyView_js__WEBPACK_IMPORTED_MODULE_2__.default,
  collectionEvents: {
    'sync': 'render'
  },
  // Bubble up child view events
  childViewTriggers: {
    'model:delete': 'model:delete',
    'model:duplicate': 'model:duplicate',
    'model:edit': 'model:edit',
    'model:archive': 'model:archive'
  }
});
var ProductTable = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  regions: {
    tbody: {
      el: 'tbody',
      replaceElement: true
    }
  },
  ui: {},
  // Listen to the current's view events
  events: {
    "click .sortable": "sortTable"
  },
  triggers: {
    "navigate:page": "navigate:page"
  },
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {
    'model:delete': 'model:delete',
    'model:duplicate': 'model:duplicate',
    'model:edit': 'model:edit',
    'model:archive': 'model:archive'
  },
  initialize: function initialize() {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {
    this.showChildView('tbody', new ProductCollectionView({
      collection: this.collection,
      childViewOptions: {
        tvaMode: this.config.request('get', 'tva_mode_enabled')
      }
    }));
  },
  sortTable: function sortTable(e) {
    e.preventDefault();
    var $e = $(e.currentTarget);
    var newSortKey = $e.data("sort");
    var isSameSortKey = this.collection.state.sortKey == newSortKey;
    var isOldOrderAsc = this.collection.state.order == -1;
    var orderCodes = {
      "asc": -1,
      "desc": 1
    };
    var newOrderCode = isSameSortKey && isOldOrderAsc ? orderCodes["desc"] : orderCodes["asc"];
    this.collection.setSorting(newSortKey, newOrderCode);
    this.triggerMethod("navigate:page", this.collection.state.currentPage);
  },
  templateContext: function templateContext() {
    var sortKey = this.collection.state.sortKey;
    var order = this.collection.state.order == -1 ? "asc" : "desc";
    /*  Description des en-têtes de colonnes pour le "template":
     *   - label: Intitulé de l'en-tête de colonne
     *   - type: "number" ou "text" (valeur par défaut)
     *   - key: Clef de tri pour les colonnes triables
     *   - aria: Libellé ARIA (par défaut: "label" en minuscules)
     */

    var headers = [{
      label: "ID",
      key: "id",
      aria: "identifiant"
    }, {
      label: "Nom interne",
      key: "label"
    }, {
      label: "Référence",
      key: "ref"
    }, {
      label: "Fournisseur",
      key: "supplier_id"
    }, {
      label: "Réf frns",
      key: "supplier_ref"
    }, {
      label: "Catégorie"
    }, {
      label: "Stock",
      type: "number"
    }, {
      label: "Coût",
      type: "number"
    }, {
      label: "Prix de vente",
      type: "number",
      key: "ht",
      aria: "prix hors taxes"
    }, {
      label: "Unité"
    }, {
      label: "Modifié le",
      key: "updated_at",
      aria: "date de mise à jour"
    }];

    if (this.config.request('get', 'tva_mode_enabled')) {
      headers.push({
        label: "TVA"
      });
    }

    for (var i = 0; i < headers.length; i++) {
      if (headers[i].key) {
        headers[i].isSortable = true;
      }

      if (!headers[i].type) {
        headers[i].type = "text";
      }

      if (!headers[i].aria) {
        headers[i].aria = headers[i].label.toLowerCase();
      }

      if (headers[i].key == sortKey) {
        headers[i].sortClass = " current " + order;
        headers[i].orderIcon = order;
      } else {
        headers[i].sortClass = "";
        headers[i].orderIcon = "arrow";
      }
    }

    var context = {
      colHeaders: headers
    };
    return context;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductTable);

/***/ }),

/***/ "./src/sale_product/views/list/ProductView.js":
/*!****************************************************!*\
  !*** ./src/sale_product/views/list/ProductView.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonCollection_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/models/ButtonCollection.js */ "./src/base/models/ButtonCollection.js");
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ActionButtonsWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/ActionButtonsWidget.js */ "./src/widgets/ActionButtonsWidget.js");
/* harmony import */ var _date__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../date */ "./src/date.js");
/*
 * File Name : ProductView.js
 */







var template = __webpack_require__(/*! ./templates/ProductView.mustache */ "./src/sale_product/views/list/templates/ProductView.mustache");

var ProductView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  tagName: 'tr',
  className: "white_tr clickable-row",
  template: template,
  regions: {
    'actions': 'td.col_actions'
  },
  events: {
    "click @ui.clickableTd": "onLineClicked"
  },
  ui: {
    'edit': '.edit',
    'delete': '.delete',
    'duplicate': '.duplicate',
    'clickableTd': 'td:not(.col_actions)'
  },
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  getViewButtonModel: function getViewButtonModel(label, action, icon) {
    var showLabel = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    var css = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
    return {
      label: label,
      action: action,
      showLabel: showLabel,
      icon: icon,
      css: css
    };
  },
  onRender: function onRender() {
    var primary = [this.getViewButtonModel("Voir / Modifier", "edit", "pen"), this.getViewButtonModel("Dupliquer", "duplicate", "copy")];

    if (!this.model.get('locked')) {
      primary.push(this.getViewButtonModel("Supprimer", "delete", "trash-alt", false, 'negative'));
    } else {
      primary.push(this.getViewButtonModel("Archiver", "archive", "archive", false));
    }

    var view = new _widgets_ActionButtonsWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      primary: primary
    });
    this.showChildView('actions', view);
  },
  onLineClicked: function onLineClicked() {
    this.onActionClicked("edit");
  },
  onActionClicked: function onActionClicked(actionName) {
    this.triggerMethod("model:" + actionName, this);
  },
  templateContext: function templateContext() {
    var icons = false;

    if (this.model.is_complex) {
      icons = this.model.getIcons();
    }

    return {
      id: this.model.get('id'),
      category_label: this.model.category_label(),
      tvaMode: this.getOption('tvaMode'),
      tva_label: this.model.tva_label(),
      ht_label: this.model.ht_label(),
      supplier_ht_label: this.model.supplier_ht_label(),
      supplier_label: this.model.supplier_label(),
      supplier_ref: this.model.get('supplier_ref'),
      current_stock: this.model.get("current_stock"),
      updated_at: (0,_date__WEBPACK_IMPORTED_MODULE_4__.formatDate)(this.model.get('updated_at')),
      icons: icons,
      tooltipTitle: "Cliquer pour voir le produit « " + this.model.get('label') + " »"
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductView);

/***/ }),

/***/ "./src/sale_product/views/product_form/AddProductForm.js":
/*!***************************************************************!*\
  !*** ./src/sale_product/views/product_form/AddProductForm.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/*
 * Module name : AddProductForm
 */






var template = __webpack_require__(/*! ./templates/AddProductForm.mustache */ "./src/sale_product/views/product_form/templates/AddProductForm.mustache");

var AddProductForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  className: 'main_content',
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  regions: {
    label: '.field-label',
    type_: '.field-type_'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onSuccessSync: function onSuccessSync() {
    var app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
    app.trigger('navigate', 'products/' + this.model.get('id'));
  },
  onCancelForm: function onCancelForm() {
    var app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
    app.trigger('navigate', 'index');
  },
  onRender: function onRender() {
    this.showChildView('label', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'label',
      label: "Nom interne",
      description: "Nom du produit dans le catalogue",
      required: true
    }));
    this.showChildView("type_", new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      field_name: "type_",
      label: "Type de produit",
      value: this.model.get('type_'),
      options: this.config.request('get:options', 'product_types'),
      label_key: 'label',
      id_key: 'value',
      required: true
    }));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AddProductForm);

/***/ }),

/***/ "./src/sale_product/views/product_form/HelpTextView.js":
/*!*************************************************************!*\
  !*** ./src/sale_product/views/product_form/HelpTextView.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);



var template = __webpack_require__(/*! ./templates/HelpTextView.mustache */ "./src/sale_product/views/product_form/templates/HelpTextView.mustache");

var HelpTextView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template,
  events: {
    'click button.compute': 'forceCompute'
  },
  triggers: {
    'click button.close': 'hide:help'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  },
  templateContext: function templateContext() {
    console.log("Template context"); // Collect data sent to the template (model attributes are already transmitted)

    var result = {};
    var use_margin_rate = this.config.request('get:options', 'margin_rate_enabled');
    var computing_info = this.config.request('get:options', 'computing_info');
    var margin_rate = 0;

    if (use_margin_rate) {
      margin_rate = this.model.get('margin_rate') || 0;
    } else {
      margin_rate = computing_info['margin_rate'] || 0;
    }

    result['margin_rate'] = margin_rate;
    result['use_margin_rate'] = use_margin_rate;
    result['computing_info'] = computing_info;
    return result;
  },
  forceCompute: function forceCompute() {
    console.log("Force compute");
    this.model.save({
      'mode': 'supplier_ht'
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HelpTextView);

/***/ }),

/***/ "./src/sale_product/views/product_form/ProductForm.js":
/*!************************************************************!*\
  !*** ./src/sale_product/views/product_form/ProductForm.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/* harmony import */ var backbone_tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone-tools */ "./src/backbone-tools.js");
/* harmony import */ var base_models_ActionButtonCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! base/models/ActionButtonCollection.js */ "./src/base/models/ActionButtonCollection.js");
/* harmony import */ var widgets_ButtonCollectionWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! widgets/ButtonCollectionWidget.js */ "./src/widgets/ButtonCollectionWidget.js");
/* harmony import */ var base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! widgets/CheckboxWidget.js */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! widgets/DateWidget.js */ "./src/widgets/DateWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var base_views_MessageView_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! base/views/MessageView.js */ "./src/base/views/MessageView.js");
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* harmony import */ var _work_item_WorkItemComponent_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./work_item/WorkItemComponent.js */ "./src/sale_product/views/product_form/work_item/WorkItemComponent.js");
/* harmony import */ var _stock_StockOperationComponent_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./stock/StockOperationComponent.js */ "./src/sale_product/views/product_form/stock/StockOperationComponent.js");
/* harmony import */ var _ProductResume_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./ProductResume.js */ "./src/sale_product/views/product_form/ProductResume.js");
/* harmony import */ var widgets_RadioChoiceButtonWidget_js__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! widgets/RadioChoiceButtonWidget.js */ "./src/widgets/RadioChoiceButtonWidget.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _HelpTextView_js__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./HelpTextView.js */ "./src/sale_product/views/product_form/HelpTextView.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * Module name : ProductForm
 */






















var ProductForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_21___default().View.extend(base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_12__.default).extend({
  template: __webpack_require__(/*! ./templates/ProductForm.mustache */ "./src/sale_product/views/product_form/templates/ProductForm.mustache"),
  behaviors: [base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_6__.default],
  partial: true,
  className: 'main_content',
  regions: {
    resume: '.resume',
    messageContainer: '.message-container',
    errors: '.errors',
    type_: '.field-type_',
    label: '.field-label',
    title: ".field-title",
    description: '.field-description',
    mode: ".field-mode",
    margin_rate: '.field-margin_rate',
    ht: '.field-ht',
    ttc: '.field-ttc',
    unity: '.field-unity',
    tva_id: '.field-tva_id',
    product_id: '.field-product_id',
    supplier_id: '.field-supplier_id',
    supplier_ref: '.field-supplier_ref',
    supplier_unity_amount: '.field-supplier_unity_amount',
    supplier_ht: '.field-supplier_ht',
    stocks: '.stocks',
    items: '.items',
    category_id: '.field-category_id',
    ref: '.field-ref',
    notes: '.field-notes',
    // Training
    goals: ".field-goals",
    prerequisites: ".field-prerequisites",
    for_who: ".field-for_who",
    duration: ".field-duration",
    content: ".field-content",
    teaching_method: ".field-teaching_method",
    logistics_means: ".field-logistics_means",
    more_stuff: ".field-more_stuff",
    evaluation: ".field-evaluation",
    place: ".field-place",
    modality_one: ".field-modality_one",
    modality_two: ".field-modality_two",
    types: ".field-types",
    date: ".field-date",
    price: ".field-price",
    free_1: ".field-free_1",
    free_2: ".field-free_2",
    free_3: ".field-free_3",
    other_buttons: {
      el: '.other_buttons',
      replaceElement: true
    }
  },
  events: {
    /*'data:invalid': 'onDataInvalid',*/
  },
  modelEvents: {
    /*'updated:ht': 'renderHT',*/
    'saved:supplier_ht': 'onAmountChange',
    'saved:margin_rate': 'onAmountChange',
    'saved:ht': 'onAmountChange',
    'saved:ttc': 'onAmountChange',
    'saved:tva_id': 'onAmountChange',
    'saved:mode': 'onModeChange',
    'change:tva_id': 'refreshTvaProductSelect',
    'change:type_': 'render',
    // Si on change le type_ on veut (ou pas) les stocks
    'validated:invalid': 'showErrors',
    'validated:valid': 'hideErrors'
  },
  childViewEvents: {
    'action:clicked': 'onActionClicked',
    'change:productMode': 'onModeChange',
    'show:help': 'showHelpMessage',
    'hide:help': 'hideHelpMessage'
  },
  childViewTriggers: {
    'finish': 'data:persist',
    'change': 'data:modified'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('app');
    this.unity_options = this.config.request('get:options', 'unities');
    this.tva_mode_enabled = this.config.request('get:options', 'tva_mode_enabled');

    if (this.tva_mode_enabled) {
      this.tva_options = this.config.request('get:options', 'tvas'); // Form should have no tva by default

      this.tva_options.forEach(function (item) {
        item['default'] = false;
      });
      this.product_options = this.config.request('get:options', 'products');
      this.all_product_options = this.config.request('get:options', 'products');
    }

    this.supplier_options = this.config.request('get:options', 'suppliers');
    this.category_options = this.facade.request('get:collection', 'categories').toJSON();
    this.work_form = false;
    this.training_form = false;
    this.ttc_mode_enabled = this.config.request('get:options', 'ttc_mode_enabled'); // On traîte le cas des services dynamiquement car pour les types
    // simples on permet le changement de type à la volée

    if (this.model.get('type_') == 'sale_product_work') {
      this.work_form = true;
    } else if (this.model.get('type_') == 'sale_product_training') {
      this.training_form = true;
    }

    this.margin_rate_enabled = this.config.request('get:options', 'margin_rate_enabled');
    this.model.setupSyncEvents();
  },
  isServiceForm: function isServiceForm() {
    /* Permet de savoir si on doit afficher les stocks */
    return this.model.get('type_') == 'sale_product_service_delivery';
  },
  showMessageView: function showMessageView() {
    var model = new (backbone__WEBPACK_IMPORTED_MODULE_0___default().Model)();
    var view = new base_views_MessageView_js__WEBPACK_IMPORTED_MODULE_13__.default({
      model: model
    });
    this.showChildView('messageContainer', view);
  },
  renderLabel: function renderLabel() {
    this.showChildView('label', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Nom interne",
      field_name: 'label',
      value: this.model.get('label'),
      description: "Nom du produit dans le catalogue",
      required: true
    }));
  },
  renderCategory: function renderCategory() {
    if (this.category_options.length) {
      this.showChildView('category_id', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default({
        title: "Catégorie",
        field_name: 'category_id',
        options: this.category_options,
        id_key: 'id',
        label_key: 'title',
        value: this.model.get('category_id'),
        placeholder: 'Choisir une catégorie'
      }));
    } else {
      var region = this.getRegion('category_id');
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_3__.hideRegion)(region);
    }
  },
  renderInternalRef: function renderInternalRef() {
    this.showChildView('ref', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Référence interne",
      field_name: 'ref',
      value: this.model.get('ref')
    }));
  },
  renderSupplierHT: function renderSupplierHT() {
    this.showChildView('supplier_ht', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Coût d’achat HT",
      field_name: 'supplier_ht',
      value: this.model.get('supplier_ht'),
      description: "Déboursé sec du produit, utilisé pour calculer le prix de vente HT grâce au coefficients de marge et de frais généraux ainsi qu'aux différentes contributions"
    }));
  },
  renderHT: function renderHT() {
    if (!this.work_form && !this.training_form) {
      var label = "Montant HT";
      var editable = false;
      var mode = this.model.get('mode');
      var description = "";

      if (mode == 'ht') {
        editable = true;
      } else if (mode == 'supplier_ht') {
        description = "Calculé depuis le \"Coût d'achat\"";
      } else if (mode == "ttc") {
        description = "Calculé depuis le TTC avec la valeur de TVA sélectionnée";
      }

      this.showChildView('ht', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
        title: label,
        field_name: 'ht',
        value: this.model.get('ht'),
        editable: editable,
        description: description
      }));
    }
  },
  renderMarginRate: function renderMarginRate() {
    if (this.margin_rate_enabled && this.model.get('mode') == 'supplier_ht') {
      var region = this.getRegion('margin_rate');
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_3__.showRegion)(region);
      this.showChildView('margin_rate', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
        title: "Coefficient de marge",
        field_name: 'margin_rate',
        description: "Nombre entre 0 et 1 permettant le calcul du prix de vente depuis le coût d'achat",
        value: this.model.get('margin_rate')
      }));
    } else {
      var _region = this.getRegion('margin_rate');

      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_3__.hideRegion)(_region);
    }
  },
  renderTva: function renderTva() {
    if (this.tva_mode_enabled) {
      this.showChildView('tva_id', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default({
        title: "TVA",
        field_name: 'tva_id',
        options: this.tva_options,
        id_key: 'id',
        value: this.model.get('tva_id'),
        placeholder: 'Choisir un taux de TVA'
      }));
    } else {
      var region = this.getRegion('tva_id');
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_3__.hideRegion)(region);
    }
  },
  renderTTC: function renderTTC() {
    if (this.tva_mode_enabled || this.ttc_mode_enabled) {
      if (!this.work_form && !this.training_form) {
        var label = "Montant TTC";
        var editable = this.model.get('mode') == 'ttc';
        this.showChildView('ttc', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
          title: label,
          field_name: 'ttc',
          value: this.model.get('ttc'),
          editable: editable
        }));
      }
    } else {
      var region = this.getRegion('ttc');
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_3__.hideRegion)(region);
    }
  },
  renderProduct: function renderProduct() {
    if (this.tva_mode_enabled) {
      this.product_options = this.getProductOptions(this.tva_options, this.all_product_options);
      this.showChildView('product_id', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default({
        title: "Compte produit",
        field_name: 'product_id',
        options: this.product_options,
        id_key: 'id',
        value: this.model.get('product_id'),
        description: "Les comptes produits sont proposés après le choix de la TVA",
        placeholder: 'Choisir un compte produit'
      }));
    } else {
      var region = this.getRegion('product_id');
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_3__.hideRegion)(region);
    }
  },
  showCommonFields: function showCommonFields() {
    /* Section Informations internes */
    this.renderLabel();
    this.renderCategory();
    this.renderInternalRef();

    if (this.work_form || this.training_form) {
      this.showChildView('type_', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
        type: 'hidden',
        field_name: 'type_',
        value: this.model.get('type_')
      }));
    } else {
      this.showChildView('type_', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default({
        field_name: "type_",
        label: "Type de produit",
        value: this.model.get('type_'),
        options: this.config.request('get:options', 'base_product_types'),
        label_key: 'label',
        id_key: 'value'
      }));
      var modeOptions = [{
        'label': 'HT',
        'value': 'ht'
      }, {
        'label': "Coût d'achat",
        'value': 'supplier_ht'
      }];

      if (this.ttc_mode_enabled) {
        modeOptions.push({
          'label': 'TTC',
          'value': 'ttc'
        });
      }

      this.showChildView("mode", new widgets_RadioChoiceButtonWidget_js__WEBPACK_IMPORTED_MODULE_18__.default({
        field_name: "mode",
        label: "Mode de calcul du prix",
        value: this.model.get('mode'),
        "options": modeOptions
      }));
    } //


    this.showChildView('description', new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default({
      title: "Description",
      field_name: 'description',
      value: this.model.get('description'),
      tinymce: true
    }));
    this.showChildView('unity', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default({
      title: "Unité",
      field_name: 'unity',
      options: this.unity_options,
      value: this.model.get('unity'),
      placeholder: 'Choisir une unité'
    }));
    this.renderMarginRate();
    this.renderHT();
    this.renderTva();
    this.renderTTC();
    this.renderProduct();
    /* Section Notes */

    this.showChildView('notes', new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default({
      title: "Notes",
      field_name: 'notes',
      value: this.model.get('notes')
    }));
  },
  showWorkFields: function showWorkFields() {
    this.showChildView('title', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      label: "Titre du produit composé",
      description: "Titre du chapitre ou de l’ouvrage quand le produit composé sera inséré dans le document final",
      field_name: "title",
      value: this.model.get('title'),
      required: true
    }));
    /* Section Produit composé (Chapitre ou Ouvrage) */

    this.showChildView('items', new _work_item_WorkItemComponent_js__WEBPACK_IMPORTED_MODULE_15__.default({
      collection: this.model.items
    }));
  },
  showSupplierFields: function showSupplierFields() {
    this.showChildView('supplier_id', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default({
      title: "Fournisseur",
      field_name: 'supplier_id',
      options: this.supplier_options,
      id_key: 'id',
      label_key: 'label',
      value: this.model.get('supplier_id'),
      placeholder: 'Choisir un fournisseur'
    }));
    this.showChildView('supplier_ref', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Référence Fournisseur",
      field_name: 'supplier_ref',
      value: this.model.get('supplier_ref')
    }));
    this.showChildView('supplier_unity_amount', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Unité de vente Fournisseur",
      field_name: 'supplier_unity_amount',
      value: this.model.get('supplier_unity_amount')
    }));
    this.renderSupplierHT();
  },
  showStockFields: function showStockFields() {
    this.showChildView('stocks', new _stock_StockOperationComponent_js__WEBPACK_IMPORTED_MODULE_16__.default({
      collection: this.model.stock_operations
    }));
  },
  showTrainingFields: function showTrainingFields() {
    var fields = {
      goals: {
        label: "Objectifs à atteindre à l'issue de la formation",
        description: "Les objectifs doivent être obligatoirement décrit avec des verbes d'actions",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      prerequisites: {
        'label': "Pré-requis obligatoire de la formation",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      for_who: {
        label: "Pour qui ?",
        description: "Public susceptible de participer à cette formation.",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      duration: {
        label: "Durée en heures et en jour(s) pour la formation",
        description: "Durée obligatoire minimale 7 heures soit 1 jour.",
        widget: widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_7__.default
      },
      content: {
        label: "Contenu détaillé de la formation",
        description: "Trame par étapes.",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      teaching_method: {
        label: "Les moyens pédagogiques utilisés",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      logistics_means: {
        label: "Les moyens logistiques à disposition",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      more_stuff: {
        label: "Quels sont les plus de cette formation ?",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      evaluation: {
        label: "Modalités d'évaluation de la formation",
        description: "Par exemple : questionnaire d'évaluation, exercices-tests, questionnaire de satisfaction, évaluation formative.",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      place: {
        label: "Lieu de la formation",
        description: "Villes, zones géographiques où la formation peut être mise en place.",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      modality_one: {
        label: "Formation intra-entreprise",
        widget: widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_9__.default
      },
      modality_two: {
        label: "Formation inter-entreprise",
        widget: widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_9__.default
      },
      types: {
        label: "Type de formation",
        options: this.config.request('get:options', 'training_types'),
        widget: widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_11__.default,
        multiple: true,
        id_key: "id",
        label_key: "label"
      },
      date: {
        label: "Date de la formation",
        widget: widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_10__.default
      },
      free_1: {
        label: "Champ libre 1",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      free_2: {
        label: "Champ libre 2",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      },
      free_3: {
        label: "Champ libre 3",
        widget: widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default
      }
    };
    var this_ = this;

    _.each(fields, function (field, key) {
      var options = _.clone(field);

      options['value'] = this_.model.get(key);
      options['field_name'] = key;
      this_.showChildView(key, new field['widget'](options));
    });
  },
  showOtherActionButtons: function showOtherActionButtons() {
    var collection = new base_models_ActionButtonCollection_js__WEBPACK_IMPORTED_MODULE_4__.default();
    var buttons = [{
      label: 'Dupliquer',
      action: 'duplicate',
      icon: 'copy',
      showLabel: false
    }];

    if (!this.model.get('locked')) {
      buttons.push({
        label: "Supprimer",
        action: "delete",
        icon: "trash-alt",
        showLabel: false,
        css: 'negative'
      });
    }

    collection.add(buttons);
    var view = new widgets_ButtonCollectionWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      collection: collection
    });
    this.showChildView('other_buttons', view);
  },
  templateContext: function templateContext() {
    var result = {
      complex: this.work_form || this.training_form,
      work_form: this.work_form,
      service_form: this.isServiceForm(),
      training_form: this.training_form,
      margin_rate: this.margin_rate_enabled
    };
    return result;
  },
  onRender: function onRender() {
    this.showMessageView();
    this.showCommonFields();

    if (!this.work_form && !this.training_form) {
      this.showSupplierFields();
    }

    if (this.work_form || this.training_form) {
      this.showWorkFields();
    }

    if (this.training_form) {
      this.showTrainingFields();
    } // Le champ type_ peut changer en cours de route pour les produits
    // simples on utilise donc une méthode plutôt qu'un attribut


    if (!this.isServiceForm()) {
      this.showStockFields();
    }

    this.showChildView('resume', new _ProductResume_js__WEBPACK_IMPORTED_MODULE_17__.default({
      model: this.model
    }));
    this.showOtherActionButtons();
  },
  onAttach: function onAttach() {
    (0,tools_js__WEBPACK_IMPORTED_MODULE_2__.scrollTop)();
  },
  renderAmounts: function renderAmounts() {
    this.renderHT();
    this.renderSupplierHT();
    this.renderTTC();
  },
  onAmountChange: function onAmountChange(key, value) {
    this.renderAmounts();
  },
  onModeChange: function onModeChange(key, value) {
    this.renderMarginRate();
    this.renderAmounts();
  },
  onActionClicked: function onActionClicked(actionName) {
    console.log("Action clicked : %s", actionName);
    this.app.trigger("product:" + actionName, this);
  },
  onFormSubmitted: function onFormSubmitted() {
    this.app.trigger('navigate', 'index');
  },
  onCancelForm: function onCancelForm() {
    this.app.trigger('navigate', 'index');
  },
  onDataInvalid: function onDataInvalid(model, errors) {
    console.log("ProductForm.onInvalid");
    this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_14__.default({
      errors: errors
    }));
  },
  showHelpMessage: function showHelpMessage() {
    this.showChildView("messageContainer", new _HelpTextView_js__WEBPACK_IMPORTED_MODULE_20__.default({
      model: this.model
    }));
  },
  hideHelpMessage: function hideHelpMessage() {
    this.getRegion('messageContainer').empty();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductForm);

/***/ }),

/***/ "./src/sale_product/views/product_form/ProductResume.js":
/*!**************************************************************!*\
  !*** ./src/sale_product/views/product_form/ProductResume.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : ProductResume
 */


var ProductResume = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: __webpack_require__(/*! ./templates/ProductResume.mustache */ "./src/sale_product/views/product_form/templates/ProductResume.mustache"),
  modelEvents: {
    'change': 'render'
  },
  triggers: {
    'click .help': 'show:help'
  },
  templateContext: function templateContext() {
    var result = {
      ht_mode: this.model.get('mode') == 'ht',
      ttc_mode: this.model.get('mode') == 'ttc',
      supplier_ht_mode: this.model.get('mode') == 'supplier_ht',
      ht_label: this.model.ht_label(),
      ttc_label: this.model.ttc_label(),
      supplier_ht_label: this.model.supplier_ht_label()
    };
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductResume);

/***/ }),

/***/ "./src/sale_product/views/product_form/stock/StockOperationComponent.js":
/*!******************************************************************************!*\
  !*** ./src/sale_product/views/product_form/stock/StockOperationComponent.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../tools.js */ "./src/tools.js");
/* harmony import */ var _models_StockOperationModel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../models/StockOperationModel.js */ "./src/sale_product/models/StockOperationModel.js");
/* harmony import */ var _StockOperationView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./StockOperationView.js */ "./src/sale_product/views/product_form/stock/StockOperationView.js");
/* harmony import */ var _StockOperationForm_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./StockOperationForm.js */ "./src/sale_product/views/product_form/stock/StockOperationForm.js");
/*
 * Module name : StockOperationComponent
 */









var template = __webpack_require__(/*! ./templates/StockOperationComponent.mustache */ "./src/sale_product/views/product_form/stock/templates/StockOperationComponent.mustache");

var StockOperationCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().CollectionView.extend({
  tagName: 'tbody',
  childView: _StockOperationView_js__WEBPACK_IMPORTED_MODULE_5__.default,
  childViewTriggers: {
    'model:edit': 'model:edit',
    'model:delete': 'model:delete'
  }
});
var StockOperationComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend({
  tagName: "fieldset",
  className: "separate_block border_left_block",
  template: template,
  regions: {
    current_stock: '.current_stock',
    list: {
      el: 'tbody',
      replaceElement: true
    },
    addButton: '.add',
    popin: '.popin'
  },
  ui: {},
  bb_sync: true,
  // Listen to the current's view events
  events: {},
  // Listen to collection events
  collectionEvents: {
    'add': 'showCurrentStock',
    'change': 'showCurrentStock'
  },
  childViewEventPrefix: 'stock:operation',
  // Listen to child view events
  childViewEvents: {
    'action:clicked': "onActionClicked",
    'cancel:form': 'render',
    'destroy:modal': 'render',
    'model:edit': 'onModelEdit',
    'model:delete': 'onModelDelete'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
  },
  showCurrentStock: function showCurrentStock() {
    var this_ = this;
    (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.collection.url().replace("/stock_operations", "?action=get_current_stock"), {}, 'GET', {
      success: function success(result) {
        if (result !== "") {
          this_.$('.current_stock').show();
          this_.$('table').show();
          this_.showChildView('current_stock', "<h3 class='highlight'>Stock actuel : " + result + "</h3>");
        } else {
          this_.$('.current_stock').hide();
          this_.$('table').hide();
        }
      }
    });
  },
  showAddButton: function showAddButton() {
    var model = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: "Mouvement de stock",
      icon: "plus",
      action: "add"
    });
    var view = new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model
    });
    this.showChildView('addButton', view);
  },
  showModelForm: function showModelForm(model, edit) {
    var view = new _StockOperationForm_js__WEBPACK_IMPORTED_MODULE_6__.default({
      model: model,
      edit: edit,
      destCollection: this.collection,
      bb_sync: this.bb_sync
    });
    this.app.trigger('show:modal', view);
  },
  onRender: function onRender() {
    var view = new StockOperationCollectionView({
      collection: this.collection
    });
    this.showChildView('list', view);
    this.showAddButton();
    this.showCurrentStock();
  },
  templateContext: function templateContext() {
    return {};
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'add') {
      var model = new _models_StockOperationModel_js__WEBPACK_IMPORTED_MODULE_4__.default();
      this.showModelForm(model, false);
    }
  },
  onModelEdit: function onModelEdit(model, childView) {
    this.showModelForm(model, true);
  },
  onModelDelete: function onModelDelete(model, childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce mouvement de stock ?");

    if (result) {
      childView.model.destroy({
        success: this.onModelDeleteSuccess.bind(this),
        error: this.onModelDeleteError.bind(this),
        wait: true
      });
    }
  },
  onModelDeleteSuccess: function onModelDeleteSuccess() {
    this.showCurrentStock();
  },
  onModelDeleteError: function onModelDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression du mouvement de stock");
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StockOperationComponent);

/***/ }),

/***/ "./src/sale_product/views/product_form/stock/StockOperationForm.js":
/*!*************************************************************************!*\
  !*** ./src/sale_product/views/product_form/stock/StockOperationForm.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../tools.js */ "./src/tools.js");
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../widgets/DateWidget.js */ "./src/widgets/DateWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/*
 * Module name : StockOperationForm
 */








var template = __webpack_require__(/*! ./templates/StockOperationForm.mustache */ "./src/sale_product/views/product_form/stock/templates/StockOperationForm.mustache");

var StockOperationForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  template: template,
  tagName: 'section',
  id: 'stock_operation_form',
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  partial: false,
  regions: {
    erros: ".errors",
    date: ".field-date",
    description: '.field-description',
    stock_variation: '.field-stock_variation',
    base_sale_product_id: '.field-base_sale_product_id'
  },
  ui: {},
  // event_prefix: "stock:operation",
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {
    'finish': 'data:modified'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.bb_sync = this.getOption('bb_sync');
    this.forceEditForm = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.getOpt)(this, 'edit', false);
  },
  onRender: function onRender() {
    if (!this.model.has('base_sale_product_id')) {}

    this.showChildView('date', new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      title: "Date",
      field_name: 'date',
      value: this.model.get('date')
    }));
    this.showChildView('description', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      label: "Description",
      field_name: 'description',
      value: this.model.get('description')
    }));
    this.showChildView('stock_variation', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      label: "Variation du stock",
      field_name: 'stock_variation',
      value: this.model.get('stock_variation'),
      description: "Quantité entrée ou sortie du stock (en négatif pour une sortie)"
    }));
  },
  templateContext: function templateContext() {
    var title = "Nouveau mouvement de stock";

    if (this.getOption('edit')) {
      title = "Modifier le mouvement de stock";
    }

    return {
      title: title
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StockOperationForm);

/***/ }),

/***/ "./src/sale_product/views/product_form/stock/StockOperationView.js":
/*!*************************************************************************!*\
  !*** ./src/sale_product/views/product_form/stock/StockOperationView.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../date.js */ "./src/date.js");
/*
 * Module name : StockOperationView
 */






var template = __webpack_require__(/*! ./templates/StockOperationView.mustache */ "./src/sale_product/views/product_form/stock/templates/StockOperationView.mustache");

var StockOperationView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  tagName: 'tr',
  regions: {
    editButtonContainer: {
      el: '.col_actions .edit',
      replaceElement: true
    },
    delButtonContainer: {
      el: '.col_actions .delete',
      replaceElement: true
    }
  },
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  modelEvents: {
    'change': 'render'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {
    var editModel = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      ariaLabel: 'Modifier ce mouvement de stock',
      icon: 'pen',
      showLabel: false,
      action: 'edit'
    });
    var deleteModel = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      ariaLabel: 'Supprimer ce mouvement de stock',
      css: 'negative',
      icon: 'trash-alt',
      showLabel: false,
      action: 'delete'
    });
    this.showChildView('editButtonContainer', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: editModel
    }));
    this.showChildView('delButtonContainer', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: deleteModel
    }));
  },
  templateContext: function templateContext() {
    return {
      date: (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.formatDate)(this.model.get("date")),
      description: this.model.get("description"),
      stock_variation: this.model.get("stock_variation")
    };
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'edit') {
      this.triggerMethod('model:edit', this.model, this);
    } else if (action == 'delete') {
      this.triggerMethod('model:delete', this.model, this);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StockOperationView);

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/WorkItemCollectionView.js":
/*!*********************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/WorkItemCollectionView.js ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _WorkItemEmptyView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WorkItemEmptyView.js */ "./src/sale_product/views/product_form/work_item/WorkItemEmptyView.js");
/* harmony import */ var _WorkItemView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WorkItemView.js */ "./src/sale_product/views/product_form/work_item/WorkItemView.js");
/*
 * Module name : WorkItemCollectionView
 */



var WorkItemCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().CollectionView.extend({
  emptyView: _WorkItemEmptyView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  tagName: 'tbody',
  childView: _WorkItemView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  childViewTriggers: {
    'model:edit': 'model:edit',
    'model:delete': 'model:delete'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollectionView);

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/WorkItemComponent.js":
/*!****************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/WorkItemComponent.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _models_WorkItemModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../models/WorkItemModel.js */ "./src/sale_product/models/WorkItemModel.js");
/* harmony import */ var _WorkItemCollectionView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./WorkItemCollectionView.js */ "./src/sale_product/views/product_form/work_item/WorkItemCollectionView.js");
/* harmony import */ var _WorkItemForm_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./WorkItemForm.js */ "./src/sale_product/views/product_form/work_item/WorkItemForm.js");
/*
 * Module name : WorkItemComponent
 */








var template = __webpack_require__(/*! ./templates/WorkItemComponent.mustache */ "./src/sale_product/views/product_form/work_item/templates/WorkItemComponent.mustache");

var WorkItemComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  tagName: "fieldset",
  className: "separate_block border_left_block",
  template: template,
  regions: {
    list: {
      el: 'tbody',
      replaceElement: true
    },
    addButton: '.add',
    popin: '.popin'
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to collection events
  collectionEvents: {},
  childViewEventPrefix: 'work:item',
  // Listen to child view events
  childViewEvents: {
    'action:clicked': "onActionClicked",
    'cancel:form': 'render',
    'destroy:modal': 'render',
    'model:edit': 'onModelEdit',
    'model:delete': 'onModelDelete',
    'work:item:model:delete': 'onModelDelete'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
  },
  showAddButton: function showAddButton() {
    var model = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: "Ajouter un produit",
      icon: "plus",
      action: "add"
    });
    var view = new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model
    });
    this.showChildView('addButton', view);
  },
  showModelForm: function showModelForm(model, edit) {
    var view = new _WorkItemForm_js__WEBPACK_IMPORTED_MODULE_5__.default({
      model: model,
      edit: edit,
      destCollection: this.collection
    });
    this.app.trigger('show:modal', view);
  },
  onRender: function onRender() {
    var view = new _WorkItemCollectionView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      collection: this.collection
    });
    this.showChildView('list', view);
    this.showAddButton();
  },
  templateContext: function templateContext() {
    return {};
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'add') {
      var model = new _models_WorkItemModel_js__WEBPACK_IMPORTED_MODULE_3__.default();
      this.showModelForm(model, false);
    }
  },
  onModelDeleteSuccess: function onModelDeleteSuccess() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('success', this, "Vos données ont bien été supprimées");
  },
  onModelDeleteError: function onModelDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  onModelDelete: function onModelDelete(model, childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce produit ?");

    if (result) {
      childView.model.destroy({
        success: this.onModelDeleteSuccess.bind(this),
        error: this.onModelDeleteError.bind(this),
        wait: true
      });
    }
  },
  onModelEdit: function onModelEdit(model, childView) {
    this.showModelForm(model, true);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemComponent);

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/WorkItemEmptyView.js":
/*!****************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/WorkItemEmptyView.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : WorkItemEmptyView
 */



var template = __webpack_require__(/*! ./templates/WorkItemEmptyView.mustache */ "./src/sale_product/views/product_form/work_item/templates/WorkItemEmptyView.mustache");

var WorkItemEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template,
  tagName: "tr"
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemEmptyView);

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/WorkItemForm.js":
/*!***********************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/WorkItemForm.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-tools */ "./src/backbone-tools.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! widgets/CheckboxWidget.js */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var widgets_RadioChoiceButtonWidget_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! widgets/RadioChoiceButtonWidget.js */ "./src/widgets/RadioChoiceButtonWidget.js");
/* harmony import */ var base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/*
 * Module name : WorkItemForm
 */












var template = __webpack_require__(/*! ./templates/WorkItemForm.mustache */ "./src/sale_product/views/product_form/work_item/templates/WorkItemForm.mustache");

var WorkItemForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_10___default().View.extend(base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_8__.default).extend({
  template: template,
  tagName: 'section',
  id: 'workitem_form',
  partial: false,
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  regions: {
    erros: ".errors",
    locked: '.field-locked',
    label: ".field-label",
    type_: '.field-type_',
    description: '.field-description',
    mode: '.field-mode',
    supplier_ht: '.field-supplier_ht',
    ht: '.field-ht',
    quantity: '.field-quantity',
    unity: '.field-unity',
    catalogContainer: "#catalog-container",
    sync_catalog: ".field-sync_catalog"
  },
  ui: {
    unlockButton: 'button.unlock',
    lockContainer: 'div.alert-locked',
    main_tab: "ul.nav-tabs li:first a"
  },
  // event_prefix: "work:item",
  // Listen to the current's view events
  events: {
    'click @ui.unlockButton': 'unlockForm'
  },
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': "onCatalogInsert",
    'mode:change': 'onModeChange'
  },
  // Bubble up child view events
  childViewTriggers: {
    'finish': 'data:modified'
  },
  initialize: function initialize() {
    if (this.getOption('edit')) {
      // Only partial edit needed
      this.partial = true;
    } else {
      // All model datas will be submitted
      this.partial = false;
    }

    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.unity_options = this.config.request('get:options', 'unities');
  },
  refreshForm: function refreshForm() {
    // All the field are editable if not locked or no base_sale_product_id set
    this.showLocked();
    this.showLabelAndType();
    this.showDescription();
    this.showModeToggle();
    this.renderModeRelatedFields();
    this.showQuantity();
    this.showUnity();
    this.showSyncCatalog();
  },
  getFieldCommonOptions: function getFieldCommonOptions(attribute, base_label) {
    var result = {
      label: base_label,
      field_name: attribute,
      value: this.model.get(attribute)
    };

    if (this.model.isFromCatalog(attribute)) {
      result.label += " hérité(e) du catalogue";
    }

    return result;
  },
  showLocked: function showLocked() {
    this.showChildView('locked', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      type: 'hidden',
      field_name: 'locked',
      value: this.model.get('locked')
    }));
  },
  showLabelAndType: function showLabelAndType() {
    if (!this.model.has('base_sale_product_id')) {
      this.showChildView('label', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        title: "Nom interne du produit (utilisé dans le catalogue)",
        field_name: 'label',
        value: this.model.get('label'),
        description: "Après insertion dans le produit composé courant, ce produit sera également inséré dans votre catalogue produit",
        required: true
      }));
      var type_options = this.config.request('get:options', 'base_product_types');
      this.showChildView('type_', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_6__.default({
        title: "Type de produit (utilisé dans le catalogue)",
        field_name: 'type_',
        value: this.model.get('type_'),
        options: type_options,
        label_key: 'label',
        id_key: 'value',
        required: true
      }));
    } else {
      var view = this.getChildView('label');

      if (view) {
        view.destroy();
        view = this.getChildView('type_');

        if (view) {
          view.destroy();
        }

        this.$el.find('div.alert-info').hide();
      }
    }
  },
  showDescription: function showDescription() {
    this.showChildView('description', new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      label: "Description",
      description: "Description utilisée dans les devis ou factures",
      field_name: 'description',
      value: this.model.get('description'),
      tinymce: true,
      required: true
    }));
  },
  showModeToggle: function showModeToggle() {
    var options = this.getFieldCommonOptions('mode', 'Mode de calcul des prix');
    options['options'] = [{
      'label': 'HT',
      'value': 'ht'
    }, {
      'label': "Coût d'achat",
      'value': 'supplier_ht'
    }];
    options["finishEventName"] = "mode:change";
    var view = new widgets_RadioChoiceButtonWidget_js__WEBPACK_IMPORTED_MODULE_7__.default(options);
    this.showChildView("mode", view);
  },
  renderModeRelatedFields: function renderModeRelatedFields() {
    this.showSupplierHt();
    this.showHt();
  },
  showSupplierHt: function showSupplierHt() {
    var region = this.getRegion('supplier_ht');

    if (this.model.get('mode') == 'supplier_ht') {
      var options = this.getFieldCommonOptions('supplier_ht', "Coût unitaire HT");
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
      this.showChildView('supplier_ht', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.hideRegion)(region);
    }
  },
  showHt: function showHt(editable) {
    var region = this.getRegion('ht');

    if (this.model.get('mode') == 'ht') {
      var options = this.getFieldCommonOptions('ht', 'Montant unitaire HT');
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
      this.showChildView('ht', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.hideRegion)(region);
    }
  },
  showQuantity: function showQuantity() {
    this.showChildView('quantity', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      label: "Quantité par unité d'ouvrage",
      field_name: "quantity",
      value: this.model.get('quantity')
    }));
  },
  showUnity: function showUnity() {
    var options = this.getFieldCommonOptions('unity', "Unité");
    options['options'] = this.unity_options;
    options['placeholder'] = 'Choisir une unité';
    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_6__.default(options);
    this.showChildView('unity', view);
  },
  showSyncCatalog: function showSyncCatalog(open_form) {
    if (this.model.get('base_sale_product_id') && !this.model.get('locked')) {
      this.showChildView('sync_catalog', new widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
        label: "Synchroniser avec le catalogue",
        description: "Appliquer les modifications ci-dessus au produit " + this.model.get('label') + " du catalogue",
        field_name: "sync_catalog",
        value: false,
        true_val: true,
        false_val: false
      }));
    }
  },
  templateContext: function templateContext() {
    var title = "Ajouter un produit à ce produit composé";
    var raw_create = false;
    var help_text = "";

    if (this.getOption('edit')) {
      title = "Modifier le produit " + this.model.get('label') + " de ce produit composé";
    }

    if (!this.model.get('base_sale_product_id')) {
      help_text = "Lorsque vous ajoutez une entrée directement dans ce produit composé, un produit correspondant sera également créé dans le catalogue";
      raw_create = true;
    }

    return {
      title: title,
      help_text: help_text,
      raw_create: raw_create,
      add: !this.getOption('edit')
    };
  },
  onRender: function onRender() {
    this.refreshForm();

    if (!this.getOption('edit')) {
      var view = new common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_9__.default({
        query_params: {
          type_: 'product'
        },
        collection_name: 'catalog_tree',
        multiple: true,
        url: AppOption['catalog_tree_url']
      });
      this.showChildView('catalogContainer', view);
    }
  },
  unlockForm: function unlockForm() {
    this.model.set('locked', false);
    this.ui.lockContainer.hide();
    this.refreshForm();
  },
  onModeChange: function onModeChange(key, value) {
    this.model.set(key, value);
    this.renderModeRelatedFields();
  },
  onAttach: function onAttach() {
    this.getUI('main_tab').tab('show');
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    var collection = this.getOption("destCollection");
    var req = collection.load_from_catalog(sale_products);
    req.then(function () {
      collection._parent.fetch();

      _this.triggerMethod('modal:close');
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemForm);

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/WorkItemView.js":
/*!***********************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/WorkItemView.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/*
 * Module name : WorkItemView
 */





var template = __webpack_require__(/*! ./templates/WorkItemView.mustache */ "./src/sale_product/views/product_form/work_item/templates/WorkItemView.mustache");

var WorkItemView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  tagName: 'tr',
  regions: {
    editButtonContainer: '.col_actions .edit',
    delButtonContainer: '.col_actions .delete'
  },
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  modelEvents: {
    'change': 'render'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.unity_options = this.config.request('get:options', 'unities');
  },
  onRender: function onRender() {
    var editModel = new base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: 'Modifier cet élément',
      icon: 'pen',
      showLabel: false,
      action: 'edit'
    });
    var deleteModel = new base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: 'Supprimer cet élément',
      icon: 'trash-alt',
      showLabel: false,
      action: 'delete',
      css: "negative"
    });
    this.showChildView('editButtonContainer', new widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: editModel
    }));
    this.showChildView('delButtonContainer', new widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: deleteModel
    }));
  },
  templateContext: function templateContext() {
    return {
      ht_label: this.model.ht_label(),
      supplier_ht_label: this.model.supplier_ht_label(),
      total_ht_label: this.model.total_ht_label()
    };
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'edit') {
      this.triggerMethod('model:edit', this.model, this);
    } else if (action == 'delete') {
      this.triggerMethod('model:delete', this.model, this);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemView);

/***/ }),

/***/ "./src/widgets/ActionButtonsWidget.js":
/*!********************************************!*\
  !*** ./src/widgets/ActionButtonsWidget.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ButtonWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _DropDownWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DropDownWidget.js */ "./src/widgets/DropDownWidget.js");
/* harmony import */ var _base_models_ActionButtonCollection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base/models/ActionButtonCollection */ "./src/base/models/ActionButtonCollection.js");
/* harmony import */ var _ButtonCollectionWidget__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ButtonCollectionWidget */ "./src/widgets/ButtonCollectionWidget.js");
/*
 * File Name :  ActionButtonsWidget.js
 * Action Widget is composed by :
 
 *  - an optionnal primary Button
 *  - an optionnal DropDown description
 */





var ActionButtonsWidget = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  /*
   *
   * Renders Action buttons
   *
   * Can render:
   * A primary item composed of one or several button(s) presented separately
   * from the others
   * A dropdown with a list of buttons
   *
   * For the primary button(s)
   * :param obj primary: An ButtonModel for a single button or
   * an Array of objects for several button
   *
   * ex for several buttons :
   *  primary: [
                  {
                      label: "Voir / Modifier",
                      action: "edit",
                      icon: "pen"
                   },
                   {
                      label: "Dupliquer",
                      action: "duplicate",
                      icon: "copy"
                   },
              ]
   *
   *
   * 
   * For the dropdown
   * :param obj collection: A ButtonCollection
   * :param str dropdownLabel: The label for the optionnal dropdown
   * :param str icon: The icon used for the dropdown
   * :param bool showLabel: Should the label of the dropdown button be showed
   * :param str orientation: dropdown right/left orientation regarding the need
   *
   * Full example : 
   *     let primary = new ButtonModel({label: 'View', icon: 'pencil', action: 'view'});
   *      let dropdown = new ButtonCollection([
              new ButtonModel({label: Duplicate', icon: 'copy', action: 'duplicate'}),
              new ButtonModel({label: 'Delete', icon: 'trash-alt', action: 'delete'}),
          ]);
   *     let view = new ActionButtonsWidget(
              {primary: primary, dropDownLabel: 'Actions', collection: dropdown}
          );
   *     this.showChildView('actions-container', view);
   *
   * Triggers action:clicked with param 'action'
   */
  template: __webpack_require__(/*! ./templates/ActionButtonsWidget.mustache */ "./src/widgets/templates/ActionButtonsWidget.mustache"),
  regions: {
    primary: {
      'el': '.primary',
      replaceElement: true
    },
    dropdown: {
      'el': '.dropdown',
      replaceElement: true
    }
  },
  // On forward l'évènement action:clicked
  childViewTriggers: {
    'action:clicked': 'action:clicked'
  },
  onRender: function onRender() {
    if (this.getOption('primary')) {
      if (Array.isArray(this.getOption('primary'))) {
        var collection = new _base_models_ActionButtonCollection__WEBPACK_IMPORTED_MODULE_2__.default(this.getOption('primary'));
        this.showChildView('primary', new _ButtonCollectionWidget__WEBPACK_IMPORTED_MODULE_3__.default({
          collection: collection
        }));
      } else {
        this.showChildView('primary', new _ButtonWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
          model: this.getOption('primary')
        }));
      }
    }

    if (this.collection) {
      this.showChildView('dropdown', new _DropDownWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(this.options));
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ActionButtonsWidget);

/***/ }),

/***/ "./src/widgets/PagerWidget.js":
/*!************************************!*\
  !*** ./src/widgets/PagerWidget.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools.js */ "./src/tools.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* File Name : PagerWidget.js
 * Description: Composant de pagination pour les listes d'items
 */



var PagerWidget = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: __webpack_require__(/*! ./templates/PagerWidget.mustache */ "./src/widgets/templates/PagerWidget.mustache"),
  tagName: "div",
  className: "pager display_selector",
  regions: {},
  ui: {
    btn_cipp: 'a.btn[data-action=change_items_per_page]',
    btn_sp: 'a.btn[data-action=show_page]',
    sel_bottom: '#items_per_page_bottom',
    sel_top: '#items_per_page_top'
  },
  events: {
    'click @ui.btn_cipp': 'onButtonChangeItemsPerPageClicked',
    'click @ui.btn_sp': 'onButtonShowPageClicked',
    'change @ui.sel_bottom': 'onSelectChanged',
    'change @ui.sel_top': 'onSelectChanged'
  },
  triggers: {
    'navigate:page': 'navigate:page'
  },
  initialize: function initialize() {
    this.position = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.getOpt)(this, 'position', '');
  },
  onButtonChangeItemsPerPageClicked: function onButtonChangeItemsPerPageClicked(event) {
    var selectid = $(event.currentTarget).data('selectid');
    var items_per_page = Number($('#' + selectid)[0].value);
    this.triggerMethod('navigate:itemsperpage', items_per_page);
  },
  onButtonShowPageClicked: function onButtonShowPageClicked(event) {
    var page = $(event.currentTarget).data('page');
    this.triggerMethod('navigate:page', page - 1);
  },
  onSelectChanged: function onSelectChanged(event) {
    var selects = $('.pager select');

    for (var i = 0; i < selects.length; i++) {
      selects[i].value = event.currentTarget.value;
    }
  },
  pagerElements: function pagerElements() {
    // Retourne une liste d'items décrivant chaque composant du Pager
    // à afficher pour le template .mustache
    var currentPageScope = 2; // Nombre de boutons affichés de chaque côté de la page courante

    var currentPage = this.collection.state.currentPage + 1;
    var totalPages = this.collection.state.totalPages;
    var items = [];

    if (totalPages > 1) {
      for (var p = 1; p <= totalPages; p++) {
        var isCurrentPage = p == currentPage;
        var isEllipsis = 1 < p && p < totalPages && Math.abs(currentPage - p) > currentPageScope;
        var isButton = !isCurrentPage && !isEllipsis;

        if (!(isEllipsis && items[items.length - 1]["isEllipsis"])) {
          // On concatène les ellipses
          items.push({
            isButton: isButton,
            isCurrentPage: isCurrentPage,
            isEllipsis: isEllipsis,
            page: isEllipsis ? false : p
          });
        }
      }
    }

    return items;
  },
  selectOptions: function selectOptions() {
    // Retourne une liste d'items décrivant chaque option pour le
    // sélecteur de nombre d'items par page dans le .mustache
    var valueForAll = 1000000;
    var pageSizes = [10, 20, 30, 40, 50, valueForAll];
    var items = [];

    for (var i = 0; i < pageSizes.length; i++) {
      items.push({
        label: pageSizes[i] == valueForAll ? 'Tous' : pageSizes[i] + ' par page',
        selected: pageSizes[i] == this.collection.state.pageSize ? ' selected="selected"' : '',
        value: pageSizes[i]
      });
    }

    return items;
  },
  templateContext: function templateContext() {
    var currentPage = this.collection.state.currentPage + 1;
    var position = ['bottom', 'top'].includes(this.position) ? '_' + this.position : '';
    var context = {
      nextPage: this.collection.hasNextPage() ? currentPage + 1 : false,
      pagerElements: this.pagerElements(),
      position: position,
      previousPage: this.collection.hasPreviousPage() ? currentPage - 1 : false,
      selectOptions: this.selectOptions(),
      totalPagesLabel: this.collection.state.totalPages + ' page' + (this.collection.state.totalPages > 1 ? 's' : ''),
      totalRecords: this.collection.state.totalRecords
    };
    return context;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PagerWidget);

/***/ }),

/***/ "./src/sale_product/views/categories/templates/CategoriesComponent.mustache":
/*!**********************************************************************************!*\
  !*** ./src/sale_product/views/categories/templates/CategoriesComponent.mustache ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2>Catégories</h2>\n<div class='category-list'></div>\n<div class='modal-container'></div>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/categories/templates/CategoryCollectionView.mustache":
/*!*************************************************************************************!*\
  !*** ./src/sale_product/views/categories/templates/CategoryCollectionView.mustache ***!
  \*************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<table class=\"hover_table\">\n	<thead>\n		<tr>\n			<th class=\"col_text\" scope=\"col\">Titre</th>\n			<th scope=\"col\" class=\"col_actions\" title=\"Actions\"><span class=\"screen-reader-text\">Actions</span></th>\n		</tr>\n	</thead>\n    <tbody>\n    </tbody>\n    <tfoot>\n    	<tr>\n    		<td class=\"col_actions\" colspan=\"2\">\n    			<button class='btn icon only add' type='button' title=\"Ajouter une catégorie\" aria-label=\"Ajouter une catégorie\">\n    				<svg><use href=\"/static/icons/endi.svg#plus\"></use></svg>\n        		</button>\n    		</td>\n    	</tr>\n    </tfoot>\n</table>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/categories/templates/CategoryEmptyView.mustache":
/*!********************************************************************************!*\
  !*** ./src/sale_product/views/categories/templates/CategoryEmptyView.mustache ***!
  \********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<td colspan='2' class='col_text'><em>Aucune catégorie n’a été configurée</em></td>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/categories/templates/CategoryForm.mustache":
/*!***************************************************************************!*\
  !*** ./src/sale_product/views/categories/templates/CategoryForm.mustache ***!
  \***************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<form autocomplete=\"off\">\n	<div role=\"dialog\" id=\"category-modal\" aria-modal=\"true\" aria-labelledby=\"category-modal-title\">\n		<div class=\"modal_layout\">\n			<header>\n		 		<button\n					class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" type='button'>\n					<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n				</button>\n				<h2 id=\"category-modal-title\">\n					"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":10,"column":5},"end":{"line":10,"column":16}}}) : helper)))
    + " une catégorie\n				</h2>\n			</header>\n			<div class=\"modal_content_layout\">\n				<div class=\"modal_content\">\n                    <fieldset>\n						<div class='field-title'></div>\n						<div class='field-description content_vertical_padding'></div>\n                    </fieldset>\n				</div>\n				<footer>\n					<button class='btn btn-primary icon' type='submit' value='insert'><svg><use href=\"/static/icons/endi.svg#check\"></use></svg>"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":21,"column":129},"end":{"line":21,"column":140}}}) : helper)))
    + "\n					</button>\n					<button class='btn icon' type='reset' value='cancel'><svg><use href=\"/static/icons/endi.svg#times\"></use></svg>Annuler\n					</button>\n				</footer>\n			</div>\n		</div>\n	</div>\n</form>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/categories/templates/CategoryView.mustache":
/*!***************************************************************************!*\
  !*** ./src/sale_product/views/categories/templates/CategoryView.mustache ***!
  \***************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class=\"col_text\" title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":28},"end":{"line":1,"column":43}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":1,"column":45},"end":{"line":1,"column":56}}}) : helper)))
    + "</td>\n<td class='col_actions width_two'>\n    <button\n    class='btn icon only edit'\n    type='button'\n    title=\"Modifier cette catégorie\">\n        <svg><use href=\"/static/icons/endi.svg#pen\"></use></svg>\n    </button>\n    <button\n    class='btn icon only delete negative'\n    type='button'\n    title=\"Supprimer cette catégorie\">\n        <svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg>\n    </button>\n</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/list/templates/DeleteFiltersView.mustache":
/*!**************************************************************************!*\
  !*** ./src/sale_product/views/list/templates/DeleteFiltersView.mustache ***!
  \**************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<span class='help_text'>\n    <small><em>Des filtres sont actifs</em></small>\n</span>\n<span class='help_text'>\n    <a class=\"handle_delete_filters\" href=\"#\">\n        <svg class=\"icon\"><use href=\"/static/icons/endi.svg#times\"></use></svg> Supprimer tous les filtres\n    </a>\n</span>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/list/templates/ProductEmptyView.mustache":
/*!*************************************************************************!*\
  !*** ./src/sale_product/views/list/templates/ProductEmptyView.mustache ***!
  \*************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<td class=\"col_text\" colspan=\"11\"><em>Aucun produit n'est présent dans votre catalogue</em></td>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/list/templates/ProductFilterForm.mustache":
/*!**************************************************************************!*\
  !*** ./src/sale_product/views/list/templates/ProductFilterForm.mustache ***!
  \**************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class=\"collapse_title\">\n    <a href=\"#filter-form\" data-toggle=\"collapse\" aria-expanded=\"true\" aria-controls=\"filter-form\" accesskey=\"R\">\n        <span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#search\"></use></svg></span>\n        Recherche\n        <svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n    </a>\n    <div id=\"delete_filters\"></div>\n</h2>\n<div class=\"collapse_content\">\n    <div id=\"filter-form\" class=\"collapse in\" aria-expanded=\"true\">\n        <form class='form-search'>\n            <div class='field-type_'></div>\n            <div class='field-name'></div>\n            <div class='field-description'></div>\n            <div class='field-category-id'></div>\n            <div class='field-ref'></div>\n            <div class='field-supplier-id'></div>\n            <div class='field-supplier-ref'></div>\n            <div class='field-mode'></div>\n            <div>\n                <button class='btn btn-primary icon only' type='submit' title=\"Rechercher\" aria-label=\"Rechercher\">\n                	<svg><use href=\"/static/icons/endi.svg#search\"></use></svg>\n                </button>\n            </div>\n        </form>\n    </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/list/templates/ProductListComponent.mustache":
/*!*****************************************************************************!*\
  !*** ./src/sale_product/views/list/templates/ProductListComponent.mustache ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='main_content'>\n    <div class='main_toolbar'>\n        <div class=\"layout flex main_actions\">\n            <button class='btn btn-primary icon' type='button' value='add'>\n                <svg><use href=\"/static/icons/endi.svg#plus\"></use></svg>\n                Ajouter un produit\n            </button>\n            <button class='btn btn-primary icon' type='button' value='export'>\n                <svg><use href=\"/static/icons/endi.svg#file-csv\"></use></svg>\n                Exporter le catalogue\n            </button>\n        </div>\n    </div>\n    <div class='message-container'></div>\n\n    <div class='layout flex product_catalogue'>\n        <div>\n            <div class='collapsible search_filters'></div>\n            <div class=\"pager_widget_top\"></div>\n            <div class='table_container'></div>\n            <div class=\"pager_widget_bottom\"></div>\n        </div>\n        <div class='category-container'></div>\n    </div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/list/templates/ProductTable.mustache":
/*!*********************************************************************!*\
  !*** ./src/sale_product/views/list/templates/ProductTable.mustache ***!
  \*********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"isSortable") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":7,"column":6},"end":{"line":16,"column":13}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <th class=\"col_"
    + alias4(((helper = (helper = lookupProperty(helpers,"type") || (depth0 != null ? lookupProperty(depth0,"type") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data,"loc":{"start":{"line":8,"column":21},"end":{"line":8,"column":29}}}) : helper)))
    + " sortable"
    + alias4(((helper = (helper = lookupProperty(helpers,"sortClass") || (depth0 != null ? lookupProperty(depth0,"sortClass") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sortClass","hash":{},"data":data,"loc":{"start":{"line":8,"column":38},"end":{"line":8,"column":51}}}) : helper)))
    + "\" scope=\"col\" data-sort=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"key") || (depth0 != null ? lookupProperty(depth0,"key") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data,"loc":{"start":{"line":8,"column":76},"end":{"line":8,"column":83}}}) : helper)))
    + "\">\n        <a href=\"#\" aria-label=\"Trier par "
    + alias4(((helper = (helper = lookupProperty(helpers,"aria") || (depth0 != null ? lookupProperty(depth0,"aria") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"aria","hash":{},"data":data,"loc":{"start":{"line":9,"column":42},"end":{"line":9,"column":50}}}) : helper)))
    + "\" title=\"Trier par "
    + alias4(((helper = (helper = lookupProperty(helpers,"aria") || (depth0 != null ? lookupProperty(depth0,"aria") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"aria","hash":{},"data":data,"loc":{"start":{"line":9,"column":69},"end":{"line":9,"column":77}}}) : helper)))
    + "\" class=\"icon"
    + alias4(((helper = (helper = lookupProperty(helpers,"sortClass") || (depth0 != null ? lookupProperty(depth0,"sortClass") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sortClass","hash":{},"data":data,"loc":{"start":{"line":9,"column":90},"end":{"line":9,"column":103}}}) : helper)))
    + "\">\n          <svg><use href=\"/static/icons/endi.svg#sort-"
    + alias4(((helper = (helper = lookupProperty(helpers,"orderIcon") || (depth0 != null ? lookupProperty(depth0,"orderIcon") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"orderIcon","hash":{},"data":data,"loc":{"start":{"line":10,"column":54},"end":{"line":10,"column":67}}}) : helper)))
    + "\"></use></svg>\n          "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":11,"column":10},"end":{"line":11,"column":19}}}) : helper)))
    + "\n        </a>\n      </th>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <th class=\"col_"
    + alias4(((helper = (helper = lookupProperty(helpers,"type") || (depth0 != null ? lookupProperty(depth0,"type") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data,"loc":{"start":{"line":15,"column":21},"end":{"line":15,"column":29}}}) : helper)))
    + "\" scope=\"col\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":15,"column":43},"end":{"line":15,"column":52}}}) : helper)))
    + "</th>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<table class=\"hover_table sortable-table\">\n  <caption class=\"screen-reader-text\">Liste des produits du catalogue</caption>\n  <thead>\n    <tr>\n      <th class=\"col_text\"></th>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"colHeaders") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":6},"end":{"line":17,"column":15}}})) != null ? stack1 : "")
    + "    </tr>\n  </thead>\n  <tbody></tbody>\n</table>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/list/templates/ProductView.mustache":
/*!********************************************************************!*\
  !*** ./src/sale_product/views/list/templates/ProductView.mustache ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "{\n    <td title=\"Produit composé - "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":2,"column":33},"end":{"line":2,"column":51}}}) : helper)))
    + "\">\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"icons") : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":12},"end":{"line":8,"column":21}}})) != null ? stack1 : "")
    + "    </td>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "        <span class='icon type'>\n        <svg><use href='/static/icons/endi.svg#"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "'></use></svg>\n            </span>\n        <br />\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <td title='"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":11,"column":15},"end":{"line":11,"column":33}}}) : helper)))
    + "'></td>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":47,"column":28},"end":{"line":47,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":48,"column":0},"end":{"line":48,"column":15}}}) : helper)))
    + "\n</td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"icons") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":12,"column":7}}})) != null ? stack1 : "")
    + "<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":13,"column":28},"end":{"line":13,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":14,"column":0},"end":{"line":14,"column":8}}}) : helper)))
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":16,"column":28},"end":{"line":16,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":17,"column":0},"end":{"line":17,"column":11}}}) : helper)))
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":19,"column":28},"end":{"line":19,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"ref") || (depth0 != null ? lookupProperty(depth0,"ref") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ref","hash":{},"data":data,"loc":{"start":{"line":20,"column":0},"end":{"line":20,"column":9}}}) : helper)))
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":22,"column":28},"end":{"line":22,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"supplier_label") || (depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_label","hash":{},"data":data,"loc":{"start":{"line":23,"column":0},"end":{"line":23,"column":20}}}) : helper)))
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":25,"column":28},"end":{"line":25,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"supplier_ref") || (depth0 != null ? lookupProperty(depth0,"supplier_ref") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_ref","hash":{},"data":data,"loc":{"start":{"line":26,"column":0},"end":{"line":26,"column":18}}}) : helper)))
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":28,"column":28},"end":{"line":28,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"category_label") || (depth0 != null ? lookupProperty(depth0,"category_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"category_label","hash":{},"data":data,"loc":{"start":{"line":29,"column":0},"end":{"line":29,"column":20}}}) : helper)))
    + "\n</td>\n<td class='col_number' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":31,"column":30},"end":{"line":31,"column":48}}}) : helper)))
    + "'>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"current_stock") || (depth0 != null ? lookupProperty(depth0,"current_stock") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"current_stock","hash":{},"data":data,"loc":{"start":{"line":32,"column":0},"end":{"line":32,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class='col_number' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":34,"column":30},"end":{"line":34,"column":48}}}) : helper)))
    + "'>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":35,"column":0},"end":{"line":35,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class='col_number' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":37,"column":30},"end":{"line":37,"column":48}}}) : helper)))
    + "'>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":38,"column":0},"end":{"line":38,"column":16}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":40,"column":28},"end":{"line":40,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":41,"column":0},"end":{"line":41,"column":11}}}) : helper)))
    + "\n</td>\n<td class='col_text' title='"
    + alias4(((helper = (helper = lookupProperty(helpers,"tooltipTitle") || (depth0 != null ? lookupProperty(depth0,"tooltipTitle") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tooltipTitle","hash":{},"data":data,"loc":{"start":{"line":43,"column":28},"end":{"line":43,"column":46}}}) : helper)))
    + "'>\n"
    + alias4(((helper = (helper = lookupProperty(helpers,"updated_at") || (depth0 != null ? lookupProperty(depth0,"updated_at") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"updated_at","hash":{},"data":data,"loc":{"start":{"line":44,"column":0},"end":{"line":44,"column":16}}}) : helper)))
    + "\n</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"tvaMode") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":46,"column":0},"end":{"line":50,"column":7}}})) != null ? stack1 : "")
    + "<td class='col_actions width_two'>\n</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/stock/templates/StockOperationComponent.mustache":
/*!**********************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/stock/templates/StockOperationComponent.mustache ***!
  \**********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "\n<h2 class='title'>\n	<span class=\"icon status neutral\" title=\"Ces informations internes n’apparaissent pas dans les documents finaux (Devis/Factures)\">\n		<svg><use href=\"/static/icons/endi.svg#eye-slash\"></use></svg>\n	</span>\n	Stock\n	<small>N’apparait pas dans les documents finaux</small>\n</h2>\n<div class='layout flex'>\n	<div class='col-md-4'>        \n        <div class=\"current_stock\"></div>\n        <div class=\"add\"></div>\n    </div>\n	<div class='col-md-8'>\n        <div class=\"table_container\">\n            <table class=\"hover_table\">\n                    <caption class='screen-reader-text'>\n                        Liste des mouvements de stock\n                    </caption>\n                    <thead>\n                        <tr>\n                            <th scope='col' class='col_date'>Date</th>\n                            <th scope=\"col\" class=\"col_text\">Description</th>\n                            <th scope=\"col\" title=\"Variation du stock\" class=\"col_number\">\n                                Variation<span class=\"screen-reader-text\"> du stock</span>\n                            </th>\n                            <th scope=\"col\" class=\"col_actions\" title=\"Actions\">\n                                <span class=\"screen-reader-text\">Actions</span>\n                            </th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                    </tbody>\n            </table>\n            <div class='popin'></div>\n        </div>\n    </div>\n</div>\n\n\n\n\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/stock/templates/StockOperationForm.mustache":
/*!*****************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/stock/templates/StockOperationForm.mustache ***!
  \*****************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<form>\n    <div role=\"dialog\" id=\"stock_operation-forms\" aria-modal=\"true\" aria-labelledby=\"stock_operation-forms_title\">\n        <div class=\"modal_layout\">\n            <header>\n                <button class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" type='button'>\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"stock_operation-forms_title\">\n                    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":9,"column":20},"end":{"line":9,"column":29}}}) : helper)))
    + "\n                </h2>\n            </header>\n            <div class=\"modal_content_layout\">\n                <div class=\"modal_content\">\n                    <div class='errors'></div>\n                    <fieldset>                \n                        <div class='field-date'></div>\n                        <div class='field-description'></div>\n                        <div class=\"row form-row\">\n                            <div class='col-md-6 field-stock_variation'></div>\n                        </div>\n\n                        <div class='field-base_sale_product_id'></div>\n                    </fieldset>\n                </div>\n                <footer>\n                    <button class='btn btn-primary icon' type='submit' value='submit'><svg> <use href=\"/static/icons/endi.svg#check\"></use></svg>Valider\n                    </button>\n                    <button class='btn icon' type='reset' value='cancel'><svg><use href=\"/static/icons/endi.svg#times\"></use></svg>Annuler\n                    </button>\n                </footer>\n            </div>\n        </div>\n    </div>\n</form>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/stock/templates/StockOperationView.mustache":
/*!*****************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/stock/templates/StockOperationView.mustache ***!
  \*****************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_date'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"date") || (depth0 != null ? lookupProperty(depth0,"date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data,"loc":{"start":{"line":1,"column":21},"end":{"line":1,"column":33}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":2,"column":21},"end":{"line":2,"column":40}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"stock_variation") || (depth0 != null ? lookupProperty(depth0,"stock_variation") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"stock_variation","hash":{},"data":data,"loc":{"start":{"line":3,"column":23},"end":{"line":3,"column":46}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_actions width_two\">\n	<ul>\n		<li class='edit'></li>\n		<li class='delete'></li>\n	</ul>\n</td>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/templates/AddProductForm.mustache":
/*!*******************************************************************************!*\
  !*** ./src/sale_product/views/product_form/templates/AddProductForm.mustache ***!
  \*******************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<form name=\"current\">\n	<div class='main_toolbar'>\n		<div class=\"layout flex main_actions\">\n			<div role='group'>\n                <button class='btn btn-primary icon' type='submit' value='submit'>\n                    <svg><use href=\"/static/icons/endi.svg#save\"></use></svg>Enregistrer\n                </button>\n				<button class='icon' type='reset' value='submit' title=\"Annuler et revenir en arrière\" aria-label=\"Annuler et revenir en arrière\">\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>Annuler\n                </button>\n			</div>\n			<div class='resume'></div>\n		</div>\n	</div>\n	<div class='limited_width width40'>\n		<div class='message-container'></div>\n		<div class='errors'></div>\n		<fieldset>\n			<h2 class=\"title\">Ajout d’un nouveau produit</h2>\n			<div class='field-label'></div>\n			<div class='field-type_'></div>\n		</fieldset>\n	</div>\n</form>\n\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/templates/HelpTextView.mustache":
/*!*****************************************************************************!*\
  !*** ./src/sale_product/views/product_form/templates/HelpTextView.mustache ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <li>Coefficient de marge du produit "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":11,"column":40},"end":{"line":11,"column":57}}}) : helper)))
    + "</li>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <li>Coefficient de marge de votre enseigne "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":13,"column":47},"end":{"line":13,"column":62}}}) : helper)))
    + "</li>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <li>\n        Taux de contribution "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"contribution") : stack1), depth0))
    + "&nbsp;%\n    </li>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <li>\n    Taux d'assurance "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"insurance") : stack1), depth0))
    + "&nbsp;%\n    </li>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return " x (100 - "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"contribution") : stack1), depth0))
    + ")";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return " x (100 - "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"insurance") : stack1), depth0))
    + ")";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda, alias3=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"alert alert-info\">\n    <button type=\"button\" class=\"icon only unstyled close\" title=\"Fermer cette aide\" aria-label=\"Fermer cette aide\">\n        <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n    </button>\n    <span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#info-circle\"></use></svg></span>\n    Le prix de vente indiqué ici est donné à titre indicatif.<br />\nLors de l'utilisation du produit dans une étude de prix, les coefficients de marge et de frais généraux pourront être personnalisés    <br />\n    <h4>Coefficients utilisés</h4>\n    <ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"use_margin_rate") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":10,"column":4},"end":{"line":14,"column":11}}})) != null ? stack1 : "")
    + "    <li>\n    Coefficient de frais généraux de votre enseigne "
    + alias3(alias2(((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"general_overhead") : stack1), depth0))
    + "\n    </li>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"use_contribution") : stack1),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":19,"column":4},"end":{"line":23,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"insurance") : stack1),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":4},"end":{"line":28,"column":11}}})) != null ? stack1 : "")
    + "    </ul>\n    <h4>Formule de calcul</h4>\n    <code>Prix de vente = Coût d'achat * (1 + "
    + alias3(alias2(((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"general_overhead") : stack1), depth0))
    + ") / (  (1-"
    + alias3(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":31,"column":91},"end":{"line":31,"column":106}}}) : helper)))
    + ")"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"use_contribution") : stack1),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":107},"end":{"line":31,"column":195}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"computing_info") : depth0)) != null ? lookupProperty(stack1,"use_contribution") : stack1),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":195},"end":{"line":31,"column":280}}})) != null ? stack1 : "")
    + "  )</code>\n    <br />\n    <h4>Forcer le calcul</h4>\n    Si vous avez modifiez des éléments dans votre fiche enseigne, vous pouvez recalculer le prix de vente indicatif de votre produit en cliquant ici \n    <button type=\"button\" class=\"unstyled icon only compute\" title=\"Forcer le re-calcul des montants\">\n        <span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#calculator\"></use></svg></span>\n    </button>\n</div>}";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/templates/ProductForm.mustache":
/*!****************************************************************************!*\
  !*** ./src/sale_product/views/product_form/templates/ProductForm.mustache ***!
  \****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class='field-title'></div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "            <div class='field-ht'></div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "            <div class='field-ttc'></div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "    <fieldset class='separate_block border_left_block'>\n        <h2 class='title'>Achat</h2>\n        <div class='layout flex'>\n            <div class='field-supplier_id col-md-4'></div>\n            <div class='field-supplier_ref col-md-4'></div>\n            <div class='field-supplier_unity_amount col-md-4'></div>\n        </div>\n        <div class='layout flex'>\n            <div class='field-supplier_ht col-md-4'></div>\n        </div>\n    </fieldset>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "    <div class='stocks private'></div>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "    <div class='items'></div>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "    <fieldset class='separate_block border_left_block'>\n        <h2 class='title'>Formation</h2>\n        <div class=\"field-goals\"></div>\n        <div class=\"field-prerequisites\"></div>\n        <div class=\"field-for_who\"></div>\n        <div class=\"field-duration\"></div>\n        <div class=\"field-content\"></div>\n        <div class=\"field-teaching_method\"></div>\n        <div class=\"field-logistics_means\"></div>\n        <div class=\"field-more_stuff\"></div>\n        <div class=\"field-evaluation\"></div>\n        <div class=\"field-place\"></div>\n        <div class=\"field-modality_one\"></div>\n        <div class=\"field-modality_two\"></div>\n        <div class=\"field-types\"></div>\n        <div class=\"field-date\"></div>\n        <div class=\"field-price\"></div>\n        <div class=\"field-free_1\"></div>\n        <div class=\"field-free_2\"></div>\n        <div class=\"field-free_3\"></div>\n    </fieldset>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "<form name=\"current\">\n\n    <div class='main_toolbar'>\n        <div class=\"layout flex main_actions\">\n            <div role='group'>\n                <button class='btn btn-primary icon icon_only_mobile' type='submit' value='submit'>\n                    <svg><use href=\"/static/icons/endi.svg#save\"></use></svg>Enregistrer\n                </button>\n				<button class='icon icon_only_mobile' type='reset' value='submit' title=\"Annuler et revenir en arrière\" aria-label=\"Annuler et revenir en arrière\">\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>Annuler\n                </button>\n            </div>\n            <div class='resume' role='group'></div>\n            <div role='group' class='other_buttons'></div>\n        </div>\n        <div class='message-container'></div>\n        <div class='errors'></div>\n    </div>\n    <div class=\"private\">\n		<fieldset class='separate_block border_left_block'>\n			<h2 class='title'>\n				<span class=\"icon status neutral\" title=\"Ces informations internes n’apparaissent pas dans les documents finaux (Devis/Factures)\">\n					<svg><use href=\"/static/icons/endi.svg#eye-slash\"></use></svg>\n				</span>\n				Informations internes\n				<small>N’apparaissent pas dans les documents finaux</small>\n			</h2>\n			<div class='layout flex four_cols'>\n				<div class='field-label'></div>\n				<div class='field-category_id'></div>\n                <div class='field-ref'></div>\n                <div class='field-type_'></div>\n			</div>\n		</fieldset>\n    </div>\n\n    <fieldset class='separate_block border_left_block'>\n        <h2 class='title'>Vente</h2>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"complex") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":39,"column":8},"end":{"line":41,"column":15}}})) != null ? stack1 : "")
    + "        <div class='field-description'></div>\n        <div class='field-mode'></div>\n        <div class=\"layout flex four_cols\">\n            <div class=\"field-margin_rate\"></div>\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"complex") : depth0),{"name":"unless","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":46,"column":12},"end":{"line":48,"column":23}}})) != null ? stack1 : "")
    + "            <div class='field-tva_id'></div>\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"complex") : depth0),{"name":"unless","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":50,"column":12},"end":{"line":52,"column":23}}})) != null ? stack1 : "")
    + "            <div class='field-product_id'></div>\n            <div class='field-unity'></div>\n        </div>\n    </fieldset>\n\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"complex") : depth0),{"name":"unless","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":58,"column":4},"end":{"line":70,"column":15}}})) != null ? stack1 : "")
    + "\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"service_form") || (depth0 != null ? lookupProperty(depth0,"service_form") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"service_form","hash":{},"fn":container.noop,"inverse":container.program(9, data, 0),"data":data,"loc":{"start":{"line":72,"column":4},"end":{"line":74,"column":21}}}),(typeof helper === "function" ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"service_form")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"complex") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":76,"column":4},"end":{"line":78,"column":11}}})) != null ? stack1 : "")
    + "\n	<div class=\"private\">\n    	<fieldset class='separate_block border_left_block'>\n        	<h2 class='title'>\n				<span class=\"icon status neutral\" title=\"Ces informations internes n’apparaissent pas dans les documents finaux (Devis/Factures)\">\n					<svg><use href=\"/static/icons/endi.svg#eye-slash\"></use></svg>\n				</span>\n        		Notes\n				<small>N’apparaissent pas dans les documents finaux</small>\n        	</h2>\n	        <div class='field-notes'></div>\n    	</fieldset>\n	</div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"training_form") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":93,"column":4},"end":{"line":115,"column":11}}})) != null ? stack1 : "")
    + "\n</form>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/templates/ProductResume.mustache":
/*!******************************************************************************!*\
  !*** ./src/sale_product/views/product_form/templates/ProductResume.mustache ***!
  \******************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <b>Déboursé Sec&nbsp;: "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":5,"column":27},"end":{"line":5,"column":50}}}) : helper))) != null ? stack1 : "")
    + "</b>\n    <br />\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <b>Prix de vente TTC&nbsp;: "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc_label") || (depth0 != null ? lookupProperty(depth0,"ttc_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ttc_label","hash":{},"data":data,"loc":{"start":{"line":9,"column":32},"end":{"line":9,"column":47}}}) : helper))) != null ? stack1 : "")
    + "</b>\n    <br />\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":4},"end":{"line":16,"column":11}}})) != null ? stack1 : "")
    + "    <b>Prix de vente HT&nbsp;: "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":17,"column":31},"end":{"line":17,"column":45}}}) : helper))) != null ? stack1 : "")
    + "\n    \n    </b>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "    <a onclick=\"javascript:void(0)\" class=\"unstyled icon only help\" title=\"Afficher le détail sur les méthodes de calcul\">\n        <svg><use href=\"/static/icons/endi.svg#info-circle\"></use></svg>\n    </a>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div>\n    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":13}}}) : helper)))
    + "\n    <br/>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":4},"end":{"line":7,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":20,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/templates/WorkItemComponent.mustache":
/*!********************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/templates/WorkItemComponent.mustache ***!
  \********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class=\"title\">\n  <!-- Ne pas mettre cette icône tout le temps, mais seulement quand il n’y a pas de produit ? Oui ! -->\n  <span\n    class=\"icon status caution\"\n    title=\"Il faut au moins un produit pour créer un produit composé\"\n  >\n    <svg>\n      <use href=\"/static/icons/endi.svg#danger\"></use>\n    </svg>\n  </span>\n  Liste des produits\n</h2>\n<div class=\"table_container items\">\n  <table class=\"hover_table\">\n    <caption class=\"screen-reader-text\">\n      Liste des produits compris dans le produit composé\n    </caption>\n    <thead>\n      <tr>\n        <th scope=\"col\" class=\"col_text\"></th>\n        <th scope=\"col\" class=\"col_text\">\n          Description\n        </th>\n        <th scope=\"col\" title=\"Coût unitaire Hors Taxes\" class=\"col_number\">\n          Coût U<span class=\"screen-reader-text\">nitaire</span>. H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n        </th>\n        <th scope=\"col\" title=\"Prix unitaire Hors Taxes\" class=\"col_number\">\n          Prix U<span class=\"screen-reader-text\">nitaire</span>. H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n        </th>\n        <th scope=\"col\" class=\"col_number\" title=\"Quantité par unité d’œuvre\">\n          Q<span class=\"screen-reader-text\">uanti</span>té<span class=\"screen-reader-text\"> par unité d’œuvre</span>\n        </th>\n        <th scope=\"col\">\n          Unité\n        </th>\n        <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxes\">\n          <span class=\"screen-reader-text\">Prix</span> H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n        </th>\n        <th scope=\"col\" class=\"col_actions\" title=\"Actions\">\n          <span class=\"screen-reader-text\">Actions</span>\n        </th>\n      </tr>\n    </thead>\n    <tbody></tbody>\n    <tfoot>\n      <tr>\n        <td class=\"col_actions\" colspan=\"12\">\n          <ul>\n            <li class=\"add\"></li>\n          </ul>\n        </td>\n      </tr>\n    </tfoot>\n  </table>\n  <div class=\"popin\"></div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/templates/WorkItemEmptyView.mustache":
/*!********************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/templates/WorkItemEmptyView.mustache ***!
  \********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<td class='col_text' colspan='11'>\n<em>\nAucun produit n’a encore été ajouté\n</em>\n</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/templates/WorkItemForm.mustache":
/*!***************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/templates/WorkItemForm.mustache ***!
  \***************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <nav>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a\n                            href=\"#form-container\"\n                            aria-controls=\"form-container\"\n                            id=\"form-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\"\n                            tabindex=\"-1\"\n                        >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a\n                            href=\"#catalog-container\"\n                            aria-controls=\"catalog-container\"\n                            id=\"catalog-tabtitle\"\n                            role=\"tab\"\n                            tabindex=\"-1\"\n                            data-toggle=\"tab\"\n                        >\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <div class=\"alert alert-info\">\n                                "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"help_text") || (depth0 != null ? lookupProperty(depth0,"help_text") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"help_text","hash":{},"data":data,"loc":{"start":{"line":65,"column":32},"end":{"line":65,"column":47}}}) : helper))) != null ? stack1 : "")
    + "\n                            </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"locked") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data,"loc":{"start":{"line":67,"column":28},"end":{"line":91,"column":28}}})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <div class=\"alert alert-info alert-locked\">\n                                <button\n                                    class=\"btn unlock icon only\"\n                                    type=\"button\"\n                                    title=\"Délier ce champ de l’entrée du catalogue produit correspondante\"\n                                >\n                                    <svg>\n                                        <use href=\"/static/icons/endi.svg#link\"></use>\n                                    </svg>\n                                </button>\n                                Cet élément est lié au produit du catalogue correspondant <strong>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"label","hash":{},"data":data,"loc":{"start":{"line":78,"column":98},"end":{"line":78,"column":107}}}) : helper)))
    + "</strong>.\n                                <br />\n                                Les modifications apportées aux champs «&nbsp;hérité(e) du catalogue&nbsp;» seront répercutés sur ce produit.\n                            </div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                            <div class=\"alert alert-info\">\n                                <span class=\"icon only\">\n                                    <svg>\n                                        <use href=\"/static/icons/endi.svg#lock-open\"></use>\n                                    </svg>\n                                </span>\n                                Cet élément n’est pas lié au produit du catalogue <strong>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"label","hash":{},"data":data,"loc":{"start":{"line":89,"column":90},"end":{"line":89,"column":99}}}) : helper)))
    + "</strong>. Les valeurs spécifiées ici seront synchronisées avec le produit du catalogue correspondant uniquement si vous cochez la case « Synchroniser avec le catalogue ».\n                            </div>\n                            ";
},"10":function(container,depth0,helpers,partials,data) {
    return "                <div \n                    role=\"tabpanel\" \n                    class=\"tab-pane\" \n                    id=\"catalog-container\"\n                    aria-labelledby=\"catalog-tabtitle\">\n                </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"size_middle\">\n    <div\n        role=\"dialog\"\n        id=\"product-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"product-forms_title\"\n    >\n        <div class=\"modal_layout\">\n            <header>\n                <button\n                    class=\"icon only unstyled close\"\n                    title=\"Fermer cette fenêtre\"\n                    aria-label=\"Fermer cette fenêtre\"\n                    type='button'\n                >\n                    <svg>\n                        <use href=\"/static/icons/endi.svg#times\"></use>\n                    </svg>\n                </button>\n                <h2 id=\"product-forms_title\">\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":21,"column":20},"end":{"line":21,"column":29}}}) : helper)))
    + "\n                </h2>\n            </header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":12},"end":{"line":53,"column":19}}})) != null ? stack1 : "")
    + "            <div class=\"tab-content\">\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\"\n                    aria-labelledby=\"form-tabtitle\"\n                >\n                    <form class=\"modal_content_layout layout product-form\">\n                        <div class=\"modal_content\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"raw_create") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":63,"column":28},"end":{"line":91,"column":35}}})) != null ? stack1 : "")
    + "                            <div class=\"errors\"></div>\n                            <fieldset>\n                                <div class=\"field-label\"></div>\n                                <div class=\"field-type_\"></div>\n                            </fieldset>\n                            <fieldset>\n                                <div class=\"field-locked\"></div>\n                                <div class=\"field-description\"></div>\n                                <div class=\"field-mode\"></div>\n                                <div class=\"field-supplier_ht\"></div>\n                                <div class=\"field-ht\"></div>\n                                <div class=\"layout flex\">\n                                    <div class=\"col-md-6 field-quantity\"></div>\n                                    <div class=\"col-md-6 field-unity\"></div>\n                                </div>\n                                <div class=\"layout flex\">\n                                    <div class=\"col-md-6 field-sync_catalog\"></div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <footer>\n                            <button \n                                class=\"btn btn-primary\" \n                                type=\"submit\" \n                                value=\"submit\"\n                                title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":117,"column":39},"end":{"line":117,"column":48}}}) : helper)))
    + "\"\n                                aria-label=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":118,"column":44},"end":{"line":118,"column":53}}}) : helper)))
    + "\"\n                            >\n                                <svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n                                Valider\n                            </button>\n                            <button class=\"btn\" type=\"reset\" value=\"submit\">\n                                Annuler\n                            </button>\n                        </footer>\n                    </form>\n                </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":129,"column":16},"end":{"line":136,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/product_form/work_item/templates/WorkItemView.mustache":
/*!***************************************************************************************!*\
  !*** ./src/sale_product/views/product_form/work_item/templates/WorkItemView.mustache ***!
  \***************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "    <span\n      class=\"icon\"\n      title=\"Cet élément hérite des informations du catalogue produit\"\n    >\n      <svg>\n        <use href=\"/static/icons/endi.svg#lock\"></use>\n      </svg>\n    </span>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <span\n      class=\"icon\"\n      title=\"Cet élément n’hérite pas des informations du catalogue produit\"\n    >\n      <svg>\n        <use href=\"/static/icons/endi.svg#lock-open\"></use>\n      </svg>\n    </span>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"locked") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":2,"column":2},"end":{"line":20,"column":9}}})) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":23,"column":2},"end":{"line":23,"column":19}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":26,"column":2},"end":{"line":26,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":29,"column":2},"end":{"line":29,"column":16}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">\n  "
    + alias4(((helper = (helper = lookupProperty(helpers,"quantity") || (depth0 != null ? lookupProperty(depth0,"quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data,"loc":{"start":{"line":32,"column":2},"end":{"line":32,"column":14}}}) : helper)))
    + "\n</td>\n<td class=\"col_text\">\n  "
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":35,"column":2},"end":{"line":35,"column":11}}}) : helper)))
    + "\n</td>\n<td class=\"col_number\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":38,"column":2},"end":{"line":38,"column":22}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_actions width_two\">\n  <ul>\n    <li class=\"edit\"></li>\n    <li class=\"delete\"></li>\n  </ul>\n</td>";
},"useData":true});

/***/ }),

/***/ "./src/sale_product/views/templates/RootComponent.mustache":
/*!*****************************************************************!*\
  !*** ./src/sale_product/views/templates/RootComponent.mustache ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='modal-container'></div>\n<div class='main'></div>";
},"useData":true});

/***/ }),

/***/ "./src/widgets/templates/ActionButtonsWidget.mustache":
/*!************************************************************!*\
  !*** ./src/widgets/templates/ActionButtonsWidget.mustache ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"product_actions\">\n    <div class=\"flex\">\n        <div class='primary'></div>\n        <span class='dropdown'></span>\n    </div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/widgets/templates/PagerWidget.mustache":
/*!****************************************************!*\
  !*** ./src/widgets/templates/PagerWidget.mustache ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "  <nav aria-label=\"Pagination\">\n    <ul>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <li>\n        <a class=\"btn\" data-action=\"show_page\" data-page=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"previousPage") || (depth0 != null ? lookupProperty(depth0,"previousPage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"previousPage","hash":{},"data":data,"loc":{"start":{"line":8,"column":58},"end":{"line":8,"column":76}}}) : helper)))
    + "\" href=\"javascript:void(0);\"\n          title=\"Voir la page précédente ("
    + alias4(((helper = (helper = lookupProperty(helpers,"previousPage") || (depth0 != null ? lookupProperty(depth0,"previousPage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"previousPage","hash":{},"data":data,"loc":{"start":{"line":9,"column":42},"end":{"line":9,"column":60}}}) : helper)))
    + ")\" aria-label=\"Voir la page précédente\">\n          <svg>\n            <use href=\"/static/icons/endi.svg#chevron-left\"></use>\n          </svg>\n        </a>\n      </li>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isButton") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":17,"column":6},"end":{"line":20,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isCurrentPage") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":21,"column":6},"end":{"line":24,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isEllipsis") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":25,"column":6},"end":{"line":27,"column":13}}})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <li><a class=\"btn\" data-action=\"show_page\" data-page=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":18,"column":60},"end":{"line":18,"column":70}}}) : helper)))
    + "\" href=\"javascript:void(0);\"\n          title=\"Voir la page "
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":19,"column":30},"end":{"line":19,"column":40}}}) : helper)))
    + "\" aria-label=\"Voir la page "
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":19,"column":67},"end":{"line":19,"column":77}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":19,"column":79},"end":{"line":19,"column":89}}}) : helper)))
    + "</a></li>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <li><span class=\"current\" title=\"Page en cours : page "
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":22,"column":60},"end":{"line":22,"column":70}}}) : helper)))
    + "\" aria-label=\"Page en cours : page "
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":22,"column":105},"end":{"line":22,"column":115}}}) : helper)))
    + "\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"page") || (depth0 != null ? lookupProperty(depth0,"page") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"page","hash":{},"data":data,"loc":{"start":{"line":22,"column":117},"end":{"line":23,"column":17}}}) : helper)))
    + "</span></li>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "      <li><span class=\"spacer\">…</span></li>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <li>\n        <a class=\"btn\" data-action=\"show_page\" data-page=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"nextPage") || (depth0 != null ? lookupProperty(depth0,"nextPage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"nextPage","hash":{},"data":data,"loc":{"start":{"line":31,"column":58},"end":{"line":31,"column":72}}}) : helper)))
    + "\" href=\"javascript:void(0);\"\n          title=\"Voir la page suivante ("
    + alias4(((helper = (helper = lookupProperty(helpers,"nextPage") || (depth0 != null ? lookupProperty(depth0,"nextPage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"nextPage","hash":{},"data":data,"loc":{"start":{"line":32,"column":40},"end":{"line":32,"column":54}}}) : helper)))
    + ")\" aria-label=\"Voir la page suivante\">\n          <svg>\n            <use href=\"/static/icons/endi.svg#chevron-right\"></use>\n          </svg>\n        </a>\n      </li>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "    </ul>\n  </nav>\n";
},"16":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <option value=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"value") || (depth0 != null ? lookupProperty(depth0,"value") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data,"loc":{"start":{"line":50,"column":23},"end":{"line":50,"column":34}}}) : helper)))
    + "\" "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"selected") || (depth0 != null ? lookupProperty(depth0,"selected") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"selected","hash":{},"data":data,"loc":{"start":{"line":50,"column":36},"end":{"line":50,"column":52}}}) : helper))) != null ? stack1 : "")
    + ">"
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":50,"column":53},"end":{"line":50,"column":64}}}) : helper)))
    + "</option>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "<div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"pagerElements") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":2},"end":{"line":5,"column":14}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"previousPage") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":6},"end":{"line":15,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"pagerElements") : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":16,"column":6},"end":{"line":28,"column":15}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"nextPage") : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":29,"column":6},"end":{"line":38,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"pagerElements") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":39,"column":6},"end":{"line":42,"column":9}}})) != null ? stack1 : "")
    + "</div>\n<form>\n  <div class=\"form-group\">\n    <label for=\"items_per_page"
    + alias4(((helper = (helper = lookupProperty(helpers,"position") || (depth0 != null ? lookupProperty(depth0,"position") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data,"loc":{"start":{"line":46,"column":30},"end":{"line":46,"column":44}}}) : helper)))
    + "\" class=\"screen-reader-text\">Éléments affichés</label>\n    <select id=\"items_per_page"
    + alias4(((helper = (helper = lookupProperty(helpers,"position") || (depth0 != null ? lookupProperty(depth0,"position") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data,"loc":{"start":{"line":47,"column":30},"end":{"line":47,"column":44}}}) : helper)))
    + "\">\n      <optgroup label=\"Affichage\">\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"selectOptions") || (depth0 != null ? lookupProperty(depth0,"selectOptions") : depth0)) != null ? helper : alias2),(options={"name":"selectOptions","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":49,"column":8},"end":{"line":51,"column":28}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"selectOptions")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "      </optgroup>\n    </select>\n    <a class=\"btn\" data-action=\"change_items_per_page\" data-selectid=\"items_per_page"
    + alias4(((helper = (helper = lookupProperty(helpers,"position") || (depth0 != null ? lookupProperty(depth0,"position") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data,"loc":{"start":{"line":54,"column":84},"end":{"line":54,"column":98}}}) : helper)))
    + "\"\n      href=\"javascript:void(0);\" title=\"Valider l’affichage\" aria-label=\"Valider l’affichage\">Valider</a>\n  </div>\n</form>\n</div>";
},"useData":true});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"sale_product": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) var result = runtime(__webpack_require__);
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkenDI"] = self["webpackChunkenDI"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendor"], () => (__webpack_require__("./src/sale_product/sale_product.js")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=sale_product.js.map