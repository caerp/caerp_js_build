/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/common/views/PDFViewerPopupView.js":
/*!************************************************!*\
  !*** ./src/common/views/PDFViewerPopupView.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var common_views_PDFViewerView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! common/views/PDFViewerView */ "./src/common/views/PDFViewerView.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _BaseViewerPopupView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BaseViewerPopupView */ "./src/common/views/BaseViewerPopupView.js");



var PDFViewerPopupView = _BaseViewerPopupView__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  initialize: function initialize(options) {
    this.pdfUrl = options.pdfUrl;
    this.popupTitle = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.getOpt)(this, 'popupTitle');
    this.documentTitle = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.getOpt)(this, 'documentTitle');
  },
  showPreview: function showPreview() {
    var view = new common_views_PDFViewerView__WEBPACK_IMPORTED_MODULE_0__.default({
      fileUrl: this.pdfUrl,
      title: this.documentTitle
    });
    this.showChildView('pdfpreview', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PDFViewerPopupView);

/***/ }),

/***/ "./src/common/views/StatusView.js":
/*!****************************************!*\
  !*** ./src/common/views/StatusView.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/behaviors/ModalBehavior.js */ "./src/base/behaviors/ModalBehavior.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/* harmony import */ var task_views_files_FileCollectionView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! task/views/files/FileCollectionView.js */ "./src/task/views/files/FileCollectionView.js");
/* harmony import */ var task_views_files_FileRequirementView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! task/views/files/FileRequirementView.js */ "./src/task/views/files/FileRequirementView.js");
/* harmony import */ var common_views_PDFViewerView__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! common/views/PDFViewerView */ "./src/common/views/PDFViewerView.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_7__);










var template = __webpack_require__(/*! ./templates/StatusView.mustache */ "./src/common/views/templates/StatusView.mustache");

var StatusView = backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default().View.extend({
  template: template,
  ui: {
    'textarea': 'textarea',
    btn_cancel: '.cancel',
    submit: 'button[name=submit]',
    form: 'form',
    force_file_validation: "input[name=force_file_validation]"
  },
  regions: {
    files: ".files",
    preview: {
      el: ".preview",
      replaceElement: true
    }
  },
  behaviors: {
    modal: {
      behaviorClass: _base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default
    }
  },
  // Event remonté par la FileCollectionView
  childViewEvents: {
    'file:updated': 'refreshFileRequirements'
  },
  events: {
    'click @ui.force_file_validation': "onForceFileValidationClick",
    'click @ui.btn_cancel': 'destroy',
    'click @ui.submit': 'onSubmit'
  },
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    this.action = options.action;
    this.pdfUrl = window.location.pathname + ".pdf";
  },
  submitCallback: function submitCallback(result) {},
  onForceFileValidationClick: function onForceFileValidationClick(event) {
    var checked = this.ui.force_file_validation.is(':checked');
    this.ui.submit.attr("disabled", !checked);
    this.ui.submit.attr("aria-disabled", !checked);
  },
  onSubmit: function onSubmit(event) {
    event.preventDefault();
    var datas = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.serializeForm)(this.getUI('form'));
    datas['submit'] = this.action.get('status');
    var url = this.action.get('url');
    (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.showLoader)();
    this.serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.ajax_call)(url, datas, "POST");
    this.serverRequest.then(this.submitCallback.bind(this));
  },
  fileWarning: function fileWarning() {
    var validation_status = this.action.get('status');
    var has_warning;

    if (validation_status != "invalid") {
      var collection = this.facade.request('get:collection', "file_requirements");

      if (collection) {
        has_warning = !collection.validate();
      }
    } else {
      has_warning = false;
    }

    return has_warning;
  },
  dateWarning: function dateWarning() {
    var validation_status = this.action.get('status');
    var result = {};
    console.log("validation_status : %s", validation_status);

    if (validation_status == 'valid') {
      var model = this.getOption('model');
      var date = model.get('date');
      var today = new Date();

      if (date != (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.dateToIso)(today)) {
        result['ask_for_date'] = true;
        date = (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.parseDate)(date);
        result['date'] = date.toLocaleDateString();
        result['today'] = today.toLocaleDateString();
        result['has_error'] = true;
      }
    }

    return result;
  },
  templateContext: function templateContext() {
    var validation_status = this.action.get('status');
    var result = {
      ask_for_date: false,
      has_file_warning: this.fileWarning(),
      has_error: this.fileWarning()
    };

    underscore__WEBPACK_IMPORTED_MODULE_7___default().extend(result, this.dateWarning());

    underscore__WEBPACK_IMPORTED_MODULE_7___default().extend(result, this.action.attributes);

    if (validation_status == 'wait') {
      result['label'] = this.model.get('label') || "Demander la validation";
    } else {
      result['label'] = this.model.get('label') || "Enregistrer";
    }

    return result;
  },
  showPreview: function showPreview() {
    var view = new common_views_PDFViewerView__WEBPACK_IMPORTED_MODULE_6__.default({
      fileUrl: this.pdfUrl,
      title: "Prévisualisation"
    });
    this.showChildView('preview', view);
  },
  onRender: function onRender() {
    console.log("Rendering Status View");

    if (this.fileWarning()) {
      var collection = this.facade.request('get:collection', 'file_requirements');
      var view = new task_views_files_FileCollectionView_js__WEBPACK_IMPORTED_MODULE_4__.default({
        collection: collection,
        childView: task_views_files_FileRequirementView_js__WEBPACK_IMPORTED_MODULE_5__.default
      });
      this.showChildView('files', view);
    }

    this.showPreview();
  },
  // Recharge la collection de requirement sur les fichiers et refait
  // le rendu de la popup
  refreshFileRequirements: function refreshFileRequirements() {
    var _this = this;

    var collection = this.facade.request('get:collection', 'file_requirements');
    collection.fetch().then(function () {
      return _this.render();
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (StatusView);

/***/ }),

/***/ "./src/task/components/App.js":
/*!************************************!*\
  !*** ./src/task/components/App.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/* harmony import */ var _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../views/RootComponent.js */ "./src/task/views/RootComponent.js");
/* harmony import */ var _Controller_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Controller.js */ "./src/task/components/Controller.js");
/* harmony import */ var _Router_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Router.js */ "./src/task/components/Router.js");






var AppClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().Application.extend({
  region: '#js-main-area',
  onBeforeStart: function onBeforeStart(app, options) {
    console.log("AppClass.onBeforeStart");
    this.rootView = new _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_2__.default();
    this.controller = new _Controller_js__WEBPACK_IMPORTED_MODULE_3__.default({
      rootView: this.rootView
    });
    this.router = new _Router_js__WEBPACK_IMPORTED_MODULE_4__.default({
      controller: this.controller
    });
    console.log("AppClass.onBeforeStart finished");
  },
  onStart: function onStart(app, options) {
    this.showView(this.rootView);
    console.log("Starting the history");
    (0,tools_js__WEBPACK_IMPORTED_MODULE_1__.hideLoader)();
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.start();
  }
});
var App = new AppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (App);

/***/ }),

/***/ "./src/task/components/Controller.js":
/*!*******************************************!*\
  !*** ./src/task/components/Controller.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);


var Controller = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().Object.extend({
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.rootView = options['rootView'];
  },
  index: function index() {
    console.log("Base Controller index");
    this.rootView.render();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Controller);

/***/ }),

/***/ "./src/task/components/Facade.js":
/*!***************************************!*\
  !*** ./src/task/components/Facade.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/* harmony import */ var _base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/components/FacadeModelApiMixin.js */ "./src/base/components/FacadeModelApiMixin.js");
/* harmony import */ var _models_FileRequirementCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/FileRequirementCollection.js */ "./src/task/models/FileRequirementCollection.js");
/* harmony import */ var _models_CommonModel_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/CommonModel.js */ "./src/task/models/CommonModel.js");
/* harmony import */ var _models_GeneralModel_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/GeneralModel.js */ "./src/task/models/GeneralModel.js");
/* harmony import */ var _models_ExpenseHtModel_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/ExpenseHtModel.js */ "./src/task/models/ExpenseHtModel.js");
/* harmony import */ var _models_NotesModel_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../models/NotesModel.js */ "./src/task/models/NotesModel.js");
/* harmony import */ var _models_PaymentConditionsModel_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../models/PaymentConditionsModel.js */ "./src/task/models/PaymentConditionsModel.js");
/* harmony import */ var _models_TaskGroupCollection_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../models/TaskGroupCollection.js */ "./src/task/models/TaskGroupCollection.js");
/* harmony import */ var _models_DiscountCollection_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../models/DiscountCollection.js */ "./src/task/models/DiscountCollection.js");
/* harmony import */ var _models_PaymentLineCollection_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../models/PaymentLineCollection.js */ "./src/task/models/PaymentLineCollection.js");
/* harmony import */ var _models_TotalModel_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../models/TotalModel.js */ "./src/task/models/TotalModel.js");
/* harmony import */ var _models_RelatedEstimationCollection_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../models/RelatedEstimationCollection.js */ "./src/task/models/RelatedEstimationCollection.js");
/* harmony import */ var _models_PaymentOptionsModel_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../models/PaymentOptionsModel.js */ "./src/task/models/PaymentOptionsModel.js");
/* harmony import */ var _common_models_StatusLogEntryCollection__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../common/models/StatusLogEntryCollection */ "./src/common/models/StatusLogEntryCollection.js");
/* harmony import */ var _models_DisplayOptionsModel_js__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../models/DisplayOptionsModel.js */ "./src/task/models/DisplayOptionsModel.js");
/* harmony import */ var _models_AttachedFileCollection_js__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../models/AttachedFileCollection.js */ "./src/task/models/AttachedFileCollection.js");
/* harmony import */ var _models_price_study_PriceStudyModel_js__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../models/price_study/PriceStudyModel.js */ "./src/task/models/price_study/PriceStudyModel.js");
/* harmony import */ var _models_price_study_ChapterCollection_js__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../models/price_study/ChapterCollection.js */ "./src/task/models/price_study/ChapterCollection.js");
/* harmony import */ var _models_price_study_DiscountCollection__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../models/price_study/DiscountCollection */ "./src/task/models/price_study/DiscountCollection.js");
/* harmony import */ var _models_progress_invoicing_ChapterCollection_js__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../models/progress_invoicing/ChapterCollection.js */ "./src/task/models/progress_invoicing/ChapterCollection.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
























/**
 * Task Form Facade
 * 
 * Manage all the models and collections of the interface
 * - Document
 * - PaymentLines
 * - TaskLineGroups or ProgressInvoicingLines or PriceStudy data
 * - Discounts
 * - FileRequirements
 * - Attached Files
 * 
 * 
 * Process the setup of models collections and load the data
 * 
 * 1- setup is called to configure the urls ...
 * 2- start
 *      - onBeforeStart -> Initialize the models/collections and set the urls
 *      - query the Data
 *      - onAfterStart : apply the loaded data to the different Task models
 */

var FacadeClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_23___default().Object.extend(_base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_3__.default).extend({
  ht: 5,
  radioEvents: {
    'changed:task': 'computeTotals',
    'changed:discount': 'computeTotals',
    'changed:expense_ht': 'computeTotals',
    'changed:date': "onDateChanged"
  },
  radioRequests: {
    'get:model': 'getModelRequest',
    'get:collection': 'getCollectionRequest',
    'is:valid': "isDataValid",
    'load:model': 'loadModel',
    'load:collection': 'loadCollection',
    'save:model': 'saveModel',
    'save:all': 'saveAll'
  },
  initialize: function initialize() {
    this.syncModel = this.syncModel.bind(this);
    this.models = {};
    this.collections = {};
  },
  setup: function setup(options) {
    /*
     * Lancé avant tout appel ajax, options : les options passées depuis la
     * page html
     */
    console.log("Facade.setup");
    this.app_options = _.clone(options);
  },

  /**
   * Run on application startup, initialize all models that will be fetched before Building the ui
   */
  onBeforeStart: function onBeforeStart(options) {
    console.log("Facade.onBeforeStart");

    if ('task_line_group_api_url' in options) {
      this.collections['task_groups'] = new _models_TaskGroupCollection_js__WEBPACK_IMPORTED_MODULE_10__.default();
      this.collections.task_groups.url = options['task_line_group_api_url'];

      if ('discount_api_url' in options) {
        this.collections['discounts'] = new _models_DiscountCollection_js__WEBPACK_IMPORTED_MODULE_11__.default();
        this.collections['discounts'].url = options['discount_api_url'];
      }
    } else if ("price_study_url" in options) {
      this.models['price_study'] = new _models_price_study_PriceStudyModel_js__WEBPACK_IMPORTED_MODULE_19__.default();
      this.models['price_study'].url = options["price_study_url"];
      this.collections['price_study_chapters'] = new _models_price_study_ChapterCollection_js__WEBPACK_IMPORTED_MODULE_20__.default();
      this.collections['price_study_chapters'].url = options['price_study_chapters_url'];
    } else if ("progress_invoicing_chapters_url" in options) {
      this.collections['progress_invoicing_chapters'] = new _models_progress_invoicing_ChapterCollection_js__WEBPACK_IMPORTED_MODULE_22__.default();
      this.collections['progress_invoicing_chapters'].url = options['progress_invoicing_chapters_url'];
    } // On peut avoir les deux du coup on s'assure d'utiliser celle des études si étude il y a


    if ('price_study_discount_api_url' in options) {
      this.collections['discounts'] = new _models_price_study_DiscountCollection__WEBPACK_IMPORTED_MODULE_21__.default();
      this.collections['discounts'].url = options['price_study_discount_api_url'];
    } else if ('discount_api_url' in options) {
      this.collections['discounts'] = new _models_DiscountCollection_js__WEBPACK_IMPORTED_MODULE_11__.default();
      this.collections['discounts'].url = options['discount_api_url'];
    }

    if ("payment_lines_api_url" in options) {
      this.collections['payment_lines'] = new _models_PaymentLineCollection_js__WEBPACK_IMPORTED_MODULE_12__.default();
      this.collections['payment_lines'].url = options["payment_lines_api_url"];
    }

    if ("file_requirement_url" in options) {
      this.collections['file_requirements'] = new _models_FileRequirementCollection_js__WEBPACK_IMPORTED_MODULE_4__.default();
      this.collections['file_requirements'].url = options['file_requirement_url'];
    }

    if ("file_attachment_url" in options) {
      this.collections['attached_files'] = new _models_AttachedFileCollection_js__WEBPACK_IMPORTED_MODULE_18__.default();
      this.collections['attached_files'].url = options['file_attachment_url'];
    }

    if ('related_estimation_url' in options) {
      this.collections['related_estimations'] = new _models_RelatedEstimationCollection_js__WEBPACK_IMPORTED_MODULE_14__.default();
      this.collections['related_estimations'].url = options['related_estimation_url'];
    }

    this.models['total'] = new _models_TotalModel_js__WEBPACK_IMPORTED_MODULE_13__.default();
    this.models['total'].url = options['total_url'];
  },
  start: function start() {
    var _$;

    console.log("Starting the facade");
    this.onBeforeStart(this.app_options);
    console.log("Loading document data");
    var load_document_request = (0,tools_js__WEBPACK_IMPORTED_MODULE_2__.ajax_call)(this.app_options['context_url']);
    var requests = [load_document_request];

    for (var name in this.models) {
      console.log("Loading model %s", name);
      requests.push(this.loadModel(name));
    }

    for (var _name in this.collections) {
      console.log("Loading collection %s", _name);
      requests.push(this.loadCollection(_name));
    }

    return (_$ = $).when.apply(_$, requests).then(this.onAfterStart.bind(this));
  },
  _setupPaymentLinesEvents: function _setupPaymentLinesEvents() {
    var _this = this;

    var collection = this.collections['payment_lines']; // On update la collection lors de ces évènements

    this.listenTo(collection, 'saved', function () {
      return collection.fetch();
    });
    this.listenTo(this.models['total'], 'change:ttc', function () {
      _this.models['payment_options'].fetch();

      collection.fetch();
    });
    this.listenTo(this.models['payment_options'], 'saved:deposit', function () {
      return collection.fetch();
    });
    this.listenTo(this.models['payment_options'], 'saved:payment_times', function () {
      return collection.fetch();
    });
  },

  /**
   * Setup task models 
   * 
   * Models that are populated from the main context api call and are then autonomous to edit part of it
   */
  _setupTaskModels: function _setupTaskModels() {
    // Task models (submodels handling only part of the attributes,     
    // associated to different view components)
    this.models['general'] = new _models_GeneralModel_js__WEBPACK_IMPORTED_MODULE_6__.default();
    this.models['general'].url = this.app_options['context_url'];
    this.models['common'] = new _models_CommonModel_js__WEBPACK_IMPORTED_MODULE_5__.default();
    this.models['common'].url = this.app_options['context_url'];
    this.models['display_options'] = new _models_DisplayOptionsModel_js__WEBPACK_IMPORTED_MODULE_17__.default();
    this.models['display_options'].url = this.app_options['context_url'];
    this.models['expense_ht'] = new _models_ExpenseHtModel_js__WEBPACK_IMPORTED_MODULE_7__.default();
    this.models['expense_ht'].url = this.app_options['context_url'];
    this.models['notes'] = new _models_NotesModel_js__WEBPACK_IMPORTED_MODULE_8__.default();
    this.models['notes'].url = this.app_options['context_url']; // #Todo : ne pas instancier les modèles ci-dessous si ils ne sont pas utilisés

    this.models['payment_conditions'] = new _models_PaymentConditionsModel_js__WEBPACK_IMPORTED_MODULE_9__.default();
    this.models['payment_conditions'].url = this.app_options['context_url']; // Estimation only model

    this.models['payment_options'] = new _models_PaymentOptionsModel_js__WEBPACK_IMPORTED_MODULE_15__.default();
    this.models['payment_options'].url = this.app_options['context_url'];
  },
  onAfterStart: function onAfterStart(loaded_datas) {
    var _this2 = this;

    this._setupTaskModels();

    var context_datas = loaded_datas[0];
    console.log("setup Context Models");
    console.log(context_datas);
    ['general', 'common', 'display_options', 'expense_ht', 'notes', 'payment_conditions', 'payment_options'].map(function (modelName) {
      _this2.models[modelName].set(context_datas);

      _this2.models[modelName].changed = {};
    });

    if ('payment_lines' in this.collections) {
      this._setupPaymentLinesEvents();
    }

    if (_.has(context_datas, 'status_history')) {
      var history = context_datas['status_history'];
      this.collections['status_history'] = new _common_models_StatusLogEntryCollection__WEBPACK_IMPORTED_MODULE_16__.default(history);
    }

    this.computeTotals();
    this.listenTo(this.models.display_options, 'saved:input_mode', function () {
      return window.location.reload();
    });
  },
  syncModel: function syncModel(modelName) {
    var modelName = modelName || 'common';
    return this.models[modelName].save(null, {
      wait: true,
      sync: true,
      patch: true
    });
  },
  computeTotals: function computeTotals() {
    console.log("Computing totals");
    this.models.total.fetch({
      async: false
    });
    console.log("Total retrieved"); // const new_values = {
    //     'ht_before_discounts': this.HTBeforeDiscounts(),
    //     'ttc_before_discounts': this.TTCBeforeDiscounts(),
    //     'ht': this.HT(),
    //     'tvas': this.TVAParts(),
    //     'ttc': this.TTC(),
    // }
  },
  tasklines_ht: function tasklines_ht() {
    return this.collections['task_groups'].ht();
  },
  getComputeCollections: function getComputeCollections() {
    var result = [];

    for (var name in this.collections) {
      if (this.collections[name].ht) {
        result.push(this.collections[name]);
      }
    }

    return result;
  },
  getComputeModels: function getComputeModels() {
    return [this.models['expense_ht']];
  },
  HTBeforeDiscounts: function HTBeforeDiscounts() {
    var result = this.HT();

    if ('discounts' in this.collections) {
      result = result - this.collections.discounts.ht();
    }

    return result;
  },
  TTCBeforeDiscounts: function TTCBeforeDiscounts() {
    var result = this.TTC();

    if ('discount' in this.collections) {
      result = result - this.collections.discounts.ttc();
    }

    return result;
  },
  HT: function HT() {
    var result = 0;

    _.each(this.getComputeCollections(), function (collection) {
      result += collection.ht();
    });

    _.each(this.getComputeModels(), function (model) {
      result += model.ht();
    });

    console.log("Computing HT : %s", result);
    return result;
  },
  TVAParts: function TVAParts() {
    var result = {};

    _.each(this.getComputeCollections(), function (collection) {
      var tva_parts = collection.tvaParts();

      _.each(tva_parts, function (value, key) {
        if (key in result) {
          value += result[key];
        }

        result[key] = value;
      });
    });

    _.each(this.getComputeModels(), function (model) {
      var tva_parts = model.tvaParts();

      _.each(tva_parts, function (value, key) {
        if (key in result) {
          value += result[key];
        }

        result[key] = value;
      });
    });

    return result;
  },
  TTC: function TTC() {
    var result = (0,math_js__WEBPACK_IMPORTED_MODULE_1__.round)(this.HT());

    _.each(this.TVAParts(), function (value) {
      result += (0,math_js__WEBPACK_IMPORTED_MODULE_1__.round)(value);
    });

    return result;
  },
  insurance: function insurance(totalHt) {
    var rate = this.models.common.getInsuranceRate();
    var result = null;

    if (rate) {
      result = (0,math_js__WEBPACK_IMPORTED_MODULE_1__.getPercent)(totalHt, rate);
    }

    return result;
  },
  onDateChanged: function onDateChanged() {
    this.models.general.fetch();
  }
});
var Facade = new FacadeClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Facade);

/***/ }),

/***/ "./src/task/components/Router.js":
/*!***************************************!*\
  !*** ./src/task/components/Router.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var marionette_approuter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! marionette.approuter */ "./node_modules/marionette.approuter/lib/marionette.approuter.esm.js");


var Router = marionette_approuter__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  appRoutes: {
    '': 'index'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Router);

/***/ }),

/***/ "./src/task/components/ToolbarApp.js":
/*!*******************************************!*\
  !*** ./src/task/components/ToolbarApp.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var common_components_ValidationLimitToolbarAppClass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! common/components/ValidationLimitToolbarAppClass */ "./src/common/components/ValidationLimitToolbarAppClass.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _views_resume_SmallResumeView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../views/resume/SmallResumeView */ "./src/task/views/resume/SmallResumeView.js");
/* harmony import */ var tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tools */ "./src/tools.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");




var ToolbarAppClass = common_components_ValidationLimitToolbarAppClass__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  callSaveAll: function callSaveAll(event) {
    return new Promise(function (resolve) {
      var btn = $(event.currentTarget);
      (0,tools__WEBPACK_IMPORTED_MODULE_3__.showBtnLoader)(btn);
      backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade').request("save:all").then(function () {
        setTimeout(function () {
          return (0,tools__WEBPACK_IMPORTED_MODULE_3__.hideBtnLoader)(btn);
        }, 200);
        resolve();
      });
    });
  },
  callPreview: function callPreview(event) {
    this.callSaveAll(event).then(function () {
      backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade').trigger("show:preview");
    });
  },
  getSaveButton: function getSaveButton() {
    var _this = this;

    return {
      widget: 'button',
      option: {
        onclick: function onclick(event) {
          return _this.callSaveAll(event);
        },
        title: "Sauvegarder",
        css: "btn icon only",
        icon: "save"
      }
    };
  },
  getPreviewButton: function getPreviewButton() {
    var _this2 = this;

    return {
      widget: 'button',
      option: {
        onclick: function onclick(event) {
          return _this2.callPreview(event);
        },
        title: "Prévisualiser votre document",
        css: "btn icon only",
        icon: "eye"
      }
    };
  },
  getMoreActions: function getMoreActions(actions) {
    var result = actions['more'].slice();
    result.unshift(this.getSaveButton());
    result.splice(2, 0, this.getPreviewButton());
    console.log("More");
    console.log(result);
    return result;
  },
  getResumeView: function getResumeView(actions) {
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    var model = facade.request('get:model', 'total');
    return new _views_resume_SmallResumeView__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model
    });
  }
});
var ToolbarApp = new ToolbarAppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ToolbarApp);

/***/ }),

/***/ "./src/task/components/UserPreferences.js":
/*!************************************************!*\
  !*** ./src/task/components/UserPreferences.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base_components_ConfigBus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/components/ConfigBus */ "./src/base/components/ConfigBus.js");
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math */ "./src/math.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");



var UserPreferencesClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().Object.extend({
  channelName: 'user_preferences',
  radioRequests: {
    'formatAmount': 'formatAmount'
  },
  setFormConfig: function setFormConfig(form_config) {
    this.decimal_to_display = form_config.options.decimal_to_display;
    console.log(this.decimal_to_display);
  },
  formatAmount: function formatAmount(amount, rounded, strip) {
    var precision;

    if (!rounded) {
      precision = this.decimal_to_display;
    }

    return (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(amount, true, strip, precision);
  },
  setup: function setup(form_config_url) {
    console.log("UserPreferencesClass.setup");
  },
  start: function start() {
    console.log("UserPreferencesClass.start");
    this.setFormConfig(_base_components_ConfigBus__WEBPACK_IMPORTED_MODULE_0__.default.form_config);
    var result = $.Deferred();
    result.resolve(_base_components_ConfigBus__WEBPACK_IMPORTED_MODULE_0__.default.form_config, null, null);
    return result;
  }
});
var UserPreferences = new UserPreferencesClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (UserPreferences);

/***/ }),

/***/ "./src/task/models/AttachedFileCollection.js":
/*!***************************************************!*\
  !*** ./src/task/models/AttachedFileCollection.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _common_models_NodeFileModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/models/NodeFileModel */ "./src/common/models/NodeFileModel.js");


var AttachedFileCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  model: _common_models_NodeFileModel__WEBPACK_IMPORTED_MODULE_1__.default,

  /**
   * Custom parse method
   * Traite les données renvoyées par le serveur pour construire la collection
   */
  parse: function parse(xhrResponse) {
    if ('attachments' in xhrResponse) {
      return xhrResponse['attachments'];
    } else {
      return xhrResponse;
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AttachedFileCollection);

/***/ }),

/***/ "./src/task/models/CommonModel.js":
/*!****************************************!*\
  !*** ./src/task/models/CommonModel.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
var _validation;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function getCustomFieldValidation(field_name) {
  var validator = function validator(value) {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
    var field_def = config.request('get:section', "common:" + field_name);

    if (field_def && field_def.required) {
      if (!value) {
        return "Veuillez saisir une valeur (" + field_def.title + ")";
      }
    }
  };

  return validator;
}

var CommonModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'name', 'altdate', 'date', 'description', 'address', 'mentions', 'workplace', 'start_date', 'end_date', 'first_visit', 'validity_duration', 'insurance_id'],
  validation: (_validation = {
    date: {
      required: true,
      msg: "Veuillez saisir une date"
    },
    description: {
      required: true,
      msg: "Veuillez saisir un objet"
    },
    address: {
      required: true,
      msg: "Veuillez saisir une adresse"
    },
    start_date: getCustomFieldValidation("start_date"),
    end_date: getCustomFieldValidation("end_date")
  }, _defineProperty(_validation, "start_date", getCustomFieldValidation("start_date")), _defineProperty(_validation, "first_visit", getCustomFieldValidation("first_visit")), _defineProperty(_validation, "validity_duration", getCustomFieldValidation("validity_duration")), _defineProperty(_validation, "insurance_id", getCustomFieldValidation("insurance_id")), _defineProperty(_validation, "workplace", getCustomFieldValidation('workplace')), _validation),
  initialize: function initialize(options) {
    this.on('saved', this.onDateChanged);
  },
  onDateChanged: function onDateChanged(attributes) {
    if (underscore__WEBPACK_IMPORTED_MODULE_0___default().isObject(attributes)) {
      var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');

      if (attributes.hasOwnProperty('date')) {
        channel.trigger('changed:date');
      }

      if (attributes.hasOwnProperty('insurance_id')) {
        channel.trigger('changed:task');
      }
    }
  },
  getInsuranceRate: function getInsuranceRate() {
    var insurance_id = this.get('insurance_id');
    var result = null;

    if (insurance_id) {
      var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
      var insurance_options = channel.request('get:options', 'insurance_options');
      var insurance_option = insurance_options.find(function (x) {
        return x.id === parseInt(insurance_id);
      });

      if (insurance_option) {
        result = insurance_option.rate;
      }
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CommonModel);

/***/ }),

/***/ "./src/task/models/DiscountCollection.js":
/*!***********************************************!*\
  !*** ./src/task/models/DiscountCollection.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountModel.js */ "./src/task/models/DiscountModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");




var DiscountCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  model: _DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize(options) {
    this.on('saved', this.channelCall);
    this.on('destroyed', this.channelCall);
  },
  channelCall: function channelCall() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    channel.trigger('changed:discount');
  },
  ht: function ht() {
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  tvaParts: function tvaParts() {
    var result = {};
    this.each(function (model) {
      var tva_amount = model.tva();
      var tva = model.get('tva');

      if (tva in result) {
        tva_amount += result[tva];
      }

      result[tva] = tva_amount;
    });
    return result;
  },
  ttc: function ttc() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  afterInsertPercent: function afterInsertPercent() {
    var this_ = this;
    this.fetch().then(function () {
      this_.trigger('saved');
    });
  },
  insert_percent: function insert_percent(model) {
    /*
     * Call the server to generate percent based Discounts
     * :param obj model: A DiscountPercentModel instance
     */
    var serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.url + '?action=insert_percent', model.toJSON(), 'POST');
    serverRequest.then(this.afterInsertPercent.bind(this));
  },
  validate: function validate() {
    var result = {};
    this.each(function (model) {
      var res = model.validate();

      if (res) {
        _.extend(result, res);
      }
    });
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountCollection);

/***/ }),

/***/ "./src/task/models/DiscountModel.js":
/*!******************************************!*\
  !*** ./src/task/models/DiscountModel.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");



var DiscountModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  props: ['id', 'amount', 'tva', 'ht', 'description', 'mode'],
  validation: {
    description: {
      required: true,
      msg: "Remise : Veuillez saisir un objet"
    },
    amount: {
      required: true,
      pattern: "amount",
      msg: "Remise : Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
    },
    tva: {
      required: true,
      pattern: "number",
      msg: "Remise : Veuillez sélectionner une TVA"
    }
  },

  /**
   * 
   * @returns default tva and mode retrieved from config and user session
   */
  defaults: function defaults() {
    console.log("Defaults");
    var result = {};
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    result['mode'] = config.request('get:options', 'compute_mode');
    var defaults = config.request('get:options', 'defaults');
    var default_tva = defaults['tva'];
    var user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('session');
    result['tva'] = user_session.request('get', 'tva', default_tva);
    return result;
  },
  initialize: function initialize() {
    this.user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('session');
    this.on('saved', this.onSaved, this);
    this.user_preferences = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
  },
  onSaved: function onSaved() {
    this.user_session.request('set', 'tva', this.get('tva'));
    this.user_session.request('set', 'product_id', this.get('product_id'));
  },
  ht: function ht() {
    if (this.get('mode') == 'ht') {
      return -1 * (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.strToFloat)(this.get('amount'));
    } else {
      return (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.htFromTtc)(this.ttc(), this.tva_value());
    }
  },
  tva_value: function tva_value() {
    var tva = this.get('tva');

    if (tva < 0) {
      tva = 0;
    }

    return tva;
  },
  tva: function tva() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.getTvaPart)(this.ht(), this.tva_value());
  },
  ttc: function ttc() {
    if (this.get('mode') == 'ht') {
      return this.ht() + this.tva();
    } else {
      return -1 * (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.strToFloat)(this.get('amount'));
    }
  },
  amount_label: function amount_label() {
    var result;

    if (this.get('mode') === 'ht') {
      result = this.user_preferences.request('formatAmount', this.ht(), false);
    } else {
      result = this.user_preferences.request('formatAmount', this.ttc(), false);
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountModel);

/***/ }),

/***/ "./src/task/models/DiscountPercentModel.js":
/*!*************************************************!*\
  !*** ./src/task/models/DiscountPercentModel.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);

var DiscountPercentModel = backbone__WEBPACK_IMPORTED_MODULE_0___default().Model.extend({
  validation: {
    description: {
      required: true,
      msg: "Veuillez saisir un objet"
    },
    percentage: {
      required: true,
      range: [1, 99],
      msg: "Veuillez saisir un pourcentage"
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountPercentModel);

/***/ }),

/***/ "./src/task/models/DisplayOptionsModel.js":
/*!************************************************!*\
  !*** ./src/task/models/DisplayOptionsModel.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/BaseModel */ "./src/base/models/BaseModel.js");

var DisplayOptionsModel = _base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  props: ['id', 'display_units', 'display_ttc', 'input_mode' // Mode de saisie du formulaire (étude de prix ou libre)
  ]
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DisplayOptionsModel);

/***/ }),

/***/ "./src/task/models/ExpenseHtModel.js":
/*!*******************************************!*\
  !*** ./src/task/models/ExpenseHtModel.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);




var ExpenseHtModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'expenses_ht'],
  validation: {
    expenses_ht: {
      required: false,
      pattern: 'amount',
      msg: "Le montant doit être un nombre"
    }
  },
  initialize: function initialize() {
    ExpenseHtModel.__super__.initialize.apply(this, arguments);

    var channel = this.channel = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('facade');
    this.on('saved', function () {
      channel.trigger('changed:discount');
    });
  },
  ht: function ht() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(this.get('expenses_ht'));
  },
  tva_key: function tva_key() {
    var config_channel = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('config');
    this.tva_options = config_channel.request('get:options', 'tvas');
    var result;

    var tva_object = underscore__WEBPACK_IMPORTED_MODULE_0___default().find(this.tva_options, function (val) {
      return val['default'];
    });

    if (underscore__WEBPACK_IMPORTED_MODULE_0___default().isUndefined(tva_object)) {
      result = 0;
    } else {
      result = (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(tva_object.value);
    }

    if (result < 0) {
      result = 0;
    }

    return result;
  },
  tva_amount: function tva_amount() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.getTvaPart)(this.ht(), this.tva_key());
  },
  tvaParts: function tvaParts() {
    var result = {};
    var tva_key = this.tva_key();
    var tva_amount = this.tva_amount();

    if (tva_amount == 0) {
      return result;
    }

    result[tva_key] = tva_amount;
    return result;
  },
  ttc: function ttc() {
    return this.ht() + this.tva_amount();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseHtModel);

/***/ }),

/***/ "./src/task/models/FileRequirementCollection.js":
/*!******************************************************!*\
  !*** ./src/task/models/FileRequirementCollection.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _FileRequirementModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FileRequirementModel.js */ "./src/task/models/FileRequirementModel.js");


var FileRequirementCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  model: _FileRequirementModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  url: function url() {
    return AppOption['context_url'] + '/' + 'file_requirements';
  },
  validate: function validate(status) {
    var result = true;
    this.each(function (model) {
      var res = model.validate(status);

      if (!res) {
        result = false;
      }
    });
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileRequirementCollection);

/***/ }),

/***/ "./src/task/models/FileRequirementModel.js":
/*!*************************************************!*\
  !*** ./src/task/models/FileRequirementModel.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/* harmony import */ var _common_models_NodeFileModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/models/NodeFileModel */ "./src/common/models/NodeFileModel.js");



var FileRequirementModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  label: function label() {
    var status = this.get('status');
    var requirement_type = this.get('requirement_type');
    var file_id = this.get('file_id');
    var validation_status = this.get('validation_status');
    var forced = this.get('forced');
    var file_type = this.get('file_type');
    var label = file_type.label;

    if (status == 'danger') {
      label += ": <b>Aucun fichier n'a été fourni</b>";
    } else if (status == 'warning') {
      if (requirement_type == 'recommended') {
        label += " (recommandé)";
      } else if (validation_status != 'valid') {
        label += ": <b>Fichier en attente de validation";
      }
    } else if (forced) {
      label += "La validation a été forcée";
    }

    return label;
  },
  missingFile: function missingFile() {
    var status = this.get('status');
    return status != 'success' && !this.has('file_id');
  },
  hasFile: function hasFile() {
    return this.has('file_id');
  },
  get: function get(attribute) {
    var v = FileRequirementModel.__super__.get.apply(this, arguments); // BB does not handle nested models natively ; thus we rehydrate it by hand.


    if (attribute === 'file_object') {
      v = new _common_models_NodeFileModel__WEBPACK_IMPORTED_MODULE_2__.default(v);
    }

    return v;
  },
  setValid: function setValid() {
    var serverRequest = (0,tools_js__WEBPACK_IMPORTED_MODULE_1__.ajax_call)(this.url() + '?action=validation_status', {
      "validation_status": "valid"
    }, "POST");
    return serverRequest.then(this.fetch.bind(this));
  },
  error: function error(message) {
    return {
      'file_requirements': message
    };
  },
  validate: function validate(validation_status) {
    var result = true;

    if (this.get('status') != 'success') {
      if (this.missingFile()) {
        result = false;
      } else if (validation_status == 'valid') {
        result = false;
      }
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileRequirementModel);

/***/ }),

/***/ "./src/task/models/GeneralModel.js":
/*!*****************************************!*\
  !*** ./src/task/models/GeneralModel.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);



var GeneralModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'name', 'business_type_id'],
  validation: {
    name: {
      required: true,
      msg: "Veuillez saisir un nom"
    }
  },
  initialize: function initialize() {
    GeneralModel.__super__.initialize.apply(this, arguments);

    this.config_channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
  },
  getBusinessType: function getBusinessType() {
    this.business_type_options = this.config_channel.request('get:options', 'business_types');

    var business_type = underscore__WEBPACK_IMPORTED_MODULE_0___default().findWhere(this.business_type_options, {
      value: this.get("business_type_id")
    });

    return business_type;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (GeneralModel);

/***/ }),

/***/ "./src/task/models/NotesModel.js":
/*!***************************************!*\
  !*** ./src/task/models/NotesModel.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);



var NotesModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'notes']
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NotesModel);

/***/ }),

/***/ "./src/task/models/PaymentConditionsModel.js":
/*!***************************************************!*\
  !*** ./src/task/models/PaymentConditionsModel.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);



var PaymentConditionsModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'payment_conditions'],
  validation: {
    payment_conditions: function payment_conditions(value) {
      var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');

      if (channel.request('has:form_section', 'payment_conditions')) {
        if (!value) {
          return "Veuillez saisir des conditions de paiements";
        }
      }
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentConditionsModel);

/***/ }),

/***/ "./src/task/models/PaymentLineCollection.js":
/*!**************************************************!*\
  !*** ./src/task/models/PaymentLineCollection.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _PaymentLineModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PaymentLineModel.js */ "./src/task/models/PaymentLineModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");


var PaymentLineCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _PaymentLineModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  url: function url() {
    return AppOption['context_url'] + '/' + 'payment_lines';
  },
  validate: function validate() {
    var result = {};
    this.each(function (model) {
      var res = model.validate();

      if (res) {
        _.extend(result, res);
      }
    });
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentLineCollection);

/***/ }),

/***/ "./src/task/models/PaymentLineModel.js":
/*!*********************************************!*\
  !*** ./src/task/models/PaymentLineModel.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");





var PaymentLineModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'task_id', 'order', 'description', 'amount', 'date'],
  defaults: {
    date: (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.dateToIso)(new Date()),
    order: 1
  },
  validation: {
    description: {
      required: true,
      msg: "Veuillez saisir un objet"
    },
    amount: function amount(value) {
      var total = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade').request('get:model', 'total').get('ttc');

      if (Math.abs(value) > Math.abs(total)) {
        return "Échéances de paiements : veuillez saisir un montant inférieur ou égal au total TTC";
      } else if (total < 0 && !/^-?(\d+(?:[\.\,]\d{1,2})?)$/.test(value)) {
        return "Échéances de paiements : veuillez saisir un montant valide avec au maximum 2 chiffres après la virgule";
      } else if (total >= 0 && !/^(\d+(?:[\.\,]\d{1,2})?)$/.test(value)) {
        return "Échéances de paiements : veuillez saisir un montant positif avec au maximum 2 chiffres après la virgule";
      }
    }
  },
  isLast: function isLast() {
    var order = this.get('order');
    var max_order = this.collection.getMaxOrder();
    return order == max_order;
  },
  isFirst: function isFirst() {
    var min_order = this.model.collection.getMinOrder();
    var order = this.get('order');
    return order == min_order;
  },
  getAmount: function getAmount() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.strToFloat)(this.get('amount'));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentLineModel);

/***/ }),

/***/ "./src/task/models/PaymentOptionsModel.js":
/*!************************************************!*\
  !*** ./src/task/models/PaymentOptionsModel.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/BaseModel */ "./src/base/models/BaseModel.js");
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math */ "./src/math.js");


var PaymentOptionsModel = _base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  props: ['id', "deposit", 'paymentDisplay', 'payment_times', 'deposit_amount_ttc'],
  isAmountEditable: function isAmountEditable() {
    return (0,_math__WEBPACK_IMPORTED_MODULE_1__.strToFloat)(this.get('payment_times')) === -1;
  },
  isDateEditable: function isDateEditable() {
    return this.get('paymentDisplay') !== 'ALL_NO_DATE';
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentOptionsModel);

/***/ }),

/***/ "./src/task/models/RelatedEstimationCollection.js":
/*!********************************************************!*\
  !*** ./src/task/models/RelatedEstimationCollection.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _RelatedEstimationModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RelatedEstimationModel */ "./src/task/models/RelatedEstimationModel.js");


var RelatedEstimationCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  model: _RelatedEstimationModel__WEBPACK_IMPORTED_MODULE_1__.default
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RelatedEstimationCollection);

/***/ }),

/***/ "./src/task/models/RelatedEstimationModel.js":
/*!***************************************************!*\
  !*** ./src/task/models/RelatedEstimationModel.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);


var RelatedEstimationModel = base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Marque ce modèle comme local pour qu'il ne soit pas sauvegardé automatiquement
  isLocalModel: true,
  props: ['id', 'label', 'url']
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RelatedEstimationModel);

/***/ }),

/***/ "./src/task/models/TaskGroupCollection.js":
/*!************************************************!*\
  !*** ./src/task/models/TaskGroupCollection.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _TaskGroupModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TaskGroupModel.js */ "./src/task/models/TaskGroupModel.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");




var TaskGroupCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _TaskGroupModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize(options) {
    TaskGroupCollection.__super__.initialize.apply(this, options);

    this.on('saved', this.channelCall);
    this.on('destroyed', this.channelCall);
  },
  channelCall: function channelCall() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('facade');
    channel.trigger('changed:task');
  },
  afterLoadFromCatalog: function afterLoadFromCatalog() {
    var this_ = this;
    this.fetch().then(function () {
      this_.trigger('saved');
    });
  },
  load_from_catalog: function load_from_catalog(sale_product_group_ids) {
    var serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_2__.ajax_call)(this.url + '?action=load_from_catalog', {
      sale_product_group_ids: sale_product_group_ids
    }, 'POST');
    serverRequest.then(this.afterLoadFromCatalog.bind(this));
  },
  ht: function ht() {
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  tvaParts: function tvaParts() {
    var result = {};
    this.each(function (model) {
      var tva_parts = model.tvaParts();

      _.each(tva_parts, function (value, key) {
        if (key in result) {
          value += result[key];
        }

        result[key] = value;
      });
    });
    return result;
  },
  ttc: function ttc() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  validate: function validate() {
    var result = this.constructor.__super__.validate.call(this);

    if (this.models.length === 0) {
      result['groups'] = "Veuillez ajouter au moins un chapitre";
      this.trigger('validated:invalid', this, {
        groups: "Veuillez ajouter au moins un chapitre"
      });
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskGroupCollection);

/***/ }),

/***/ "./src/task/models/TaskGroupModel.js":
/*!*******************************************!*\
  !*** ./src/task/models/TaskGroupModel.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TaskLineCollection_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TaskLineCollection.js */ "./src/task/models/TaskLineCollection.js");
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);




var TaskGroupModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  props: ['id', 'order', 'title', 'description', 'lines', 'task_id', 'display_details', 'total_ht', 'total_ttc'],
  defaults: function defaults() {
    return {
      'display_details': true
    };
  },
  validation: {
    lines: function lines(value) {
      if (value.length === 0) {
        return "Chapitre \xAB ".concat(this.get('title'), " \xBB : Veuillez saisir au moins un produit");
      }
      /* Doing line validation here, because cannot find a way to
       * call TaskLineModel.validate() cleanly :/.
       */


      var dateRequired = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('config').request('has:form_section', 'composition:classic:lines:date');

      if (dateRequired) {
        console.log('validating TaskLine.date');

        for (var i = 0; i < value.length; i++) {
          if (value[i].date == null) {
            return "Tous les produits doivent mentionner une date d'exécution";
          }
        }
      }
    }
  },
  initialize: function initialize() {
    var _this = this;

    this.populate();
    this.on('change:id', function () {
      return _this.populate();
    });
    this.listenTo(this.lines, 'saved', this.updateLines);
    this.listenTo(this.lines, 'destroyed', this.updateLines);
    this.listenTo(this.lines, 'synced', this.updateLines);
  },
  populate: function populate() {
    if (this.get('id')) {
      this.lines = new _TaskLineCollection_js__WEBPACK_IMPORTED_MODULE_1__.default(this.get('lines'));
      this.lines.url = this.url() + '/task_lines';
    }
  },
  updateLines: function updateLines() {
    this.set('lines', this.lines.toJSON());
  },
  ht: function ht() {
    return this.get('total_ht');
  },
  tvaParts: function tvaParts() {
    return this.lines.tvaParts();
  },
  ttc: function ttc() {
    return this.get('total_ttc');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskGroupModel);

/***/ }),

/***/ "./src/task/models/TaskLineCollection.js":
/*!***********************************************!*\
  !*** ./src/task/models/TaskLineCollection.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _TaskLineModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TaskLineModel.js */ "./src/task/models/TaskLineModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");





var TaskLineCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_4__.default.extend({
  model: _TaskLineModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize(options) {
    TaskLineCollection.__super__.initialize.apply(this, options);

    this.on('saved', this.channelCall);
    this.on('destroyed', this.channelCall);
  },
  channelCall: function channelCall() {
    console.log("Calling changed:task");
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    channel.trigger('changed:task');
    this.trigger('synced');
  },
  load_from_catalog: function load_from_catalog(sale_product_ids) {
    var _this = this;

    var serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.url + '?action=load_from_catalog', {
      sale_product_ids: sale_product_ids
    }, 'POST');
    serverRequest.then(function () {
      return _this.fetch();
    }).then(function () {
      return _this.channelCall();
    });
  },
  ht: function ht() {
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  tvaParts: function tvaParts() {
    var result = {};
    this.each(function (model) {
      var tva_amount = model.tva();
      var tva = model.get('tva');

      if (tva in result) {
        tva_amount += result[tva];
      }

      result[tva] = tva_amount;
    });
    return result;
  },
  ttc: function ttc() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  validate: function validate() {
    var result = {};
    this.each(function (model) {
      var res = model.validate();

      if (res) {
        underscore__WEBPACK_IMPORTED_MODULE_0___default().extend(result, res);
      }
    });
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskLineCollection);

/***/ }),

/***/ "./src/task/models/TaskLineModel.js":
/*!******************************************!*\
  !*** ./src/task/models/TaskLineModel.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");




var TaskLineModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_3__.default.extend({
  props: ['id', 'order', 'date', 'description', 'cost', 'quantity', 'unity', 'tva', 'product_id', 'task_id', 'mode'],
  validation: function validation() {
    var validators = {
      description: {
        required: true,
        msg: "Veuillez saisir un objet"
      },
      cost: {
        required: true,
        pattern: "amount",
        msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
      },
      quantity: {
        required: true,
        pattern: "amount",
        msg: "Veuillez saisir une quantité, dans la limite de 5 chiffres après la virgule"
      },
      tva: {
        required: true,
        pattern: "number",
        msg: "Veuillez sélectionner une TVA"
      }
    };
    var dateRequired = this.config.request('has:form_section', 'composition:classic:lines:date');

    if (dateRequired) {
      validators.date = {
        required: true,
        msg: "Veuillez indiquer la date d'exécution de la prestation"
      };
    }

    return validators;
  },
  defaults: function defaults() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    var result = this.config.request('get:options', 'defaults');
    this.user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('session');
    result['tva'] = this.user_session.request('get', 'tva', result['tva']);
    var product_id = this.user_session.request('get', 'product_id', '');

    if (product_id !== '') {
      result['product_id'] = product_id;
    }

    return result;
  },
  initialize: function initialize() {
    this.user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('session');
    this.on('saved', this.onSaved, this);
  },
  onSaved: function onSaved() {
    this.user_session.request('set', 'tva', this.get('tva'));
    this.user_session.request('set', 'product_id', this.get('product_id'));
  },
  ht: function ht() {
    if (this.get('mode') === 'ht') {
      return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(this.get('cost')) * (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(this.get('quantity'));
    } else {
      return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.htFromTtc)(this.ttc(), this.tva_value());
    }
  },
  tva_value: function tva_value() {
    var tva = this.get('tva');

    if (tva < 0) {
      tva = 0;
    }

    return tva;
  },
  tva: function tva() {
    var val = (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.getTvaPart)(this.ht(), this.tva_value());
    return val;
  },
  ttc: function ttc() {
    if (this.get('mode') === 'ht') {
      return this.ht() + this.tva();
    } else {
      return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(this.get('cost')) * (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(this.get('quantity'));
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskLineModel);

/***/ }),

/***/ "./src/task/models/TotalModel.js":
/*!***************************************!*\
  !*** ./src/task/models/TotalModel.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var base_models_BaseModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! base/models/BaseModel */ "./src/base/models/BaseModel.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }





var TotalModel = base_models_BaseModel__WEBPACK_IMPORTED_MODULE_3__.default.extend({
  isLocalModel: true,
  // Avoids to be saved to server

  /**
   * @param {*} tva_key : The TVA's value 
   * @returns The TVA's name (label)
   */
  getTvaOptionLabel: function getTvaOptionLabel(tva_key) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
    var tva_options = channel.request('get:options', 'tvas');
    var tva_option = tva_options.find(function (option) {
      return option.value == tva_key;
    });
    return tva_option ? tva_option.name : "Inconnue";
  },
  tva_labels: function tva_labels() {
    var values = [];

    for (var _i = 0, _Object$entries = Object.entries(this.get('tvas')); _i < _Object$entries.length; _i++) {
      var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
          key = _Object$entries$_i[0],
          value = _Object$entries$_i[1];

      var label = this.getTvaOptionLabel(key);
      values.push({
        'label': label,
        value: (0,math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(value, true)
      });
    }

    return values;
  },

  /**
   * 
   * @returns values of the tva objects used in the document
   */
  tva_values: function tva_values() {
    if (this.has('tvas')) {
      // Return tva values used in the form in float format
      var result = Object.keys(this.get('tvas')).map(function (tva) {
        return parseFloat(tva);
      });
      return result;
    } else {
      return {};
    }
  },
  hasContributions: function hasContributions() {
    var contribution = 0;
    var insurance = 0;

    if (this.hasPriceStudy()) {
      var contributions = this.get('price_study').contributions;
      contribution = contributions['cae'] || 0;
      insurance = contributions['insurance'] || 0;
    }

    return contribution != 0 || insurance != 0;
  },
  hasPriceStudy: function hasPriceStudy() {
    return this.has('price_study');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TotalModel);

/***/ }),

/***/ "./src/task/models/price_study/ChapterCollection.js":
/*!**********************************************************!*\
  !*** ./src/task/models/price_study/ChapterCollection.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _ChapterModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChapterModel */ "./src/task/models/price_study/ChapterModel.js");


var ChapterCollection = base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _ChapterModel__WEBPACK_IMPORTED_MODULE_1__.default
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterCollection);

/***/ }),

/***/ "./src/task/models/price_study/ChapterModel.js":
/*!*****************************************************!*\
  !*** ./src/task/models/price_study/ChapterModel.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel */ "./src/base/models/BaseModel.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ProductCollection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductCollection */ "./src/task/models/price_study/ProductCollection.js");



var ChapterModel = base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  props: ['id', 'title', 'description', 'order', 'products', 'total_ht'],
  validation: {
    products: function products(value) {
      if (this.products.length == 0) {
        return "Chapitre ".concat(this.get('title'), " : Veuillez saisir au moins un produit");
      }
    }
  },
  initialize: function initialize(options) {
    var _this = this;

    ChapterModel.__super__.initialize.apply(this, arguments);

    this.populate();
    this.on('change:products', function () {
      return _this.populate();
    });
  },
  populate: function populate() {
    // On ne crée la "sous-collection" que si le modèle ici a déjà un id
    if (this.get('id')) {
      if (!this.products) {
        this.products = new _ProductCollection__WEBPACK_IMPORTED_MODULE_2__.default([], {
          url: this.url() + '/' + "products"
        });
        this.products._parent = this;
      }

      this.products.fetch();
    }
  },
  validateModel: function validateModel() {
    console.log(this); // Valide la présence de produit dans le chapitre

    if (this.products.length == 0) {
      // this.trigger('validated:invalid', this, {}});
      return {
        "Produits": "Veuillez saisir au moins un produit"
      };
    } // Valide les produits (compte tenu que l'on présente les vues sans
    // Backbone, on a besoin de trigger les erreurs de validation au niveau 
    // de ce modèle)


    var childValidation = this.products.validate();

    if (childValidation && !underscore__WEBPACK_IMPORTED_MODULE_1___default().isEmpty(childValidation)) {
      // this.trigger('validated:invalid', this, childValidation);
      // Ici on return car le validate ci-dessous risque de trigger
      // un valideted:valid qui va cacher les erreurs qu'on a trigger ici
      return {
        'Produits': 'Produits : il manque des informations requises'
      };
    } // Si le reste est valide, on valide le modèle courant


    return this.validate();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterModel);

/***/ }),

/***/ "./src/task/models/price_study/DiscountCollection.js":
/*!***********************************************************!*\
  !*** ./src/task/models/price_study/DiscountCollection.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountModel.js */ "./src/task/models/price_study/DiscountModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }





var DiscountCollection = base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_3__.default.extend({
  amount_related_props: ['amount', 'percentage', 'tva_id'],
  model: _DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize(options) {
    this.on('saved', this.channelCall);
    this.on('remove', this.onDeleteCall);
  },
  onDeleteCall: function onDeleteCall() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('priceStudyFacade');
    channel.trigger('changed:discount');
  },
  channelCall: function channelCall(key_or_attributes) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('priceStudyFacade');
    var changed_keys;

    if (_typeof(key_or_attributes) == 'object') {
      changed_keys = [key_or_attributes];
    } else {
      changed_keys = _.keys(key_or_attributes);
    }

    var fire = Boolean(_.intersection(this.amount_related_props, key_or_attributes));

    if (fire) {
      channel.trigger('changed:discount');
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountCollection);

/***/ }),

/***/ "./src/task/models/price_study/DiscountModel.js":
/*!******************************************************!*\
  !*** ./src/task/models/price_study/DiscountModel.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");


var DiscountModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'type_', 'amount', 'tva_id', 'description', 'total_ht', 'total_tva', 'total_ttc', 'order', 'percentage'],
  validation: {
    description: {
      required: true,
      msg: "Remise : Veuillez saisir un objet"
    },
    value: {
      required: function required(value, attr, computedState) {
        if (this.get('type_') == 'amount') {
          return true;
        }
      },
      pattern: "amount",
      msg: "Remise : Veuillez saisir un montant"
    },
    percentage: {
      required: function required(value, attr, computedState) {
        if (this.get('type_') == 'percentage') {
          return true;
        }
      },
      range: [1, 99],
      msg: "Veuillez saisir un pourcentage"
    },
    tva_id: {
      required: true,
      pattern: "number",
      msg: "Remise : Veuillez sélectionner une TVA"
    }
  },
  initialize: function initialize() {
    base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.__super__.initialize.apply(this, arguments);

    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.tva_options = this.config.request('get:options', 'tvas');
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  is_percentage: function is_percentage() {
    return this.get('type_') == 'percentage';
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountModel);

/***/ }),

/***/ "./src/task/models/price_study/PriceStudyModel.js":
/*!********************************************************!*\
  !*** ./src/task/models/price_study/PriceStudyModel.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name :  PriceStudyModel
 */



var PriceStudyModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  props: ["id", "ht", "general_overhead", "margin_rate", "mask_hours", "total_ht", "total_ttc", "tva_parts", "total_ht_before_discount"],
  validation: {
    label: {
      required: true,
      msg: "Ce champ est obligatoire"
    }
  },
  initialize: function initialize() {
    base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyFacade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_prefs');
  },
  tva_labels: function tva_labels() {
    var values = [];
    var this_ = this;
    var tva_options = this.config.request('get:options', 'tvas');

    _.each(this.get('tva_parts'), function (item, tva_id) {
      var tva = _.findWhere(tva_options, {
        id: parseInt(tva_id)
      });

      values.push({
        'value': (0,math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(item),
        label: tva.label
      });
    });

    return values;
  },
  ht_label: function ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.get('total_ht'));
  },
  ht_before_discounts_label: function ht_before_discounts_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.get('total_ht_before_discount'));
  },
  ttc_label: function ttc_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.get('total_ttc'));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PriceStudyModel);

/***/ }),

/***/ "./src/task/models/price_study/ProductCollection.js":
/*!**********************************************************!*\
  !*** ./src/task/models/price_study/ProductCollection.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _ProductModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductModel.js */ "./src/task/models/price_study/ProductModel.js");
/* harmony import */ var _WorkModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkModel.js */ "./src/task/models/price_study/WorkModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_4__);
/*
 * File Name : ProductCollection.js
 */





var ProductCollection = base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: function model(modeldict, options) {
    if (modeldict.type_ == 'price_study_work') {
      return new _WorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default(modeldict, options);
    } else {
      return new _ProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default(modeldict, options);
    }
  },
  HT: function HT() {
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  TVAParts: function TVAParts() {
    var result = {};
    this.each(function (model) {
      var tva_amount = model.tva();
      var tva = model.tva_label();

      if (tva in result) {
        tva_amount += result[tva];
      }

      result[tva] = tva_amount;
    });
    return result;
  },
  TTC: function TTC() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  validate: function validate() {
    var result = {};

    if (this.models.length == 0) {
      result['products'] = "Étude: veuillez saisir au moins une prestation";
    }

    this.each(function (model) {
      var res = model.validateModel();
      console.log("We've got a validation error on one model");

      if (res && !underscore__WEBPACK_IMPORTED_MODULE_4___default().isEmpty(res)) {
        Object.assign(result, {
          'products': res
        });
      }
    });
    console.log("Returning result");
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCollection);

/***/ }),

/***/ "./src/task/models/price_study/ProductModel.js":
/*!*****************************************************!*\
  !*** ./src/task/models/price_study/ProductModel.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/models/DuplicableMixin.js */ "./src/base/models/DuplicableMixin.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_tools_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone-tools.js */ "./src/backbone-tools.js");
/*
 * File Name : ProductModel.js
 *
 */





var ProductModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend(base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_2__.default).extend((backbone_validation__WEBPACK_IMPORTED_MODULE_3___default())).extend({
  props: ['id', 'type_', 'label', 'description', 'supplier_ht', 'margin_rate', "quantity", 'ht', 'total_ht', 'unity', 'tva_id', 'product_id', "order", 'chapter_id', 'mode'],
  validation: {
    'description': {
      required: true,
      msg: "Veuillez saisir une description"
    },
    ht: {
      required: function required(value, attr, computedState) {
        return this.get('mode') == 'ht';
      },
      pattern: "amount",
      msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
    },
    supplier_ht: {
      required: function required(value, attr, computedState) {
        return this.get('mode') == 'supplier_ht';
      },
      pattern: "amount",
      msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule"
    },
    margin_rate: [{
      required: false,
      pattern: 'amount',
      msg: "Le coefficient de marge doit être un nombre entre 0 et 1, dans la limite de 5 chiffres après la virgule"
    }, function (value) {
      value = parseInt(value.replace(',', '.'));

      if (value < 0 || value >= 1) {
        return "Le coefficient de marge doit être un nombre entre 0 et 1";
      }
    }],
    tva_id: {
      required: true,
      msg: 'Requis : Veuillez saisir le taux de TVA à appliquer'
    },
    product_id: {
      required: true,
      msg: 'Requis : Veuillez saisir le compte produit'
    }
  },
  defaults: function defaults() {
    console.log("In Product model defaults");
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    var form_defaults = config.request('get:options', 'defaults');
    this.user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('session');
    form_defaults['tva_id'] = this.user_session.request('get', 'tva_id', form_defaults['tva_id']);
    var product_id = this.user_session.request('get', 'product_id', '');

    if (product_id !== '') {
      form_defaults['product_id'] = product_id;
    }

    form_defaults['type_'] = 'price_study_product';
    form_defaults['quantity'] = 1;
    form_defaults['mode'] = 'ht';
    return form_defaults;
  },
  initialize: function initialize() {
    ProductModel.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('priceStudyFacade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('session');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.on('saved', this.onSaved, this);
  },
  onSaved: function onSaved() {
    this.user_session.request('set', 'tva_id', this.get('tva_id'));
    this.user_session.request('set', 'product_id', this.get('product_id'));
  },
  product_object: function product_object() {
    var product_id = parseInt(this.get('product_id'));
    return this.product_options.find(function (value) {
      return product_id == value.id;
    });
  },
  tva_object: function tva_object() {
    var tva_id = parseInt(this.get('tva_id'));
    return this.tva_options.find(function (value) {
      return tva_id == value.id;
    });
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  validateModel: function validateModel() {
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_4__.bindModelValidation)(this);
    var result = this.validate() || {};
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_4__.unbindModelValidation)(this);
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductModel);

/***/ }),

/***/ "./src/task/models/price_study/WorkItemCollection.js":
/*!***********************************************************!*\
  !*** ./src/task/models/price_study/WorkItemCollection.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _WorkItemModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemModel.js */ "./src/task/models/price_study/WorkItemModel.js");
/*
 * File Name :  WorkItemCollection
 */



var WorkItemCollection = base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _WorkItemModel_js__WEBPACK_IMPORTED_MODULE_2__.default,
  ht: function ht() {
    /* Sum the HT amounts for this collection */
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  tvaParts: function tvaParts() {
    /*
     * Collect tva amounts by tva  label
     */
    var result = {};
    this.each(function (model) {
      var tva_amount = model.tva();
      var tva = model.tva_label();

      if (tva != '-') {
        if (tva in result) {
          tva_amount += result[tva];
        }

        result[tva] = tva_amount;
      }
    });
    return result;
  },
  ttc: function ttc() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  refreshAll: function refreshAll(models) {
    return this.syncAll(models);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollection);

/***/ }),

/***/ "./src/task/models/price_study/WorkItemModel.js":
/*!******************************************************!*\
  !*** ./src/task/models/price_study/WorkItemModel.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/models/DuplicableMixin.js */ "./src/base/models/DuplicableMixin.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_tools__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone-tools */ "./src/backbone-tools.js");
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/*
 * File Name :  WorkItemModel
 */







var WorkItemModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend(base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_1__.default).extend((backbone_validation__WEBPACK_IMPORTED_MODULE_2___default().mixin)).extend({
  props: ["id", "type_", "label", "description", "ht", "supplier_ht", "work_unit_quantity", "total_quantity", 'quantity_inherited', "unity", "work_unit_ht", "total_ht", "sync_catalog", 'price_study_work_id', 'mode', 'order'],
  inherited_props: ['margin_rate'],
  defaults: function defaults() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('config');
    var defaults = config.request('get:options', 'defaults');
    return {
      work_unit_quantity: 1,
      quantity_inherited: true,
      margin_rate_editable: true,
      mode: 'ht'
    };
  },
  validation: {
    description: {
      required: true,
      msg: "Veuillez saisir une description"
    },
    work_unit_quantity: {
      required: function required(value, attr, computedState) {
        if (!this.get('locked')) {
          return true;
        }
      }
    },
    // unity: {
    //     required: true,
    //     msg: "Veuillez saisir une unité"
    // },
    ht: {
      required: function required(value, attr, computedState) {
        return this.get('mode') == 'ht';
      },
      pattern: "amount",
      msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
    },
    supplier_ht: {
      required: function required(value, attr, computedState) {
        return this.get('mode') == 'supplier_ht';
      },
      pattern: "amount",
      msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule"
    }
  },
  initialize: function initialize() {
    base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('priceStudyFacade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('user_preferences');
  },
  ht: function ht() {
    /* Return the ht value of this entry */
    return (0,math_js__WEBPACK_IMPORTED_MODULE_5__.strToFloat)(this.get('total_ht'));
  },
  validateModel: function validateModel() {
    (0,backbone_tools__WEBPACK_IMPORTED_MODULE_4__.bindModelValidation)(this);
    var result = this.validate();
    (0,backbone_tools__WEBPACK_IMPORTED_MODULE_4__.unbindModelValidation)(this);
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemModel);

/***/ }),

/***/ "./src/task/models/price_study/WorkModel.js":
/*!**************************************************!*\
  !*** ./src/task/models/price_study/WorkModel.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemCollection.js */ "./src/task/models/price_study/WorkItemCollection.js");
/* harmony import */ var _ProductModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ProductModel.js */ "./src/task/models/price_study/ProductModel.js");
/* harmony import */ var backbone_tools_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_5__);
/*
 * File Name :  WorkModel
 */






var WorkModel = _ProductModel_js__WEBPACK_IMPORTED_MODULE_3__.default.extend({
  /* props spécifique à ce modèle */
  props: ['title', 'items', 'flat_cost', 'display_details'],
  validation: {
    items: function items(value) {
      if (this.items.length == 0) {
        return "Ouvrage \xAB ".concat(this.get('title'), " \xBB : Veuillez saisir au moins un produit");
      }
    },
    title: {
      required: true,
      msg: "Veuillez saisir un titre"
    }
  },
  defaults: function defaults() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    var form_defaults = config.request('get:options', 'defaults');
    this.user_session = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('session');
    form_defaults['tva_id'] = this.user_session.request('get', 'tva_id', form_defaults['tva_id']);
    var product_id = this.user_session.request('get', 'product_id', '');

    if (product_id !== '') {
      form_defaults['product_id'] = product_id;
    }

    form_defaults['quantity'] = 1;
    form_defaults['type_'] = "price_study_work";
    form_defaults['display_details'] = true;
    return form_defaults;
  },
  initialize: function initialize() {
    var _this = this;

    WorkModel.__super__.initialize.apply(this, arguments);

    this.populate();
    this.on('change:items', function () {
      return _this.populate();
    });
    this.on('saved:margin_rate', function () {
      return _this.items.fetch();
    });
  },
  populate: function populate() {
    var _this2 = this;

    if (this.get('id')) {
      if (!this.items) {
        this.items = new _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_2__.default([], {
          url: this.url() + '/' + "work_items"
        });
        this.items._parent = this; // this.items.stopListening(this);
        // this.items.listenTo(this, 'saved', this.items.syncAll.bind(this.items));
      }

      this.items.fetch().done(function () {
        return _this2.collection.trigger('fetched');
      });
    }
  },
  supplier_ht_label: function supplier_ht_label() {
    return (0,math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)((0,math_js__WEBPACK_IMPORTED_MODULE_0__.round)(this.get('flat_cost'), 5));
  },
  validateModel: function validateModel() {
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_4__.bindModelValidation)(this);
    var result = this.validate() || {};
    var label = this.get('title');
    var item_validation = this.items.validate() || {};

    underscore__WEBPACK_IMPORTED_MODULE_5___default().each(item_validation, function (item, index) {
      item_validation[index] = label + " " + item;
    });

    if (!underscore__WEBPACK_IMPORTED_MODULE_5___default().isEmpty(item_validation)) {
      console.log("Item validation is something");
      console.log(item_validation);

      underscore__WEBPACK_IMPORTED_MODULE_5___default().extend(result, {
        'subproducts': item_validation
      });
    }

    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_4__.unbindModelValidation)(this);
    return result;
  }
});
/*
 * On complète les 'props' du ProductModel avec celle du WorkModel
 */

WorkModel.prototype.props = WorkModel.prototype.props.concat(_ProductModel_js__WEBPACK_IMPORTED_MODULE_3__.default.prototype.props);
Object.assign(WorkModel.prototype.validation, _ProductModel_js__WEBPACK_IMPORTED_MODULE_3__.default.prototype.validation);
delete WorkModel.prototype.validation.description;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkModel);

/***/ }),

/***/ "./src/task/models/progress_invoicing/ChapterCollection.js":
/*!*****************************************************************!*\
  !*** ./src/task/models/progress_invoicing/ChapterCollection.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseCollection.js */ "./src/base/models/BaseCollection.js");
/* harmony import */ var _ChapterModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChapterModel */ "./src/task/models/progress_invoicing/ChapterModel.js");


var ChapterCollection = base_models_BaseCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  model: _ChapterModel__WEBPACK_IMPORTED_MODULE_1__.default
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterCollection);

/***/ }),

/***/ "./src/task/models/progress_invoicing/ChapterModel.js":
/*!************************************************************!*\
  !*** ./src/task/models/progress_invoicing/ChapterModel.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var _ProductCollection__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductCollection */ "./src/task/models/progress_invoicing/ProductCollection.js");


var ChapterModel = base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  props: ["id", "title", "description"],
  initialize: function initialize(options) {
    ChapterModel.__super__.initialize.apply(this, arguments);

    this.populate();
  },
  populate: function populate() {
    // On ne crée la "sous-collection" que si le modèle ici a déjà un id
    if (this.get('id')) {
      if (!this.products) {
        this.products = new _ProductCollection__WEBPACK_IMPORTED_MODULE_1__.default([], {
          url: this.url() + '/' + "products"
        });
        this.products._parent = this;
      }

      this.products.fetch();
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterModel);

/***/ }),

/***/ "./src/task/models/progress_invoicing/ProductCollection.js":
/*!*****************************************************************!*\
  !*** ./src/task/models/progress_invoicing/ProductCollection.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _ProductModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductModel.js */ "./src/task/models/progress_invoicing/ProductModel.js");
/* harmony import */ var _WorkModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkModel.js */ "./src/task/models/progress_invoicing/WorkModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_4__);
/*
 * File Name : ProductCollection.js
 */





var ProductCollection = base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: function model(modeldict, options) {
    if (modeldict.type_ == 'progress_invoicing_work') {
      return new _WorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default(modeldict, options);
    } else {
      return new _ProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default(modeldict, options);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCollection);

/***/ }),

/***/ "./src/task/models/progress_invoicing/ProductModel.js":
/*!************************************************************!*\
  !*** ./src/task/models/progress_invoicing/ProductModel.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils */ "./src/task/models/progress_invoicing/utils.js");
var _BaseModel$extend;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var ProductModel = base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__.default.extend((_BaseModel$extend = {
  // Define the props that should be set on your model
  props: ['id', 'chapter_id', "type_", 'description', 'unity', 'quantity', 'tva', 'product_id', "percentage", "already_invoiced", // avant cette facture
  // Dans l'affaire
  'total_ht_to_invoice', 'total_tva_to_invoice', 'total_ttc_to_invoice', // Après cette facture
  'percent_left', 'total_ht_left', 'total_ht', 'tva_amount', 'total_ttc', // "Acompte ?"
  "has_deposit"],
  validation: function validation() {
    var range;
    var already_invoiced = this.get('already_invoiced') || 0;

    if ((0,_utils__WEBPACK_IMPORTED_MODULE_2__.isCancelinvoice)()) {
      range = [-1 * already_invoiced, 0];
    } else {
      range = [0, 100 - already_invoiced];
    }

    return {
      'percentage': {
        required: true,
        range: range,
        msg: "Veuillez saisir un pourcentage entre ".concat(range[0], " et ").concat(range[1])
      }
    };
  },
  initialize: function initialize() {
    ProductModel.__super__.initialize.apply(this, arguments);

    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('user_preferences');
  },
  product_object: function product_object() {
    var product_id = parseInt(this.get('product_id'));
    return this.product_options.find(function (value) {
      return product_id == value.id;
    });
  },
  tva_object: function tva_object() {
    var tva_id = parseInt(this.get('tva_id'));
    return this.tva_options.find(function (value) {
      return tva_id == value.id;
    });
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  _price_label: function _price_label(key) {
    return this.user_prefs.request('formatAmount', this.get(key), false);
  },
  total_ht_to_invoice_label: function total_ht_to_invoice_label() {
    return this._price_label('total_ht_to_invoice');
  },
  tva_to_invoice_label: function tva_to_invoice_label() {
    return this._price_label('total_tva_to_invoice');
  },
  total_ttc_to_invoice_label: function total_ttc_to_invoice_label() {
    return this._price_label('total_ttc_to_invoice');
  },
  total_ht_label: function total_ht_label() {
    return this._price_label('total_ht');
  }
}, _defineProperty(_BaseModel$extend, "tva_label", function tva_label() {
  return this._price_label('tva_amount');
}), _defineProperty(_BaseModel$extend, "total_ttc_label", function total_ttc_label() {
  return this._price_label('total_ttc');
}), _BaseModel$extend));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductModel);

/***/ }),

/***/ "./src/task/models/progress_invoicing/WorkItemCollection.js":
/*!******************************************************************!*\
  !*** ./src/task/models/progress_invoicing/WorkItemCollection.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseCollection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseCollection */ "./src/base/models/BaseCollection.js");
/* harmony import */ var _WorkItemModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WorkItemModel */ "./src/task/models/progress_invoicing/WorkItemModel.js");


var WorkItemCollection = base_models_BaseCollection__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _WorkItemModel__WEBPACK_IMPORTED_MODULE_1__.default
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollection);

/***/ }),

/***/ "./src/task/models/progress_invoicing/WorkItemModel.js":
/*!*************************************************************!*\
  !*** ./src/task/models/progress_invoicing/WorkItemModel.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! base/models/BaseModel */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils */ "./src/task/models/progress_invoicing/utils.js");



var WorkItemModel = base_models_BaseModel__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  props: ["id", "percentage", "already_invoiced", 'work_id', 'description', 'unity', 'quantity', "percentage", "already_invoiced", // avant cette facture
  // Dans l'affaire
  'total_ht_to_invoice', 'total_tva_to_invoice', 'total_ttc_to_invoice', // Après cette facture
  'percent_left', 'total_ht_left', 'total_ht', // 'tva_amount',
  // 'total_ttc',
  // "Acompte ?"
  "has_deposit"],
  validation: function validation() {
    var range;
    var already_invoiced = this.get('already_invoiced') || 0;

    if ((0,_utils__WEBPACK_IMPORTED_MODULE_2__.isCancelinvoice)()) {
      range = [-1 * already_invoiced, 0];
    } else {
      range = [0, 100 - already_invoiced];
    }

    return {
      'percentage': {
        required: true,
        range: range,
        msg: "Veuillez saisir un pourcentage entre ".concat(range[0], " et ").concat(range[1])
      }
    };
  },
  initialize: function initialize() {
    WorkItemModel.__super__.initialize.apply(this, arguments);

    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('user_preferences');
  },
  _price_label: function _price_label(key) {
    return this.user_prefs.request('formatAmount', this.get(key), false);
  },
  total_ht_to_invoice_label: function total_ht_to_invoice_label() {
    return this._price_label('total_ht_to_invoice');
  },
  tva_to_invoice_label: function tva_to_invoice_label() {
    return this._price_label('total_tva_to_invoice');
  },
  total_ttc_to_invoice_label: function total_ttc_to_invoice_label() {
    return this._price_label('total_ttc_to_invoice');
  },
  total_ht_label: function total_ht_label() {
    return this._price_label('total_ht');
  },
  tva_label: function tva_label() {
    return this._price_label('tva');
  },
  total_ttc_label: function total_ttc_label() {
    return this._price_label('ttc');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemModel);

/***/ }),

/***/ "./src/task/models/progress_invoicing/WorkModel.js":
/*!*********************************************************!*\
  !*** ./src/task/models/progress_invoicing/WorkModel.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ProductModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductModel */ "./src/task/models/progress_invoicing/ProductModel.js");
/* harmony import */ var _WorkItemCollection__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WorkItemCollection */ "./src/task/models/progress_invoicing/WorkItemCollection.js");


var WorkModel = _ProductModel__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  // Define the props that should be set on your model
  props: ["title", "items", 'locked'],
  initialize: function initialize() {
    WorkModel.__super__.initialize.apply(this, arguments);

    this.populate();
  },
  populate: function populate() {
    var _this = this;

    if (this.get('id')) {
      if (!this.items) {
        this.items = new _WorkItemCollection__WEBPACK_IMPORTED_MODULE_1__.default([], {
          url: this.url() + '/' + "work_items"
        });
        this.items._parent = this;
        this.stopListening(this.items);
        this.listenTo(this.items, 'fetched', function () {
          return _this.collection.trigger('fetched');
        });
      }

      this.items.fetch();
    }
  }
});
WorkModel.prototype.props = WorkModel.prototype.props.concat(_ProductModel__WEBPACK_IMPORTED_MODULE_0__.default.prototype.props);
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkModel);

/***/ }),

/***/ "./src/task/models/progress_invoicing/utils.js":
/*!*****************************************************!*\
  !*** ./src/task/models/progress_invoicing/utils.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "isCancelinvoice": () => (/* binding */ isCancelinvoice)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);

var isCancelinvoice = function isCancelinvoice() {
  var config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  return config.request('get:options', 'is_cancelinvoice', false);
};

/***/ }),

/***/ "./src/task/task.js":
/*!**************************!*\
  !*** ./src/task/task.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _backbone_tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var _components_App_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/App.js */ "./src/task/components/App.js");
/* harmony import */ var _components_Facade_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/Facade.js */ "./src/task/components/Facade.js");
/* harmony import */ var _components_ToolbarApp_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/ToolbarApp.js */ "./src/task/components/ToolbarApp.js");
/* harmony import */ var _components_UserPreferences_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/UserPreferences.js */ "./src/task/components/UserPreferences.js");
/* harmony import */ var _common_components_PreviewService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/components/PreviewService */ "./src/common/components/PreviewService.js");
/* global AppOption; */







jquery__WEBPACK_IMPORTED_MODULE_0___default()(function () {
  (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_1__.applicationStartup)(AppOption, _components_App_js__WEBPACK_IMPORTED_MODULE_2__.default, _components_Facade_js__WEBPACK_IMPORTED_MODULE_3__.default, {
    actionsApp: _components_ToolbarApp_js__WEBPACK_IMPORTED_MODULE_4__.default,
    customServices: [_common_components_PreviewService__WEBPACK_IMPORTED_MODULE_6__.default, _components_UserPreferences_js__WEBPACK_IMPORTED_MODULE_5__.default]
  });
});

/***/ }),

/***/ "./src/task/views/CommonView.js":
/*!**************************************!*\
  !*** ./src/task/views/CommonView.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_CheckboxListWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/CheckboxListWidget.js */ "./src/widgets/CheckboxListWidget.js");
/* harmony import */ var _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/DateWidget.js */ "./src/widgets/DateWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");











var template = __webpack_require__(/*! ./templates/CommonView.mustache */ "./src/task/views/templates/CommonView.mustache");

var CommonView = backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default().View.extend({
  /*
   * Wrapper around the component making part of the 'common'
   * invoice/estimation form, provide a main layout with regions for each
   * field
   */
  behaviors: [{
    behaviorClass: _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default,
    errorMessage: "Vérifiez votre saisie"
  }],
  tagName: 'div',
  className: 'separate_block border_left_block',
  template: template,
  fields: ['date', 'address', 'description', 'validity_duration', 'workplace', 'start_date', 'end_date', 'first_visit', 'mentions', 'insurance_id'],
  ui: {
    'moreAccordion': '#common-more'
  },
  regions: {
    errors: '.errors',
    date: '.date',
    address: '.address',
    description: '.description',
    validity_duration: '.validity_duration',
    workplace: '.workplace',
    start_date: '.start_date',
    mentions: '.mentions',
    insurance_id: '.insurance_id',
    first_visit: ".first_visit",
    end_date: '.end_date'
  },
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:persist'
  },
  modelEvents: {
    'validated:invalid': 'showErrors',
    'validated:valid': 'hideErrors',
    'saved': 'renderField'
  },
  events: {
    'focusout .validity_duration': "OnValidityDurationFocusOut"
  },
  initialize: function initialize(options) {
    this.section = options['section'];
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_6___default().channel('facade');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_6___default().channel('config');
    this.mentions_options = config.request('get:options', 'mentions');
    this.insurance_options = config.request('get:options', 'insurance_options');
  },
  OnValidityDurationFocusOut: function OnValidityDurationFocusOut() {
    if (underscore__WEBPACK_IMPORTED_MODULE_0___default().isEmpty(this.model.get('validity_duration'))) {
      var default_duration = backbone_radio__WEBPACK_IMPORTED_MODULE_6___default().channel('config').request('get:options', 'estimation_validity_duration_default');
      this.model.set('validity_duration', default_duration);
      this.onRender();
    }
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_7___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_7___default().unbind(this);
  },
  getMentionIds: function getMentionIds() {
    var mention_ids = this.model.get('mentions');
    return mention_ids;
  },
  isMoreSet: function isMoreSet() {
    var mention_ids = this.getMentionIds();

    if (mention_ids.length > 0) {
      return true;
    }

    return false;
  },
  hasAvailableMentions: function hasAvailableMentions() {
    return this.mentions_options.length > 0;
  },
  templateContext: function templateContext() {
    return {
      is_more_set: this.isMoreSet(),
      has_available_mentions: this.hasAvailableMentions()
    };
  },
  showDate: function showDate() {
    this.showChildView('date', new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      value: this.model.get('date'),
      title: "Date",
      field_name: "date",
      required: true
    }));
  },
  showAddress: function showAddress() {
    this.showChildView('address', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      title: 'Adresse du client',
      value: this.model.get('address'),
      field_name: 'address',
      rows: 4
    }));
  },
  showDescription: function showDescription() {
    this.showChildView('description', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      title: 'Objet du document',
      value: this.model.get('description'),
      field_name: 'description',
      rows: 4,
      required: true
    }));
  },
  showValidityDuration: function showValidityDuration() {
    if (!this.hasRegion('validity_duration')) {
      // We already destroyed the region
      return;
    }

    var field_def = this.section.validity_duration;

    if (field_def) {
      var default_duration = backbone_radio__WEBPACK_IMPORTED_MODULE_6___default().channel('config').request('get:options', 'estimation_validity_duration_default');
      var description = "";

      if (default_duration) {
        description = "Prend la valeur par défaut de la CAE si vide : " + default_duration;
      }

      this.showChildView('validity_duration', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
        title: field_def.title,
        value: this.model.get('validity_duration'),
        description: description,
        field_name: 'validity_duration',
        required: field_def.required
      }));
    } else {
      this.getRegion('validity_duration').destroy();
    }
  },
  showWorkplace: function showWorkplace() {
    if (!this.hasRegion('workplace')) {
      // We already destroyed the region
      return;
    }

    var field_def = this.section.workplace;

    if (field_def) {
      this.showChildView('workplace', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
        title: field_def.title,
        value: this.model.get('workplace'),
        field_name: 'workplace',
        required: field_def.required,
        rows: 3
      }));
    } else {
      this.getRegion('workplace').destroy();
    }
  },
  showFirstVisit: function showFirstVisit() {
    if (!this.hasRegion('first_visit')) {
      // We already destroyed the region
      return;
    }

    var field_def = this.section.first_visit;

    if (field_def) {
      this.showChildView('first_visit', new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        date: this.model.get('first_visit'),
        title: field_def.title,
        field_name: "first_visit",
        required: field_def.required,
        default_value: ""
      }));
    } else {
      this.getRegion('first_visit').destroy();
    }
  },
  showStartDate: function showStartDate() {
    if (!this.hasRegion('start_date')) {
      // We already destroyed the region
      return;
    }

    var field_def = this.section.start_date;

    if (field_def) {
      this.showChildView('start_date', new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        date: this.model.get('start_date'),
        title: field_def.title,
        field_name: "start_date",
        required: field_def.required,
        default_value: ""
      }));
    } else {
      this.getRegion('start_date').destroy();
    }
  },
  showEndDate: function showEndDate() {
    if (!this.hasRegion('end_date')) {
      // We already destroyed the region
      return;
    }

    var field_def = this.section.end_date;

    if (field_def) {
      this.showChildView('end_date', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
        value: this.model.get('end_date'),
        title: field_def.title,
        field_name: 'end_date',
        required: field_def.required
      }));
    } else {
      this.getRegion('end_date').destroy();
    }
  },
  showMentionList: function showMentionList() {
    if (!this.hasRegion('mentions')) {
      // We already destroyed the region
      return;
    }

    if (this.hasAvailableMentions()) {
      var mention_list = new _widgets_CheckboxListWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
        options: this.mentions_options,
        value: this.getMentionIds(),
        title: "",
        field_name: "mentions",
        layout: "two_cols"
      });
      this.showChildView('mentions', mention_list);
    } else {
      this.getRegion('mentions').destroy();
    }
  },
  showInsurance: function showInsurance() {
    if (!this.hasRegion('insurance_id')) {
      // We already destroyed the region
      return;
    }

    var field_def = this.section.insurance_id;

    if (field_def && this.insurance_options.length > 0) {
      var insurance_select = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_8__.default({
        options: this.insurance_options,
        value: this.model.get('insurance_id'),
        title: field_def.title,
        field_name: "insurance_id",
        id_key: 'id',
        placeholder: "Sélectionnez",
        // TODO: éditable sous conditions (facture généré depuis un devis ou avoir)
        editable: field_def.edit,
        required: field_def.required
      });
      this.showChildView('insurance_id', insurance_select);
    } else {
      this.getRegion('insurance_id').destroy();
    }
  },
  onRender: function onRender() {
    this.showDate();
    this.showValidityDuration();
    this.showAddress();
    this.showDescription();
    this.showFirstVisit();
    this.showStartDate();
    this.showEndDate();
    this.showWorkplace();
    this.showMentionList();
    this.showInsurance();
  },
  renderField: function renderField(values) {
    if (values instanceof Object) {
      if ('date' in values) {
        this.showDate();
      }

      if (true) {
        this.showValidityDuration();
      }

      if ('address' in values) {
        this.showAddress();
      }

      if ('description' in values) {
        this.showDescription();
      }

      if ('start_date' in values) {
        this.showStartDate();
      }

      if ('first_visit' in values) {
        this.showFirstVisit();
      }

      if ('end_date' in values) {
        this.showEndDate();
      }

      if ('workplace' in values) {
        this.showWorkplace();
      }

      if ('mentions' in values) {
        this.showMentionList();
      }

      if ('insurance_id' in values) {
        this.showInsurance();
      }
    } else {
      this.onRender();
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CommonView);

/***/ }),

/***/ "./src/task/views/DisplayOptionsView.js":
/*!**********************************************!*\
  !*** ./src/task/views/DisplayOptionsView.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/CheckboxWidget */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tools */ "./src/tools.js");





var DisplayOptionsView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  /**
   * Display DisplayUnitsView and DisplayTTCView
   */
  template: __webpack_require__(/*! ./templates/DisplayOptionsView.mustache */ "./src/task/views/templates/DisplayOptionsView.mustache"),
  tagName: 'div',
  className: 'form-section',
  behaviors: [base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  regions: {
    display_units_container: '.display-units-container',
    display_ttc_container: '.display-ttc-container',
    input_mode_container: '.input-mode-container'
  },
  childViewEvents: {
    'inputModeChange': "onInputModeChange"
  },
  childViewTriggers: {
    'finish': 'data:persist'
  },
  initialize: function initialize(options) {
    this.section = options['section'];
  },
  showInputMode: function showInputMode() {
    this.showChildView('input_mode_container', new widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_2__.default({
      'field_name': 'input_mode',
      'title': '',
      inline_label: "Étude de prix",
      description: "Le calcul se base sur le cout d’achat HT et les coefficients de frais généraux et de marge.",
      'finishEventName': 'inputModeChange',
      true_val: 'price_study',
      false_val: 'classic',
      value: this.model.get('input_mode')
    }));
  },
  showDisplayUnits: function showDisplayUnits() {
    var view = new widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_2__.default({
      title: "",
      inline_label: "Afficher le détail (prix unitaire et quantité) des produits dans le PDF",
      description: "Les informations qui seront masquées dans le PDF sont hachurées.",
      field_name: "display_units",
      value: this.model.get('display_units')
    });
    this.showChildView('display_units_container', view);
  },
  showDisplayTtc: function showDisplayTtc() {
    var view = new widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_2__.default({
      title: "",
      inline_label: "Afficher les prix TTC dans le PDF",
      field_name: "display_ttc",
      value: this.model.get('display_ttc')
    });
    this.showChildView('display_ttc_container', view);
  },
  onRender: function onRender() {
    if (this.section['input_mode_edit']) {
      this.showInputMode();
    }

    if (this.section['display_units']) {
      this.showDisplayUnits();
    }

    if (this.section['display_ttc']) {
      this.showDisplayTtc();
    }
  },
  onInputModeChange: function onInputModeChange(field_name, value) {
    var confirm = true;

    if (value == 'classic') {
      confirm = window.confirm("Le détail des calculs de l'étude de prix seront perdus, continuez ?");
    } else if (value == 'price_study') {
      confirm = window.confirm("Les prestations déjà saisies seront supprimées");
    }

    if (confirm) {
      (0,tools__WEBPACK_IMPORTED_MODULE_3__.showLoader)();
      this.triggerMethod('data:persist', field_name, value);
    } else {
      this.showInputMode();
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DisplayOptionsView);

/***/ }),

/***/ "./src/task/views/GeneralView.js":
/*!***************************************!*\
  !*** ./src/task/views/GeneralView.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _common_views_StatusHistoryView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../common/views/StatusHistoryView.js */ "./src/common/views/StatusHistoryView.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_5__);








var template = __webpack_require__(/*! ./templates/GeneralView.mustache */ "./src/task/views/templates/GeneralView.mustache");

var GeneralView = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  /*
   * Wrapper around the component making part of the 'common'
   * invoice/estimation form, provide a main layout with regions for each
   * field
   */
  behaviors: [{
    behaviorClass: _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default,
    errorMessage: "Vérifiez votre saisie"
  }],
  tagName: 'div',
  className: 'separate_block border_left_block',
  template: template,
  regions: {
    status_history: '.status_history',
    name: '.name',
    financial_year: '.financial_year'
  },
  modelEvents: {
    'validated:invalid': 'showErrors',
    'validated:valid': 'hideErrors'
  },
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:persist'
  },
  initialize: function initialize(options) {
    this.section = options['section'];
    this.business_types_options = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config').request('get:options', 'business_types');
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().unbind(this);
  },
  showErrors: function showErrors(model, errors) {
    console.log("We show errors");
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    console.log("We hide errors");
    this.$el.removeClass('error');
  },
  templateContext: function templateContext() {
    var result = {};

    if (this.business_types_options.length > 1) {
      result['business_type'] = this.model.getBusinessType();
    }

    result['financial_year'] = 'financial_year' in this.section;
    return result;
  },
  showStatusHistory: function showStatusHistory() {
    var collection = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade').request('get:collection', 'status_history');
    var view = new _common_views_StatusHistoryView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      collection: collection
    });
    this.showChildView('status_history', view);
  },
  showName: function showName() {
    this.showChildView('name', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      title: "Nom du document",
      value: this.model.get('name'),
      field_name: 'name',
      required: true
    }));
  },
  onRender: function onRender() {
    this.showStatusHistory();
    this.showName();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (GeneralView);

/***/ }),

/***/ "./src/task/views/NotesBlockView.js":
/*!******************************************!*\
  !*** ./src/task/views/NotesBlockView.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");




var template = __webpack_require__(/*! ./templates/NotesBlockView.mustache */ "./src/task/views/templates/NotesBlockView.mustache");

var NotesBlockView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  tagName: 'div',
  className: 'separate_block border_left_block',
  template: template,
  regions: {
    notes: {
      el: '.notes',
      replaceElement: true
    }
  },
  behaviors: [{
    behaviorClass: _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default,
    errorMessage: "Vérifiez votre saisie"
  }],
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:persist'
  },
  isMoreSet: function isMoreSet() {
    if (this.model.get('notes')) {
      return true;
    }

    return false;
  },
  templateContext: function templateContext() {
    return {
      is_more_set: this.isMoreSet()
    };
  },
  onRender: function onRender() {
    var view = new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      title: 'Notes',
      description: 'Notes complémentaires',
      field_name: 'notes',
      value: this.model.get('notes')
    });
    this.showChildView('notes', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NotesBlockView);

/***/ }),

/***/ "./src/task/views/RootComponent.js":
/*!*****************************************!*\
  !*** ./src/task/views/RootComponent.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _files_FileComponent_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./files/FileComponent.js */ "./src/task/views/files/FileComponent.js");
/* harmony import */ var _GeneralView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./GeneralView.js */ "./src/task/views/GeneralView.js");
/* harmony import */ var _CommonView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./CommonView.js */ "./src/task/views/CommonView.js");
/* harmony import */ var _NotesBlockView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./NotesBlockView.js */ "./src/task/views/NotesBlockView.js");
/* harmony import */ var _payments_PaymentConditionBlockView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payments/PaymentConditionBlockView.js */ "./src/task/views/payments/PaymentConditionBlockView.js");
/* harmony import */ var _payments_PaymentBlockView_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payments/PaymentBlockView.js */ "./src/task/views/payments/PaymentBlockView.js");
/* harmony import */ var _DisplayOptionsView__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./DisplayOptionsView */ "./src/task/views/DisplayOptionsView.js");
/* harmony import */ var common_views_StatusView_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! common/views/StatusView.js */ "./src/common/views/StatusView.js");
/* harmony import */ var _related_estimation_RelatedEstimationCollectionView__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./related_estimation/RelatedEstimationCollectionView */ "./src/task/views/related_estimation/RelatedEstimationCollectionView.js");
/* harmony import */ var base_views_LoginView_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! base/views/LoginView.js */ "./src/base/views/LoginView.js");
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _composition__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./composition */ "./src/task/views/composition/index.js");
/* harmony import */ var _resume_ResumeView_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./resume/ResumeView.js */ "./src/task/views/resume/ResumeView.js");
/* harmony import */ var _common_views_PDFViewerPopupView__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../common/views/PDFViewerPopupView */ "./src/common/views/PDFViewerPopupView.js");
/* harmony import */ var _common_views_NodeFileViewerPopupView__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../common/views/NodeFileViewerPopupView */ "./src/common/views/NodeFileViewerPopupView.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");



















var template = __webpack_require__(/*! ./templates/RootComponent.mustache */ "./src/task/views/templates/RootComponent.mustache");
/**
 * Root Component showing all the app blocks
 * 
 * NB : action toolbar is handled separately because for now the status of the 
 * document is computed server-side
 * 
 * Ideally we should have all under this component
 */


var RootComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_17___default().View.extend({
  template: template,
  regions: {
    modalRegion: '#modalregion',
    resumeRegion: "#resumeregion",
    actions_toolbar: {
      el: "#actions_toolbar",
      replaceElement: true
    },
    errors: '#main-error-region',
    related_estimation: '.related-estimation',
    general: '.general-informations',
    files: "#files",
    common: '.common-informations',
    display_options: '.display-options',
    composition: '.composition',
    notes: '.notes',
    payment_conditions: '.payment-conditions',
    payments: '.payments'
  },
  initialize: function initialize(options) {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade'); // This event is fired by the ActionToolBar on the Facade and we connect 
    // here to show the StatusView (modal)

    this.listenTo(this.facade, 'status:change', this.onStatusChange);
    this.listenTo(this.facade, 'show:preview', this.onShowPreview);
  },
  showStatusView: function showStatusView(action) {
    /*
        Modale de changement de statut
    */
    var model = this.facade.request('get:model', 'common');
    var view = new common_views_StatusView_js__WEBPACK_IMPORTED_MODULE_8__.default({
      action: action,
      model: model
    });
    this.showChildView('modalRegion', view);
  },

  /**
   * Vue résumé (Affiche les Totaux)
   */
  showResumeView: function showResumeView() {
    var model = this.facade.request('get:model', 'total');
    var view = new _resume_ResumeView_js__WEBPACK_IMPORTED_MODULE_14__.default({
      model: model
    });
    this.showChildView("resumeRegion", view);
  },
  showEstimationLinked: function showEstimationLinked() {
    /*
        Lien vers les devis liés (pour les factures)
    */
    var collection = this.facade.request('get:collection', 'related_estimations');

    if (collection && collection.length > 0) {
      var view = new _related_estimation_RelatedEstimationCollectionView__WEBPACK_IMPORTED_MODULE_9__.default({
        collection: collection
      });
      this.showChildView('related_estimation', view);
    }
  },
  showGeneralBlock: function showGeneralBlock() {
    var section = this.config.request('get:form_section', 'general');
    var model = this.facade.request('get:model', 'general');
    var view = new _GeneralView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model,
      section: section
    });
    this.showChildView('general', view);
  },
  showFileBlock: function showFileBlock() {
    var section = this.config.request('get:form_section', 'files');
    var view = new _files_FileComponent_js__WEBPACK_IMPORTED_MODULE_1__.default({
      section: section
    });
    this.showChildView('files', view);
  },
  showCommonBlock: function showCommonBlock() {
    var section = this.config.request('get:form_section', 'common');
    var model = this.facade.request('get:model', 'common');
    var view = new _CommonView_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: model,
      section: section
    });
    this.showChildView('common', view);
  },
  showDisplayOptions: function showDisplayOptions() {
    var section = this.config.request('get:form_section', 'display_options');
    var model = this.facade.request('get:model', 'display_options');
    var view = new _DisplayOptionsView__WEBPACK_IMPORTED_MODULE_7__.default({
      model: model,
      section: section
    });
    this.showChildView('display_options', view);
  },
  showCompositionBlock: function showCompositionBlock() {
    var totalmodel = this.facade.request('get:model', 'total');
    var section = this.config.request('get:form_section', 'composition');
    var region = this.getRegion("composition");
    (0,_composition__WEBPACK_IMPORTED_MODULE_13__.default)(region, section, totalmodel);
  },
  showNotesBlock: function showNotesBlock() {
    var section = this.config.request('get:form_section', 'notes');
    var model = this.facade.request('get:model', 'notes');
    var view = new _NotesBlockView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      model: model,
      section: section
    });
    this.showChildView('notes', view);
  },
  showPaymentConditionsBlock: function showPaymentConditionsBlock() {
    var section = this.config.request('get:form_section', 'payment_conditions');
    var model = this.facade.request('get:model', 'payment_conditions');
    var view = new _payments_PaymentConditionBlockView_js__WEBPACK_IMPORTED_MODULE_5__.default({
      model: model
    });
    this.showChildView('payment_conditions', view);
  },
  showPaymentBlock: function showPaymentBlock() {
    var section = this.config.request('get:form_section', 'payments');
    var model = this.facade.request('get:model', 'payment_options');
    var collection = this.facade.request('get:collection', 'payment_lines');
    var view = new _payments_PaymentBlockView_js__WEBPACK_IMPORTED_MODULE_6__.default({
      model: model,
      collection: collection,
      section: section
    });
    this.showChildView('payments', view);
  },
  showLogin: function showLogin() {
    var view = new base_views_LoginView_js__WEBPACK_IMPORTED_MODULE_10__.default({});
    this.showChildView('modalRegion', view);
  },
  onRender: function onRender() {
    this.showResumeView();
    this.showEstimationLinked();

    if (this.config.request('has:form_section', 'files')) {
      this.showFileBlock();
    }

    if (this.config.request('has:form_section', 'general')) {
      this.showGeneralBlock();
    }

    if (this.config.request('has:form_section', 'common')) {
      this.showCommonBlock();
    }

    if (this.config.request('has:form_section', 'display_options')) {
      this.showDisplayOptions();
    }

    if (this.config.request('has:form_section', 'composition')) {
      this.showCompositionBlock();
    }

    if (this.config.request('has:form_section', "notes")) {
      this.showNotesBlock();
    }

    if (this.config.request('has:form_section', "payment_conditions")) {
      this.showPaymentConditionsBlock();
    }

    if (this.config.request('has:form_section', "payments")) {
      this.showPaymentBlock();
    }
  },
  formOk: function formOk() {
    var result = true;
    var errors = this.facade.request('is:valid');

    if (!_.isEmpty(errors)) {
      this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_11__.default({
        errors: errors
      }));
      result = false;
    } else {
      this.detachChildView('errors');
    }

    return result;
  },
  onShowPreview: function onShowPreview(target, popupTitle, documentTitle) {
    if (target instanceof Object) {
      this.onShowPreviewNodeFileModel(target, popupTitle);
    } else {
      this.onShowPreviewPdfUrl(target, popupTitle, documentTitle);
    }
  },
  onShowPreviewPdfUrl: function onShowPreviewPdfUrl(url, popupTitle, documentTitle) {
    console.log("RootComponent.onShowPreviewPdfUrl");

    if (url === undefined) {
      url = window.location.pathname + ".pdf";
    }

    if (popupTitle === undefined) {
      popupTitle = "Prévisualisation";
    }

    if (documentTitle === undefined) {
      documentTitle = "";
    }

    console.log("RootComponent.onShowPreview");
    var view = new _common_views_PDFViewerPopupView__WEBPACK_IMPORTED_MODULE_15__.default({
      pdfUrl: url,
      popupTitle: popupTitle,
      documentTitle: documentTitle
    });
    this.showChildView('modalRegion', view);
  },

  /**
   *
   * @param {NodeFileModel} file
   * @param {String} popupTitle optional popup title (defaults to file name)
   */
  onShowPreviewNodeFileModel: function onShowPreviewNodeFileModel(file, popupTitle) {
    console.log("RootComponent.onShowPreviewNodeFileModel");
    var view = new _common_views_NodeFileViewerPopupView__WEBPACK_IMPORTED_MODULE_16__.default({
      file: file,
      popupTitle: popupTitle
    });
    this.showChildView('modalRegion', view);
  },
  onStatusChange: function onStatusChange(model) {
    var _this = this;

    if (!model.get('status')) {
      return;
    }

    console.log("showloader");
    (0,_tools_js__WEBPACK_IMPORTED_MODULE_12__.showLoader)();

    if (model.get('status') != 'draft') {
      console.log("Status is not draft, validating all data");

      if (!this.formOk()) {
        console.log("Scroll top ?");
        document.getElementById('target_content').scrollTop = document.documentElement.scrollTop = 0;
        (0,_tools_js__WEBPACK_IMPORTED_MODULE_12__.hideLoader)();
        return;
      }
    }

    var deferred = this.facade.request('save:all');
    deferred.then(function () {
      (0,_tools_js__WEBPACK_IMPORTED_MODULE_12__.hideLoader)();

      _this.showStatusView(model);
    }, function () {
      (0,_tools_js__WEBPACK_IMPORTED_MODULE_12__.hideLoader)();
    });
  },
  onChildviewDestroyModal: function onChildviewDestroyModal() {
    this.getRegion('modalRegion').empty();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RootComponent);

/***/ }),

/***/ "./src/task/views/composition/classic/ClassicCompositionComponent.js":
/*!***************************************************************************!*\
  !*** ./src/task/views/composition/classic/ClassicCompositionComponent.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _lines_LinesComponent_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./lines/LinesComponent.js */ "./src/task/views/composition/classic/lines/LinesComponent.js");
/* harmony import */ var _discount_HtBeforeDiscountsView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./discount/HtBeforeDiscountsView.js */ "./src/task/views/composition/classic/discount/HtBeforeDiscountsView.js");
/* harmony import */ var _discount_DiscountComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./discount/DiscountComponent.js */ "./src/task/views/composition/classic/discount/DiscountComponent.js");
/* harmony import */ var _ExpenseHtComponent_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ExpenseHtComponent.js */ "./src/task/views/composition/classic/ExpenseHtComponent.js");
/*
 * Module name : CompositionComponent
 */







var template = __webpack_require__(/*! ./templates/CompositionComponent.mustache */ "./src/task/views/composition/classic/templates/CompositionComponent.mustache");
/**
 * Main Composition View
 * 
 * Displays the classic task line edition mode
 * 
 */


var ClassicCompositionComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  template: template,
  regions: {
    tasklines: '#tasklines',
    ht_before_discounts: '.ht-before-discounts',
    discounts: '#discounts',
    expenses_ht: '#expenses_ht',
    link_container: '.link-container'
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize(options) {
    console.log(options);
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.section_options = options['section'];
    this.totalmodel = options['totalmodel'];
    this.edit = options['edit'];
  },
  onRender: function onRender() {
    if (this.section_options.hasOwnProperty('lines')) {
      this.showLinesComponent();
    }

    if (this.section_options.hasOwnProperty('discounts')) {
      var view = new _discount_HtBeforeDiscountsView_js__WEBPACK_IMPORTED_MODULE_2__.default({
        model: this.totalmodel
      });
      this.showChildView('ht_before_discounts', view);
      this.showDiscountComponent();
    }

    if (this.section_options.hasOwnProperty('expenses_ht')) {
      this.showExpenseHtBlock();
    }
  },
  showLinesComponent: function showLinesComponent() {
    var lineSection = this.section_options['lines'];
    var model = this.facade.request('get:model', 'common');
    var collection = this.facade.request('get:collection', 'task_groups');
    var view = new _lines_LinesComponent_js__WEBPACK_IMPORTED_MODULE_1__.default({
      collection: collection,
      edit: this.edit,
      model: model,
      section: lineSection
    });
    this.showChildView('tasklines', view);
  },
  showDiscountComponent: function showDiscountComponent() {
    var section = this.section_options['discounts'];
    var collection = this.facade.request('get:collection', 'discounts');
    var view = new _discount_DiscountComponent_js__WEBPACK_IMPORTED_MODULE_3__.default({
      collection: collection,
      edit: this.edit,
      section: section
    });
    this.showChildView('discounts', view);
  },
  showExpenseHtBlock: function showExpenseHtBlock() {
    var model = this.facade.request('get:model', 'expense_ht');
    var view = new _ExpenseHtComponent_js__WEBPACK_IMPORTED_MODULE_4__.default({
      model: model
    });
    this.showChildView('expenses_ht', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ClassicCompositionComponent);

/***/ }),

/***/ "./src/task/views/composition/classic/ExpenseHtComponent.js":
/*!******************************************************************!*\
  !*** ./src/task/views/composition/classic/ExpenseHtComponent.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
var _Mn$View$extend;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var template = __webpack_require__(/*! ./templates/ExpenseHtComponent.mustache */ "./src/task/views/composition/classic/templates/ExpenseHtComponent.mustache");

var ExpenseHtComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend((_Mn$View$extend = {
  template: template,
  behaviors: [{
    behaviorClass: _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default,
    errorMessage: "Vérifiez votre saisie"
  }],
  tagName: 'div',
  className: 'form-section'
}, _defineProperty(_Mn$View$extend, "template", template), _defineProperty(_Mn$View$extend, "regions", {
  expenses_ht: ".expenses_ht"
}), _defineProperty(_Mn$View$extend, "childViewTriggers", {
  'change': 'data:modified',
  'finish': 'data:persist'
}), _defineProperty(_Mn$View$extend, "initialize", function initialize(options) {
  this.section = options['section'];
}), _defineProperty(_Mn$View$extend, "onRender", function onRender() {
  this.showChildView('expenses_ht', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
    title: "Frais forfaitaires (HT)",
    value: this.model.get('expenses_ht'),
    field_name: 'expenses_ht'
  }));
}), _Mn$View$extend));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseHtComponent);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/DiscountCollectionView.js":
/*!*******************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/DiscountCollectionView.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountView.js */ "./src/task/views/composition/classic/discount/DiscountView.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_2__);




var DiscountCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  tagName: 'tbody',
  className: 'lines',
  childView: _DiscountView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  // Bubble up child view events
  childViewTriggers: {
    'edit': 'line:edit',
    'delete': 'line:delete'
  },
  childViewOptions: function childViewOptions(model) {
    // Forward the edit option to the children
    return {
      edit: this.getOption('edit')
    };
  },
  initialize: function initialize(options) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_2___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_2___default().unbind(this);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountCollectionView);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/DiscountComponent.js":
/*!**************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/DiscountComponent.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _models_DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../models/DiscountModel.js */ "./src/task/models/DiscountModel.js");
/* harmony import */ var _DiscountCollectionView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DiscountCollectionView.js */ "./src/task/views/composition/classic/discount/DiscountCollectionView.js");
/* harmony import */ var _DiscountFormPopupView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./DiscountFormPopupView.js */ "./src/task/views/composition/classic/discount/DiscountFormPopupView.js");
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* harmony import */ var backbone_tools_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone-tools.js */ "./src/backbone-tools.js");







var DiscountComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  tagName: 'div',
  className: 'form-section discount-group',
  template: __webpack_require__(/*! ./templates/DiscountComponent.mustache */ "./src/task/views/composition/classic/discount/templates/DiscountComponent.mustache"),
  regions: {
    lines: {
      el: '.lines',
      replaceElement: true
    },
    'modalRegion': '.modalregion',
    'errors': ".block-errors"
  },
  ui: {
    add_button: 'button.btn-add'
  },
  triggers: {
    "click @ui.add_button": "line:add"
  },
  childViewEvents: {
    'line:edit': 'onLineEdit',
    'line:delete': 'onLineDelete',
    'destroy:modal': 'render',
    'insert:percent': 'onInsertPercent'
  },
  collectionEvents: {
    'change': 'hideErrors',
    'validated:invalid': "showErrors",
    'validated:valid': "hideErrors",
    'sync': 'render'
  },
  initialize: function initialize(options) {
    this.collection = options['collection'];
    this.listenTo(this.collection, 'validated:invalid', this.showErrors);
    this.listenTo(this.collection, 'validated:valid', this.hideErrors.bind(this));
    this.edit = options['edit'];
    this.compute_mode = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config').request('get:options', 'compute_mode');
  },
  showErrors: function showErrors(model, errors) {
    this.detachChildView('errors');
    this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      errors: errors
    }));
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.detachChildView('errors');
    this.$el.removeClass('error');
  },
  isEmpty: function isEmpty() {
    return this.collection.length === 0;
  },
  onLineAdd: function onLineAdd() {
    var model = new _models_DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__.default();
    this.showDiscountLineForm(model, "Ajouter la remise", false);
  },
  onLineEdit: function onLineEdit(childView) {
    this.showDiscountLineForm(childView.model, "Modifier la remise", true);
  },
  showDiscountLineForm: function showDiscountLineForm(model, title, edit) {
    var form = new _DiscountFormPopupView_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: model,
      title: title,
      destCollection: this.collection,
      edit: edit
    });
    this.showChildView('modalRegion', form);
  },
  onDeleteSuccess: function onDeleteSuccess() {
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_5__.displayServerSuccess)("Vos données ont bien été supprimées");
  },
  onDeleteError: function onDeleteError() {
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_5__.displayServerError)("Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  onLineDelete: function onLineDelete(childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette remise ?");

    if (result) {
      childView.model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError
      });
    }
  },
  onInsertPercent: function onInsertPercent(model) {
    this.collection.insert_percent(model);
    this.getChildView('modalRegion').triggerMethod('modal:close');
  },
  templateContext: function templateContext() {
    var empty = this.isEmpty();
    return {
      not_empty: !empty,
      collapsed: empty,
      edit: this.edit,
      is_ttc_mode: this.compute_mode == 'ttc'
    };
  },
  onRender: function onRender() {
    if (!this.isEmpty()) {
      this.showChildView('lines', new _DiscountCollectionView_js__WEBPACK_IMPORTED_MODULE_2__.default({
        collection: this.collection,
        edit: this.edit
      }));
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountComponent);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/DiscountFormPopupView.js":
/*!******************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/DiscountFormPopupView.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../base/behaviors/ModalBehavior.js */ "./src/base/behaviors/ModalBehavior.js");
/* harmony import */ var _DiscountFormView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountFormView.js */ "./src/task/views/composition/classic/discount/DiscountFormView.js");
/* harmony import */ var _models_DiscountPercentModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/DiscountPercentModel.js */ "./src/task/models/DiscountPercentModel.js");
/* harmony import */ var _DiscountPercentView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./DiscountPercentView.js */ "./src/task/views/composition/classic/discount/DiscountPercentView.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../tools.js */ "./src/tools.js");







var template = __webpack_require__(/*! ./templates/DiscountFormPopupView.mustache */ "./src/task/views/composition/classic/discount/templates/DiscountFormPopupView.mustache");

var DiscountFormPopupView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  id: 'discount-form-popup',
  behaviors: [_base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default],
  template: template,
  regions: {
    'simple-form': '.simple-form',
    'percent-form': '.percent-form'
  },
  // Here we bind the child FormBehavior with our ModalBehavior
  // Like it's done in the ModalFormBehavior
  childViewTriggers: {
    'cancel:form': 'modal:close',
    'success:sync': 'modal:close',
    'insert:percent': 'insert:percent'
  },
  onModalBeforeClose: function onModalBeforeClose() {
    this.model.rollback();
  },
  isAddView: function isAddView() {
    return !(0,_tools_js__WEBPACK_IMPORTED_MODULE_4__.getOpt)(this, 'edit', false);
  },
  onRender: function onRender() {
    this.showChildView('simple-form', new _DiscountFormView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      model: this.model,
      title: this.getOption('title'),
      destCollection: this.getOption('destCollection')
    }));

    if (this.isAddView()) {
      this.showChildView('percent-form', new _DiscountPercentView_js__WEBPACK_IMPORTED_MODULE_3__.default({
        title: this.getOption('title'),
        model: new _models_DiscountPercentModel_js__WEBPACK_IMPORTED_MODULE_2__.default(),
        destCollection: this.getOption('destCollection')
      }));
    }
  },
  templateContext: function templateContext() {
    return {
      title: this.getOption('title'),
      add: this.isAddView()
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountFormPopupView);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/DiscountFormView.js":
/*!*************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/DiscountFormView.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_4__);







var template = __webpack_require__(/*! ./templates/DiscountFormView.mustache */ "./src/task/views/composition/classic/discount/templates/DiscountFormView.mustache");

var DiscountFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  tagName: 'div',
  className: 'modal_overflow',
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_3__.default],
  template: template,
  regions: {
    'order': '.order',
    'description': '.description',
    'amount': '.amount',
    'tva': '.tva'
  },
  childViewTriggers: {
    'change': 'data:modified'
  },
  initialize: function initialize() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('config');
    var tva_options = config.request('get:options', 'tvas');
    this.compute_mode = config.request('get:options', 'compute_mode'); // Only allow discounts on existing tva types

    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('facade');
    var total_model = facade.request('get:model', 'total');
    var used_tvas = total_model.tva_values();
    this.tva_options = tva_options.filter(function (tva) {
      return used_tvas.indexOf(tva.value) != -1;
    });
  },
  onRender: function onRender() {
    var label;
    this.showChildView('order', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
    this.showChildView('description', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('description'),
      title: "Description",
      field_name: "description",
      tinymce: true,
      cid: this.model.cid,
      required: true
    }));

    if (this.compute_mode == 'ttc') {
      label = "Montant TTC";
    } else {
      label = "Montant HT";
    }

    this.showChildView('amount', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('amount'),
      label: label,
      field_name: "amount",
      required: true
    }));
    this.showChildView('tva', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      options: this.tva_options,
      title: "TVA",
      value: this.model.get('tva'),
      id_key: 'value',
      field_name: 'tva',
      required: true
    }));
  },
  templateContext: function templateContext() {
    return {
      title: this.getOption('title')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountFormView);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/DiscountPercentView.js":
/*!****************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/DiscountPercentView.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _base_behaviors_BaseFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../base/behaviors/BaseFormBehavior.js */ "./src/base/behaviors/BaseFormBehavior.js");
/* harmony import */ var _tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../tools */ "./src/tools.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");





var DiscountPercentView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  tagName: 'div',
  className: 'modal_overflow',
  behaviors: [_base_behaviors_BaseFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  template: __webpack_require__(/*! ./templates/DiscountPercentView.mustache */ "./src/task/views/composition/classic/discount/templates/DiscountPercentView.mustache"),
  regions: {
    'description': '.description',
    'percentage': '.percentage'
  },
  ui: {
    form: 'form',
    submit: "button[type=submit]",
    btn_cancel: "button[type=reset]"
  },
  triggers: {
    'click @ui.btn_cancel': 'cancel:form'
  },
  childViewTriggers: {
    'change': 'data:modified'
  },
  events: {
    'submit @ui.form': "onSubmit"
  },
  serializeForm: function serializeForm() {
    return (0,_tools__WEBPACK_IMPORTED_MODULE_3__.serializeForm)(this.getUI('form'));
  },
  syncFormWithModel: function syncFormWithModel() {
    /* Set form datas on the model and fires datas validation */
    var datas = this.serializeForm();
    this.model.set(datas, {
      validate: true
    });
    return datas;
  },
  onSubmit: function onSubmit(event) {
    event.preventDefault();
    var datas = this.syncFormWithModel();
    console.log("Validating data");
    console.log(this);
    console.log(datas);

    if (this.model.isValid(_.keys(datas))) {
      console.log("Data are valid");
      this.triggerMethod('insert:percent', this.model);
    }
  },
  onRender: function onRender() {
    this.showChildView('description', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      title: "Description",
      field_name: "description",
      tinymce: true,
      cid: '11111'
    }));
    this.showChildView('percentage', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      title: "Pourcentage",
      field_name: 'percentage',
      addon: "%"
    }));
    this.trigger('data:modified', 'description', '');
  },
  templateContext: function templateContext() {
    return {
      title: this.getOption('title')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountPercentView);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/DiscountView.js":
/*!*********************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/DiscountView.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../math.js */ "./src/math.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);





var template = __webpack_require__(/*! ./templates/DiscountView.mustache */ "./src/task/views/composition/classic/discount/templates/DiscountView.mustache");

var DiscountView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  tagName: 'tr',
  ui: {
    edit_button: 'button.edit',
    delete_button: 'button.delete'
  },
  // Trigger to the parent
  triggers: {
    'click @ui.edit_button': 'edit',
    'click @ui.delete_button': 'delete'
  },
  modelEvents: {
    'change': 'render'
  },
  initialize: function initialize() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
    this.tva_options = channel.request('get:options', 'tvas');
    this.user_session = channel;
  },
  getTvaLabel: function getTvaLabel() {
    var res = "";
    var current_value = this.model.get('tva');

    underscore__WEBPACK_IMPORTED_MODULE_0___default().each(this.tva_options, function (tva) {
      if (tva.value == current_value) {
        res = tva.name;
      }
    });

    return res;
  },
  templateContext: function templateContext() {
    return {
      edit: this.getOption('edit'),
      amount_label: this.model.amount_label(),
      tva_label: this.getTvaLabel()
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountView);

/***/ }),

/***/ "./src/task/views/composition/classic/discount/HtBeforeDiscountsView.js":
/*!******************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/HtBeforeDiscountsView.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../math.js */ "./src/math.js");
/* harmony import */ var _widgets_LabelRowWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../widgets/LabelRowWidget.js */ "./src/widgets/LabelRowWidget.js");



var HtBeforeDiscountsView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  className: 'table_container',
  template: __webpack_require__(/*! ./templates/HtBeforeDiscountsView.mustache */ "./src/task/views/composition/classic/discount/templates/HtBeforeDiscountsView.mustache"),
  regions: {
    line: ".line"
  },
  modelEvents: {
    'change': 'render'
  },
  onRender: function onRender() {
    var values = (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('ht_before_discounts'), false);
    var view = new _widgets_LabelRowWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: 'Total HT avant remise',
      values: values,
      colspan: 5
    });
    this.showChildView('line', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HtBeforeDiscountsView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/LinesComponent.js":
/*!********************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/LinesComponent.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_tools_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* harmony import */ var task_models_TaskGroupModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! task/models/TaskGroupModel.js */ "./src/task/models/TaskGroupModel.js");
/* harmony import */ var _TaskGroupCollectionView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TaskGroupCollectionView.js */ "./src/task/views/composition/classic/lines/TaskGroupCollectionView.js");
/* harmony import */ var _TaskGroupFormView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./TaskGroupFormView.js */ "./src/task/views/composition/classic/lines/TaskGroupFormView.js");






var LinesComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  /*
   * wrapper for TaskGroup and TaskLine collections
   * It takes the following parameters
       collection
          TaskLineGroups
       edit
           Can we edit groups and lines
       section
           The form section configuration
    *
   */
  template: __webpack_require__(/*! ./templates/LinesComponent.mustache */ "./src/task/views/composition/classic/lines/templates/LinesComponent.mustache"),
  tagName: 'div',
  className: 'form-section',
  regions: {
    errors: '.group-errors',
    container: '.group-container',
    modalRegion: ".group-modalregion"
  },
  ui: {
    add_button: 'button.add'
  },
  triggers: {
    "click @ui.add_button": "group:add"
  },
  childViewEvents: {
    'group:edit': 'onGroupEdit',
    'group:delete': 'onGroupDelete',
    'catalog:insert': 'onCatalogInsert'
  },
  collectionEvents: {
    'change': 'hideErrors'
  },
  initialize: function initialize(options) {
    this.collection = options['collection'];
    this.listenTo(this.collection, 'validated:invalid', this.showErrors);
    this.listenTo(this.collection, 'validated:valid', this.hideErrors.bind(this));
    this.edit = options['edit'];
    this.section = options['section'];
  },
  showErrors: function showErrors(model, errors) {
    this.detachChildView('errors');
    this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      errors: errors
    }));
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.detachChildView('errors');
    this.$el.removeClass('error');
  },
  onDeleteSuccess: function onDeleteSuccess() {
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_0__.displayServerSuccess)("Vos données ont bien été supprimées");
  },
  onDeleteError: function onDeleteError() {
    (0,backbone_tools_js__WEBPACK_IMPORTED_MODULE_0__.displayServerError)("Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  onGroupDelete: function onGroupDelete(childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce chapitre ?");

    if (result) {
      childView.model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError
      });
    }
  },
  onGroupEdit: function onGroupEdit(childView) {
    this.showTaskGroupForm(childView.model, "Modifier ce chapitre", true);
  },
  onGroupAdd: function onGroupAdd() {
    var model = new task_models_TaskGroupModel_js__WEBPACK_IMPORTED_MODULE_2__.default({
      order: this.collection.getMaxOrder() + 1
    });
    this.showTaskGroupForm(model, "Ajouter un chapitre", false);
  },
  showTaskGroupForm: function showTaskGroupForm(model, title, edit) {
    var form = new _TaskGroupFormView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      model: model,
      title: title,
      destCollection: this.collection,
      section: this.section,
      edit: edit
    });
    this.showChildView('modalRegion', form);
  },
  onCatalogInsert: function onCatalogInsert(models) {
    var ids = [];
    models.forEach(function (item) {
      return ids.push(item.get('id'));
    });
    this.collection.load_from_catalog(ids);
    this.getChildView('modalRegion').triggerMethod('modal:close');
  },
  onChildviewDestroyModal: function onChildviewDestroyModal() {
    this.detachChildView('modalRegion');
    this.getRegion('modalRegion').empty();
  },
  onRender: function onRender() {
    var view = new _TaskGroupCollectionView_js__WEBPACK_IMPORTED_MODULE_3__.default({
      collection: this.collection,
      edit: this.edit,
      section: this.section
    });
    this.showChildView('container', view);
  },
  templateContext: function templateContext() {
    return {
      can_add: this.section['can_add'] && this.edit
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LinesComponent);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskGroupCollectionView.js":
/*!*****************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskGroupCollectionView.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TaskGroupView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TaskGroupView.js */ "./src/task/views/composition/classic/lines/TaskGroupView.js");


var TaskGroupCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().CollectionView.extend({
  tagName: 'div',
  childView: _TaskGroupView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  collectionEvents: {
    'change:reorder': 'render',
    'sync': 'render'
  },
  childViewOptions: function childViewOptions(model) {
    // Forward the edit option to the children
    return {
      edit: this.getOption('edit'),
      section: this.getOption('section')
    };
  },
  // Bubble up child view events
  childViewTriggers: {
    'edit': 'group:edit',
    'delete': 'group:delete',
    'catalog:insert': 'catalog:insert'
  },
  childViewEvents: {
    'order:up': "onChildViewOrderUp",
    'order:down': "onChildViewOrderDown"
  },
  onChildViewOrderUp: function onChildViewOrderUp(childView) {
    console.log("Moving group up");
    this.collection.moveUp(childView.model);
  },
  onChildViewOrderDown: function onChildViewOrderDown(childView) {
    console.log("Moving group down");
    this.collection.moveDown(childView.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskGroupCollectionView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskGroupFormView.js":
/*!***********************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskGroupFormView.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/* harmony import */ var common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! widgets/CheckboxWidget */ "./src/widgets/CheckboxWidget.js");









var template = __webpack_require__(/*! ./templates/TaskGroupFormView.mustache */ "./src/task/views/composition/classic/lines/templates/TaskGroupFormView.mustache");

var TaskGroupFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend({
  id: 'taskgroup-form-modal',
  template: template,
  regions: {
    'order': '.order',
    'title': '.taskgroup-title',
    'description': '.description',
    'display_details': '.field-display_details',
    'catalog_container': '#catalog-container'
  },
  ui: {
    main_tab: 'ul.nav-tabs li:first a'
  },
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  childViewEvents: {
    'catalog:edit': 'onCatalogEdit'
  },
  childViewTriggers: {
    'catalog:insert': 'catalog:insert',
    'change': 'data:modified'
  },
  modelEvents: {
    'set:product_group': 'refreshForm'
  },
  initialize: function initialize(options) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_5___default().channel('config');
    this.compute_mode = channel.request('get:options', 'compute_mode');
  },
  isAddView: function isAddView() {
    return !(0,tools_js__WEBPACK_IMPORTED_MODULE_3__.getOpt)(this, 'edit', false);
  },
  templateContext: function templateContext() {
    return {
      title: this.getOption('title'),
      add: this.isAddView()
    };
  },
  refreshForm: function refreshForm() {
    this.showChildView('order', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
    this.showChildView('title', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('title'),
      title: "Titre (optionnel)",
      description: "Titre du chapitre tel qu’affiché dans la sortie pdf, laissez vide pour ne pas le faire apparaître",
      field_name: "title"
    }));
    this.showChildView('description', new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      value: this.model.get('description'),
      title: "Description (optionnel)",
      field_name: "description",
      tinymce: true,
      cid: this.model.cid
    }));
    this.showChildView('display_details', new widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_6__.default({
      inline_label: "Afficher le détail des prestations dans le document final",
      description: "En décochant cette case, le chapitre apparaîtra comme une seule ligne de prestation, sans le détail des produits qui le composent.",
      field_name: "display_details",
      value: this.model.get('display_details')
    }));

    if (this.isAddView()) {
      this.getUI('main_tab').tab('show');
    }
  },
  onRender: function onRender() {
    this.refreshForm();

    if (this.isAddView()) {
      this.showChildView('catalog_container', new common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_4__.default({
        query_params: {
          type_: 'work'
        },
        multiple: true,
        url: AppOption['catalog_tree_url']
      }));
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskGroupFormView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskGroupTotalView.js":
/*!************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskGroupTotalView.js ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../tools.js */ "./src/tools.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../math.js */ "./src/math.js");
/* harmony import */ var _widgets_LabelRowWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../widgets/LabelRowWidget.js */ "./src/widgets/LabelRowWidget.js");





var TaskGroupTotalView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: __webpack_require__(/*! ./templates/LineContainerView.mustache */ "./src/task/views/composition/classic/lines/templates/LineContainerView.mustache"),
  tagName: 'tbody',
  regions: {
    line: {
      el: '.line',
      replaceElement: true
    }
  },
  collectionEvents: {
    'change': 'render',
    'remove': 'render',
    'add': 'render'
  },
  onRender: function onRender() {
    var configChannel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    var user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
    this.has_price_study = configChannel.request('get:options', 'has_price_study');
    var compute_mode = configChannel.request('get:options', 'compute_mode');
    var is_ttc_mode = compute_mode == 'ttc';
    var value, label;

    if (is_ttc_mode) {
      value = this.collection.ttc();
      label = 'Sous total TTC';
    } else {
      label = 'Sous total HT';
      value = this.collection.ht();
    }

    var view = new _widgets_LabelRowWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      label: label,
      values: user_prefs.request('formatAmount', value, false),
      colspan: (0,_tools_js__WEBPACK_IMPORTED_MODULE_1__.getOpt)(this, 'colspan', 5)
    });
    this.showChildView('line', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskGroupTotalView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskGroupView.js":
/*!*******************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskGroupView.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _TaskLineCollectionView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TaskLineCollectionView.js */ "./src/task/views/composition/classic/lines/TaskLineCollectionView.js");
/* harmony import */ var _TaskLineFormView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TaskLineFormView.js */ "./src/task/views/composition/classic/lines/TaskLineFormView.js");
/* harmony import */ var _models_TaskLineModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/TaskLineModel.js */ "./src/task/models/TaskLineModel.js");
/* harmony import */ var _TaskGroupTotalView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TaskGroupTotalView.js */ "./src/task/views/composition/classic/lines/TaskGroupTotalView.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../math.js */ "./src/math.js");
/* harmony import */ var _backbone_tools_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_7__);
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");










var template = __webpack_require__(/*! ./templates/TaskGroupView.mustache */ "./src/task/views/composition/classic/lines/templates/TaskGroupView.mustache");

var TaskGroupView = backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default().View.extend({
  tagName: 'div',
  className: 'taskline-group row quotation_item border_left_block composite content_double_padding',
  template: template,
  regions: {
    errors: '.errors',
    lines: {
      el: '.lines',
      replaceElement: true
    },
    modalRegion: ".modalregion",
    subtotal: {
      el: '.subtotal',
      replaceElement: true
    }
  },
  ui: {
    btn_add: ".btn-add",
    up_button: 'button.up',
    down_button: 'button.down',
    edit_button: 'button.edit',
    delete_button: 'button.delete'
  },
  triggers: {
    'click @ui.up_button': 'order:up',
    'click @ui.down_button': 'order:down',
    'click @ui.edit_button': 'edit',
    'click @ui.delete_button': 'delete'
  },
  events: {
    "click @ui.btn_add": "onLineAdd"
  },
  childViewEvents: {
    'line:edit': 'onLineEdit',
    'line:delete': 'onLineDelete',
    'catalog:insert': 'onCatalogInsert',
    'destroy:modal': 'render'
  },
  initialize: function initialize(options) {
    console.log("Options in TaskGroupView");
    console.log(options); // Collection of task lines

    this.collection = this.model.lines;
    this.listenTo(this.collection, 'sync', this.render.bind(this));
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_7___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_7___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_7___default().channel('user_preferences');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    this.listenTo(this.model, 'validated:invalid', this.showErrors);
    this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
    this.compute_mode = this.config.request('get:options', 'compute_mode');
    this.is_ttc_mode = this.compute_mode == 'ttc';
    this.edit = this.getOption('edit');
    this.section = options['section'];
    this.useDate = this.section.hasOwnProperty('date');
  },
  isEmpty: function isEmpty() {
    return this.model.lines.length === 0;
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  bindValidation: function bindValidation() {
    console.log("bindValidation");
    console.log(this.model);
    backbone_validation__WEBPACK_IMPORTED_MODULE_6___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_6___default().unbind(this);
  },
  showLines: function showLines() {
    /*
     * Show lines if it's not done yet
     */
    if (!_.isNull(this.getChildView('lines'))) {
      this.showChildView('lines', new _TaskLineCollectionView_js__WEBPACK_IMPORTED_MODULE_0__.default({
        collection: this.collection,
        edit: this.edit,
        is_ttc_mode: this.is_ttc_mode,
        section: this.section
      }));
    }
  },
  onRender: function onRender() {
    if (!this.isEmpty()) {
      this.showLines();
    }

    var totalColspan = 5;

    if (this.useDate) {
      totalColspan += 1;
    }

    this.showChildView('subtotal', new _TaskGroupTotalView_js__WEBPACK_IMPORTED_MODULE_3__.default({
      collection: this.collection,
      colspan: totalColspan
    }));
  },
  onLineEdit: function onLineEdit(childView) {
    this.showTaskLineForm(childView.model, "Modifier le produit", true);
  },
  onLineAdd: function onLineAdd() {
    var model = new _models_TaskLineModel_js__WEBPACK_IMPORTED_MODULE_2__.default({
      task_id: this.model.get('id'),
      order: this.collection.getMaxOrder() + 1
    });
    this.showTaskLineForm(model, "Ajouter un produit", false);
  },
  showTaskLineForm: function showTaskLineForm(model, title, edit) {
    var form = new _TaskLineFormView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      model: model,
      title: title,
      destCollection: this.collection,
      edit: edit,
      section: this.section
    });
    this.showChildView('modalRegion', form);
  },
  onDeleteSuccess: function onDeleteSuccess() {
    (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_5__.displayServerSuccess)("Vos données ont bien été supprimées");
  },
  onDeleteError: function onDeleteError() {
    (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_5__.displayServerError)("Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  onLineDelete: function onLineDelete(childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce produit ?");

    if (result) {
      childView.model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError
      });
    }
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var ids = [];
    sale_products.forEach(function (item) {
      return ids.push(item.get('id'));
    });
    this.collection.load_from_catalog(ids);
    this.getChildView('modalRegion').triggerMethod('modal:close');
  },
  onChildviewDestroyModal: function onChildviewDestroyModal() {
    this.getRegion('modalRegion').empty();
  },
  templateContext: function templateContext() {
    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    return {
      no_title_no_description: this.model.get('title') === '' && this.model.get('description') === '',
      group_description: this.model.get('description').replace('<p>', '').replace('</p>', ''),
      not_is_empty: !this.isEmpty(),
      total_ht: this.user_prefs.request('formatAmount', this.model.ht(), false),
      is_not_first: order != min_order,
      is_not_last: order != max_order,
      is_ttc_mode: this.is_ttc_mode,
      edit: this.edit,
      can_delete: this.section['can_delete'],
      hasDate: this.useDate,
      can_add: this.section['can_add'] && this.edit
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskGroupView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskLineCollectionView.js":
/*!****************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskLineCollectionView.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TaskLineView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TaskLineView.js */ "./src/task/views/composition/classic/lines/TaskLineView.js");


var TaskLineCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().CollectionView.extend({
  tagName: 'tbody',
  className: 'lines',
  childView: _TaskLineView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  collectionEvents: {
    'change:reorder': 'render'
  },
  childViewOptions: function childViewOptions(model) {
    // Forward the edit option to the children
    return {
      edit: this.getOption('edit'),
      is_ttc_mode: this.getOption('is_ttc_mode'),
      section: this.getOption('section')
    };
  },
  // Bubble up child view events
  childViewTriggers: {
    'edit': 'line:edit',
    'delete': 'line:delete'
  },
  childViewEvents: {
    'order:up': "onChildViewOrderUp",
    'order:down': "onChildViewOrderDown"
  },
  onChildViewOrderUp: function onChildViewOrderUp(childView) {
    console.log("Order up");
    this.collection.moveUp(childView.model);
  },
  onChildViewOrderDown: function onChildViewOrderDown(childView) {
    console.log("Order down");
    this.collection.moveDown(childView.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskLineCollectionView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskLineFormView.js":
/*!**********************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskLineFormView.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var tools_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tools.js */ "./src/tools.js");
/* harmony import */ var widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! widgets/DateWidget.js */ "./src/widgets/DateWidget.js");
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/* harmony import */ var base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_8__);
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");











var template = __webpack_require__(/*! ./templates/TaskLineFormView.mustache */ "./src/task/views/composition/classic/lines/templates/TaskLineFormView.mustache");

var TaskLineFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default().View.extend(base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_7__.default).extend({
  id: 'task_line_form',
  template: template,
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_5__.default],
  regions: {
    'order': '.order',
    'description': '.description',
    'date': '.date',
    'cost': '.cost',
    'quantity': '.quantity',
    'unity': '.unity',
    'tva': '.tva',
    'product_id': '.product_id',
    'catalogContainer': '#catalog-container'
  },
  ui: {
    main_tab: 'ul.nav-tabs li:first a'
  },
  // Bubble up child view events
  //
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified',
    'catalog:insert': 'catalog:insert'
  },
  modelEvents: {
    'set:product': 'refreshForm',
    'change:tva': 'refreshTvaProductSelect'
  },
  initialize: function initialize(options) {
    this.session = backbone_radio__WEBPACK_IMPORTED_MODULE_8___default().channel('session');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_8___default().channel('config');
    this.workunit_options = this.config.request('get:options', 'workunits');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.all_product_options = this.config.request('get:options', 'products');
    this.compute_mode = this.config.request('get:options', 'compute_mode');
    this.section = options['section'];
  },
  isAddView: function isAddView() {
    return !(0,tools_js__WEBPACK_IMPORTED_MODULE_0__.getOpt)(this, 'edit', false);
  },
  templateContext: function templateContext() {
    return {
      dateRequired: this.section['date'],
      title: this.getOption('title'),
      add: this.isAddView()
    };
  },
  showOrderfield: function showOrderfield() {
    this.showChildView('order', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
  },
  showDescriptionField: function showDescriptionField() {
    this.showChildView('description', new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      value: this.model.get('description'),
      title: "Description",
      field_name: "description",
      tinymce: true,
      required: true,
      cid: this.model.cid
    }));
  },
  showDateField: function showDateField() {
    this.showChildView('date', new widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      value: this.model.get('date'),
      default_value: null,
      title: "Date d'exécution",
      field_name: "date",
      required: true
    }));
  },
  showCostField: function showCostField() {
    var field_config = this.section['cost'];
    var label = "Prix unitaire ";

    if (this.compute_mode == 'ttc') {
      label += "TTC";
    } else {
      label += "HT";
    }

    this.showChildView('cost', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('cost'),
      title: label,
      field_name: "cost",
      required: true,
      addon: "€",
      editable: field_config['edit']
    }));
  },
  showQuantityField: function showQuantityField() {
    var field_config = this.section['quantity'];
    this.showChildView('quantity', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('quantity'),
      title: "Quantité",
      field_name: "quantity",
      required: true,
      editable: field_config['edit']
    }));
  },
  showUnityField: function showUnityField() {
    this.showChildView('unity', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      options: this.workunit_options,
      title: "Unité",
      value: this.model.get('unity'),
      field_name: 'unity',
      id_key: 'value'
    }));
  },
  showTvaField: function showTvaField() {
    var field_config = this.section['tva'];
    var tva_value = this.model.get('tva');
    this.showChildView('tva', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      options: this.tva_options,
      title: "TVA",
      value: tva_value,
      field_name: 'tva',
      id_key: 'value',
      required: true,
      editable: field_config['edit']
    }));
  },
  refreshForm: function refreshForm() {
    this.showOrderfield();
    this.showDescriptionField();
    var hasDate = this.section['date'];

    if (hasDate) {
      this.showDateField();
    }

    this.showCostField();
    this.showQuantityField();
    this.showUnityField();
    this.showTvaField();
    this.refreshTvaProductSelect();

    if (this.isAddView()) {
      this.getUI('main_tab').tab('show');
    }
  },
  getCurrentTvaId: function getCurrentTvaId() {
    /* Override the default method of the TvaProductFormMixin
     */
    var tva_id;

    if (this.model.has('tva')) {
      var tva_value = this.model.get('tva');

      var tva_object = _.findWhere(this.tva_options, {
        value: parseFloat(tva_value)
      });

      if (tva_object) {
        tva_id = tva_object.id;
      }
    }

    return tva_id;
  },
  onRender: function onRender() {
    this.refreshForm();

    if (this.isAddView()) {
      this.showChildView('catalogContainer', new common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__.default({
        query_params: {
          type_: 'product'
        },
        multiple: true,
        url: AppOption['catalog_tree_url']
      }));
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskLineFormView);

/***/ }),

/***/ "./src/task/views/composition/classic/lines/TaskLineView.js":
/*!******************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/TaskLineView.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var date_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! date.js */ "./src/date.js");
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! math.js */ "./src/math.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);






var template = __webpack_require__(/*! ./templates/TaskLineView.mustache */ "./src/task/views/composition/classic/lines/templates/TaskLineView.mustache");

var TaskLineView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  tagName: 'tr',
  className: 'row taskline',
  template: template,
  ui: {
    up_button: 'button.up',
    down_button: 'button.down',
    edit_button: 'button.edit',
    delete_button: 'button.delete'
  },
  triggers: {
    'click @ui.up_button': 'order:up',
    'click @ui.down_button': 'order:down',
    'click @ui.edit_button': 'edit',
    'click @ui.delete_button': 'delete'
  },
  modelEvents: {
    'change': 'render'
  },
  initialize: function initialize(options) {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('config');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('user_preferences');
    this.section = options['section'];
    this.useDate = this.section.hasOwnProperty('date');
  },
  getTvaLabel: function getTvaLabel() {
    var res = "";
    var current_value = this.model.get('tva');

    underscore__WEBPACK_IMPORTED_MODULE_0___default().each(this.tva_options, function (tva) {
      if (tva.value == current_value) {
        res = tva.name;
      }
    });

    return res;
  },
  getProductLabel: function getProductLabel() {
    var res = "";
    var current_value = this.model.get('product_id');

    underscore__WEBPACK_IMPORTED_MODULE_0___default().each(this.product_options, function (product) {
      if (product.id == current_value) {
        res = product.label;
      }
    });

    return res;
  },
  templateContext: function templateContext() {
    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    var full_cost = (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.model.get('cost'), false);
    var cost = this.user_prefs.request('formatAmount', this.model.get('cost'), false);
    var is_rounded = cost != full_cost;
    var date = this.model.get('date');
    return {
      ht: this.user_prefs.request('formatAmount', this.model.ht(), false),
      ttc: this.user_prefs.request('formatAmount', this.model.ttc(), false),
      date: date ? (0,date_js__WEBPACK_IMPORTED_MODULE_1__.formatDate)(date) : 'non précisée',
      hasDate: this.useDate,
      product: this.getProductLabel(),
      tva_label: this.getTvaLabel(),
      is_not_first: order != min_order,
      is_not_last: order != max_order,
      cost_amount: cost,
      full_cost_amount: full_cost,
      is_rounded: is_rounded,
      edit: this.getOption('edit'),
      is_ttc_mode: this.getOption('is_ttc_mode'),
      can_delete: this.section['can_delete']
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskLineView);

/***/ }),

/***/ "./src/task/views/composition/index.js":
/*!*********************************************!*\
  !*** ./src/task/views/composition/index.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ showCompositionComponent)
/* harmony export */ });
/* harmony import */ var _price_study_App_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./price_study/App.js */ "./src/task/views/composition/price_study/App.js");
/* harmony import */ var _progress_invoicing_App__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress_invoicing/App */ "./src/task/views/composition/progress_invoicing/App.js");
/* harmony import */ var _classic_ClassicCompositionComponent__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./classic/ClassicCompositionComponent */ "./src/task/views/composition/classic/ClassicCompositionComponent.js");



function showCompositionComponent(region, section, totalmodel) {
  console.log(section);

  if (section['mode'] == 'price_study') {
    var subapp = new _price_study_App_js__WEBPACK_IMPORTED_MODULE_0__.default({
      region: region
    });
    subapp.start({
      section: section['price_study'],
      edit: section['edit']
    });
  } else if (section['mode'] == 'progress_invoicing') {
    var _subapp = new _progress_invoicing_App__WEBPACK_IMPORTED_MODULE_1__.default({
      region: region
    });

    _subapp.start({
      section: section['progress_invoicing'],
      edit: section['edit']
    });
  } else {
    console.log("Build the Classic Composition component");
    var view = new _classic_ClassicCompositionComponent__WEBPACK_IMPORTED_MODULE_2__.default({
      section: section['classic'],
      edit: section['edit'],
      totalmodel: totalmodel
    });
    region.show(view);
  }
}

/***/ }),

/***/ "./src/task/views/composition/price_study/App.js":
/*!*******************************************************!*\
  !*** ./src/task/views/composition/price_study/App.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Router_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Router.js */ "./src/task/views/composition/price_study/components/Router.js");
/* harmony import */ var _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./views/RootComponent.js */ "./src/task/views/composition/price_study/views/RootComponent.js");
/* harmony import */ var _components_Controller_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Controller.js */ "./src/task/views/composition/price_study/components/Controller.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");






var AppClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().Application.extend({
  channelName: 'priceStudyApp',
  radioEvents: {
    "navigate": 'onNavigate',
    "show:modal": "onShowModal",
    "product:delete": "onModelDelete",
    "discount:add": "onDiscountAdd",
    "discount:delete": "onModelDelete",
    "product:duplicate": "onProductDuplicate",
    "chapter:delete": "onModelDelete",
    "model:move:up": "onModelMoveUp",
    "model:move:down": "onModelMoveDown",
    "work:additem": "onWorkItemAdd",
    "workitem:edit": "onWorkItemEdit",
    "workitem:delete": "onModelDelete",
    "workitem:duplicate": "onWorkItemDuplicate",
    // product changed
    "product:changed": "onProductChanged",
    "reset:margin_rate": "onResetMarginRate"
  },
  radioRequests: {
    "insert:from:catalog": "onInsertFromCatalog"
  },
  onBeforeStart: function onBeforeStart(app, options) {
    var _this = this;

    console.log("AppClass.onBeforeStart");
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.price_study = this.facade.request('get:model', 'price_study');
    this.chapters = this.facade.request('get:collection', 'price_study_chapters');
    this.discounts = this.facade.request('get:collection', 'discounts');
    var model = this.facade.request('get:model', 'display_options');
    this.rootView = new _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: this.price_study,
      collection: this.chapters,
      section: options['section']
    });
    this.controller = new _components_Controller_js__WEBPACK_IMPORTED_MODULE_4__.default({
      rootView: this.rootView,
      priceStudy: this.price_study,
      chapters: this.chapters,
      section: options['section']
    });
    console.log("Creating the Price Study router");
    this.listenTo(model, 'saved:display_units', function () {
      return _this.onNavigate('index');
    });
    new _components_Router_js__WEBPACK_IMPORTED_MODULE_2__.default({
      controller: this.controller
    });
    console.log("Price Study AppClass.onBeforeStart finished");
  },
  onStart: function onStart(app, options) {
    console.log("Price Study App onStart");
    this.showView(this.rootView);
  },
  onNavigate: function onNavigate(route_name, parameters) {
    console.log("App.onNavigate");
    var dest_route = route_name;

    if (!_.isUndefined(parameters)) {
      dest_route += "/" + parameters;
    }

    window.location.hash = dest_route;
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.loadUrl(dest_route);
  },
  onModelDelete: function onModelDelete(model) {
    var _this2 = this;

    var promise = this.controller.modelDelete(model);

    if (promise) {
      promise.then(function () {
        return _this2.onProductChanged(null);
      });
    }
  },
  onProductDuplicate: function onProductDuplicate(product, chapterId) {
    this.controller.duplicateProduct(product, chapterId);
  },
  onWorkItemAdd: function onWorkItemAdd(workModel) {
    this.controller.addWorkItem(workModel);
  },
  onWorkItemEdit: function onWorkItemEdit(model) {
    this.controller.editWorkItem(model);
  },
  onWorkItemDuplicate: function onWorkItemDuplicate(model) {
    this.controller.duplicateWorkItem(model);
  },
  onModelMoveUp: function onModelMoveUp(model) {
    this.controller.moveUp(model);
  },
  onModelMoveDown: function onModelMoveDown(model) {
    this.controller.moveDown(model);
  },
  onInsertFromCatalog: function onInsertFromCatalog(collection, sale_products) {
    console.log("App.onInsertFromCatalog");
    return this.controller.insertFromCatalog(collection, sale_products);
  },
  onProductChanged: function onProductChanged(product) {
    var _this3 = this;

    $.when([this.price_study.fetch(), this.chapters.fetch(), this.discounts.fetch()]).then(function () {
      window.location.hash = "#index";

      _this3.facade.trigger('changed:task');
    });
  },
  onResetMarginRate: function onResetMarginRate() {
    this.controller.resetMarginRate();
  },
  onDiscountAdd: function onDiscountAdd() {
    this.controller.addDiscount();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AppClass);

/***/ }),

/***/ "./src/task/views/composition/price_study/components/Controller.js":
/*!*************************************************************************!*\
  !*** ./src/task/views/composition/price_study/components/Controller.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var task_models_price_study_ChapterModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! task/models/price_study/ChapterModel.js */ "./src/task/models/price_study/ChapterModel.js");
/* harmony import */ var task_models_price_study_ProductModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! task/models/price_study/ProductModel.js */ "./src/task/models/price_study/ProductModel.js");
/* harmony import */ var task_models_price_study_WorkModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! task/models/price_study/WorkModel.js */ "./src/task/models/price_study/WorkModel.js");
/* harmony import */ var task_models_price_study_DiscountModel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! task/models/price_study/DiscountModel.js */ "./src/task/models/price_study/DiscountModel.js");
/* harmony import */ var task_models_price_study_WorkItemModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! task/models/price_study/WorkItemModel */ "./src/task/models/price_study/WorkItemModel.js");
/* harmony import */ var tools__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tools */ "./src/tools.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");








var Controller = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().Object.extend({
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    console.log("Price Study Controller.initialize");
    this.rootView = options['rootView'];
    this.priceStudy = options['priceStudy'];
    this.chapters = options['chapters'];
  },
  index: function index() {
    console.log("Price Study Controller.index");
    this.rootView.index();
  },
  showModal: function showModal(view) {
    this.rootView.showModal(view);
  },
  // Chapter related views
  addChapter: function addChapter() {
    var collection = this.chapters;
    var options = {
      order: collection.getMaxOrder() + 1
    };
    var model = new task_models_price_study_ChapterModel_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.rootView.showAddChapterForm(model, collection);
  },
  editChapter: function editChapter(chapterId) {
    var model = this.chapters.get(chapterId);
    this.rootView.showEditChapterForm(model, this.chapters);
  },
  // Product related views
  addProduct: function addProduct(chapterId) {
    var chapterModel = this.chapters.get(chapterId);
    var collection = chapterModel.products;
    var defaults = {
      order: collection.getMaxOrder() + 1,
      chapter_id: chapterId
    };
    Object.assign(defaults, this.productDefaults);
    var model = new task_models_price_study_ProductModel_js__WEBPACK_IMPORTED_MODULE_2__.default(defaults);
    this.rootView.showAddProductForm(model, collection);
  },
  editProduct: function editProduct(chapterId, modelId) {
    var chapterModel = this.chapters.get(chapterId);
    var collection = chapterModel.products;
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditProductForm.bind(this.rootView));
  },
  duplicateProduct: function duplicateProduct(model, chapterId) {
    var request = model.duplicate();
    var this_ = this;
    request.done(function (model) {
      var route;

      if (model.get('type_') == 'price_study_work') {
        route = "chapters/" + chapterId + "/works/";
        this_.rootView.showEditWorkForm(model);
      } else {
        route = "chapters/" + chapterId + "/products/";
        this_.rootView.showEditProductForm(model);
      }

      var dest_route = route + model.get('id');
      window.location.hash = dest_route;
    });
  },
  insertFromCatalog: function insertFromCatalog(collection, sale_products) {
    var _this = this;

    console.log("Controller.insertFromCatalog");
    var sale_product_ids = sale_products.map(function (item) {
      return item.id;
    });

    var url = _.result(collection, 'url');

    url += "?action=load_from_catalog";
    var serverRequest = (0,tools__WEBPACK_IMPORTED_MODULE_6__.ajax_call)(url, {
      sale_product_ids: sale_product_ids
    }, 'POST');
    return serverRequest.then(function (models) {
      _this.facade.trigger('task:changed');

      return collection.fetch().then(function () {
        return models;
      });
    });
  },
  // Work related views
  addWork: function addWork(chapterId) {
    var chapterModel = this.chapters.get(chapterId);
    var collection = chapterModel.products;
    var defaults = {
      order: collection.getMaxOrder() + 1
    };
    var model = new task_models_price_study_WorkModel_js__WEBPACK_IMPORTED_MODULE_3__.default(defaults);
    this.rootView.showAddWorkForm(model, collection);
  },
  editWork: function editWork(chapterId, modelId) {
    var chapterModel = this.chapters.get(chapterId);
    var collection = chapterModel.products;
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditWorkForm.bind(this.rootView));
  },
  addWorkItem: function addWorkItem(workModel) {
    var collection = workModel.items;
    var defaults = {
      order: collection.getMaxOrder() + 1
    };

    if (workModel.get('margin_rate')) {
      defaults['margin_rate_editable'] = false;
    }

    var model = new task_models_price_study_WorkItemModel__WEBPACK_IMPORTED_MODULE_5__.default(defaults);
    this.rootView.showAddWorkItemForm(model, collection);
  },
  // WorkItem 
  editWorkItem: function editWorkItem(model) {
    this.rootView.showEditWorkItemForm(model);
  },
  duplicateWorkItem: function duplicateWorkItem(model) {
    var _this2 = this;

    var request = model.duplicate();
    request.done(function (model) {
      return _this2.rootView.showEditWorkItemForm(model);
    });
  },
  // Remise : Discount
  addDiscount: function addDiscount() {
    var collection = this.facade.request('get:collection', 'discounts');
    var model = new task_models_price_study_DiscountModel_js__WEBPACK_IMPORTED_MODULE_4__.default({
      order: collection.getMaxOrder() + 1,
      type_: 'amount'
    });
    this.rootView.showAddDiscountForm(model, collection);
  },
  editDiscount: function editDiscount(modelId) {
    var collection = this.facade.request('get:collection', 'discounts');
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditDiscountForm.bind(this.rootView));
  },
  // Common model views
  _onModelDeleteSuccess: function _onModelDeleteSuccess() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('success', this, "Vos données ont bien été supprimées");
    this.rootView.index();
  },
  _onModelDeleteError: function _onModelDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  modelDelete: function modelDelete(model) {
    console.log("Controller.modelDelete");
    var confirm = window.confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");
    var promise;

    if (confirm) {
      promise = model.destroy({
        success: this._onModelDeleteSuccess.bind(this),
        error: this._onModelDeleteError.bind(this),
        wait: true
      });
    }

    return promise;
  },
  moveUp: function moveUp(model) {
    var _this3 = this;

    var promise = model.collection.moveUp(model);
    promise.then(function () {
      return _this3.index();
    });
  },
  moveDown: function moveDown(model) {
    var _this4 = this;

    var promise = model.collection.moveDown(model);
    promise.then(function () {
      return _this4.index();
    });
  },
  resetMarginRate: function resetMarginRate() {
    var url = _.result(this.priceStudy, 'url');

    url += "?reset_margin_rate";
    var request = (0,tools__WEBPACK_IMPORTED_MODULE_6__.ajax_call)(url, {}, 'POST');
    request.then(this.chapters.fetch());
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Controller);

/***/ }),

/***/ "./src/task/views/composition/price_study/components/Router.js":
/*!*********************************************************************!*\
  !*** ./src/task/views/composition/price_study/components/Router.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var marionette_approuter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! marionette.approuter */ "./node_modules/marionette.approuter/lib/marionette.approuter.esm.js");

var Router = marionette_approuter__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  appRoutes: {
    '': 'index',
    'index': 'index',
    'addchapter': "addChapter",
    "chapters/:id": "editChapter",
    'chapters/:id/addproduct': 'addProduct',
    'chapters/:id/products/:id': 'editProduct',
    'chapters/:id/addwork': "addWork",
    "chapters/:id/works/:id": "editWork",
    'adddiscount': "addDiscount",
    "discounts/:id": "editDiscount"
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Router);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/BlockView.js":
/*!*******************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/BlockView.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PriceStudyView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PriceStudyView.js */ "./src/task/views/composition/price_study/views/PriceStudyView.js");
/* harmony import */ var _discount_DiscountComponent_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./discount/DiscountComponent.js */ "./src/task/views/composition/price_study/views/discount/DiscountComponent.js");
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* harmony import */ var _chapter_ChapterCollectionView_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chapter/ChapterCollectionView.js */ "./src/task/views/composition/price_study/views/chapter/ChapterCollectionView.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * Module name : BlockView
 */







var template = __webpack_require__(/*! ./templates/BlockView.mustache */ "./src/task/views/composition/price_study/views/templates/BlockView.mustache");

var BlockView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  template: template,
  regions: {
    common: '.common',
    products: '.products',
    discounts: '#discounts',
    errors: '.errors'
  },
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.section = options['section'];
  },
  showPriceStudy: function showPriceStudy() {
    this.showChildView('common', new _PriceStudyView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      model: this.model,
      edit: this.section['edit'],
      section: this.section['common']
    }));
  },
  showChapters: function showChapters() {
    console.log("Show collection of chapters");
    var view = new _chapter_ChapterCollectionView_js__WEBPACK_IMPORTED_MODULE_4__.default({
      collection: this.collection,
      childViewOptions: {
        edit: this.section['edit'],
        section: this.section['products']
      }
    });
    this.showChildView('products', view);
  },
  onRender: function onRender() {
    this.showPriceStudy();
    this.showChapters();
    this.showDiscounts();
  },
  showDiscounts: function showDiscounts() {
    var collection = this.facade.request("get:collection", 'discounts');
    this.showChildView('discounts', new _discount_DiscountComponent_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: collection,
      editable: this.section['edit']
    }));
  },
  templateContext: function templateContext() {
    return {
      editable: this.section['edit']
    };
  },
  formOk: function formOk() {
    var result = true;
    var errors = this.facade.request('is:valid');

    if (!_.isEmpty(errors)) {
      this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_3__.default({
        errors: errors
      }));
      result = false;
    } else {
      this.detachChildView('errors');
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BlockView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/PriceStudyView.js":
/*!************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/PriceStudyView.js ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/CheckboxWidget */ "./src/widgets/CheckboxWidget.js");
8;
/*
 * Module name : PriceStudyView
 */







var template = __webpack_require__(/*! ./templates/PriceStudyView.mustache */ "./src/task/views/composition/price_study/views/templates/PriceStudyView.mustache");
/**
 * View presenting main Price Study parameters configuration 
 * - general overhead
 * - reset margin rate to global value
 */


var PriceStudyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  className: 'form-section',
  behaviors: [base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  template: template,
  regions: {
    general_overhead: '.field-general_overhead',
    margin_rate: '.field-margin_rate',
    mask_hours: ".field-mask_hours"
  },
  // Bubble up child view events
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:persist'
  },
  childViewEvents: {
    "margin_rate:reset": "onMarginRateReset"
  },
  initialize: function initialize() {
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    this.section = this.getOption('section') || {};
  },
  showMaskHours: function showMaskHours() {
    var view = new widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_3__.default({
      'field_name': 'mask_hours',
      'title': '',
      inline_label: "Masquer les unités de main d’œuvre dans le PDF",
      description: "Pour les prestations de main d’œuvre, l’unité affichée dans le PDF final sera « forfait ». ",
      true_val: true,
      false_val: false,
      value: this.model.get('mask_hours')
    });
    this.showChildView('mask_hours', view);
  },
  showGeneralOverhead: function showGeneralOverhead() {
    this.showChildView('general_overhead', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: "general_overhead",
      title: "Coefficient de frais généraux",
      required: true,
      value: this.model.get('general_overhead')
    }));
  },
  showMarginRate: function showMarginRate() {
    var label = "Remettre tous les coefficients de marge à la valeur par défaut (" + this.model.get('margin_rate') + ")";
    this.showChildView('margin_rate', new widgets_CheckboxWidget__WEBPACK_IMPORTED_MODULE_3__.default({
      title: "",
      inline_label: label,
      value: 0,
      finishEventName: "margin_rate:reset"
    }));
  },
  onMarginRateReset: function onMarginRateReset() {
    this.app.trigger('reset:margin_rate');
  },
  onRender: function onRender() {
    this.showMaskHours();

    if (this.section.general_overhead.edit) {
      this.showGeneralOverhead();
    }

    if (this.section.margin_rate.edit && this.model.get('margin_rate')) {
      this.showMarginRate();
    }
  },
  onSuccessSync: function onSuccessSync() {
    this.app.trigger('product:changed');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PriceStudyView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/RootComponent.js":
/*!***********************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/RootComponent.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _discount_DiscountForm_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./discount/DiscountForm.js */ "./src/task/views/composition/price_study/views/discount/DiscountForm.js");
/* harmony import */ var _product_ProductForm_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product/ProductForm.js */ "./src/task/views/composition/price_study/views/product/ProductForm.js");
/* harmony import */ var _BlockView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BlockView.js */ "./src/task/views/composition/price_study/views/BlockView.js");
/* harmony import */ var _workform_WorkForm_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./workform/WorkForm.js */ "./src/task/views/composition/price_study/views/workform/WorkForm.js");
/* harmony import */ var _chapter_ChapterForm_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./chapter/ChapterForm.js */ "./src/task/views/composition/price_study/views/chapter/ChapterForm.js");
/* harmony import */ var _workform_WorkItemForm_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./workform/WorkItemForm.js */ "./src/task/views/composition/price_study/views/workform/WorkItemForm.js");
/*
 * Module name : RootComponent
 */








var template = __webpack_require__(/*! ./templates/RootComponent.mustache */ "./src/task/views/composition/price_study/views/templates/RootComponent.mustache");

var RootComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  className: '',
  template: template,
  regions: {
    main: '.main',
    modalContainer: '.modal-container'
  },
  initialize: function initialize(options) {
    this.initialized = false;
    this.section = options['section'];
  },
  index: function index() {
    this.initialized = true;
    var view = new _BlockView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: this.model,
      collection: this.collection,
      section: this.section
    });
    this.showChildView('main', view);
  },
  showModal: function showModal(view) {
    this.showChildView('modalContainer', view);
  },
  _showFormInModal: function _showFormInModal(FormClass, model, collection, edit) {
    this.index();
    var view = new FormClass({
      model: model,
      destCollection: collection,
      edit: edit
    });
    this.showModal(view);
  },
  _showChapterForm: function _showChapterForm(model, collection, edit) {
    this._showFormInModal(_chapter_ChapterForm_js__WEBPACK_IMPORTED_MODULE_4__.default, model, collection, edit);
  },
  showAddChapterForm: function showAddChapterForm(model, collection) {
    this._showChapterForm(model, collection, false);
  },
  showEditChapterForm: function showEditChapterForm(model, collection) {
    this._showChapterForm(model, collection, true);
  },
  _showProductForm: function _showProductForm(model, collection, edit) {
    this._showFormInModal(_product_ProductForm_js__WEBPACK_IMPORTED_MODULE_1__.default, model, collection, edit);
  },
  showAddProductForm: function showAddProductForm(model, collection) {
    this._showProductForm(model, collection, false);
  },
  showEditProductForm: function showEditProductForm(model) {
    this._showProductForm(model, model.collection, true);
  },
  _showDiscountForm: function _showDiscountForm(model, collection, edit) {
    this._showFormInModal(_discount_DiscountForm_js__WEBPACK_IMPORTED_MODULE_0__.default, model, collection, edit);
  },
  showAddDiscountForm: function showAddDiscountForm(model, collection) {
    this._showDiscountForm(model, collection, false);
  },
  showEditDiscountForm: function showEditDiscountForm(model) {
    this._showDiscountForm(model, model.collection, true);
  },
  _showWorkForm: function _showWorkForm(model, collection, edit) {
    this._showFormInModal(_workform_WorkForm_js__WEBPACK_IMPORTED_MODULE_3__.default, model, collection, edit);
  },
  showAddWorkForm: function showAddWorkForm(model, collection) {
    this._showWorkForm(model, collection, false);
  },
  showEditWorkForm: function showEditWorkForm(model) {
    this._showWorkForm(model, model.collection, true);
  },
  _showWorkItemForm: function _showWorkItemForm(model, collection, edit) {
    this._showFormInModal(_workform_WorkItemForm_js__WEBPACK_IMPORTED_MODULE_5__.default, model, collection, edit);
  },
  showAddWorkItemForm: function showAddWorkItemForm(model, collection) {
    this._showWorkItemForm(model, collection, false);
  },
  showEditWorkItemForm: function showEditWorkItemForm(model) {
    this._showWorkItemForm(model, model.collection, true);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RootComponent);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/ChapterCollectionView.js":
/*!***************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/ChapterCollectionView.js ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ChapterView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ChapterView */ "./src/task/views/composition/price_study/views/chapter/ChapterView.js");




var ChapterEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: underscore__WEBPACK_IMPORTED_MODULE_1___default().template('<div class="border_left_block composite content_double_padding"><p><em>Aucun élément n’a été ajouté, cliquez sur Ajouter un chapitre pour commencer à en ajouter.</em></p></div>')
});
var ChapterCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  childView: _ChapterView__WEBPACK_IMPORTED_MODULE_2__.default,
  emptyView: ChapterEmptyView,
  tagName: 'div',
  childViewTriggers: {},
  collectionEvents: {
    'change:reorder': 'render',
    'sync': 'render'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterCollectionView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/ChapterForm.js":
/*!*****************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/ChapterForm.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_behaviors_ModalFormBehavior__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var widgets_InputWidget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/InputWidget */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_TextAreaWidget__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/TextAreaWidget */ "./src/widgets/TextAreaWidget.js");






var template = __webpack_require__(/*! ./templates/ChapterForm.mustache */ "./src/task/views/composition/price_study/views/chapter/templates/ChapterForm.mustache");

var ChapterForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  partial: false,
  behaviors: [base_behaviors_ModalFormBehavior__WEBPACK_IMPORTED_MODULE_1__.default],
  template: template,
  regions: {
    "title": ".field-title",
    "description": ".field-description"
  },
  ui: {},
  events: {},
  childViewEvents: {},
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified',
    'cancel:click': 'cancel:click'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  },
  showTitle: function showTitle() {
    this.showChildView('title', new widgets_InputWidget__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('title'),
      title: "Titre (optionnel)",
      description: "Titre du chapitre tel qu'affiché dans la sortie pdf, laissez vide pour ne pas le faire apparaître",
      field_name: "title"
    }));
  },
  showDescription: function showDescription() {
    this.showChildView('description', new widgets_TextAreaWidget__WEBPACK_IMPORTED_MODULE_3__.default({
      value: this.model.get('description'),
      title: "Description (optionnel)",
      field_name: "description",
      tinymce: true,
      cid: this.model.cid
    }));
  },
  onRender: function onRender() {
    this.showTitle();
    this.showDescription();
  },
  templateContext: function templateContext() {
    // Collect data sent to the template (model attributes are already transmitted)
    return {
      title: this.getOption('edit') ? "Modifier ce chapitre" : "Ajouter un chapitre"
    };
  },
  onDestroyModal: function onDestroyModal() {
    console.log("Modal close");
    var app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    app.trigger('navigate', 'index');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterForm);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/ChapterView.js":
/*!*****************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/ChapterView.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _custom_views__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom_views */ "./src/task/views/composition/price_study/views/chapter/custom_views.js");
/* harmony import */ var widgets_DropDownWidget__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/DropDownWidget */ "./src/widgets/DropDownWidget.js");
/* harmony import */ var base_views_ErrorView__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! base/views/ErrorView */ "./src/base/views/ErrorView.js");
/* harmony import */ var widgets_ButtonCollectionWidget__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! widgets/ButtonCollectionWidget */ "./src/widgets/ButtonCollectionWidget.js");
/* harmony import */ var base_models_ButtonCollection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! base/models/ButtonCollection */ "./src/base/models/ButtonCollection.js");
/* harmony import */ var _widgets_ButtonWidget__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../../widgets/ButtonWidget */ "./src/widgets/ButtonWidget.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











var template = __webpack_require__(/*! ./templates/ChapterView.mustache */ "./src/task/views/composition/price_study/views/chapter/templates/ChapterView.mustache");

var ChapterView = backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default().View.extend({
  template: template,
  className: 'taskline-group row quotation_item border_left_block composite content_double_padding',
  regions: {
    'errors': '.errors',
    'addproduct': {
      el: '.chapter-addproduct',
      replaceElement: true
    },
    'addwork': {
      el: '.chapter-addwork',
      replaceElement: true
    }
  },
  events: {
    'click .btn.edit-chapter': 'onEdit',
    'click .btn.delete-chapter': 'onDelete',
    'click .btn.up-chapter': 'onMoveUp',
    'click .btn.down-chapter': 'onMoveDown',
    // Enfants insérés depuis les Vues en Raw Html (custom_views) et leur template
    // Produits et ouvrage
    'click .btn[data-action="product:duplicate"]': 'onProductDuplicate',
    'click .btn[data-action="product:delete"]': 'onProductDelete',
    'click .btn[data-action="work:additem"]': 'onProductAddWorkItem',
    'click .btn[data-action="product:down"]': 'onProductMoveDown',
    'click .btn[data-action="product:up"]': 'onProductMoveUp',
    'click .btn[data-action="product:change_details"]': "onWorkToggleDetail",
    // Items des ouvrages
    'click .btn[data-action="workitem:edit"]': 'onWorkItemEdit',
    'click .btn[data-action="workitem:duplicate"]': 'onWorkItemDuplicate',
    'click .btn[data-action="workitem:delete"]': 'onWorkItemDelete',
    'click .btn[data-action="workitem:down"]': 'onWorkItemMoveDown',
    'click .btn[data-action="workitem:up"]': 'onWorkItemMoveUp'
  },
  childViewEvents: {
    'action:clicked': "onButtonClicked"
  },
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    this.productCollection = this.model.products;
    this.setupEvents();
  },
  setupEvents: function setupEvents() {
    var _this = this;

    this.listenTo(this.productCollection, 'fetched', function () {
      return _this.render();
    });
    this.listenTo(this.productCollection, 'saved:display_details', function () {
      return _this.render();
    });
    this.listenTo(this.model.collection, 'fetched', function () {
      return _this.render();
    });
    this.listenTo(this.facade, 'bind:validation', function () {
      return backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().bind(_this);
    });
    this.listenTo(this.facade, 'unbind:validation', function () {
      return backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().unbind(_this);
    });
    this.listenTo(this.model, 'validated:invalid', this.showErrors);
    this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
    this.listenTo(this.productCollection, 'validated:invalid', this.showProductErrors);
  },
  showErrors: function showErrors(model, errors) {
    console.log("Show errors");
    console.log(errors);
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
    console.log("hide errors");
  },
  showProductErrors: function showProductErrors(model, errors) {
    this.$el.addClass('error');
    this.showChildView('errors', new base_views_ErrorView__WEBPACK_IMPORTED_MODULE_4__.default({
      errors: errors
    }));
  },
  showButtons: function showButtons(hasProducts) {
    var buttons = [{
      label: 'Ajouter un produit',
      title: 'Ajouter un produit à ce chapitre',
      icon: 'plus',
      showLabel: true,
      action: 'addproduct'
    }, {
      label: 'Ajouter un ouvrage',
      title: 'Ajouter un ouvrage à ce chapitre',
      icon: 'plus',
      showLabel: true,
      action: 'addwork'
    }];

    if (hasProducts) {
      buttons[0].label = "Produit";
      buttons[0]['css'] = "btn-primary";
      buttons[1].label = "Ouvrage";
      buttons[1]['css'] = "btn-primary";
    }

    var view = new _widgets_ButtonWidget__WEBPACK_IMPORTED_MODULE_7__.default(_objectSpread({
      showLabel: true,
      icon: "plus"
    }, buttons[0]));
    this.showChildView('addproduct', view);
    view = new _widgets_ButtonWidget__WEBPACK_IMPORTED_MODULE_7__.default(_objectSpread({
      showLabel: true,
      icon: "plus"
    }, buttons[1]));
    this.showChildView('addwork', view);
  },
  onRender: function onRender() {
    console.log("Rendering ChaterView");
    var hasProducts = this.productCollection.length > 0;
    this.showButtons(hasProducts);
  },
  templateContext: function templateContext() {
    console.log("Rendering"); // Collect data sent to the template (model attributes are already transmitted)

    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    var productHtml = (0,_custom_views__WEBPACK_IMPORTED_MODULE_2__.getProductCollectionHtml)(this.productCollection);
    return {
      is_not_first: order != min_order,
      is_not_last: order != max_order,
      edit: this.getOption('edit'),
      collectionRawHtml: productHtml,
      total_ht: this.user_prefs.request('formatAmount', this.model.get('total_ht'), true)
    };
  },
  onButtonClicked: function onButtonClicked(action) {
    this.app.trigger('navigate', 'chapters' + '/' + this.model.get('id') + '/' + action);
  },
  onEdit: function onEdit() {
    this.app.trigger('navigate', 'chapters' + '/' + this.model.get('id'));
  },
  onDelete: function onDelete() {
    this.app.trigger('chapter:delete', this.model);
  },
  onMoveUp: function onMoveUp() {
    this.app.trigger('model:move:up', this.model);
  },
  onMoveDown: function onMoveDown() {
    this.app.trigger('model:move:down', this.model);
  },

  /**
   * Product RawHtml subviews events management
   * 
   * @param {Event} event : The click event
   * 
   * @returns : The product Instance
   */
  getProductFromEvent: function getProductFromEvent(event) {
    var tag = $(event.currentTarget);
    var productId = parseInt(tag.data('id'));
    return this.productCollection.get(productId);
  },
  onProductDuplicate: function onProductDuplicate(event) {
    console.log("on Product Duplicate");
    var product = this.getProductFromEvent(event);
    console.log(product);
    this.app.trigger('product:duplicate', product, this.model.get('id'));
  },
  onProductDelete: function onProductDelete(event) {
    console.log("on Product Delete");
    var product = this.getProductFromEvent(event);
    console.log(product);
    this.app.trigger('product:delete', product);
  },
  onProductAddWorkItem: function onProductAddWorkItem(event) {
    console.log("on Product add WorkItem");
    var product = this.getProductFromEvent(event);
    console.log(product);
    this.app.trigger('work:additem', product);
  },
  onProductMoveDown: function onProductMoveDown(event) {
    var product = this.getProductFromEvent(event);
    this.app.trigger('model:move:down', product);
  },
  onProductMoveUp: function onProductMoveUp(event) {
    var product = this.getProductFromEvent(event);
    this.app.trigger('model:move:up', product);
  },
  onWorkToggleDetail: function onWorkToggleDetail(event) {
    var product = this.getProductFromEvent(event);
    var display = product.get('display_details');
    var newValues = {
      'display_details': !display
    };
    product.save(newValues);
  },

  /**
   * WorkItem RawHtml Subviews event management
   * 
   */

  /**
   * 
   * @param {Event} event : The onclick event
   * @returns A WorkItemModel instance
   */
  getWorkItemFromEvent: function getWorkItemFromEvent(event) {
    var tag = $(event.currentTarget);
    var workId = parseInt(tag.data('workid'));
    var workItemId = parseInt(tag.data('id'));
    var workModel = this.productCollection.get(workId);
    return workModel.items.get(workItemId);
  },
  onWorkItemEdit: function onWorkItemEdit(event) {
    var model = this.getWorkItemFromEvent(event);
    this.app.trigger('workitem:edit', model);
  },
  onWorkItemDuplicate: function onWorkItemDuplicate(event) {
    var model = this.getWorkItemFromEvent(event);
    this.app.trigger('workitem:duplicate', model);
  },
  onWorkItemDelete: function onWorkItemDelete(event) {
    var model = this.getWorkItemFromEvent(event);
    this.app.trigger('workitem:delete', model);
  },
  onWorkItemMoveDown: function onWorkItemMoveDown(event) {
    var model = this.getWorkItemFromEvent(event);
    this.app.trigger('model:move:down', model);
  },
  onWorkItemMoveUp: function onWorkItemMoveUp(event) {
    var model = this.getWorkItemFromEvent(event);
    this.app.trigger('model:move:up', model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/custom_views.js":
/*!******************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/custom_views.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getProductCollectionHtml": () => (/* binding */ getProductCollectionHtml),
/* harmony export */   "getProductViewHtml": () => (/* binding */ getProductViewHtml),
/* harmony export */   "getWorkViewHtml": () => (/* binding */ getWorkViewHtml),
/* harmony export */   "getWorkItemViewHtml": () => (/* binding */ getWorkItemViewHtml)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/**
 * Custom non marionette views used to render non hierarchical structures
 * 
 * Here we render the html snippets for the different products/work/work_item
 * elements that are rendered in tbody s
 * 
 */


var PRODUCT_VIEW_TEMPLATE = __webpack_require__(/*! ./templates/ProductRawView.mustache */ "./src/task/views/composition/price_study/views/chapter/templates/ProductRawView.mustache");

var WORK_VIEW_TEMPLATE = __webpack_require__(/*! ./templates/WorkRawView.mustache */ "./src/task/views/composition/price_study/views/chapter/templates/WorkRawView.mustache");

var WORK_ITEM_VIEW_TEMPLATE = __webpack_require__(/*! ./templates/WorkItemRawView.mustache */ "./src/task/views/composition/price_study/views/chapter/templates/WorkItemRawView.mustache");
/**
 * 
 * @param {BaseModel} model 
 * @returns template context variables used to order the model
 */


var _OrderContextVariable = function _OrderContextVariable(model) {
  var min_order = model.collection.getMinOrder();
  var max_order = model.collection.getMaxOrder();
  var order = model.get('order');
  return {
    is_not_first: order != min_order,
    is_not_last: order != max_order
  };
};

var _tva_labels = function _tva_labels(model) {
  var tva = model.tva_object();
  var result = {};

  if (tva) {
    result['tva_label'] = tva.label;
  }

  var product = model.product_object();

  if (product) {
    result['product_label'] = product.label;
  }

  return result;
};

var _modeContext = function _modeContext(model, index) {
  return {
    supplier_ht_mode: model.get('mode') === 'supplier_ht'
  };
};
/**
 * 
 * @returns template vars related to the current price study model
 */


var _priceStudyContext = function _priceStudyContext() {
  var model = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade').request('get:model', 'display_options');
  return {
    'display_units': model.get('display_units')
  };
};
/**
 * 
 * @param {BaseProductModel} model 
 * @param {integer} index 
 * @returns Template variables related to the Product 
 */


var _baseProductContext = function _baseProductContext(model, index) {
  var ctx = _OrderContextVariable(model);

  ctx = Object.assign(ctx, model.attributes);
  var user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel("user_preferences");

  for (var _i = 0, _arr = ['supplier_ht', 'ht', 'total_ht']; _i < _arr.length; _i++) {
    var key = _arr[_i];
    ctx[key] = user_prefs.request('formatAmount', model.get(key), true);
  }

  ctx = Object.assign(ctx, _tva_labels(model));
  ctx = Object.assign(ctx, _modeContext(model, index));
  ctx = Object.assign(ctx, _priceStudyContext());
  ctx["htmlIndex"] = index;
  return ctx;
};
/**
 * 
 * @param {ProductCollection} collection 
 * @returns The representation of the collection as a serie of tbodys
 */


var getProductCollectionHtml = function getProductCollectionHtml(collection) {
  var result = "";
  collection.each(function (model, index) {
    if (model.get('type_') == 'price_study_work') {
      result += getWorkViewHtml(model, index);
    } else {
      result += getProductViewHtml(model, index);
    }
  });
  return result;
};
/**
 * 
 * @param {ProductModel} model 
 * @param {number} index 
 * @returns The html representation of this product model
 */

var getProductViewHtml = function getProductViewHtml(model, index) {
  var ctx = _baseProductContext(model, index);

  return PRODUCT_VIEW_TEMPLATE(ctx);
};
/**
 * 
 * @param {WorkModel} model 
 * @param {number} index 
 * @returns An html representation of the model and its items
 */

var getWorkViewHtml = function getWorkViewHtml(model, index) {
  var itemsRawHtml = '';
  model.items.each(function (item, index) {
    itemsRawHtml += getWorkItemViewHtml(item, index, model);
  });

  var ctx = _baseProductContext(model, index);

  ctx["workitemsHtml"] = itemsRawHtml;
  return WORK_VIEW_TEMPLATE(ctx);
};
/**
 * 
 * @param {WorkItemModel} model 
 * @param {number} index 
 * @returns A html representation of the WorkItemModel
 */

var getWorkItemViewHtml = function getWorkItemViewHtml(model, index, work) {
  var ctx = Object.assign({}, model.attributes);
  ctx["htmlIndex"] = index;
  var user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');

  for (var _i2 = 0, _arr2 = ['ht', 'supplier_ht', 'work_unit_ht', 'total_ht']; _i2 < _arr2.length; _i2++) {
    var key = _arr2[_i2];
    ctx[key] = user_prefs.request('formatAmount', model.get(key), true);
  }

  ctx = Object.assign(ctx, _priceStudyContext());
  ctx = Object.assign(ctx, _OrderContextVariable(model));
  ctx = Object.assign(ctx, _modeContext(model, index));
  ctx['hidden'] = !work.get('display_details');

  if (model.get('quantity_inherited')) {
    ctx['quantity_label'] = "" + model.get('work_unit_quantity') + " x " + work.get('quantity');
  } else {
    ctx['quantity_label'] = false;
  }

  return WORK_ITEM_VIEW_TEMPLATE(ctx);
};

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/DiscountCollectionView.js":
/*!*****************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/DiscountCollectionView.js ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountView.js */ "./src/task/views/composition/price_study/views/discount/DiscountView.js");
/* harmony import */ var _DiscountEmptyView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DiscountEmptyView.js */ "./src/task/views/composition/price_study/views/discount/DiscountEmptyView.js");
/*
 * Module name : DiscountCollectionView
 */




var DiscountCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  tagName: 'tbody',
  className: "lines",
  childView: _DiscountView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  emptyView: _DiscountEmptyView_js__WEBPACK_IMPORTED_MODULE_2__.default,
  collectionEvents: {
    'change:reorder': 'render',
    sync: 'render'
  },
  childViewTriggers: {
    'edit': 'edit',
    'delete': 'delete',
    'down': 'down',
    'up': 'up'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountCollectionView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/DiscountComponent.js":
/*!************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/DiscountComponent.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountCollectionView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountCollectionView.js */ "./src/task/views/composition/price_study/views/discount/DiscountCollectionView.js");
/*
 * Module name : DiscountComponent
 */




var template = __webpack_require__(/*! ./templates/DiscountComponent.mustache */ "./src/task/views/composition/price_study/views/discount/templates/DiscountComponent.mustache");

var DiscountComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  regions: {
    collection: {
      el: 'tbody.lines',
      replaceElement: true
    }
  },
  // Listen to child view events
  childViewEvents: {
    "up": "onUp",
    "down": "onDown",
    "edit": "onEdit",
    'delete': "onDelete",
    'destroy:modal': 'render',
    'cancel:form': 'render'
  },
  events: {
    'click .add-discount': 'onAdd'
  },
  initialize: function initialize() {
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
  },
  onRender: function onRender() {
    if (this.collection.length > 0) {
      this.showChildView('collection', new _DiscountCollectionView_js__WEBPACK_IMPORTED_MODULE_1__.default({
        collection: this.collection
      }));
    }
  },
  templateContext: function templateContext() {
    return {
      editable: this.getOption('editable'),
      hasItems: this.collection.length > 0
    };
  },
  onUp: function onUp(model) {
    console.log("Moving up the element");
    this.app.trigger('model:move:up', model);
  },
  onDown: function onDown(model) {
    this.app.trigger('model:move:down', model);
  },
  onEdit: function onEdit(model) {
    var route = '/discounts/';
    this.app.trigger('navigate', route + model.get('id'));
  },
  onDelete: function onDelete(model) {
    this.app.trigger('discount:delete', model);
  },
  onAdd: function onAdd() {
    this.app.trigger('discount:add');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountComponent);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/DiscountEmptyView.js":
/*!************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/DiscountEmptyView.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : DiscountEmptyView
 */



var template = __webpack_require__(/*! ./templates/DiscountEmptyView.mustache */ "./src/task/views/composition/price_study/views/discount/templates/DiscountEmptyView.mustache");

var DiscountEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountEmptyView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/DiscountForm.js":
/*!*******************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/DiscountForm.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/*
 * Module name : DiscountForm
 */







var template = __webpack_require__(/*! ./templates/DiscountForm.mustache */ "./src/task/views/composition/price_study/views/discount/templates/DiscountForm.mustache");

var DiscountForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  // Send the whole model on submit
  partial: false,
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_4__.default],
  template: template,
  regions: {
    'order': '.field-order',
    'type_': '.field-type_',
    'description': '.field-description',
    'amount': '.field-amount',
    'percentage': '.field-percentage',
    'tva_id': '.field-tva_id'
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'finish': 'onDatasFinished'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    var tva_options = config.request('get:options', 'tvas');
    var price_study_model = facade.request('get:model', 'price_study');
    var tva_parts = Object.keys(price_study_model.get('tva_parts')).map(function (x) {
      return parseInt(x);
    });
    this.tva_options = tva_options.filter(function (tva) {
      return tva_parts.indexOf(tva.id) != -1;
    });
  },
  onRender: function onRender() {
    this.refreshFormFields();
  },
  refreshFormFields: function refreshFormFields() {
    this.emptyRegions();
    this.showTypeField();
    this.showOrderField();
    this.showDescriptionField();

    if (this.model.get('type_') == 'percentage') {
      this.showPercentageField();
    } else {
      this.showAmountField();
      this.showTvaField();
    }
  },
  getCommonFieldOptions: function getCommonFieldOptions(label, key, description) {
    var result = {
      field_name: key,
      label: label,
      description: description || '',
      value: this.model.get(key)
    };
    return result;
  },
  showOrderField: function showOrderField() {
    var options = this.getCommonFieldOptions('', 'order');
    options['type'] = 'hidden';
    var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('order', view);
  },
  showTypeField: function showTypeField() {
    var options = this.getCommonFieldOptions('Type de remise', 'type_');
    options['options'] = [{
      'id': 'amount',
      'label': 'Montant fixe'
    }, {
      'id': 'percentage',
      'label': 'Pourcentage (valeur calculée)'
    }];
    options['id_key'] = 'id';
    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('type_', view);
  },
  showDescriptionField: function showDescriptionField() {
    var options = this.getCommonFieldOptions('Description', 'description', 'Visible dans le document final');
    options['tinymce'] = true;
    options['cid'] = this.model.cid;
    var view = new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
    this.showChildView('description', view);
  },
  showPercentageField: function showPercentageField() {
    var options = this.getCommonFieldOptions('Pourcentage', 'percentage', 'Le montant HT et les taux de TVA seront calculés dynamiquement depuis les valeurs des prestations');
    var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('percentage', view);
  },
  showAmountField: function showAmountField() {
    var options = this.getCommonFieldOptions('Montant', 'amount');
    var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('amount', view);
  },
  showTvaField: function showTvaField() {
    var options = this.getCommonFieldOptions('TVA', 'tva_id');
    options['options'] = this.tva_options;
    options['id_key'] = 'id';
    options['label_key'] = 'name';
    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('tva_id', view);
  },
  templateContext: function templateContext() {
    var title;

    if (this.getOption('edit')) {
      title = "Modifier cette remise";
    } else {
      title = "Ajouter une remise";
    }

    return {
      title: title
    };
  },
  onDatasFinished: function onDatasFinished(key, value) {
    if (key == 'type_') {
      this.model.set(key, value);
      this.refreshFormFields();
    }
  },
  onDestroyModal: function onDestroyModal() {
    console.log("Modal close");
    this.app.trigger('navigate', 'index');
  },
  onSuccessSync: function onSuccessSync() {
    this.app.trigger('product:changed', this.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountForm);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/DiscountView.js":
/*!*******************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/DiscountView.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : DiscountView
 */



var template = __webpack_require__(/*! ./templates/DiscountView.mustache */ "./src/task/views/composition/price_study/views/discount/templates/DiscountView.mustache");

var DiscountView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template,
  tagName: 'tr',
  events: {
    'click button.delete': 'onDeleteClicked',
    'click button.edit': 'onEditClicked',
    "click button.up": "onUpClicked",
    "click button.down": "onDownClicked"
  },
  modelEvents: {
    'sync': 'render'
  },
  initialize: function initialize() {
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
  },
  templateContext: function templateContext() {
    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    return {
      tva_label: this.model.tva_label(),
      is_percentage: this.model.is_percentage(),
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false),
      is_not_first: order != min_order,
      is_not_last: order != max_order
    };
  },
  // On remonte les events jusqu'au DiscountComponent
  onDeleteClicked: function onDeleteClicked() {
    this.triggerMethod('delete', this.model);
  },
  onEditClicked: function onEditClicked() {
    this.triggerMethod('edit', this.model);
  },
  onUpClicked: function onUpClicked() {
    this.triggerMethod('up', this.model);
  },
  onDownClicked: function onDownClicked() {
    this.triggerMethod('down', this.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountView);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/product/ProductForm.js":
/*!*****************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/product/ProductForm.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/* harmony import */ var widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! widgets/RadioChoiceButtonWidget */ "./src/widgets/RadioChoiceButtonWidget.js");
/* harmony import */ var backbone_tools__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone-tools */ "./src/backbone-tools.js");
/*
 * Module name : ProductForm
 */











var template = __webpack_require__(/*! ./templates/ProductForm.mustache */ "./src/task/views/composition/price_study/views/product/templates/ProductForm.mustache");

var ProductForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default().View.extend(base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_4__.default).extend({
  partial: false,
  template: template,
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_5__.default],
  regions: {
    'order': '.field-order',
    'description': '.field-description',
    'mode': '.field-mode',
    'supplier_ht': '.field-supplier_ht',
    'margin_rate': '.field-margin_rate',
    'ht': '.field-ht',
    'quantity': '.field-quantity',
    'unity': '.field-unity',
    'tva_id': '.field-tva_id',
    'product_id': '.field-product_id',
    'catalogContainer': '#catalog-container'
  },
  ui: {
    main_tab: "ul.nav-tabs li:first a"
  },
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': 'onCatalogInsert',
    'mode:change': 'onModeChange'
  },
  // Bubble up child view events
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified',
    'cancel:click': 'cancel:click'
  },
  modelEvents: {
    'set:product': 'refreshForm',
    'change:tva_id': 'refreshTvaProductSelect'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    this.workunit_options = this.config.request('get:options', 'workunits');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.all_product_options = this.config.request('get:options', 'products');
  },
  getCommonFieldOptions: function getCommonFieldOptions(attribute, label) {
    var result = {
      field_name: attribute,
      value: this.model.get(attribute),
      label: label,
      editable: true
    };
    return result;
  },
  refreshForm: function refreshForm() {
    this.showOrder();
    this.showDescription();
    this.showModeToggle();
    this.renderModeRelatedFields();
    this.showQuantity();
    this.showUnity();
    this.showTva();
    this.showProduct();

    if (!this.getOption('edit')) {
      this.getUI('main_tab').tab('show');
    }
  },
  renderModeRelatedFields: function renderModeRelatedFields() {
    this.showSupplierHt();
    this.showMarginRate();
    this.showHt();
  },
  showModeToggle: function showModeToggle() {
    this.showChildView("mode", new widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_7__.default({
      field_name: "mode",
      label: "Mode de calcul du prix",
      value: this.model.get('mode'),
      "options": [{
        'label': 'HT',
        'value': 'ht'
      }, {
        'label': "Coût d'achat",
        'value': 'supplier_ht'
      }],
      finishEventName: 'mode:change'
    }));
  },
  showOrder: function showOrder() {
    this.showChildView('order', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
  },
  showDescription: function showDescription() {
    var options = this.getCommonFieldOptions('description', "Description");
    options.description = "Description utilisée dans les devis/factures";
    options.tinymce = true;
    options.required = true;
    options.cid = this.model.cid;
    options.required = true;
    var view = new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
    this.showChildView('description', view);
  },
  showSupplierHt: function showSupplierHt() {
    var region = this.getRegion('supplier_ht');

    if (this.model.get('mode') == 'supplier_ht') {
      var options = this.getCommonFieldOptions('supplier_ht', "Coût unitaire HT");
      options['required'] = true;
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
      this.showChildView('supplier_ht', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_8__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_8__.hideRegion)(region);
    }
  },
  showMarginRate: function showMarginRate() {
    var region = this.getRegion('margin_rate');

    if (this.model.get('mode') == 'supplier_ht') {
      var options = this.getCommonFieldOptions('margin_rate', 'Coefficient de marge');
      options.description = "Coefficient de marge à appliquer dans les calculs";
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
      this.showChildView('margin_rate', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_8__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_8__.hideRegion)(region);
    }
  },
  showHt: function showHt() {
    var region = this.getRegion('ht');

    if (this.model.get('mode') == 'ht') {
      var options = this.getCommonFieldOptions('ht', 'Montant unitaire HT');
      options['required'] = true;
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
      this.showChildView('ht', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_8__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_8__.hideRegion)(region);
    }
  },
  showQuantity: function showQuantity() {
    var options = this.getCommonFieldOptions('quantity', "Quantité");
    options.required = true;
    var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('quantity', view);
  },
  showUnity: function showUnity() {
    var options = this.getCommonFieldOptions('unity', "Unité");
    options.options = this.workunit_options;
    options.placeholder = 'Choisir une unité';
    options.id_key = 'value';
    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView("unity", view);
  },
  showTva: function showTva() {
    // NB : ici les tva_options sont différentes de celles utilisées dans le reset de la view
    // car elles sont modifiées par le SelectWidget et on perd donc l'info de la
    // tva par défaut
    var tva_options = this.config.request('get:options', 'tvas');
    var options = this.getCommonFieldOptions('tva_id', 'TVA');
    options.id_key = 'id';
    options.options = tva_options;
    options.required = true;
    options.placeholder = 'Choisir un taux de TVA';
    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('tva_id', view);
  },
  showProduct: function showProduct() {
    var options = this.getCommonFieldOptions('product_id', 'Compte produit');
    this.product_options = this.getProductOptions(this.tva_options, this.all_product_options, this.model.get('tva_id'));
    options.required = true;
    options.options = this.product_options;
    options.id_key = 'id';
    options.placeholder = 'Choisir un compte produit';
    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('product_id', view);
  },
  templateContext: function templateContext() {
    var title = "Ajouter un produit";

    if (this.getOption('edit')) {
      title = "Modifier un produit";
    }

    return {
      add: !this.getOption('edit'),
      title: title
    };
  },
  onRender: function onRender() {
    this.refreshForm();

    if (!this.getOption('edit')) {
      this.showChildView('catalogContainer', new common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__.default({
        query_params: {
          type_: 'product'
        },
        url: AppOption['catalog_tree_url'],
        multiple: true
      }));
    }
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    var req = this.app.request('insert:from:catalog', this.getOption("destCollection"), sale_products);
    req.then(function () {
      _this.app.trigger('product:changed', _this.model);

      _this.triggerMethod('modal:close');
    });
  },
  onDestroyModal: function onDestroyModal() {
    this.app.trigger('navigate', 'index');
  },
  onModeChange: function onModeChange(key, value) {
    this.model.set(key, value);
    this.renderModeRelatedFields();
  },
  onSuccessSync: function onSuccessSync() {
    this.app.trigger('product:changed', this.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductForm);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/workform/WorkForm.js":
/*!***************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/workform/WorkForm.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! widgets/CheckboxWidget.js */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/*
 * Module name : WorkForm
 */











var editTemplate = __webpack_require__(/*! ./templates/WorkForm.mustache */ "./src/task/views/composition/price_study/views/workform/templates/WorkForm.mustache");

var addTemplate = __webpack_require__(/*! ./templates/AddWorkForm.mustache */ "./src/task/views/composition/price_study/views/workform/templates/AddWorkForm.mustache");

var WorkForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_9___default().View.extend(base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_1__.default).extend({
  partial: false,
  getTemplate: function getTemplate() {
    var template;

    if (this.getOption('edit')) {
      template = editTemplate;
    } else {
      template = addTemplate;
    }

    return template;
  },
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  regions: {
    errors: '.errors',
    title: ".field-title",
    description: '.field-description',
    quantity: '.field-quantity',
    total_ht: '.field-total_ht',
    unity: '.field-unity',
    tva_id: '.field-tva_id',
    product_id: '.field-product_id',
    margin_rate: '.field-margin_rate',
    display_details: '.field-display_details'
  },
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': 'onCatalogInsert'
  },
  // Bubble up child view events
  childViewTriggers: {
    'finish': 'data:modified',
    'change': 'data:modified'
  },
  modelEvents: {
    'change:tva_id': 'refreshTvaProductSelect'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    this.unity_options = this.config.request('get:options', 'workunits');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.all_product_options = this.config.request('get:options', 'products');
  },
  showTitle: function showTitle() {
    this.showChildView('title', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      label: "Titre de l'ouvrage",
      description: "Titre de l'ouvrage dans le document final",
      field_name: "title",
      value: this.model.get('title'),
      required: true
    }));
  },
  showDescription: function showDescription() {
    this.showChildView('description', new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_6__.default({
      title: "Description",
      field_name: 'description',
      value: this.model.get('description'),
      tinymce: true
    }));
  },
  showQuantity: function showQuantity() {
    this.showChildView('quantity', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      title: "Quantité",
      field_name: "quantity",
      value: this.model.get('quantity')
    }));
  },
  showDisplayDetails: function showDisplayDetails() {
    this.showChildView('display_details', new widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      inline_label: "Afficher le détail des prestations dans le document final",
      description: "En décochant cette case, l'ouvrage apparaîtra comme une seule ligne de prestation, sans le détail des produits",
      field_name: "display_details",
      value: this.model.get('display_details')
    }));
  },
  showUnity: function showUnity() {
    this.showChildView('unity', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      title: "Unité",
      field_name: 'unity',
      options: this.unity_options,
      value: this.model.get('unity'),
      placeholder: 'Choisir une unité'
    }));
  },
  showTva: function showTva() {
    this.showChildView('tva_id', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      title: "TVA",
      field_name: 'tva_id',
      options: this.tva_options,
      id_key: 'id',
      value: this.model.get('tva_id'),
      placeholder: 'Choisir un taux de TVA',
      required: true
    }));
  },
  showProduct: function showProduct() {
    this.product_options = this.getProductOptions(this.tva_options, this.all_product_options);
    this.showChildView('product_id', new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      title: "Compte produit",
      field_name: 'product_id',
      options: this.product_options,
      id_key: 'id',
      value: this.model.get('product_id'),
      placeholder: 'Choisir un compte produit',
      required: true
    }));
  },
  showMarginRate: function showMarginRate() {
    this.showChildView('margin_rate', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      title: "Coefficient de marge",
      field_name: 'margin_rate',
      value: this.model.get('margin_rate'),
      description: "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'"
    }));
  },
  onRender: function onRender() {
    this.refreshForm();

    if (!this.getOption('edit')) {
      this.addRegion('catalogContainer', '#catalog-container');
      this.showChildView('catalogContainer', new common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_7__.default({
        query_params: {
          type_: 'work'
        },
        url: AppOption['catalog_tree_url'],
        multiple: true
      }));
    }
  },
  refreshForm: function refreshForm() {
    this.showTitle();
    this.showDescription();
    this.showQuantity();
    this.showUnity();
    this.showDisplayDetails();
    this.showTva();
    this.showProduct();
    this.showMarginRate();
  },
  onCancelForm: function onCancelForm() {
    console.log("We cancel the form in WorkForm!!!");
    this.app.trigger('navigate', 'index');
  },
  templateContext: function templateContext() {
    return {
      title: ""
    };
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    var req = this.app.request('insert:from:catalog', this.getOption("destCollection"), sale_products);
    req.then(function () {
      _this.app.trigger('product:changed', _this.model);

      _this.triggerMethod('modal:close');
    });
  },
  onDestroyModal: function onDestroyModal() {
    this.app.trigger('navigate', 'index');
  },
  onDataInvalid: function onDataInvalid(model, errors) {
    console.log("WorkForm.onInvalid");
    console.log(errors);
    this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_8__.default({
      errors: errors
    }));
  },
  onSuccessSync: function onSuccessSync() {
    console.log("success sync in WorkForm!!!");
    this.app.trigger('product:changed', this.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkForm);

/***/ }),

/***/ "./src/task/views/composition/price_study/views/workform/WorkItemForm.js":
/*!*******************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/workform/WorkItemForm.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_tools__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-tools */ "./src/backbone-tools.js");
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/* harmony import */ var widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! widgets/RadioChoiceButtonWidget */ "./src/widgets/RadioChoiceButtonWidget.js");
/*
 * Module name : WorkItemForm
 */










var template = __webpack_require__(/*! ./templates/WorkItemForm.mustache */ "./src/task/views/composition/price_study/views/workform/templates/WorkItemForm.mustache");

var WorkItemForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default().View.extend({
  template: template,
  tagName: 'section',
  id: 'workitem_form',
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  partial: false,
  regions: {
    'order': '.field-order',
    quantity_inherited: ".field-quantity_inherited",
    description: '.field-description',
    'mode': '.field-mode',
    supplier_ht: '.field-supplier_ht',
    margin_rate: '.field-margin_rate',
    ht: '.field-ht',
    work_unit_quantity: '.field-work_unit_quantity',
    unity: '.field-unity',
    catalogContainer: "#catalog-container"
  },
  ui: {
    main_tab: "ul.nav-tabs li:first a"
  },
  // Listen to the current's view events
  events: {
    'change': 'updateTotalHt'
  },
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': "onCatalogInsert",
    'quantityMode:change': "onQuantityModeChange",
    'mode:change': 'onModeChange'
  },
  // Bubble up child view events
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified',
    'cancel:click': 'cancel:click'
  },
  modelEvents: {
    'change:tva_id': 'refreshTvaProductSelect'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('priceStudyApp');
    this.destCollection = this.getOption('destCollection');
    this.unity_options = this.config.request('get:options', 'workunits');
  },
  getCommonFieldOptions: function getCommonFieldOptions(attribute, label) {
    var result = {
      field_name: attribute,
      value: this.model.get(attribute),
      label: label,
      editable: true
    };
    return result;
  },
  refreshForm: function refreshForm() {
    this.showDescription();
    this.showModeToggle();
    this.renderModeRelatedFields();
    this.showToggleQuantityButton();
    this.showWorkQuantity();
    this.showUnity();
  },
  renderModeRelatedFields: function renderModeRelatedFields() {
    this.showSupplierHt();
    this.showMarginRate();
    this.showHt();
  },
  showOrder: function showOrder() {
    this.showChildView('order', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
  },
  showDescription: function showDescription() {
    var options = this.getCommonFieldOptions('description', "Description");
    options.description = "Description utilisée dans les devis et factures";
    options.tinymce = true;
    options.required = true;
    var view = new widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_4__.default(options);
    this.showChildView('description', view);
  },
  showModeToggle: function showModeToggle() {
    this.showChildView("mode", new widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_7__.default({
      field_name: "mode",
      className: "form-group",
      label: "Mode de calcul du prix",
      value: this.model.get('mode'),
      "options": [{
        'label': 'HT',
        'value': 'ht'
      }, {
        'label': "Coût d'achat",
        'value': 'supplier_ht'
      }],
      finishEventName: 'mode:change'
    }));
  },
  showSupplierHt: function showSupplierHt() {
    var region = this.getRegion('supplier_ht');

    if (this.model.get('mode') == 'supplier_ht') {
      var options = this.getCommonFieldOptions('supplier_ht', "Coût unitaire HT");
      options['required'] = true;
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
      this.showChildView('supplier_ht', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.hideRegion)(region);
    }
  },
  showMarginRate: function showMarginRate() {
    var region = this.getRegion('margin_rate');

    if (this.model.get('mode') == 'supplier_ht') {
      var options = this.getCommonFieldOptions('margin_rate', 'Coefficient de marge');
      options['label'] += " (hérité de l'ouvrage)";
      options['editable'] = false;
      var value = this.destCollection._parent.get('margin_rate') || 0;
      options['value'] = value;
      options.description = "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'";
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
      this.showChildView('margin_rate', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.hideRegion)(region);
    }
  },
  showHt: function showHt() {
    var region = this.getRegion('ht');

    if (this.model.get('mode') == 'ht') {
      var options = this.getCommonFieldOptions('ht', 'Montant unitaire HT');
      options['required'] = true;
      var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
      this.showChildView('ht', view);
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.showRegion)(region);
    } else {
      (0,backbone_tools__WEBPACK_IMPORTED_MODULE_1__.hideRegion)(region);
    }
  },
  showToggleQuantityButton: function showToggleQuantityButton() {
    var value = this.model.get('quantity_inherited') ? 'inherit' : 'custom';
    this.showChildView("quantity_inherited", new widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_7__.default({
      field_name: "quantitymode",
      label: "Calcul des quantités",
      value: value,
      "options": [{
        'label': "Par unité d’œuvre",
        'value': 'inherit'
      }, {
        'label': "Indépendamment de l’ouvrage",
        'value': 'custom'
      }],
      finishEventName: 'quantityMode:change'
    }));
  },
  showWorkQuantity: function showWorkQuantity() {
    var label = "Quantité par unité d’œuvre";
    var description_text = "Quantité de ce produit dans chaque unité d’œuvre";

    if (!this.model.get('quantity_inherited')) {
      label = "Quantité";
      description_text = "Quantité indépendante du nombre d’unités d’œuvre de l’ouvrage";
    }

    var options = this.getCommonFieldOptions('work_unit_quantity', label);

    if (description_text != "") {
      options.description = description_text;
    }

    var view = new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
    this.showChildView('work_unit_quantity', view);
  },
  showUnity: function showUnity() {
    var options = this.getCommonFieldOptions('unity', "Unité");
    options.options = this.unity_options;
    options.placeholder = 'Choisir une unité'; // options.required = true;

    var view = new widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_5__.default(options);
    this.showChildView("unity", view);
  },
  templateContext: function templateContext() {
    var title = "Ajouter un produit";

    if (this.getOption('edit')) {
      title = "Modifier un produit";
    }

    return {
      title: title,
      add: !this.getOption('edit')
    };
  },
  onRender: function onRender() {
    this.refreshForm();

    if (!this.getOption('edit')) {
      var view = new common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__.default({
        query_params: {
          type_: 'product'
        },
        url: AppOption['catalog_tree_url'],
        multiple: true
      });
      this.showChildView('catalogContainer', view);
    }
  },
  onQuantityModeChange: function onQuantityModeChange(key, value) {
    this.model.set('quantity_inherited', value == 'inherit');
    this.showWorkQuantity();
  },
  onModeChange: function onModeChange(key, value) {
    this.model.set(key, value);
    this.renderModeRelatedFields();
  },
  onAttach: function onAttach() {
    this.getUI('main_tab').tab('show');
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    var req = this.app.request('insert:from:catalog', this.getOption("destCollection"), sale_products);
    req.then(function () {
      _this.app.trigger('product:changed', _this.model);

      _this.triggerMethod('modal:close');
    });
  },
  onSuccessSync: function onSuccessSync() {
    console.log("success sync in WorkItemForm!!!");
    this.app.trigger('product:changed', this.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemForm);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/App.js":
/*!**************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/App.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Router_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Router.js */ "./src/task/views/composition/progress_invoicing/components/Router.js");
/* harmony import */ var _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./views/RootComponent.js */ "./src/task/views/composition/progress_invoicing/views/RootComponent.js");
/* harmony import */ var _components_Controller_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Controller.js */ "./src/task/views/composition/progress_invoicing/components/Controller.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");






var AppClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().Application.extend({
  channelName: 'progressInvoicingApp',
  radioEvents: {
    "navigate": 'onNavigate',
    "show:modal": "onShowModal",
    "workitem:edit": "onWorkItemEdit",
    // product changed
    "product:changed": "onProductChanged"
  },
  onBeforeStart: function onBeforeStart(app, options) {
    var _this = this;

    console.log("AppClass.onBeforeStart");
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.chapters = this.facade.request('get:collection', 'progress_invoicing_chapters');
    var model = this.facade.request('get:model', 'display_options');
    this.rootView = new _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__.default({
      collection: this.chapters,
      section: options['section']
    });
    this.controller = new _components_Controller_js__WEBPACK_IMPORTED_MODULE_4__.default({
      rootView: this.rootView,
      chapters: this.chapters,
      section: options['section']
    });
    console.log("Creating the Progress Invoicing router");
    this.listenTo(model, 'saved:display_units', function () {
      return _this.onNavigate('index');
    });
    new _components_Router_js__WEBPACK_IMPORTED_MODULE_2__.default({
      controller: this.controller
    });
    console.log("Progress Invoicing AppClass.onBeforeStart finished");
  },
  onStart: function onStart(app, options) {
    console.log("Progress Invoicing App onStart");
    this.showView(this.rootView);
  },
  onNavigate: function onNavigate(route_name, parameters) {
    console.log("App.onNavigate");
    var dest_route = route_name;

    if (!_.isUndefined(parameters)) {
      dest_route += "/" + parameters;
    }

    window.location.hash = dest_route;
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.loadUrl(dest_route);
  },
  onWorkItemEdit: function onWorkItemEdit(model) {
    console.log("Edit work item");
    this.controller.editWorkItem(model);
  },
  onProductChanged: function onProductChanged(model) {
    var _this2 = this;

    // Cas 1 Product: rien à faire
    var requests = [];

    if (!model.has("type_")) {
      // WorkItem : on recharge la collection et le work associé
      requests.push(model.collection.fetch());
      requests.push(model.collection._parent.fetch());
    } else if (model.get('type_') == 'progress_invoicing_work') {
      requests.push(model.items.fetch());
    }

    $.when(requests).then(function () {
      window.location.hash = "#index";

      _this2.controller.index();

      _this2.facade.trigger('changed:task');
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AppClass);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/components/Controller.js":
/*!********************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/components/Controller.js ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);


var Controller = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().Object.extend({
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    console.log("ProgressInvoicing Controller.initialize");
    this.rootView = options['rootView'];
    this.chapters = options['chapters'];
  },
  index: function index() {
    console.log("ProgressInvoicing Controller.index");
    this.rootView.index();
  },
  showModal: function showModal(view) {
    this.rootView.showModal(view);
  },
  editProduct: function editProduct(chapterId, modelId) {
    var chapterModel = this.chapters.get(chapterId);
    var collection = chapterModel.products;
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditProductForm.bind(this.rootView));
  },
  // WorkItem 
  editWorkItem: function editWorkItem(model) {
    this.rootView.showEditProductForm(model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Controller);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/components/Router.js":
/*!****************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/components/Router.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var marionette_approuter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! marionette.approuter */ "./node_modules/marionette.approuter/lib/marionette.approuter.esm.js");

var Router = marionette_approuter__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  appRoutes: {
    '': 'index',
    'index': 'index',
    'chapters/:id/products/:id': 'editProduct'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Router);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/BlockView.js":
/*!**************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/BlockView.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* harmony import */ var _chapter_ChapterCollectionView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chapter/ChapterCollectionView.js */ "./src/task/views/composition/progress_invoicing/views/chapter/ChapterCollectionView.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * Module name : BlockView
 */





var template = __webpack_require__(/*! ./templates/BlockView.mustache */ "./src/task/views/composition/progress_invoicing/views/templates/BlockView.mustache");

var BlockView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  regions: {
    products: '.products',
    errors: '.errors'
  },
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.section = options['section'];
  },
  showChapters: function showChapters() {
    console.log("Show collection of chapters");
    var view = new _chapter_ChapterCollectionView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.collection,
      childViewOptions: {
        edit: this.section['edit'],
        section: this.section['products']
      }
    });
    this.showChildView('products', view);
  },
  onRender: function onRender() {
    this.showChapters();
  },
  templateContext: function templateContext() {
    return {
      editable: this.section['edit']
    };
  },
  formOk: function formOk() {
    var result = true;
    var errors = this.facade.request('is:valid');

    if (!_.isEmpty(errors)) {
      this.showChildView('errors', new base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_1__.default({
        errors: errors
      }));
      result = false;
    } else {
      this.detachChildView('errors');
    }

    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BlockView);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/RootComponent.js":
/*!******************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/RootComponent.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _product_ProductForm_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product/ProductForm.js */ "./src/task/views/composition/progress_invoicing/views/product/ProductForm.js");
/* harmony import */ var _BlockView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlockView.js */ "./src/task/views/composition/progress_invoicing/views/BlockView.js");
/*
 * Module name : RootComponent
 */




var template = __webpack_require__(/*! ./templates/RootComponent.mustache */ "./src/task/views/composition/progress_invoicing/views/templates/RootComponent.mustache");

var RootComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  className: '',
  template: template,
  regions: {
    main: '.main',
    modalContainer: '.modal-container'
  },
  initialize: function initialize(options) {
    this.initialized = false;
    this.section = options['section'];
  },
  index: function index() {
    this.initialized = true;
    var view = new _BlockView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      model: this.model,
      collection: this.collection,
      section: this.section
    });
    this.showChildView('main', view);
  },
  showModal: function showModal(view) {
    this.showChildView('modalContainer', view);
  },
  _showFormInModal: function _showFormInModal(FormClass, model) {
    this.index();
    var view = new FormClass({
      model: model,
      destCollection: model.collection
    });
    this.showModal(view);
  },
  showEditProductForm: function showEditProductForm(model) {
    this._showFormInModal(_product_ProductForm_js__WEBPACK_IMPORTED_MODULE_0__.default, model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RootComponent);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/ChapterCollectionView.js":
/*!**********************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/ChapterCollectionView.js ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ChapterView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ChapterView */ "./src/task/views/composition/progress_invoicing/views/chapter/ChapterView.js");




var ChapterEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: underscore__WEBPACK_IMPORTED_MODULE_1___default().template('<div class="border_left_block composite content_double_padding"><p><em>Aucun élément n’a été ajouté, cliquez sur Ajouter un chapitre pour commencer à en ajouter.</em></p></div>')
});
var ChapterCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  childView: _ChapterView__WEBPACK_IMPORTED_MODULE_2__.default,
  emptyView: ChapterEmptyView,
  tagName: 'div',
  childViewTriggers: {},
  // collectionEvents: {
  //     'sync': 'render'
  // },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterCollectionView);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/ChapterView.js":
/*!************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/ChapterView.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _custom_views__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom_views */ "./src/task/views/composition/progress_invoicing/views/chapter/custom_views.js");
/* harmony import */ var base_views_ErrorView__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! base/views/ErrorView */ "./src/base/views/ErrorView.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");






var template = __webpack_require__(/*! ./templates/ChapterView.mustache */ "./src/task/views/composition/progress_invoicing/views/chapter/templates/ChapterView.mustache");

var ChapterView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  className: 'taskline-group row quotation_item border_left_block composite content_double_padding',
  regions: {
    'errors': '.errors'
  },
  events: {
    // Items des ouvrages
    'click a[data-action="workitem:edit"]': 'onWorkItemEdit'
  },
  childViewEvents: {
    'action:clicked': "onButtonClicked"
  },
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('progressInvoicingApp');
    this.productCollection = this.model.products;
    this.setupEvents();
  },
  setupEvents: function setupEvents() {
    var _this = this;

    this.listenTo(this.productCollection, 'fetched', function () {
      return _this.render();
    });
    this.listenTo(this.facade, 'bind:validation', function () {
      return backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().bind(_this);
    });
    this.listenTo(this.facade, 'unbind:validation', function () {
      return backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().unbind(_this);
    });
    this.listenTo(this.model, 'validated:invalid', this.showErrors);
    this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
    this.listenTo(this.productCollection, 'validated:invalid', this.showProductErrors);
  },
  onRender: function onRender() {
    console.log("Rendering a chapter view");
  },
  showErrors: function showErrors(model, errors) {
    console.log("Show errors");
    console.log(errors);
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
    console.log("hide errors");
  },
  showProductErrors: function showProductErrors(model, errors) {
    this.$el.addClass('error');
    this.showChildView('errors', new base_views_ErrorView__WEBPACK_IMPORTED_MODULE_3__.default({
      errors: errors
    }));
  },
  templateContext: function templateContext() {
    console.log("Rendering"); // Collect data sent to the template (model attributes are already transmitted)

    var productHtml = (0,_custom_views__WEBPACK_IMPORTED_MODULE_2__.getProductCollectionHtml)(this.productCollection);
    return {
      edit: this.getOption('edit'),
      collectionRawHtml: productHtml,
      total_ht: this.user_prefs.request('formatAmount', this.model.get('total_ht'), true)
    };
  },
  onEdit: function onEdit() {
    this.app.trigger('navigate', 'chapters' + '/' + this.model.get('id'));
  },

  /**
   * WorkItem RawHtml Subviews event management
   * 
   */

  /**
   * 
   * @param {Event} event : The onclick event
   * @returns A WorkItemModel instance
   */
  getWorkItemFromEvent: function getWorkItemFromEvent(event) {
    var tag = $(event.currentTarget);
    var workId = parseInt(tag.data('workid'));
    var workItemId = parseInt(tag.data('id'));
    var workModel = this.productCollection.get(workId);
    return workModel.items.get(workItemId);
  },
  onWorkItemEdit: function onWorkItemEdit(event) {
    var model = this.getWorkItemFromEvent(event);
    this.app.trigger('workitem:edit', model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ChapterView);

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/custom_views.js":
/*!*************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/custom_views.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getProductCollectionHtml": () => (/* binding */ getProductCollectionHtml),
/* harmony export */   "getProductViewHtml": () => (/* binding */ getProductViewHtml),
/* harmony export */   "getWorkViewHtml": () => (/* binding */ getWorkViewHtml),
/* harmony export */   "getWorkItemViewHtml": () => (/* binding */ getWorkItemViewHtml)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/**
 * Custom non marionette views used to render non hierarchical structures
 * 
 * Here we render the html snippets for the different products/work/work_item
 * elements that are rendered in tbody s
 * 
 */


var PRODUCT_VIEW_TEMPLATE = __webpack_require__(/*! ./templates/ProductRawView.mustache */ "./src/task/views/composition/progress_invoicing/views/chapter/templates/ProductRawView.mustache");

var WORK_VIEW_TEMPLATE = __webpack_require__(/*! ./templates/WorkRawView.mustache */ "./src/task/views/composition/progress_invoicing/views/chapter/templates/WorkRawView.mustache");

var WORK_ITEM_VIEW_TEMPLATE = __webpack_require__(/*! ./templates/WorkItemRawView.mustache */ "./src/task/views/composition/progress_invoicing/views/chapter/templates/WorkItemRawView.mustache");
/**
 * 
 * @param {BaseProductModel} model 
 * @param {integer} index 
 * @returns Template variables related to the Product 
 */


var _baseProductContext = function _baseProductContext(model, index) {
  var ctx = Object.assign({}, model.attributes);
  var user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel("user_preferences");

  for (var _i = 0, _arr = ['total_ht_to_invoice', 'total_tva_to_invoice', 'total_ttc_to_invoice', 'total_ht', 'tva_amount', 'total_ttc']; _i < _arr.length; _i++) {
    var key = _arr[_i];
    ctx[key] = user_prefs.request('formatAmount', model.get(key), true);
  }

  for (var _i2 = 0, _arr2 = ['already_invoiced', 'percentage']; _i2 < _arr2.length; _i2++) {
    var _key = _arr2[_i2];
    ctx[_key] = model.get(_key) || 0;
  }

  if (!model.get('percent_left') && !model.get('percent_left') == 0) {
    ctx['percent_left'] = 100;
  }

  ctx["htmlIndex"] = index;
  return ctx;
};
/**
 * 
 * @param {ProductCollection} collection 
 * @returns The representation of the collection as a serie of tbodys
 */


var getProductCollectionHtml = function getProductCollectionHtml(collection) {
  var result = "";
  collection.each(function (model, index) {
    if (model.get('type_') == 'progress_invoicing_work') {
      result += getWorkViewHtml(model, index);
    } else {
      result += getProductViewHtml(model, index);
    }
  });
  return result;
};
/**
 * 
 * @param {ProductModel} model 
 * @param {number} index 
 * @returns The html representation of this product model
 */

var getProductViewHtml = function getProductViewHtml(model, index) {
  var ctx = _baseProductContext(model, index);

  return PRODUCT_VIEW_TEMPLATE(ctx);
};
/**
 * 
 * @param {WorkModel} model 
 * @param {number} index 
 * @returns An html representation of the model and its items
 */

var getWorkViewHtml = function getWorkViewHtml(model, index) {
  console.log("Rendering WorkViewHTML");
  var itemsRawHtml = '';
  model.items.each(function (item, index) {
    itemsRawHtml += getWorkItemViewHtml(item, index, model);
  });

  var ctx = _baseProductContext(model, index);

  ctx["workitemsHtml"] = itemsRawHtml;
  ctx['locked'] = model.get('locked');
  return WORK_VIEW_TEMPLATE(ctx);
};
/**
 * 
 * @param {WorkItemModel} model 
 * @param {number} index 
 * @returns A html representation of the WorkItemModel
 */

var getWorkItemViewHtml = function getWorkItemViewHtml(model, index, work) {
  var ctx = _baseProductContext(model, index);

  ctx['locked'] = work.get('locked');
  return WORK_ITEM_VIEW_TEMPLATE(ctx);
};

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/product/ProductForm.js":
/*!************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/product/ProductForm.js ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/*
 * Module name : ProductForm
 */





var template = __webpack_require__(/*! ./templates/ProductForm.mustache */ "./src/task/views/composition/progress_invoicing/views/product/templates/ProductForm.mustache");

var ProductForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  behaviors: [base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  bb_sync: true,
  partial: true,
  regions: {
    'percent_done': '.percent_done',
    'percent_left': '.percent_left',
    'current_percent': '.current_percent'
  },
  initialize: function initialize() {
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('progressInvoicingApp');
  },
  refreshForm: function refreshForm() {
    this.showChildView('percent_done', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('already_invoiced'),
      field_name: '_percent_done',
      label: "Déjà facturé",
      editable: false
    }));
    this.showChildView('current_percent', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('percentage'),
      title: "À facturer",
      field_name: "percentage",
      addon: "€"
    }));
    this.showChildView('percent_left', new widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('percent_left'),
      field_name: '_percent_left',
      label: "Restera à facturer",
      editable: false
    }));
  },
  onRender: function onRender() {
    this.refreshForm();
  },
  templateContext: function templateContext() {
    var deposit_info = false;

    if (this.model.get('has_deposit')) {
      deposit_info = true;
    }

    return {
      popup_title: "Pourcentage à facturer",
      total_ht_to_invoice_label: this.model.total_ht_to_invoice_label(),
      tva_to_invoice_label: this.model.tva_to_invoice_label(),
      total_ttc_to_invoice_label: this.model.total_ttc_to_invoice_label(),
      deposit_info: deposit_info
    };
  },
  onSuccessSync: function onSuccessSync() {
    this.app.trigger('product:changed', this.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductForm);

/***/ }),

/***/ "./src/task/views/files/FileCollectionView.js":
/*!****************************************************!*\
  !*** ./src/task/views/files/FileCollectionView.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _FileView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FileView */ "./src/task/views/files/FileView.js");


/**
 * Collection view for file representation. 
 * The childview can be specified on init
 * 
 * @fires 'file:updated' : when a file DL popup has been closed (file added/deleted/updated)
 */

var FileCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().CollectionView.extend({
  tagName: 'tbody',
  childView: _FileView__WEBPACK_IMPORTED_MODULE_0__.default,
  childViewTriggers: {
    "file:updated": "file:updated"
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileCollectionView);

/***/ }),

/***/ "./src/task/views/files/FileComponent.js":
/*!***********************************************!*\
  !*** ./src/task/views/files/FileComponent.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _FileContentView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FileContentView */ "./src/task/views/files/FileContentView.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_2__);
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");




/**
 * File component grouping File view management
 * 
 * Presents a block with 
 * 
 * - File requirements
 * - Other attached files
 * 
 * Handles its own refresh process
 */

var FileComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: underscore__WEBPACK_IMPORTED_MODULE_2___default().template('<div class="files collapsible in"></div>'),
  regions: {
    'content': '.files'
  },
  childViewEvents: {
    "file:updated": "refresh"
  },
  initialize: function initialize(options) {
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.collection = facade.request('get:collection', 'file_requirements');
    this.other_files_collection = facade.request('get:collection', 'attached_files');
  },
  refresh: function refresh() {
    var _this = this;

    $.when(this.collection.fetch(), this.other_files_collection.fetch()).then(function () {
      return _this.render();
    });
  },
  onRender: function onRender() {
    var view = new _FileContentView__WEBPACK_IMPORTED_MODULE_1__.default({
      collection: this.collection,
      other_files_collection: this.other_files_collection
    });
    this.showChildView('content', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileComponent);

/***/ }),

/***/ "./src/task/views/files/FileContentView.js":
/*!*************************************************!*\
  !*** ./src/task/views/files/FileContentView.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _FileView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FileView */ "./src/task/views/files/FileView.js");
/* harmony import */ var _FileCollectionView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FileCollectionView */ "./src/task/views/files/FileCollectionView.js");
/* harmony import */ var _FileRequirementView__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./FileRequirementView */ "./src/task/views/files/FileRequirementView.js");





var FileContentView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  tagName: 'div',
  template: __webpack_require__(/*! ./templates/FileContentView.mustache */ "./src/task/views/files/templates/FileContentView.mustache"),
  regions: {
    required_files: {
      el: '.required-files tbody',
      replaceElement: true
    },
    other_files: {
      el: ".other-files tbody",
      replaceElement: true
    }
  },
  ui: {
    'addFileButton': '.add-file-btn'
  },
  events: {
    "click @ui.addFileButton": "onFileAdd"
  },
  childViewTriggers: {
    "file:updated": "file:updated"
  },

  /**
   *  NB : className() runs before initialize
   * 
   * @returns the css class of the View's el
   */
  className: function className() {
    var result = 'separate_block border_left_block';
    this.facade = backbone__WEBPACK_IMPORTED_MODULE_0__.Radio.channel('facade');
    this.has_warning = this.facade.request('has:filewarning');

    if (this.has_warning) {
      result += " error";
    }

    return result;
  },
  onRender: function onRender() {
    if (this.collection.length > 0) {
      var view = new _FileCollectionView__WEBPACK_IMPORTED_MODULE_2__.default({
        collection: this.collection,
        childView: _FileRequirementView__WEBPACK_IMPORTED_MODULE_3__.default
      });
      this.showChildView('required_files', view);
    }

    view = new _FileCollectionView__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.getOption('other_files_collection'),
      childView: _FileView__WEBPACK_IMPORTED_MODULE_1__.default
    });
    this.showChildView('other_files', view);
  },
  templateContext: function templateContext() {
    var attachments = backbone__WEBPACK_IMPORTED_MODULE_0__.Radio.channel('facade').request('get:attachments');
    return {
      has_warning: this.has_warning,
      attachments: attachments,
      has_requirements: this.collection.length > 0,
      has_other_files: this.getOption('other_files_collection').length > 0
    };
  },
  onFilePopupCallback: function onFilePopupCallback() {
    this.triggerMethod('file:updated');
  },
  onFileAdd: function onFileAdd() {
    var url = AppOption['file_upload_url'];
    window.openPopup(url, this.onFilePopupCallback.bind(this));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileContentView);

/***/ }),

/***/ "./src/task/views/files/FileRequirementView.js":
/*!*****************************************************!*\
  !*** ./src/task/views/files/FileRequirementView.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);



var template = __webpack_require__(/*! ./templates/FileRequirementView.mustache */ "./src/task/views/files/templates/FileRequirementView.mustache");

var FileRequirementView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  template: template,
  ui: {
    add_button: '.btn-add',
    view_button: ".btn-view",
    edit_button: ".btn-edit",
    valid_button: ".btn-validate",
    clickable_td: "td.clickable"
  },
  events: {
    "click @ui.add_button": "onAdd",
    "click @ui.edit_button": "onEdit",
    "click @ui.view_button": "onView",
    "click @ui.clickable_td": "onView",
    "click @ui.valid_button": "onValidate"
  },
  initialize: function initialize() {
    this.config_channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.section = this.config_channel.request('get:form_section', 'files');
  },
  hasPreview: function hasPreview() {
    if (this.model.hasFile()) {
      return this.config_channel.request('is:previewable', this.model.get('file_object'));
    } else {
      return false;
    }
  },
  templateContext: function templateContext() {
    return {
      has_preview: this.hasPreview(),
      has_edit: this.model.hasFile(),
      has_add: this.model.missingFile(),
      has_valid_link: this.model.hasFile() && this.model.get('validation') && this.section.can_validate && this.model.get('validation_status') != "valid",
      label: this.model.label()
    };
  },
  onFilePopupCallback: function onFilePopupCallback(options) {
    this.triggerMethod('file:updated');
  },
  getCurrentUrl: function getCurrentUrl() {
    return window.location.href.replace('#', '').split('?')[0];
  },
  onAdd: function onAdd() {
    window.openPopup(this.getCurrentUrl() + "/addfile?file_type_id=" + this.model.get('file_type_id'), this.onFilePopupCallback.bind(this));
  },
  onEdit: function onEdit() {
    window.openPopup("/files/" + this.model.get("file_id"), this.onFilePopupCallback.bind(this));
  },
  onView: function onView() {
    backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade').trigger('show:preview', this.model.get('file_object'), this.model.label());
  },
  onValidate: function onValidate() {
    var _this = this;

    var res = this.model.validate();

    if (res) {
      this.model.setValid().then(function () {
        return _this.render();
      });
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileRequirementView);

/***/ }),

/***/ "./src/task/views/files/FileView.js":
/*!******************************************!*\
  !*** ./src/task/views/files/FileView.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);



var template = __webpack_require__(/*! ./templates/FileView.mustache */ "./src/task/views/files/templates/FileView.mustache");

var FileView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  template: template,
  ui: {
    edit_button: ".btn-edit",
    view_button: ".btn-view",
    clickable_td: "td.clickable"
  },
  events: {
    "click @ui.view_button": "onView",
    "click @ui.clickable_td": "onView",
    "click @ui.edit_button": "onEdit"
  },
  modelEvents: {
    'change:name': 'render'
  },
  onFilePopupCallback: function onFilePopupCallback(options) {
    this.triggerMethod('file:updated');
  },
  onView: function onView() {
    backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade').trigger('show:preview', this.model, "Fichier Facultatif");
  },
  onEdit: function onEdit() {
    window.openPopup("/files/" + this.model.get("id"), this.onFilePopupCallback.bind(this));
  },
  isPreviewable: function isPreviewable() {
    return backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config').request('is:previewable', this.model);
  },
  templateContext: function templateContext() {
    return {
      description_differs: this.model.get("description") !== this.model.get("name"),
      is_previewable: this.isPreviewable()
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FileView);

/***/ }),

/***/ "./src/task/views/payments/PaymentBlockView.js":
/*!*****************************************************!*\
  !*** ./src/task/views/payments/PaymentBlockView.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _PaymentLineTableView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PaymentLineTableView.js */ "./src/task/views/payments/PaymentLineTableView.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_4__);







var template = __webpack_require__(/*! ./templates/PaymentBlockView.mustache */ "./src/task/views/payments/templates/PaymentBlockView.mustache");

var PaymentBlockView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  tagName: 'div',
  className: 'separate_block border_left_block',
  template: template,
  regions: {
    payment_display: '.payment_display-container',
    payment_times: '.payment_times-container',
    deposit: '.payment-deposit-container',
    lines: '.payment-lines-container'
  },
  modelEvents: {
    'sync': 'showModelSection',
    'saved:payment_times': 'onPaymentTimesChanged'
  },
  childViewEvents: {
    'finish': 'onFinish'
  },
  initialize: function initialize(options) {
    this.collection = options['collection'];
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('config');
    this.payment_display_options = channel.request('get:options', 'payment_displays');
    this.deposit_options = channel.request('get:options', 'deposits');
    this.payment_times_options = channel.request('get:options', 'payment_times');
    channel = backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('facade');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_4___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_4___default().unbind(this);
  },
  onFinish: function onFinish(key, value) {
    this.triggerMethod('data:persist', key, value);
  },
  renderTable: function renderTable() {
    var tableView = new _PaymentLineTableView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.collection,
      model: this.model
    });
    this.showChildView('lines', tableView);
  },
  showPaymentDisplaySelect: function showPaymentDisplaySelect() {
    this.showChildView('payment_display', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      options: this.payment_display_options,
      title: "Affichage des paiements",
      field_name: 'paymentDisplay',
      id_key: 'value',
      value: this.model.get('paymentDisplay')
    }));
  },
  showDepositSelect: function showDepositSelect() {
    this.showChildView('deposit', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      options: this.deposit_options,
      title: "Acompte à la commande",
      field_name: 'deposit',
      id_key: 'value',
      value: this.model.get('deposit')
    }));
  },
  showPaymentTimesSelect: function showPaymentTimesSelect() {
    this.showChildView('payment_times', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      options: this.payment_times_options,
      title: "Paiement en",
      field_name: 'payment_times',
      id_key: 'value',
      value: this.model.get('payment_times')
    }));
  },
  showModelSection: function showModelSection() {
    this.showPaymentDisplaySelect();
    this.showDepositSelect();
    this.showPaymentTimesSelect();
  },
  onRender: function onRender() {
    this.showModelSection();
    this.renderTable();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentBlockView);

/***/ }),

/***/ "./src/task/views/payments/PaymentConditionBlockView.js":
/*!**************************************************************!*\
  !*** ./src/task/views/payments/PaymentConditionBlockView.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../tools.js */ "./src/tools.js");
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_5__);
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");








var template = __webpack_require__(/*! ./templates/PaymentConditionBlockView.mustache */ "./src/task/views/payments/templates/PaymentConditionBlockView.mustache");

var PaymentConditionBlockView = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_3__.default],
  tagName: 'div',
  className: 'separate_block border_left_block',
  template: template,
  regions: {
    errors: ".errors",
    predefined_conditions: '.predefined-conditions',
    conditions: '.conditions'
  },
  modelEvents: {
    'change:payment_conditions': 'render',
    'validated:invalid': 'showErrors',
    'validated:valid': 'hideErrors'
  },
  childViewEvents: {
    'finish': 'onFinish'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('config');
    this.payment_conditions_options = this.config.request('get:options', 'payment_conditions');
    this.lookupDefault();
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('facade');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_5___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_5___default().unbind(this);
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  onFinish: function onFinish(field_name, value) {
    if (field_name == 'predefined_conditions') {
      var condition_object = this.getCondition(value);
      this.model.set('predefined_conditions', value);

      if (!_.isUndefined(condition_object)) {
        this.triggerMethod('data:persist', 'payment_conditions', condition_object.label);
      }
    } else {
      this.triggerMethod('data:persist', 'payment_conditions', value);
    }
  },
  getCondition: function getCondition(id) {
    return _.find(this.payment_conditions_options, function (item) {
      return item.id == id;
    });
  },
  lookupDefault: function lookupDefault() {
    /*
     * Setup the default payment condition if none is set
     */
    var option = (0,_tools_js__WEBPACK_IMPORTED_MODULE_2__.getDefaultItem)(this.payment_conditions_options);

    if (!_.isUndefined(option)) {
      var payment_conditions = this.model.get('payment_conditions');

      if (_.isUndefined(payment_conditions) || payment_conditions === null || payment_conditions.trim() == '') {
        this.model.set('payment_conditions', option.label);
        this.model.save({
          'payment_conditions': option.label
        }, {
          patch: true
        });
      }
    }
  },
  onRender: function onRender() {
    // #FIXME : Je ne suis pas sûr que ce champ existe dans le modèle,
    // je le laisse dans le doute
    var val = this.model.get('predefined_conditions');
    val = parseInt(val, 10);
    this.showChildView('predefined_conditions', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      options: this.payment_conditions_options,
      title: "",
      field_name: 'predefined_conditions',
      id_key: 'id',
      value: val,
      placeholder: ''
    }));
    this.showChildView('conditions', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: "Conditions de paiement applicables à ce document",
      value: this.model.get('payment_conditions'),
      field_name: 'payment_conditions',
      required: true
    }));
  },
  templateContext: function templateContext() {
    var collapsed = false;

    var default_condition = _.findWhere(this.payment_conditions_options, {
      "default": true
    });

    if (!_.isUndefined(default_condition)) {
      collapsed = true;
    }

    return {
      collapsed: collapsed
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentConditionBlockView);

/***/ }),

/***/ "./src/task/views/payments/PaymentDepositView.js":
/*!*******************************************************!*\
  !*** ./src/task/views/payments/PaymentDepositView.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../math.js */ "./src/math.js");


var PaymentDepositView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  className: 'row taskline',
  template: __webpack_require__(/*! ./templates/PaymentDepositView.mustache */ "./src/task/views/payments/templates/PaymentDepositView.mustache"),
  modelEvents: {
    'change:deposit_amount_ttc': 'render'
  },
  templateContext: function templateContext() {
    return {
      show_date: this.getOption('show_date'),
      amount_label: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('deposit_amount_ttc'))
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentDepositView);

/***/ }),

/***/ "./src/task/views/payments/PaymentLineCollectionView.js":
/*!**************************************************************!*\
  !*** ./src/task/views/payments/PaymentLineCollectionView.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _PaymentLineView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PaymentLineView.js */ "./src/task/views/payments/PaymentLineView.js");


var PaymentLineCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().CollectionView.extend({
  tagName: 'tbody',
  className: 'paymentlines lines',
  childView: _PaymentLineView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  collectionEvents: {
    'change:reorder': 'render'
  },
  childViewTriggers: {
    'edit': 'line:edit',
    'delete': 'line:delete',
    'order:up': 'order:up',
    'order:down': 'order:down'
  },
  childViewOptions: function childViewOptions(model) {
    var edit = this.getOption('edit');
    return {
      show_date: this.getOption('show_date'),
      edit: edit
    };
  },
  onChildviewOrderUp: function onChildviewOrderUp(childView) {
    this.collection.moveUp(childView.model);
  },
  onChildviewOrderDown: function onChildviewOrderDown(childView) {
    this.collection.moveDown(childView.model);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentLineCollectionView);

/***/ }),

/***/ "./src/task/views/payments/PaymentLineFormView.js":
/*!********************************************************!*\
  !*** ./src/task/views/payments/PaymentLineFormView.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../tools.js */ "./src/tools.js");
/* harmony import */ var _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../widgets/DateWidget.js */ "./src/widgets/DateWidget.js");







var template = __webpack_require__(/*! ./templates/PaymentLineFormView.mustache */ "./src/task/views/payments/templates/PaymentLineFormView.mustache");

var PaymentLineFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  id: 'payments_form',
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  template: template,
  regions: {
    'order': '.order',
    'description': ".description",
    "date": ".date",
    "amount": ".amount"
  },
  onRender: function onRender() {
    this.showChildView('order', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
    var view = new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      field_name: "description",
      value: this.model.get('description'),
      title: "Intitulé",
      required: true
    });
    this.showChildView('description', view);

    if (this.getOption('show_date')) {
      view = new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
        date: this.model.get('date'),
        title: "Date",
        field_name: "date",
        required: true
      });
      this.showChildView('date', view);
    }

    if (this.getOption('edit_amount')) {
      view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_0__.default({
        field_name: 'amount',
        value: this.model.get('amount'),
        title: "Montant",
        required: true
      });
      this.showChildView('amount', view);
    }
  },
  templateContext: function templateContext() {
    return {
      title: this.getOption('title')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentLineFormView);

/***/ }),

/***/ "./src/task/views/payments/PaymentLineTableView.js":
/*!*********************************************************!*\
  !*** ./src/task/views/payments/PaymentLineTableView.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _PaymentDepositView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PaymentDepositView.js */ "./src/task/views/payments/PaymentDepositView.js");
/* harmony import */ var _models_PaymentLineModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../models/PaymentLineModel.js */ "./src/task/models/PaymentLineModel.js");
/* harmony import */ var _PaymentLineCollectionView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PaymentLineCollectionView.js */ "./src/task/views/payments/PaymentLineCollectionView.js");
/* harmony import */ var _PaymentLineFormView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PaymentLineFormView.js */ "./src/task/views/payments/PaymentLineFormView.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../math.js */ "./src/math.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");








var PaymentLineTableView = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend({
  template: __webpack_require__(/*! ./templates/PaymentLineTableView.mustache */ "./src/task/views/payments/templates/PaymentLineTableView.mustache"),
  regions: {
    lines: {
      el: '.paymentlines',
      replaceElement: true
    },
    modalRegion: '.payment-line-modal-container',
    deposit: ".deposit",
    addbutton: '.addbutton'
  },
  modelEvents: {
    'changed:payment_times': 'onPaymentTimesChanged',
    'saved:payment_times': 'onPaymentTimesChanged',
    'saved:deposit': 'showDeposit',
    'saved:paymentDisplay': 'render'
  },
  childViewEvents: {
    'line:edit': 'onLineEdit',
    'line:delete': 'onLineDelete',
    'order:up': 'onModelOrderUp',
    'order:down': 'onModelOrderDown',
    'destroy:modal': 'render',
    'add:duedate': 'onLineAdd'
  },
  initialize: function initialize(options) {
    this.collection = options['collection'];
    this.message = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('message');
  },
  onPaymentTimesChanged: function onPaymentTimesChanged(value) {
    this.showAddButton();
    this.showDeposit();
  },
  onLineAdd: function onLineAdd() {
    var model = new _models_PaymentLineModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      task_id: this.model.get('id'),
      order: this.collection.getMaxOrder() - 1
    });
    this.showPaymentLineForm(model, "Ajouter une échéance", false);
  },
  onLineEdit: function onLineEdit(childView) {
    this.showPaymentLineForm(childView.model, "Modifier l’échéance", true);
  },
  onModelOrderUp: function onModelOrderUp(childView) {
    this.collection.moveUp(childView.model);
  },
  onModelOrderDown: function onModelOrderDown(childView) {
    this.collection.moveDown(childView.model);
  },
  showPaymentLineForm: function showPaymentLineForm(model, title, edit) {
    var edit_amount = false;

    if (this.model.isAmountEditable()) {
      edit_amount = true;

      if (edit) {
        if (model.isLast()) {
          edit_amount = false;
        }
      }
    }

    var form = new _PaymentLineFormView_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: model,
      title: title,
      destCollection: this.collection,
      edit: edit,
      edit_amount: edit_amount,
      show_date: this.model.isDateEditable()
    });
    this.showChildView('modalRegion', form);
  },
  onDeleteSuccess: function onDeleteSuccess() {
    var _this = this;

    this.message.trigger('success', "Vos données ont bien été supprimées");
    this.collection.fetch({
      success: function success() {
        return _this.model.fetch();
      }
    });
  },
  onDeleteError: function onDeleteError() {
    this.message.trigger('error', "Une erreur a été rencontrée lors de la suppression de cet élément");
    this.collection.fetch();
  },
  onLineDelete: function onLineDelete(childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette échéance ?");

    if (result) {
      childView.model.destroy({
        success: this.onDeleteSuccess.bind(this),
        error: this.onDeleteError.bind(this)
      });
    }
  },
  templateContext: function templateContext() {
    return {
      show_date: this.model.isDateEditable(),
      show_add: this.model.isAmountEditable()
    };
  },
  showDeposit: function showDeposit() {
    var deposit = (0,_math_js__WEBPACK_IMPORTED_MODULE_5__.strToFloat)(this.model.get('deposit'));

    if (deposit > 0) {
      var view = new _PaymentDepositView_js__WEBPACK_IMPORTED_MODULE_0__.default({
        model: this.model,
        show_date: this.model.isDateEditable()
      });
      this.showChildView('deposit', view);
    } else {
      this.getRegion('deposit').empty();
    }
  },
  showLines: function showLines() {
    this.showChildView('lines', new _PaymentLineCollectionView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.collection,
      show_date: this.model.isDateEditable(),
      edit: this.model.isAmountEditable()
    }));
  },
  showAddButton: function showAddButton() {
    if (this.model.isAmountEditable()) {
      var widget = new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_6__.default({
        label: "Ajouter une échéance",
        icon: "plus",
        event: "add:duedate",
        css: 'btn-info'
      });
      this.showChildView('addbutton', widget);
    } else {
      this.getRegion('addbutton').empty();
    }
  },
  onRender: function onRender() {
    this.showDeposit();
    this.showLines();
    this.showAddButton();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentLineTableView);

/***/ }),

/***/ "./src/task/views/payments/PaymentLineView.js":
/*!****************************************************!*\
  !*** ./src/task/views/payments/PaymentLineView.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../math.js */ "./src/math.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../date.js */ "./src/date.js");





var PaymentLineView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  tagName: 'tr',
  className: 'row taskline',
  template: __webpack_require__(/*! ./templates/PaymentLineView.mustache */ "./src/task/views/payments/templates/PaymentLineView.mustache"),
  modelEvents: {
    'change': 'render'
  },
  ui: {
    up_button: 'button.up',
    down_button: 'button.down',
    edit_button: 'button.edit',
    delete_button: 'button.delete'
  },
  triggers: {
    'click @ui.up_button': 'order:up',
    'click @ui.down_button': 'order:down',
    'click @ui.edit_button': 'edit',
    'click @ui.delete_button': 'delete'
  },
  initialize: function initialize(options) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.listenTo(channel, 'bind:validation', this.bindValidation);
    this.listenTo(channel, 'unbind:validation', this.unbindValidation);
    this.listenTo(this.model, 'validated:invalid', this.showErrors);
    this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().unbind(this);
  },
  templateContext: function templateContext() {
    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    return {
      edit: this.getOption('edit'),
      show_date: this.getOption('show_date'),
      date: (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.formatDate)(this.model.get('date')),
      amount: (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.model.get('amount')),
      is_not_first: order != min_order,
      is_not_before_last: order != max_order - 1,
      is_not_last: order != max_order
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PaymentLineView);

/***/ }),

/***/ "./src/task/views/related_estimation/RelatedEstimationCollectionView.js":
/*!******************************************************************************!*\
  !*** ./src/task/views/related_estimation/RelatedEstimationCollectionView.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_0__);


var template = __webpack_require__(/*! ./templates/RelatedEstimationView.mustache */ "./src/task/views/related_estimation/templates/RelatedEstimationView.mustache");

var RelatedEstimationView = backbone_marionette__WEBPACK_IMPORTED_MODULE_0___default().View.extend({
  template: template,
  tagName: 'span'
});
var RelatedEstimationCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_0___default().CollectionView.extend({
  template: __webpack_require__(/*! ./templates/RelatedEstimationCollectionView.mustache */ "./src/task/views/related_estimation/templates/RelatedEstimationCollectionView.mustache"),
  childView: RelatedEstimationView,
  className: "separate_bottom content_vertical_padding",
  childViewContainer: '.children',
  templateContext: function templateContext() {
    return {
      multiple: this.collection.length > 1
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RelatedEstimationCollectionView);

/***/ }),

/***/ "./src/task/views/resume/Contributions.js":
/*!************************************************!*\
  !*** ./src/task/views/resume/Contributions.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../math */ "./src/math.js");




var template = __webpack_require__(/*! ./templates/Contributions.mustache */ "./src/task/views/resume/templates/Contributions.mustache");

var Contributions = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  regions: {},
  ui: {},
  events: {},
  childViewEvents: {},
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.data = this.getOption('data');
  },
  templateContext: function templateContext() {
    // Collect data sent to the template (model attributes are already transmitted)
    var insurance = this.data.insurance || 0;
    var contribution = this.data.cae || 0;
    var has_insurance = insurance && insurance != 0;
    var has_contribution = contribution && contribution != 0;
    return {
      insurance: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(insurance, true),
      has_insurance: has_insurance,
      has_contribution: has_contribution,
      contribution: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(contribution, true),
      total: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(contribution + insurance, true)
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Contributions);

/***/ }),

/***/ "./src/task/views/resume/Details.js":
/*!******************************************!*\
  !*** ./src/task/views/resume/Details.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../math */ "./src/math.js");




var template = __webpack_require__(/*! ./templates/Details.mustache */ "./src/task/views/resume/templates/Details.mustache");

var Details = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  initialize: function initialize() {
    this.data = this.getOption('data');
  },
  templateContext: function templateContext() {
    // Collect data sent to the template (model attributes are already transmitted)
    var flat_cost = this.data['flat_cost'];
    var general_overhead = this.data['general_overhead'];
    var margin = this.data['margin'];
    var contribution = this.data['contribution'];
    return {
      flat_cost: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(flat_cost, true),
      general_overhead: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(general_overhead, true),
      margin: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(margin, true),
      total_ht: (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.data['total_ht'], true),
      has_hours: 'hours' in this.data,
      hours: this.data['hours'],
      title: this.getOption('title'),
      label: this.getOption('label')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Details);

/***/ }),

/***/ "./src/task/views/resume/ResumeView.js":
/*!*********************************************!*\
  !*** ./src/task/views/resume/ResumeView.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Totals__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Totals */ "./src/task/views/resume/Totals.js");
/* harmony import */ var _Details__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Details */ "./src/task/views/resume/Details.js");
/* harmony import */ var _Contributions_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Contributions.js */ "./src/task/views/resume/Contributions.js");






var template = __webpack_require__(/*! ./templates/ResumeView.mustache */ "./src/task/views/resume/templates/ResumeView.mustache");
/**
 * Show totals
 */


var ResumeView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  className: "totals grand-total",
  template: template,
  regions: {
    'totals': '.totals'
  },
  modelEvents: {
    'change': 'render'
  },
  templateContext: function templateContext() {
    return {
      has_price_study: this.model.hasPriceStudy(),
      has_contribution: this.model.hasContributions()
    };
  },
  onRender: function onRender() {
    if (this.model.hasPriceStudy()) {
      var price_study = this.model.get('price_study');
      this.addRegion('material', '.total-material');
      this.addRegion('labor', '.total-labor');
      this.showChildView('material', new _Details__WEBPACK_IMPORTED_MODULE_2__.default({
        title: "Matériel",
        label: "Déboursé sec",
        data: price_study.material
      }));
      this.showChildView('labor', new _Details__WEBPACK_IMPORTED_MODULE_2__.default({
        title: "Main d’œuvre",
        label: "Déboursé sec",
        data: price_study.labor
      }));

      if (this.model.hasContributions()) {
        this.addRegion('contribution', '.total-contribution');
        this.showChildView('contribution', new _Contributions_js__WEBPACK_IMPORTED_MODULE_3__.default({
          data: price_study.contributions
        }));
      }
    }

    this.showChildView('totals', new _Totals__WEBPACK_IMPORTED_MODULE_1__.default({
      model: this.model
    }));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ResumeView);

/***/ }),

/***/ "./src/task/views/resume/SmallResumeView.js":
/*!**************************************************!*\
  !*** ./src/task/views/resume/SmallResumeView.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! math */ "./src/math.js");




var template = __webpack_require__(/*! ./templates/SmallResumeView.mustache */ "./src/task/views/resume/templates/SmallResumeView.mustache");

var SmallResumeView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  regions: {},
  ui: {},
  events: {},
  childViewEvents: {},
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  },
  templateContext: function templateContext() {
    var compute_mode = this.config.request('get:options', 'compute_mode');
    var is_ttc_mode = compute_mode == 'ttc';
    var has_discounts = this.model.get('discount_total_ht') != 0;
    return {
      is_ttc_mode: is_ttc_mode,
      ttc: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ttc'), true),
      ht: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ht'), true),
      ht_before: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ht_before_discounts'), true),
      ttc_before: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ttc_before_discounts'), true),
      has_discounts: has_discounts
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SmallResumeView);

/***/ }),

/***/ "./src/task/views/resume/Totals.js":
/*!*****************************************!*\
  !*** ./src/task/views/resume/Totals.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! math */ "./src/math.js");




var template = __webpack_require__(/*! ./templates/Totals.mustache */ "./src/task/views/resume/templates/Totals.mustache");

var Totals = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  regions: {},
  ui: {},
  events: {},
  childViewEvents: {},
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  },
  templateContext: function templateContext() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    var compute_mode = config.request('get:options', 'compute_mode');
    var is_ttc_mode = compute_mode == 'ttc';
    var has_discounts = this.model.get('discount_total_ht') != 0;
    return {
      is_ttc_mode: is_ttc_mode,
      ttc: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ttc'), true),
      ht: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ht'), true),
      ht_before: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ht_before_discounts'), true),
      ttc_before: (0,math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ttc_before_discounts'), true),
      tvas: this.model.tva_labels(),
      has_discounts: has_discounts
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Totals);

/***/ }),

/***/ "./src/widgets/LabelRowWidget.js":
/*!***************************************!*\
  !*** ./src/widgets/LabelRowWidget.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../tools.js */ "./src/tools.js");


var LabelRowWidget = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  className: 'row_recap',
  template: __webpack_require__(/*! ./templates/LabelRowWidget.mustache */ "./src/widgets/templates/LabelRowWidget.mustache"),
  templateContext: function templateContext() {
    var values = this.getOption('values');
    var label = (0,_tools_js__WEBPACK_IMPORTED_MODULE_0__.getOpt)(this, 'label', '');

    if (!Array.isArray(values)) {
      values = [{
        'label': label,
        'value': values
      }];
    }

    return {
      values: values,
      colspan: (0,_tools_js__WEBPACK_IMPORTED_MODULE_0__.getOpt)(this, 'colspan', 1)
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LabelRowWidget);

/***/ }),

/***/ "./src/common/views/templates/StatusView.mustache":
/*!********************************************************!*\
  !*** ./src/common/views/templates/StatusView.mustache ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "class='caution'";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<div class='alert alert-info'>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"help_text") || (depth0 != null ? lookupProperty(depth0,"help_text") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"help_text","hash":{},"data":data,"loc":{"start":{"line":16,"column":37},"end":{"line":16,"column":52}}}) : helper)))
    + "</div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "									<div class='form-group'>\n										<div class='alert alert-danger'><span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#danger\"></use></svg></span> La date du document diffère de la date du jour</div>\n										<div class=\"radio\">\n										<label>\n											<input type=\"radio\" name=\"change_date\" value='1' checked>\n											<span>Mettre à la date d’aujourd’hui "
    + alias4(((helper = (helper = lookupProperty(helpers,"today") || (depth0 != null ? lookupProperty(depth0,"today") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"today","hash":{},"data":data,"loc":{"start":{"line":25,"column":48},"end":{"line":25,"column":59}}}) : helper)))
    + "</span>\n										</label>\n										</div>\n										<div class=\"radio\">\n										<label>\n											<input type=\"radio\" name=\"change_date\" value='0'>\n											<span>Conserver la date "
    + alias4(((helper = (helper = lookupProperty(helpers,"date") || (depth0 != null ? lookupProperty(depth0,"date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data,"loc":{"start":{"line":31,"column":35},"end":{"line":31,"column":45}}}) : helper)))
    + "</span>\n										</label>\n										</div>\n									</div>\n									<hr />\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "									<div class='form-group'>\n										<table class='files'></table>\n										<div class=\"checkbox\">\n										<label>\n											<input type=\"checkbox\" name=\"force_file_validation\">\n											<span>Ignorer les fichiers manquants</span>\n										</label>\n										</div>\n									</div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "disabled aria-disabled='true'";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section id=\"status_form\" class=\"modal_view size_extralarge\">\n	<form class='form' data-url='"
    + alias4(((helper = (helper = lookupProperty(helpers,"url") || (depth0 != null ? lookupProperty(depth0,"url") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data,"loc":{"start":{"line":2,"column":30},"end":{"line":2,"column":39}}}) : helper)))
    + "' method='POST'>\n		<div role=\"dialog\" id=\"status-forms\" aria-modal=\"true\" aria-labelledby=\"status-forms_title\">\n			<div class=\"modal_layout\">\n				<header "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_error") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":12},"end":{"line":5,"column":51}}})) != null ? stack1 : "")
    + ">\n					<button type='button' class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\" onclick=\"toggleModal('status_form'); return false;\">\n						<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n					</button>\n					<h2 id=\"status-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":9,"column":33},"end":{"line":9,"column":44}}}) : helper)))
    + "</h2>\n				</header>\n				<div class=\"layout flex two_cols third_reverse pdf_viewer\">\n					<div class=\"preview\"></div>\n					<div class=\"modal_content_layout\">\n						<div class=\"modal_content\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"help_text") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":15,"column":7},"end":{"line":17,"column":14}}})) != null ? stack1 : "")
    + "								<div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"ask_for_date") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":19,"column":9},"end":{"line":36,"column":16}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_file_warning") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":37,"column":9},"end":{"line":47,"column":16}}})) != null ? stack1 : "")
    + "									<div class='form-group'>\n										<label for='comment'>Commentaires</label>\n										<textarea class='form-control' name='comment' rows=4></textarea>\n									</div>\n								</div>\n						</div>\n						<footer>\n							<button class='btn btn-primary' "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_file_warning") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":55,"column":39},"end":{"line":55,"column":99}}})) != null ? stack1 : "")
    + " type='button' name='submit' value='"
    + alias4(((helper = (helper = lookupProperty(helpers,"status") || (depth0 != null ? lookupProperty(depth0,"status") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data,"loc":{"start":{"line":55,"column":135},"end":{"line":55,"column":145}}}) : helper)))
    + "'>\n								"
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":56,"column":8},"end":{"line":56,"column":17}}}) : helper)))
    + "\n							</button>\n							<button type=\"button\" class='btn cancel'>\n								Annuler\n							</button>\n						</footer>\n					</div>\n				</div>\n			</div>\n		</div>\n	</form>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/discount/templates/DiscountComponent.mustache":
/*!******************************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/templates/DiscountComponent.mustache ***!
  \******************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "	<h2 class=\"title collapse_title\">\n		<a\n			data-target=\"#discounts-content\"\n			data-toggle=\"collapse\"\n			aria-expanded=\"false\"\n			aria-controls=\"discounts-content\"\n			>\n			<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n			<span class=\"icon status error\" title=\"Ce bloc comporte une erreur\">\n				<svg><use href=\"/static/icons/endi.svg#exclamation-triangle\"></use></svg>\n			</span>\n			<span class=\"icon status success\" title=\"Ce bloc est valide\">\n				<svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n			</span>\n			Remises\n		</a>\n	</h2>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <h2 class='title'>Remises</h2>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "class=\"collapse_content\"";
},"7":function(container,depth0,helpers,partials,data) {
    return "class='content collapse' aria-expanded=\"false\"";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "			<div class='table_container'>\n				<table class=\"hover_table\">\n					<thead>\n						<tr>\n							<th scope=\"col\" class=\"col_text\">Description</th>\n                            <th scope=\"col\" class=\"col_number\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.program(12, data, 0),"data":data,"loc":{"start":{"line":34,"column":32},"end":{"line":38,"column":39}}})) != null ? stack1 : "")
    + "                            </th>\n							<th scope=\"col\" class='col_number tva' title=\"Taux de TVA\"><span class=\"screen-reader-text\">Taux de </span>Tva</th>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":7},"end":{"line":43,"column":14}}})) != null ? stack1 : "")
    + "						</tr>\n					</thead>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "                                Montant TTC\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "                                Montant HT\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "							<th scope=\"col\" class=\"col_actions\" title=\"Actions\"><span class=\"screen-reader-text\">Actions</span></th>\n";
},"16":function(container,depth0,helpers,partials,data) {
    return "					 <div class=\"actions content_double_padding align_right\">\n					<button\n						type=\"'button\"\n						class=\"btn btn-add\"\n						title=\"Ajouter une remise\"\n					>\n						<svg>\n						<use href=\"/static/icons/endi.svg#plus\"></use>\n						</svg>\n						Ajouter une remise\n					</button>\n					</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"separate_block border_left_block\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collapsed") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":22,"column":11}}})) != null ? stack1 : "")
    + "	<div class='modalregion'></div>\n	<div "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collapsed") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":6},"end":{"line":24,"column":54}}})) != null ? stack1 : "")
    + ">\n		<div id=\"discounts-content\" "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collapsed") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":25,"column":30},"end":{"line":25,"column":100}}})) != null ? stack1 : "")
    + ">\n			<div class='block-errors'></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"not_empty") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":3},"end":{"line":46,"column":10}}})) != null ? stack1 : "")
    + "					<tbody class='lines'>\n					</tbody>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":49,"column":5},"end":{"line":62,"column":12}}})) != null ? stack1 : "")
    + "				</table>\n			</div>\n		</div>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/discount/templates/DiscountFormPopupView.mustache":
/*!**********************************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/templates/DiscountFormPopupView.mustache ***!
  \**********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <nav>\n				<ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n					<li role=\"presentation\" class=\"active\">\n						<a href=\"#main-discount-container\"\n							aria-controls=\"main-discount-container\"\n							id=\"main-discount-tabtitle\"\n							role=\"tab\"\n							data-toggle=\"tab\">\n							Saisie d’un montant\n						</a>\n					</li>\n					<li role=\"presentation\">\n						<a href=\"#percentage-container\"\n							aria-controls=\"percentage-container\"\n							id=\"percentage-tabtitle\"\n							role=\"tab\"\n							data-toggle=\"tab\">\n							Remise en pourcentage\n						</a>\n					</li>\n				</ul>\n			</nav>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section id=\"discount_form\" class=\"modal_view size_middle\">\n    <div role=\"dialog\" id=\"discount-forms\" aria-modal=\"true\" aria-labelledby=\"discount-forms_title\">\n        <div class=\"modal_layout\">\n            <header>\n                <button\n                    type='button'\n                    class=\"icon only unstyled close\"\n                    title=\"Fermer cette fenêtre\"\n                    aria-label=\"Fermer cette fenêtre\"\n                    >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"discount-forms_title\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":13,"column":46},"end":{"line":13,"column":57}}}) : helper)))
    + "</h2>\n            </header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":15,"column":3},"end":{"line":38,"column":10}}})) != null ? stack1 : "")
    + "			<div class='tab-content'>\n				<div\n					role=\"tabpanel\"\n					class=\"tab-pane fade in active simple-form\"\n					id=\"main-discount-container\"\n					aria-labelledby=\"main-discount-tabtitle\">\n				</div>\n				<div\n					role=\"tabpanel\"\n					class=\"tab-pane percent-form\"\n					id=\"percentage-container\"\n					aria-labelledby=\"percentage-tabtitle\">\n				</div>\n			</div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/discount/templates/DiscountFormView.mustache":
/*!*****************************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/templates/DiscountFormView.mustache ***!
  \*****************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<form class=\"modal_content_layout layout\">\n	<div class=\"modal_content\">\n		<fieldset>\n			<div class='order'></div>\n			<div class=\"row form-row\">\n				<div class='description col-md-12'></div>\n			</div>\n			<div class=\"row form-row\">\n				<div class='amount col-md-6'></div>\n				<div class='tva col-md-6'></div>\n			</div>\n		</fieldset>\n	</div>\n	<footer>\n		<button class='btn btn-primary btn-success' type='submit' value='submit'>\n			"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":16,"column":3},"end":{"line":16,"column":14}}}) : helper)))
    + "\n		</button>\n		<button class='btn' type='reset' value='submit'>\n			Annuler\n		</button>\n	</footer>\n</form>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/discount/templates/DiscountPercentView.mustache":
/*!********************************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/templates/DiscountPercentView.mustache ***!
  \********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<form class=\"modal_content_layout layout\">\n	<div class='modal_content'>\n		<fieldset>\n			<div class=\"row form-row\">\n				<div class='description col-md-12'></div>\n			</div>\n			<div class=\"row form-row\">\n				<div class='percentage col-md-12'></div>\n			</div>\n		</fieldset>\n	</div>\n	<footer>\n		<button class='btn btn-primary btn-success' type='submit' value='submit'>\n		"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":14,"column":2},"end":{"line":14,"column":13}}}) : helper)))
    + "\n		</button>\n		<button class='btn' type='reset' value='cancel'>\n		Annuler\n		</button>\n	</footer>\n</form>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/discount/templates/DiscountView.mustache":
/*!*************************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/templates/DiscountView.mustache ***!
  \*************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "<td class='col_actions width_two actions'>\n	<ul>\n		<li>\n			<button type='button' class='btn icon only edit' title='Modifier' aria-label='Modifier'>\n				<svg><use href=\"/static/icons/endi.svg#pen\"></use></svg>\n			</button>\n		</li>\n		<li>\n			<button type='button' class='btn icon only negative delete' title='Supprimer' aria-label='Supprimer'>\n				<svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg>\n			</button>\n		</li>\n	</ul>\n</td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text description rich_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":43},"end":{"line":1,"column":62}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number amount'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"amount_label") || (depth0 != null ? lookupProperty(depth0,"amount_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"amount_label","hash":{},"data":data,"loc":{"start":{"line":2,"column":30},"end":{"line":2,"column":50}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":3,"column":23},"end":{"line":3,"column":38}}}) : helper)))
    + "</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":19,"column":7}}})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/discount/templates/HtBeforeDiscountsView.mustache":
/*!**********************************************************************************************!*\
  !*** ./src/task/views/composition/classic/discount/templates/HtBeforeDiscountsView.mustache ***!
  \**********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<table class='line'></table>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/lines/templates/LineContainerView.mustache":
/*!***************************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/templates/LineContainerView.mustache ***!
  \***************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='line'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/lines/templates/LinesComponent.mustache":
/*!************************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/templates/LinesComponent.mustache ***!
  \************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "		<div class='actions content_double_padding align_right'>\n			<button class='btn add' type='button' title='Ajouter un bloc composé de plusieurs produits que vous pourrez différencier par un titre et une description' aria-label='Ajouter un bloc composé de plusieurs produits que vous différencier par un titre et une description'>\n				<svg><use href=\"/static/icons/endi.svg#plus\"></use></svg>\n				Ajouter un chapitre\n			</button>\n		</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class='separate_block composite'>\n	<div class='border_left_block'>\n		<h2 class=\"title\">Liste des produits</h2>\n		<div class='group-errors'></div>\n	</div>\n	<div class='group-container'></div>\n	<div class='group-modalregion'></div>\n	<div class='border_left_block'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"can_add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":1},"end":{"line":16,"column":8}}})) != null ? stack1 : "")
    + "	</div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/lines/templates/TaskGroupFormView.mustache":
/*!***************************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/templates/TaskGroupFormView.mustache ***!
  \***************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <nav>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a href=\"#form-container\"\n                            aria-controls=\"form-container\"\n                            id=\"form-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\">\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a href=\"#catalog-container\"\n                            aria-controls=\"catalog-container\"\n                            id=\"catalog-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\">\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                    <div\n                        role=\"tabpanel\"\n                        class=\"tab-pane\"\n                        id=\"catalog-container\"\n                        aria-labelledby=\"catalog-tabtitle\">\n                    </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section id=\"task_group_form\" class=\"modal_view size_middle\">\n    <div role=\"dialog\" id=\"task_group-forms\" aria-modal=\"true\" aria-labelledby=\"task_group-forms_title\">\n        <div class=\"modal_layout\">\n            <header>\n                <button type='button'\n                class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"task_group-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":9,"column":48},"end":{"line":9,"column":59}}}) : helper)))
    + "</h2>\n            </header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":12},"end":{"line":34,"column":19}}})) != null ? stack1 : "")
    + "            <div class='tab-content'>\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\"\n                    aria-labelledby=\"form-tabtitle\">\n                        <form class='modal_content_layout layout taskgroup-form'>\n                            <div class=\"modal_content\">\n                                <fieldset>\n                                    <div class='order'></div>\n                                    <div class='taskgroup-title'></div>\n                                    <div class='description'></div>\n                                    <div class='field-display_details'></div>\n                                </fieldset>\n                            </div>\n                            <footer>\n                                <button \n                                    class='btn btn-primary' \n                                    type='submit' \n                                    value='submit'\n                                    title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":55,"column":43},"end":{"line":55,"column":52}}}) : helper)))
    + "\"\n                                    aria-label=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":56,"column":48},"end":{"line":56,"column":57}}}) : helper)))
    + "\"\n                                >\n                                    <svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n                                    Valider\n                                </button>\n                                <button class='btn' type='reset' value='submit'>\n                                    Annuler\n                                </button>\n                            </footer>\n                        </form>\n                    </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":67,"column":20},"end":{"line":74,"column":27}}})) != null ? stack1 : "")
    + "                </div>\n            </div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/lines/templates/TaskGroupView.mustache":
/*!***********************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/templates/TaskGroupView.mustache ***!
  \***********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                <small><em>Ce chapitre n’a pas de titre ni de description. <br/>\n                    Cliquez sur Modifier si vous voulez les ajouter.</em></small>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "					Titre : "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":12,"column":13},"end":{"line":12,"column":22}}}) : helper)))
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"no_title_no_description") : depth0),{"name":"if","hash":{},"fn":container.noop,"inverse":container.program(6, data, 0),"data":data,"loc":{"start":{"line":14,"column":20},"end":{"line":16,"column":27}}})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "					    <small><em>Aucun titre n’a été saisi pour ce chapitre</em></small>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                    Description : "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"group_description") || (depth0 != null ? lookupProperty(depth0,"group_description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"group_description","hash":{},"data":data,"loc":{"start":{"line":21,"column":34},"end":{"line":21,"column":57}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"no_title_no_description") : depth0),{"name":"if","hash":{},"fn":container.noop,"inverse":container.program(11, data, 0),"data":data,"loc":{"start":{"line":23,"column":20},"end":{"line":25,"column":27}}})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    return "					    <small><em>Aucun détail n’a été saisi pour ce chapitre</em></small>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <div>\n            <button type='button' class='btn icon only edit' aria-label=\"Modifier ce chapitre\" title=\"Modifier ce chapitre\" tabindex='-1'>\n                <svg><use href=\"/static/icons/endi.svg#pen\"></use></svg>\n            </button>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_delete") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":34,"column":3},"end":{"line":38,"column":10}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":39,"column":3},"end":{"line":43,"column":10}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":44,"column":3},"end":{"line":48,"column":10}}})) != null ? stack1 : "")
    + "        </div>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "			<button type='button' class='btn icon only negative delete' aria-label='Supprimer ce chapitre' title='Supprimer ce chapitre' tabindex='-1'>\n                <svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg>\n			</button>\n";
},"16":function(container,depth0,helpers,partials,data) {
    return "				<button type='button' class='btn icon only up' title='Déplacer ce chapitre vers le haut' aria-label='Déplacer ce chapitre vers le haut' tabindex='-1'>\n					<svg><use href=\"/static/icons/endi.svg#arrow-up\"></use></svg>\n				</button>\n";
},"18":function(container,depth0,helpers,partials,data) {
    return "				<button type='button' class='btn icon only down' title='Déplacer ce chapitre vers le bas' aria-label='Déplacer ce chapitre vers le bas' tabindex='-1'>\n					<svg><use href=\"/static/icons/endi.svg#arrow-down\"></use></svg>\n				</button>\n";
},"20":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				<thead>\n					<tr>\n						<th scope=\"col\" class=\"col_text\">Description</th>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasDate") : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":59,"column":24},"end":{"line":61,"column":31}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(23, data, 0),"inverse":container.program(25, data, 0),"data":data,"loc":{"start":{"line":62,"column":24},"end":{"line":66,"column":31}}})) != null ? stack1 : "")
    + "						<th scope=\"col\" class=\"col_number quantity\" title=\"Quantité\">Q<span class=\"screen-reader-text\">uanti</span>té</th>\n						<th scope=\"col\" class=\"col_text unity\">Unité</th>\n						<th scope=\"col\" class='col_number tva' title=\"Taux de TVA\"><span class=\"screen-reader-text\">Taux de </span>Tva</th>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.program(29, data, 0),"data":data,"loc":{"start":{"line":70,"column":24},"end":{"line":74,"column":31}}})) != null ? stack1 : "")
    + "						<th scope=\"col\" class=\"col_text\">Compte produit</th>\n						<th scope=\"col\" class=\"col_actions\" title=\"Actions\"><span class=\"screen-reader-text\">Actions</span></th>\n						<th scope=\"col\" class=\"col_actions row_ordering\" title=\"Actions (classement)\"><span class=\"screen-reader-text\">Actions (classement)</span></th>\n					</tr>\n				</thead>\n";
},"21":function(container,depth0,helpers,partials,data) {
    return "						    <th scope=\"col\" class=\"col_date date\" title=\"Date d'exécution\">Date<span class=\"screen-reader-text\"> d'exécution</span></th>\n";
},"23":function(container,depth0,helpers,partials,data) {
    return "						    <th scope=\"col\" class=\"col_number unity\" title=\"Prix Unitaire Toutes Taxes Comprises\">P<span class=\"screen-reader-text\">rix </span>U<span class=\"screen-reader-text\">nitaire</span> T<span class=\"screen-reader-text\">outes </span>T<span class=\"screen-reader-text\">axes </span>C<span class=\"screen-reader-text\">omprises</span></th>\n";
},"25":function(container,depth0,helpers,partials,data) {
    return "						    <th scope=\"col\" class=\"col_number unity\" title=\"Prix Unitaire Hors Taxes\">P<span class=\"screen-reader-text\">rix </span>U<span class=\"screen-reader-text\">nitaire</span> H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span></th>\n";
},"27":function(container,depth0,helpers,partials,data) {
    return "     		                <th scope=\"col\" class=\"col_number\" title=\"Prix Toutes Taxes Comprises\">T<span class=\"screen-reader-text\">outes </span>T<span class=\"screen-reader-text\">axes </span>C<span class=\"screen-reader-text\">omprises</span></th>\n";
},"29":function(container,depth0,helpers,partials,data) {
    return "     		                <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxes\">H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span></th>\n";
},"31":function(container,depth0,helpers,partials,data) {
    return "	<div class='actions align_right'>\n		<button type='button' class='btn btn-info btn-add' title=\"Ajouter un produit à ce chapitre\" aria-label=\"Ajouter un produit à ce chapitre\">\n			<svg><use href=\"/static/icons/endi.svg#plus\"></use></svg>\n			Ajouter un produit\n		</button>\n    </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class='errors'></div>\n\n    <div class='layout flex quotation_item_title '>\n        <div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"no_title_no_description") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":12},"end":{"line":9,"column":19}}})) != null ? stack1 : "")
    + "            <h3>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"title") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":11,"column":4},"end":{"line":17,"column":11}}})) != null ? stack1 : "")
    + "            </h3>\n            <p>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"group_description") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":20,"column":4},"end":{"line":26,"column":11}}})) != null ? stack1 : "")
    + "            </p>\n        </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":29,"column":8},"end":{"line":50,"column":15}}})) != null ? stack1 : "")
    + "    </div>\n\n    <div class=\"table_container\">\n		<table class='hover_table top_align_table'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"not_is_empty") : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":55,"column":6},"end":{"line":80,"column":13}}})) != null ? stack1 : "")
    + "			<tbody class='lines'></tbody>\n			<tbody class='subtotal'></tbody>\n 		</table>\n    </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_add") : depth0),{"name":"if","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":85,"column":4},"end":{"line":92,"column":11}}})) != null ? stack1 : "")
    + "\n    <div class='modalregion'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/lines/templates/TaskLineFormView.mustache":
/*!**************************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/templates/TaskLineFormView.mustache ***!
  \**************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <nav>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a href=\"#form-container\"\n                            aria-controls=\"form-container\"\n                            id=\"form-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\"\n                            tabindex='-1'\n                            >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a href=\"#catalog-container\"\n                            aria-controls=\"catalog-container\"\n                            id=\"catalog-tabtitle\"\n                            role=\"tab\"\n                            tabindex='-1'\n                            data-toggle=\"tab\">\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                                    <div class='date required'></div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "                                    <div class='date'></div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane\"\n                    id=\"catalog-container\"\n                    aria-labelledby=\"catalog-tabtitle\">\n                </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section id=\"task_line_form\" class=\"modal_view size_large\">\n    <div role=\"dialog\" id=\"task_line-forms\" aria-modal=\"true\" aria-labelledby=\"task_line-forms_title\">\n        <div class=\"modal_layout\">\n            <header>\n                <button type='button' class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"task_line-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":8,"column":47},"end":{"line":8,"column":58}}}) : helper)))
    + "</h2>\n            </header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":10,"column":12},"end":{"line":36,"column":19}}})) != null ? stack1 : "")
    + "            <div class='tab-content'>\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\"\n                    aria-labelledby=\"form-tabtitle\">\n                    <form class='modal_content_layout form layout taskline-form'>\n                        <div class=\"modal_content\">\n                            <div class=\"row form-row\">\n                                <div class=\"col-md-12\">\n                                    <div class='order'></div>\n                                </div>\n                            </div>\n                            <div class=\"row form-row\">\n                                <div class=\"col-md-12\">\n                                    <div class='description required'></div>\n                                </div>\n                            </div>\n                            <div class=\"row form-row\">\n                                <div class=\"col-md-6\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"dateRequired") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":57,"column":32},"end":{"line":61,"column":39}}})) != null ? stack1 : "")
    + "                                </div>\n                            </div>\n                            <div class=\"row form-row\">\n                                <div class=\"col-md-12\">\n                                    <div class='cost required'></div>\n                                </div>\n                            </div>\n                            <div class=\"row form-row\">\n                                <div class=\"col-md-6\">\n                                    <div class='quantity required'></div>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class='unity'></div>\n                                </div>\n                            </div>\n                            <div class=\"row form-row\">\n                                <div class=\"col-md-6\">\n                                    <div class='tva required'></div>\n                                </div>\n                                <div class=\"col-md-6\">\n                                    <div class='product_id'></div>\n                                </div>\n                            </div>\n                        </div>\n                        <footer>\n                            <button\n                                class='btn btn-primary'\n                                type='submit'\n                                value='submit'\n                                title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":91,"column":39},"end":{"line":91,"column":48}}}) : helper)))
    + "\"\n                                aria-label=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":92,"column":44},"end":{"line":92,"column":53}}}) : helper)))
    + "\"\n                            >\n                                <svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n                                Valider\n                            </button>\n                            <button\n                                class='btn'\n                                type='reset'\n                                value='submit'>\n                                Annuler\n                            </button>\n                        </footer>\n                    </form>\n                </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":106,"column":16},"end":{"line":113,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/lines/templates/TaskLineView.mustache":
/*!**********************************************************************************!*\
  !*** ./src/task/views/composition/classic/lines/templates/TaskLineView.mustache ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <td class='col_date'>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"date") || (depth0 != null ? lookupProperty(depth0,"date") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"date","hash":{},"data":data,"loc":{"start":{"line":3,"column":23},"end":{"line":3,"column":33}}}) : helper)))
    + "</td>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<br /><em title=\"obtenu par arrondi de "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"full_cost_amount") || (depth0 != null ? lookupProperty(depth0,"full_cost_amount") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"full_cost_amount","hash":{},"data":data,"loc":{"start":{"line":8,"column":39},"end":{"line":8,"column":61}}}) : helper))) != null ? stack1 : "")
    + "\"><small><span class=\"screen-reader-text\">obtenu par arrondi de </span>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"full_cost_amount") || (depth0 != null ? lookupProperty(depth0,"full_cost_amount") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"full_cost_amount","hash":{},"data":data,"loc":{"start":{"line":8,"column":132},"end":{"line":8,"column":154}}}) : helper))) != null ? stack1 : "")
    + "</em>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc") || (depth0 != null ? lookupProperty(depth0,"ttc") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ttc","hash":{},"data":data,"loc":{"start":{"line":16,"column":23},"end":{"line":16,"column":34}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_number'>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":19,"column":0},"end":{"line":19,"column":10}}}) : helper))) != null ? stack1 : "")
    + "\n\n</td>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_actions width_two'>\n	<ul>\n		<li>\n			<button type='button' class='btn icon only edit' title='Modifier ce produit' aria-label='Modifier ce produit'>\n				<svg><use href=\"/static/icons/endi.svg#pen\"></use></svg>\n			</button>\n		</li>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_delete") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":32,"column":2},"end":{"line":38,"column":9}}})) != null ? stack1 : "")
    + "	</ul>\n</td>\n<td class='col_actions row_ordering sort'>\n	<ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":43,"column":2},"end":{"line":49,"column":9}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":50,"column":2},"end":{"line":56,"column":9}}})) != null ? stack1 : "")
    + "	</ul>\n</td>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "		<li>\n			<button type='button' class='btn icon only negative delete' title='Supprimer ce produit' aria-label='Supprimer ce produit'>\n				<svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg>\n			</button>\n		</li>\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "		<li>\n			<button type='button' class='btn icon only up' title='Déplacer ce produit vers le haut' aria-label='Déplacer ce produit vers le haut'>\n				<svg><use href=\"/static/icons/endi.svg#arrow-up\"></use></svg>\n			</button>\n		</li>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "		<li>\n			<button type='button' class='btn icon only down' title='Déplacer ce produit vers le bas' aria-label='Déplacer ce produit vers le bas'>\n				<svg><use href=\"/static/icons/endi.svg#arrow-down\"></use></svg>\n			</button>\n		</li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text description rich_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":43},"end":{"line":1,"column":62}}}) : helper))) != null ? stack1 : "")
    + "</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasDate") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":0},"end":{"line":4,"column":7}}})) != null ? stack1 : "")
    + "<td class='col_number'>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"cost_amount") || (depth0 != null ? lookupProperty(depth0,"cost_amount") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cost_amount","hash":{},"data":data,"loc":{"start":{"line":6,"column":0},"end":{"line":6,"column":19}}}) : helper))) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_rounded") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":0},"end":{"line":9,"column":7}}})) != null ? stack1 : "")
    + "\n</td>\n<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"quantity") || (depth0 != null ? lookupProperty(depth0,"quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data,"loc":{"start":{"line":12,"column":23},"end":{"line":12,"column":37}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":13,"column":21},"end":{"line":13,"column":32}}}) : helper)))
    + "</td>\n<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":14,"column":23},"end":{"line":14,"column":38}}}) : helper)))
    + "</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":15,"column":0},"end":{"line":22,"column":7}}})) != null ? stack1 : "")
    + "<td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"product") || (depth0 != null ? lookupProperty(depth0,"product") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product","hash":{},"data":data,"loc":{"start":{"line":23,"column":21},"end":{"line":23,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":0},"end":{"line":59,"column":7}}})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/templates/CompositionComponent.mustache":
/*!************************************************************************************!*\
  !*** ./src/task/views/composition/classic/templates/CompositionComponent.mustache ***!
  \************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id='tasklines'></div>\n<div class='ht-before-discounts'></div>\n<div id='discounts' class='collapsible'></div>\n<div id='expenses_ht' class='collapsible'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/classic/templates/ExpenseHtComponent.mustache":
/*!**********************************************************************************!*\
  !*** ./src/task/views/composition/classic/templates/ExpenseHtComponent.mustache ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"separate_block border_left_block\">\n	<h2 class='title collapse_title'>\n		<a \n			data-target=\"#expenses-content\" \n			data-toggle=\"collapse\" \n			aria-expanded=\"false\" \n			aria-controls=\"expenses-content\"\n			>\n			<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n			Frais forfaitaires\n		</a>\n	</h2>\n	<div class=\"collapse_content\">\n		<div id=\"expenses-content\" class='content collapse' aria-expanded=\"false\">\n			<div class='row'>\n				<form class='form-inline'>\n					<div class='one_line expenses_ht'></div>\n				</form>\n			</div>\n		</div>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/templates/ChapterForm.mustache":
/*!*********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/templates/ChapterForm.mustache ***!
  \*********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"size_middle\">\n    <div\n        role=\"dialog\"\n        id=\"product-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"product-forms_title\"\n    >\n        <form class=\"form chapter-form\">\n            <div class=\"modal_layout\">\n                <header>\n                    <button\n                        type=\"button\"\n                        class=\"icon only unstyled close\"\n                        title=\"Fermer cette fenêtre\"\n                        aria-label=\"Fermer cette fenêtre\"\n                    >\n                        <svg>\n                            <use href=\"/static/icons/endi.svg#times\"></use>\n                        </svg>\n                    </button>\n                    <h2 id=\"product-forms_title\">\n                        "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":22,"column":24},"end":{"line":22,"column":33}}}) : helper)))
    + "\n                    </h2>\n                </header>\n                <div class=\"modal_content_layout\">\n                    <div class=\"modal_content\">\n                        <fieldset>\n                            <div class=\"field-title\"></div>\n                            <div class=\"field-description\"></div>\n                        </fieldset>\n                    </div>\n                    <footer>\n                        <button class=\"btn btn-primary\" type=\"submit\" value=\"submit\">\n                            "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":34,"column":28},"end":{"line":34,"column":37}}}) : helper)))
    + "\n                        </button>\n                        <button class=\"btn\" type=\"reset\" value=\"submit\">\n                            Annuler\n                        </button>\n                    </footer>\n                </div>\n            </div>\n        </form>\n    </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/templates/ChapterView.mustache":
/*!*********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/templates/ChapterView.mustache ***!
  \*********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":5,"column":8},"end":{"line":5,"column":17}}}) : helper)))
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        <small>\n          <em>\n            Aucun titre n’a été saisi pour ce chapitre\n          </em>\n        </small>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":16,"column":8},"end":{"line":16,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "        <small>\n          <em>\n            Aucun détail n’a été saisi pour ce chapitre\n          </em>\n        </small>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "      <button\n        type=\"button\"\n        class=\"btn icon only edit-chapter\"\n        aria-label=\"Modifier ce chapitre\"\n        title=\"Modifier ce chapitre\"\n        tabindex=\"-1\"\n      >\n        <svg>\n          <use href=\"/static/icons/endi.svg#pen\"></use>\n        </svg>\n      </button>\n      <button\n        type=\"button\"\n        class=\"btn icon only negative delete-chapter\"\n        aria-label=\"Supprimer ce chapitre\"\n        title=\"Supprimer ce chapitre\"\n        tabindex=\"-1\"\n      >\n        <svg>\n          <use href=\"/static/icons/endi.svg#trash-alt\"></use>\n        </svg>\n      </button>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "      <button\n        type=\"button\"\n        class=\"btn icon only up up-chapter\"\n        title=\"Déplacer ce chapitre vers le haut\"\n        aria-label=\"Déplacer ce chapitre vers le haut\"\n        tabindex=\"-1\"\n      >\n        <svg>\n          <use href=\"/static/icons/endi.svg#arrow-up\"></use>\n        </svg>\n      </button>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "      <button\n        type=\"button\"\n        class=\"btn icon only down down-chapter\"\n        title=\"Déplacer ce chapitre vers le bas\"\n        aria-label=\"Déplacer ce chapitre vers le bas\"\n        tabindex=\"-1\"\n      >\n        <svg>\n          <use href=\"/static/icons/endi.svg#arrow-down\"></use>\n        </svg>\n      </button>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <div class=\"table_container\">\n    <div class=\"errors\"></div>\n    <table class=\"hover_table top_align_table collapsible\">\n      <thead>\n        <tr>\n          <td class=\"level_spacer\"></td>\n          <th scope=\"col\" class=\"col_text\" colspan=\"2\">\n            Description\n          </th>\n          <th\n            scope=\"col\"\n            class=\"col_number hidden_pdf\"\n            title=\"Cout Unitaire Hors Taxes\"\n          >\n            Cout U.<span class=\"screen-reader-text\">nitaire</span> H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n          </th>\n          <th scope=\"col\" class=\"col_number\" title=\"Prix Unitaire Hors Taxes\">\n            Prix U.<span class=\"screen-reader-text\">nitaire</span> H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n          </th>\n          <th scope=\"col\" class=\"col_number quantity\" title=\"Quantité\">\n            Q<span class=\"screen-reader-text\">uanti</span>té\n          </th>\n          <th scope=\"col\" class=\"col_text unity\">\n            Unité\n          </th>\n          <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxes\">\n            <span class=\"screen-reader-text\">Prix </span>H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n          </th>\n          <th scope=\"col\" class=\"col_number tva\" title=\"Taux de TVA et compte produit\">\n            <span class=\"screen-reader-text\">Taux de </span>TVA<span class=\"screen-reader-text\"> et compte produit</span>\n          </th>\n          <th scope=\"col\" class=\"col_actions width_tree\" title=\"Actions\">\n            <span class=\"screen-reader-text\">Actions</span>\n          </th>\n          <th\n            scope=\"col\"\n            class=\"col_actions row_ordering\"\n            colspan=\"2\"\n            title=\"Actions (classement)\"\n          >\n            <span class=\"screen-reader-text\">Actions (classement)</span>\n          </th>\n        </tr>\n        <tr class=\"row_recap\">\n          <td class=\"level_spacer\"></td>\n          <th\n            scope=\"row\"\n            class=\"col_text\"\n            colspan=\"6\"\n            title=\"Sous-total Hors Taxes\"\n          >\n            Sous-total H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n          </th>\n          <td class=\"col_number\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":134,"column":12},"end":{"line":134,"column":26}}}) : helper))) != null ? stack1 : "")
    + "\n          </td>\n          <td class=\"col_actions content_double_padding align_right\" colspan=\"4\">\n            <div role=\"group\">\n              <div class=\"chapter-addproduct\"></div>\n              <div class=\"chapter-addwork\"></div>\n            </div>\n          </td>\n        </tr>\n      </thead>\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"collectionRawHtml") || (depth0 != null ? lookupProperty(depth0,"collectionRawHtml") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"collectionRawHtml","hash":{},"data":data,"loc":{"start":{"line":144,"column":6},"end":{"line":144,"column":29}}}) : helper))) != null ? stack1 : "")
    + "\n    </table>\n  </div>\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "  <div class=\"actions content_double_padding align_right\">\n    <div role=\"group\">\n      <div class=\"chapter-addproduct\"></div>\n      <div class=\"chapter-addwork\"></div>\n    </div>\n  </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"layout flex quotation_item_title\">\n  <div>\n    <h3>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"title") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":4,"column":6},"end":{"line":12,"column":13}}})) != null ? stack1 : "")
    + "    </h3>\n    <p>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":15,"column":6},"end":{"line":23,"column":13}}})) != null ? stack1 : "")
    + "    </p>\n  </div>\n  <div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":4},"end":{"line":50,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":51,"column":4},"end":{"line":63,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":64,"column":4},"end":{"line":76,"column":11}}})) != null ? stack1 : "")
    + "  </div>\n</div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collectionRawHtml") : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.program(17, data, 0),"data":data,"loc":{"start":{"line":79,"column":0},"end":{"line":154,"column":7}}})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/templates/ProductRawView.mustache":
/*!************************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/templates/ProductRawView.mustache ***!
  \************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <span class=\"hidden_pdf\">\n          Coefficient de marge :\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":9,"column":10},"end":{"line":13,"column":17}}})) != null ? stack1 : "")
    + "        </span>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":10,"column":12},"end":{"line":10,"column":27}}}) : helper)))
    + "\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "            Non renseigné\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht") || (depth0 != null ? lookupProperty(depth0,"supplier_ht") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht","hash":{},"data":data,"loc":{"start":{"line":19,"column":8},"end":{"line":19,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "hidden_pdf";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":36,"column":8},"end":{"line":36,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n        <br />\n        <small>\n          <span class=\"hidden_pdf\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"product_label") || (depth0 != null ? lookupProperty(depth0,"product_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product_label","hash":{},"data":data,"loc":{"start":{"line":40,"column":12},"end":{"line":40,"column":31}}}) : helper))) != null ? stack1 : "")
    + "\n          </span>\n        </small>\n";
},"12":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <li>\n            <button\n              data-action=\"product:up\"\n              data-id="
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":109,"column":22},"end":{"line":109,"column":28}}}) : helper)))
    + "\n              type=\"button\"\n              class=\"btn icon only up\"\n              title=\"Déplacer ce produit vers le haut\"\n              aria-label=\"Déplacer ce produit vers le haut\"\n              tabindex=\"-1\"\n            >\n              <svg>\n                <use href=\"/static/icons/endi.svg#arrow-up\"></use>\n              </svg>\n            </button>\n          </li>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <li>\n            <button\n              data-action=\"product:down\"\n              data-id="
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":126,"column":22},"end":{"line":126,"column":28}}}) : helper)))
    + "\n              type=\"button\"\n              class=\"btn icon only down\"\n              title=\"Déplacer ce produit vers le bas\"\n              aria-label=\"Déplacer ce produit vers le bas\"\n              tabindex=\"-1\"\n            >\n              <svg>\n                <use href=\"/static/icons/endi.svg#arrow-down\"></use>\n              </svg>\n            </button>\n          </li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tbody class=\"lines\">\n  <tr class=\"row taskline\">\n    <td class=\"level_spacer\"></td>\n    <td class=\"col_text description\" colspan=\"2\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":5,"column":6},"end":{"line":5,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":6},"end":{"line":15,"column":13}}})) != null ? stack1 : "")
    + "    </td>\n    <td class=\"col_number hidden_pdf\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":6},"end":{"line":20,"column":13}}})) != null ? stack1 : "")
    + "    </td>\n    <td class=\"col_number "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":26},"end":{"line":22,"column":72}}})) != null ? stack1 : "")
    + "\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":23,"column":6},"end":{"line":23,"column":14}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number quantity "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":25,"column":35},"end":{"line":25,"column":81}}})) != null ? stack1 : "")
    + "\">\n      "
    + alias4(((helper = (helper = lookupProperty(helpers,"quantity") || (depth0 != null ? lookupProperty(depth0,"quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data,"loc":{"start":{"line":26,"column":6},"end":{"line":26,"column":18}}}) : helper)))
    + "\n    </td>\n    <td class=\"col_text unity "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":28,"column":30},"end":{"line":28,"column":76}}})) != null ? stack1 : "")
    + "\">\n      "
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":29,"column":6},"end":{"line":29,"column":15}}}) : helper)))
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":32,"column":6},"end":{"line":32,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number tva\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"tva_label") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":35,"column":6},"end":{"line":43,"column":13}}})) != null ? stack1 : "")
    + "    </td>\n    <td class=\"col_actions width_two\">\n      <a\n        href=\"#chapters/"
    + alias4(((helper = (helper = lookupProperty(helpers,"chapter_id") || (depth0 != null ? lookupProperty(depth0,"chapter_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chapter_id","hash":{},"data":data,"loc":{"start":{"line":47,"column":24},"end":{"line":47,"column":38}}}) : helper)))
    + "/products/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":47,"column":48},"end":{"line":47,"column":54}}}) : helper)))
    + "\"\n        class=\"btn icon only\"\n        title=\"Modifier ce produit\"\n        aria-label=\"Modifier ce produit\"\n      >\n        <svg>\n          <use href=\"../static/icons/endi.svg#pen\"></use>\n        </svg>\n      </a>\n      <div class=\"btn-group\">\n        <button\n          class=\"btn icon only dropdown-toggle\"\n          data-toggle=\"dropdown\"\n          aria-haspopup=\"true\"\n          aria-expanded=\"false\"\n          title=\"Afficher plus d’actions\"\n          aria-label=\"Afficher plus d’actions\"\n        >\n          <svg>\n            <use href=\"../static/icons/endi.svg#dots\"></use>\n          </svg>\n        </button>\n        <ul class=\"dropdown-menu dropdown-menu-right\">\n          <li>\n            <button\n              data-action=\"product:duplicate\"\n              data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":73,"column":23},"end":{"line":73,"column":29}}}) : helper)))
    + "\"\n              type=\"button\"\n              class=\"btn icon\"\n              title=\"Dupliquer ce produit\"\n              aria-label=\"Dupliquer cet ouvrage\"\n            >\n              <svg>\n                <use href=\"../static/icons/endi.svg#copy\"></use>\n              </svg>\n              Dupliquer\n            </button>\n          </li>\n          <li>\n            <button\n              data-action=\"product:delete\"\n              data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":88,"column":23},"end":{"line":88,"column":29}}}) : helper)))
    + "\"\n              type=\"button\"\n              class=\"btn icon negative\"\n              title=\"Supprimer ce produit\"\n              aria-label=\"Supprimer ce produit\"\n            >\n              <svg>\n                <use href=\"../static/icons/endi.svg#trash-alt\"></use>\n              </svg>\n              Supprimer\n            </button>\n          </li>\n        </ul>\n      </div>\n    </td>\n    <td class=\"col_actions row_ordering sort\" colspan=\"2\">\n      <ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":105,"column":8},"end":{"line":121,"column":15}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":122,"column":8},"end":{"line":138,"column":15}}})) != null ? stack1 : "")
    + "      </ul>\n    </td>\n  </tr>\n</tbody>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/templates/WorkItemRawView.mustache":
/*!*************************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/templates/WorkItemRawView.mustache ***!
  \*************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "hidden_pdf";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht") || (depth0 != null ? lookupProperty(depth0,"supplier_ht") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht","hash":{},"data":data,"loc":{"start":{"line":9,"column":6},"end":{"line":9,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "     title=\"Quantité par unité d'oeuvre x quantité d'unité d'oeuvre\"\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "    title=\"Quantité indépendante de la quantité d'unité d'oeuvre\"\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <br />\n    <small>\n          <span>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"quantity_label") || (depth0 != null ? lookupProperty(depth0,"quantity_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"quantity_label","hash":{},"data":data,"loc":{"start":{"line":27,"column":16},"end":{"line":27,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</span>\n      </small>\n";
},"11":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <li>\n          <button\n            type=\"button\"\n            data-action=\"workitem:up\"\n            data-id="
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":107,"column":20},"end":{"line":107,"column":26}}}) : helper)))
    + "\n            data-workid=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"price_study_work_id") || (depth0 != null ? lookupProperty(depth0,"price_study_work_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"price_study_work_id","hash":{},"data":data,"loc":{"start":{"line":108,"column":25},"end":{"line":108,"column":48}}}) : helper)))
    + "\"\n            class=\"btn icon only up\"\n            title=\"Déplacer ce produit vers le haut\"\n            aria-label=\"Déplacer ce produit vers le haut\"\n            tabindex=\"-1\"\n          >\n            <svg>\n              <use href=\"/static/icons/endi.svg#arrow-up\"></use>\n            </svg>\n          </button>\n        </li>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <li>\n          <button\n            type=\"button\"\n            data-action=\"workitem:down\"\n            data-id="
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":125,"column":20},"end":{"line":125,"column":26}}}) : helper)))
    + "\n            data-workid=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"price_study_work_id") || (depth0 != null ? lookupProperty(depth0,"price_study_work_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"price_study_work_id","hash":{},"data":data,"loc":{"start":{"line":126,"column":25},"end":{"line":126,"column":48}}}) : helper)))
    + "\"\n            class=\"btn icon only down\"\n            title=\"Déplacer ce produit vers le bas\"\n            aria-label=\"Déplacer ce produit vers le bas\"\n            tabindex=\"-1\"\n          >\n            <svg>\n              <use href=\"/static/icons/endi.svg#arrow-down\"></use>\n            </svg>\n          </button>\n        </li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tr class=\"row taskline\">\n  <td class=\"level_spacer\"></td>\n  <td class=\"level_spacer\"></td>\n  <td class=\"col_text description "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":34},"end":{"line":4,"column":65}}})) != null ? stack1 : "")
    + "\">\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":5,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n  </td>\n  <td class=\"col_number hidden_pdf\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":10,"column":11}}})) != null ? stack1 : "")
    + "  </td>\n  <td class=\"col_number "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":24},"end":{"line":12,"column":55}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":55},"end":{"line":12,"column":101}}})) != null ? stack1 : "")
    + "\">\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":13,"column":4},"end":{"line":13,"column":12}}}) : helper))) != null ? stack1 : "")
    + "\n  </td>\n  <td \n    class=\"col_number "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":16,"column":22},"end":{"line":16,"column":53}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":16,"column":53},"end":{"line":16,"column":99}}})) != null ? stack1 : "")
    + "\"\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"quantity_label") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":17,"column":4},"end":{"line":21,"column":11}}})) != null ? stack1 : "")
    + "    >\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_quantity") || (depth0 != null ? lookupProperty(depth0,"total_quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_quantity","hash":{},"data":data,"loc":{"start":{"line":23,"column":4},"end":{"line":23,"column":24}}}) : helper))) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"quantity_label") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":4},"end":{"line":29,"column":13}}})) != null ? stack1 : "")
    + "  </td>\n  <td class=\"col_text "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":22},"end":{"line":31,"column":53}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":53},"end":{"line":31,"column":99}}})) != null ? stack1 : "")
    + "\">\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":32,"column":4},"end":{"line":32,"column":15}}}) : helper))) != null ? stack1 : "")
    + "\n  </td>\n  <td class=\"col_number "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":34,"column":24},"end":{"line":34,"column":55}}})) != null ? stack1 : "")
    + "\" title=\""
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":34,"column":64},"end":{"line":34,"column":72}}}) : helper))) != null ? stack1 : "")
    + " x "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_quantity") || (depth0 != null ? lookupProperty(depth0,"total_quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_quantity","hash":{},"data":data,"loc":{"start":{"line":34,"column":75},"end":{"line":34,"column":95}}}) : helper))) != null ? stack1 : "")
    + "\">\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":35,"column":4},"end":{"line":35,"column":18}}}) : helper))) != null ? stack1 : "")
    + "\n  </td>\n  <td class=\"col_actions width_two\" colspan=\"2\">\n    <button\n      data-action=\"workitem:edit\"\n      data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":40,"column":15},"end":{"line":40,"column":21}}}) : helper)))
    + "\"\n      data-workid=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"price_study_work_id") || (depth0 != null ? lookupProperty(depth0,"price_study_work_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"price_study_work_id","hash":{},"data":data,"loc":{"start":{"line":41,"column":19},"end":{"line":41,"column":42}}}) : helper)))
    + "\"\n      type=\"button\"\n      class=\"btn icon only\"\n      title=\"Modifier ce produit\"\n      aria-label=\"Modifier ce produit\"\n    >\n      <svg>\n        <use href=\"../static/icons/endi.svg#pen\"></use>\n      </svg>\n    </button>\n    <div class=\"btn-group\">\n      <button\n        class=\"btn icon only dropdown-toggle\"\n        data-toggle=\"dropdown\"\n        aria-haspopup=\"true\"\n        aria-expanded=\"false\"\n        title=\"Afficher plus d’actions\"\n        aria-label=\"Afficher plus d’actions\"\n      >\n        <svg>\n          <use href=\"../static/icons/endi.svg#dots\"></use>\n        </svg>\n      </button>\n      <ul class=\"dropdown-menu dropdown-menu-right\">\n        <li>\n          <button\n            data-action=\"workitem:duplicate\"\n            data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":68,"column":21},"end":{"line":68,"column":27}}}) : helper)))
    + "\"\n            data-workid=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"price_study_work_id") || (depth0 != null ? lookupProperty(depth0,"price_study_work_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"price_study_work_id","hash":{},"data":data,"loc":{"start":{"line":69,"column":25},"end":{"line":69,"column":48}}}) : helper)))
    + "\"\n            type=\"button\"\n            class=\"btn icon\"\n            title=\"Dupliquer ce produit\"\n            aria-label=\"Dupliquer ce produit\"\n          >\n            <svg>\n              <use href=\"../static/icons/endi.svg#copy\"></use>\n            </svg>\n            Dupliquer\n          </button>\n        </li>\n        <li>\n          <button\n            data-action=\"workitem:delete\"\n            data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":84,"column":21},"end":{"line":84,"column":27}}}) : helper)))
    + "\"\n            data-workid=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"price_study_work_id") || (depth0 != null ? lookupProperty(depth0,"price_study_work_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"price_study_work_id","hash":{},"data":data,"loc":{"start":{"line":85,"column":25},"end":{"line":85,"column":48}}}) : helper)))
    + "\"\n            type=\"button\"\n            class=\"btn icon negative\"\n            title=\"Supprimer ce produit\"\n            aria-label=\"Supprimer ce produit\"\n          >\n            <svg>\n              <use href=\"../static/icons/endi.svg#trash-alt\"></use>\n            </svg>\n            Supprimer\n          </button>\n        </li>\n      </ul>\n    </div>\n  </td>\n  <td class=\"col_actions row_ordering sort\">\n    <ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":102,"column":6},"end":{"line":119,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":120,"column":6},"end":{"line":137,"column":13}}})) != null ? stack1 : "")
    + "    </ul>\n  </td>\n  <td class=\"level_spacer\"></td>\n</tr>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/chapter/templates/WorkRawView.mustache":
/*!*********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/chapter/templates/WorkRawView.mustache ***!
  \*********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "collapsed";
},"3":function(container,depth0,helpers,partials,data) {
    return "true";
},"5":function(container,depth0,helpers,partials,data) {
    return "false";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":23,"column":8},"end":{"line":23,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"9":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":28,"column":12},"end":{"line":28,"column":27}}}) : helper)))
    + "\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "            Non renseigné\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht") || (depth0 != null ? lookupProperty(depth0,"supplier_ht") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht","hash":{},"data":data,"loc":{"start":{"line":36,"column":8},"end":{"line":36,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "hidden_pdf";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":53,"column":8},"end":{"line":53,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n        <br />\n        <small>\n          <span class=\"hidden_pdf\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"product_label") || (depth0 != null ? lookupProperty(depth0,"product_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product_label","hash":{},"data":data,"loc":{"start":{"line":57,"column":12},"end":{"line":57,"column":31}}}) : helper))) != null ? stack1 : "")
    + "\n          </span>\n        </small>\n";
},"19":function(container,depth0,helpers,partials,data) {
    return "              title=\"Masquer le détail de cet ouvrage dans la sortie PDF\"\n              aria-label=\"Masquer le détail de cet ouvrage dans la sortie PDF\"\n";
},"21":function(container,depth0,helpers,partials,data) {
    return "              title=\"Afficher le détail de cet ouvrage dans la sortie PDF\"\n              aria-label=\"Afficher le détail de cet ouvrage dans la sortie PDF\"\n";
},"23":function(container,depth0,helpers,partials,data) {
    return "              <svg>\n                <use href=\"../static/icons/endi.svg#eye-slash\"></use>\n              </svg>\n              Masquer le détail\n";
},"25":function(container,depth0,helpers,partials,data) {
    return "              <svg>\n                <use href=\"../static/icons/endi.svg#eye\"></use>\n              </svg>\n              Afficher le détail\n";
},"27":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <li>\n            <button\n              type=\"button\"\n              data-action=\"product:up\"\n              data-id="
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":169,"column":22},"end":{"line":169,"column":28}}}) : helper)))
    + "\n              class=\"btn icon only up\"\n              title=\"Déplacer ce produit vers le haut\"\n              aria-label=\"Déplacer ce produit vers le haut\"\n              tabindex=\"-1\"\n            >\n              <svg>\n                <use href=\"/static/icons/endi.svg#arrow-up\"></use>\n              </svg>\n            </button>\n          </li>\n";
},"29":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          <li>\n            <button\n              type=\"button\"\n              data-action=\"product:down\"\n              data-id="
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":186,"column":22},"end":{"line":186,"column":28}}}) : helper)))
    + "\n              class=\"btn icon only down\"\n              title=\"Déplacer ce produit vers le bas\"\n              aria-label=\"Déplacer ce produit vers le bas\"\n              tabindex=\"-1\"\n            >\n              <svg>\n                <use href=\"/static/icons/endi.svg#arrow-down\"></use>\n              </svg>\n            </button>\n          </li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tbody class=\"lines collapse_title in\" aria-expanded=\"true\">\n  <tr class=\"row taskline\">\n    <td class=\"level_spacer\"></td>\n    <td class=\"col_text description\" colspan=\"2\">\n      <h4>\n        <a\n          href=\"javascript:void(0)\"\n          data-target=\"#workitems-"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":8,"column":34},"end":{"line":8,"column":40}}}) : helper)))
    + "\"\n          data-toggle=\"collapse\"\n          class=\"collapse_button "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_details") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":10,"column":33},"end":{"line":10,"column":80}}})) != null ? stack1 : "")
    + "\"\n          aria-expanded=\""
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_details") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":11,"column":25},"end":{"line":11,"column":72}}})) != null ? stack1 : "")
    + "\"\n          aria-controls=\"workitems-"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":12,"column":35},"end":{"line":12,"column":41}}}) : helper)))
    + "\"\n          title=\"Masquer les produits de cet ouvrage\"\n          aria-label=\"Masquer les produits de cet ouvrage\"\n        >\n          <svg class=\"arrow\">\n            <use href=\"../static/icons/endi.svg#chevron-down\"></use>\n          </svg>\n          "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":19,"column":10},"end":{"line":19,"column":19}}}) : helper)))
    + "\n        </a>\n      </h4>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":6},"end":{"line":24,"column":13}}})) != null ? stack1 : "")
    + "        <span class=\"hidden_pdf\">\n          Cofficient de marge : \n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(11, data, 0),"data":data,"loc":{"start":{"line":27,"column":10},"end":{"line":31,"column":17}}})) != null ? stack1 : "")
    + "        </span>\n    </td>\n    <td class=\"col_number hidden_pdf\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":35,"column":6},"end":{"line":37,"column":15}}})) != null ? stack1 : "")
    + "    </td>\n    <td class=\"col_number "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":39,"column":26},"end":{"line":39,"column":72}}})) != null ? stack1 : "")
    + "\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":40,"column":6},"end":{"line":40,"column":14}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":42,"column":26},"end":{"line":42,"column":72}}})) != null ? stack1 : "")
    + "\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"quantity") || (depth0 != null ? lookupProperty(depth0,"quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data,"loc":{"start":{"line":43,"column":6},"end":{"line":43,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_text "
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_units") : depth0),{"name":"unless","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":45,"column":24},"end":{"line":45,"column":70}}})) != null ? stack1 : "")
    + "\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":46,"column":6},"end":{"line":46,"column":17}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":49,"column":6},"end":{"line":49,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number tva\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"tva_label") : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":52,"column":6},"end":{"line":60,"column":13}}})) != null ? stack1 : "")
    + "    </td>\n    <td class=\"col_actions width_two\">\n      <button\n        data-action=\"work:additem\"\n        data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":65,"column":17},"end":{"line":65,"column":23}}}) : helper)))
    + "\"\n        class=\"btn icon only\"\n        title=\"Ajouter un produit à cet ouvrage\"\n        aria-label=\"Ajouter un produit à cet ouvrage\"\n      >\n        <svg>\n          <use href=\"../static/icons/endi.svg#plus\"></use>\n        </svg>\n        Ajouter un produit\n      </button>\n      <div class=\"btn-group\">\n        <button\n          class=\"btn icon only dropdown-toggle\"\n          data-toggle=\"dropdown\"\n          aria-haspopup=\"true\"\n          aria-expanded=\"false\"\n          title=\"Afficher plus d’actions\"\n          aria-label=\"Afficher plus d’actions\"\n        >\n          <svg>\n            <use href=\"../static/icons/endi.svg#dots\"></use>\n          </svg>\n        </button>\n        <ul class=\"dropdown-menu dropdown-menu-right\">\n          <li>\n            <a\n              href=\"#chapters/"
    + alias4(((helper = (helper = lookupProperty(helpers,"chapter_id") || (depth0 != null ? lookupProperty(depth0,"chapter_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chapter_id","hash":{},"data":data,"loc":{"start":{"line":91,"column":30},"end":{"line":91,"column":44}}}) : helper)))
    + "/works/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":91,"column":51},"end":{"line":91,"column":57}}}) : helper)))
    + "\"\n              class=\"btn icon\"\n              title=\"Modifier cet ouvrage\"\n              aria-label=\"Modifier cet ouvrage\"\n            >\n              <svg>\n                <use href=\"../static/icons/endi.svg#pen\"></use>\n              </svg>\n              Modifier cet ouvrage\n            </a>\n          </li>\n          <li>\n            <button\n              data-action=\"product:duplicate\"\n              data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":105,"column":23},"end":{"line":105,"column":29}}}) : helper)))
    + "\"\n              type=\"button\"\n              class=\"btn icon\"\n              title=\"Dupliquer cet ouvrage\"\n              aria-label=\"Dupliquer cet ouvrage\"\n            >\n              <svg>\n                <use href=\"../static/icons/endi.svg#copy\"></use>\n              </svg>\n              Dupliquer\n            </button>\n          </li>\n          <li>\n            <button\n              data-action=\"product:change_details\"\n              data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":120,"column":23},"end":{"line":120,"column":29}}}) : helper)))
    + "\"\n              type=\"button\"\n              class=\"btn icon\"\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_details") : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.program(21, data, 0),"data":data,"loc":{"start":{"line":123,"column":14},"end":{"line":129,"column":21}}})) != null ? stack1 : "")
    + "            >\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_details") : depth0),{"name":"if","hash":{},"fn":container.program(23, data, 0),"inverse":container.program(25, data, 0),"data":data,"loc":{"start":{"line":131,"column":12},"end":{"line":141,"column":21}}})) != null ? stack1 : "")
    + "            </button>\n          </li>\n          <li>\n            <button\n              data-action=\"product:delete\"\n              data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":147,"column":23},"end":{"line":147,"column":29}}}) : helper)))
    + "\"\n              type=\"button\"\n              class=\"btn icon negative\"\n              title=\"Supprimer cet ouvrage\"\n              aria-label=\"Supprimer cet ouvrage\"\n            >\n              <svg>\n                <use href=\"../static/icons/endi.svg#trash-alt\"></use>\n              </svg>\n              Supprimer\n            </button>\n          </li>\n        </ul>\n      </div>\n    </td>\n    <td class=\"col_actions row_ordering sort\" colspan=\"2\">\n      <ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":164,"column":8},"end":{"line":180,"column":15}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(29, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":181,"column":8},"end":{"line":197,"column":15}}})) != null ? stack1 : "")
    + "      </ul>\n    </td>\n  </tr>\n</tbody>\n<tbody \n  id=\"workitems-"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":203,"column":16},"end":{"line":203,"column":22}}}) : helper)))
    + "\" \n  class=\"collapse_content collapse in\" \n  aria-expanded=\"true\"\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"workitemsHtml") || (depth0 != null ? lookupProperty(depth0,"workitemsHtml") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"workitemsHtml","hash":{},"data":data,"loc":{"start":{"line":206,"column":2},"end":{"line":206,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n</tbody>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/templates/DiscountComponent.mustache":
/*!****************************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/templates/DiscountComponent.mustache ***!
  \****************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "        <table class=\"hover_table\">\n          <caption class=\"screen-reader-text\">\n            Liste des remises accordées au client dans cette étude de prix\n          </caption>\n          <thead>\n            <tr>\n              <th scope=\"col\" class=\"col_text\">\n                Description\n              </th>\n              <th scope=\"col\" class=\"col_number\" title=\"Pourcentage\">\n                <span class=\"screen-reader-text\">Pourcentage </span>%\n              </th>\n              <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxes\">\n                <span class=\"screen-reader-text\">Prix </span>H<span class=\"screen-reader-text\">ors</span>T<span class=\"screen-reader-text\">axes</span>\n              </th>\n              <th scope=\"col\" class=\"col_text\" title=\"Taux de TVA\">\n                <span class=\"screen-reader-text\">Taux de </span>TVA\n              </th>\n              <th scope=\"col\" class=\"col_actions width_two\" title=\"Actions\">\n                <span class=\"screen-reader-text\">Actions</span>\n              </th>\n            </tr>\n          </thead>\n          <tbody class=\"lines\"></tbody>\n        </table>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        <div class=\"actions content_double_padding align_right\">\n          <button\n            type=\"'button\"\n            class=\"btn add-discount\"\n            title=\"Ajouter une remise\"\n          >\n            <svg>\n              <use href=\"/static/icons/endi.svg#plus\"></use>\n            </svg>\n            Ajouter une remise\n          </button>\n        </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"separate_block border_left_block\">\n  <h2 class=\"title collapse_title\">\n    <a\n      data-target=\"#discounts-content\"\n      data-toggle=\"collapse\"\n      aria-expanded=\"false\"\n      aria-controls=\"discounts-content\"\n      title=\"Remises\"\n      aria-label=\"Remises\"\n    >\n      <svg class=\"arrow\">\n        <use href=\"../static/icons/endi.svg#chevron-down\"></use>\n      </svg>\n      <span class=\"icon status error\" title=\"Ce bloc comporte une erreur\">\n        <svg>\n          <use href=\"../static/icons/endi.svg#exclamation-triangle\"></use>\n        </svg>\n      </span>\n      <span class=\"icon status success\" title=\"Ce bloc est valide\">\n        <svg>\n          <use href=\"../static/icons/endi.svg#check\"></use>\n        </svg>\n      </span>\n      Remises\n    </a>\n  </h2>\n  <div class=\"collapse_content collapse in\">\n    <div id=\"discounts-content\" class=\"content table_container\">\n      <div class=\"block-errors\"></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasItems") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":30,"column":6},"end":{"line":56,"column":13}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"editable") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":57,"column":6},"end":{"line":70,"column":13}}})) != null ? stack1 : "")
    + "    </div>\n  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/templates/DiscountEmptyView.mustache":
/*!****************************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/templates/DiscountEmptyView.mustache ***!
  \****************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<em>\nAucune remise n’a encore été ajoutée\n</em>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/templates/DiscountForm.mustache":
/*!***********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/templates/DiscountForm.mustache ***!
  \***********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"size_middle\">\n    <div\n        role=\"dialog\"\n        id=\"product-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"product-forms_title\"\n    >\n        <form class=\"form discount-form\">\n            <div class=\"modal_layout\">\n                <header>\n                    <button\n                    type='button'\n                        class=\"icon only unstyled close\"\n                        title=\"Fermer cette fenêtre\"\n                        aria-label=\"Fermer cette fenêtre\"\n                    >\n                        <svg>\n                            <use href=\"/static/icons/endi.svg#times\"></use>\n                        </svg>\n                    </button>\n                    <h2 id=\"product-forms_title\">\n                        "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":22,"column":24},"end":{"line":22,"column":33}}}) : helper)))
    + "\n                    </h2>\n                </header>\n                <div class=\"modal_content_layout\">\n                    <div class=\"modal_content\">\n                        <div class=\"field-order\"></div>\n                        <div class=\"field-type_\"></div>\n                        <div class=\"field-description\"></div>\n                        <div class=\"field-amount required\"></div>\n                        <div class=\"field-percentage required\"></div>\n                        <div class=\"field-tva_id required\"></div>\n                    </div>\n                    <footer>\n                        <button class=\"btn btn-primary\" type=\"submit\" value=\"submit\">\n                            "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":36,"column":28},"end":{"line":36,"column":37}}}) : helper)))
    + "\n                        </button>\n                        <button class=\"btn\" type=\"reset\" value=\"submit\">\n                            Annuler\n                        </button>\n                    </footer>\n                </div>\n            </div>\n        </form>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/discount/templates/DiscountView.mustache":
/*!***********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/discount/templates/DiscountView.mustache ***!
  \***********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":6,"column":4},"end":{"line":6,"column":18}}}) : helper)))
    + "%\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    -\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":16,"column":4},"end":{"line":16,"column":17}}}) : helper)))
    + "\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "      <li>\n        <button\n          type=\"button\"\n          class=\"btn icon only up\"\n          title=\"Déplacer cette remise vers le haut\"\n          aria-label=\"Déplacer cette remise vers le haut\"\n          tabindex=\"-1\"\n        >\n          <svg>\n            <use href=\"/static/icons/endi.svg#arrow-up\"></use>\n          </svg>\n        </button>\n      </li>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "      <li>\n        <button\n          type=\"button\"\n          class=\"btn icon only down\"\n          title=\"Déplacer cette remise vers le bas\"\n          aria-label=\"Déplacer cette remise vers le bas\"\n          tabindex=\"-1\"\n        >\n          <svg>\n            <use href=\"/static/icons/endi.svg#arrow-down\"></use>\n          </svg>\n        </button>\n      </li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class=\"col_text\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":2,"column":2},"end":{"line":2,"column":19}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_percentage") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":2},"end":{"line":7,"column":9}}})) != null ? stack1 : "")
    + "</td>\n<td class=\"col_number\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":10,"column":2},"end":{"line":10,"column":22}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_percentage") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":13,"column":2},"end":{"line":17,"column":9}}})) != null ? stack1 : "")
    + "</td>\n<td class=\"col_actions width_two\">\n  <div class=\"group\">\n    <button\n      type=\"button\"\n      class=\"btn icon only edit\"\n      title=\"Modifier cette remise\"\n      aria-label=\"Modifier cette remise\"\n    >\n      <svg>\n        <use href=\"../static/icons/endi.svg#pen\"></use>\n      </svg>\n    </button>\n    <button\n      type=\"button\"\n      class=\"btn icon only negative delete\"\n      title=\"Supprimer cette remise\"\n      aria-label=\"Supprimer cette remise\"\n    >\n      <svg>\n        <use href=\"../static/icons/endi.svg#trash-alt\"></use>\n      </svg>\n    </button>\n  </div>\n</td>\n<td class=\"col_actions row_ordering sort\">\n  <ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":45,"column":4},"end":{"line":59,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":60,"column":4},"end":{"line":74,"column":11}}})) != null ? stack1 : "")
    + "  </ul>\n</td>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/product/templates/ProductForm.mustache":
/*!*********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/product/templates/ProductForm.mustache ***!
  \*********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <nav>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a\n                            href=\"#form-container\"\n                            aria-controls=\"form-container\"\n                            id=\"form-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\"\n                            tabindex=\"-1\"\n                        >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a\n                            href=\"#catalog-container\"\n                            aria-controls=\"catalog-container\"\n                            id=\"catalog-tabtitle\"\n                            role=\"tab\"\n                            tabindex=\"-1\"\n                            data-toggle=\"tab\"\n                        >\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane\"\n                    id=\"catalog-container\"\n                    aria-labelledby=\"catalog-tabtitle\"\n                ></div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"modal_view size_middle\">\n    <div\n        role=\"dialog\"\n        id=\"product-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"product-forms_title\"\n    >\n        <div class=\"modal_layout\">\n            <header>\n                <button\n                type='button'\n                    class=\"icon only unstyled close\"\n                    title=\"Fermer cette fenêtre\"\n                    aria-label=\"Fermer cette fenêtre\"\n                >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"product-forms_title\">\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":19,"column":20},"end":{"line":19,"column":29}}}) : helper)))
    + "\n                </h2>\n            </header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":12},"end":{"line":51,"column":19}}})) != null ? stack1 : "")
    + "            <div class=\"tab-content\">\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\"\n                    aria-labelledby=\"form-tabtitle\"\n                >\n                    <form class=\"modal_content_layout layout product-form\">\n                        <div class=\"modal_content\">\n                            <fieldset>\n                                <div class=\"field-order\"></div>\n                                <div class=\"field-description required\"></div>\n                                <div class=\"field-mode\"></div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-supplier_ht\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-margin_rate\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-12\">\n                                        <div class=\"field-ht\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"field-general_overhead\"></div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-quantity required\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-unity\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-tva_id required\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-product_id\"></div>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <footer>\n                            <button\n                                class=\"btn btn-primary\"\n                                type=\"submit\"\n                                value=\"submit\"\n                                title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":102,"column":39},"end":{"line":102,"column":48}}}) : helper)))
    + "\"\n                                aria-label=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":103,"column":44},"end":{"line":103,"column":53}}}) : helper)))
    + "\"\n                            >\n                                <svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n                                Valider\n                            </button>\n                            <button\n                                class=\"btn\"\n                                type=\"reset\"\n                                value=\"submit\"\n                            >\n                                Annuler\n                            </button>\n                        </footer>\n                    </form>\n                </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":118,"column":16},"end":{"line":125,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/templates/BlockView.mustache":
/*!***********************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/templates/BlockView.mustache ***!
  \***********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "    <div class=\"border_left_block\">\n      <div class=\"actions content_double_padding align_right\">\n        <a class=\"btn\" href=\"#addchapter\" title=\"Ajouter un chapitre\">\n          <svg>\n            <use href=\"/static/icons/endi.svg#plus\"></use>\n          </svg>\n          Ajouter un chapitre\n        </a>\n      </div>\n    </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"separate_block composite\">\n  <div class=\"border_left_block\">\n    <h2 class=\"title\">\n      Liste des produits\n    </h2>\n  </div>\n  <div class=\"group-container common\"></div>\n  <div class=\"group-container products\"></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"editable") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":2},"end":{"line":20,"column":9}}})) != null ? stack1 : "")
    + "</div>\n<div\n  id=\"discounts\"\n  class=\"form-section discount-group collapsible\"\n>\n \n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/templates/PriceStudyView.mustache":
/*!****************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/templates/PriceStudyView.mustache ***!
  \****************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"border_left_block composite content_double_padding\">\n  <div class=\"layout\">\n    <div class=\"col-md-6 field-general_overhead\"></div>\n  </div>\n  <div class=\"field-margin_rate\"></div>\n  <div class=\"field-mask_hours\"></div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/templates/RootComponent.mustache":
/*!***************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/templates/RootComponent.mustache ***!
  \***************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='modal-container'>\n</div>\n<div class='main'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/workform/templates/AddWorkForm.mustache":
/*!**********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/workform/templates/AddWorkForm.mustache ***!
  \**********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<section id=\"work_form\" class=\"modal_view size_large\">\n    <div\n        role=\"dialog\"\n        id=\"work-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"work-forms_title\"\n    >\n        <div class=\"modal_layout\">\n            <header>\n                <button\n                    class=\"icon only unstyled close\"\n                    title=\"Fermer cette fenêtre\"\n                    type='button'\n                    aria-label=\"Fermer cette fenêtre\"\n                >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"work-forms_title\">\n                    Ajouter un ouvrage\n                </h2>\n            </header>\n            <nav>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a\n                            href=\"#form-container\"\n                            aria-controls=\"form-container\"\n                            id=\"form-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\"\n                            tabindex=\"-1\"\n                        >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a\n                            href=\"#catalog-container\"\n                            aria-controls=\"catalog-container\"\n                            id=\"catalog-tabtitle\"\n                            role=\"tab\"\n                            tabindex=\"-1\"\n                            data-toggle=\"tab\"\n                        >\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n            <div class=\"tab-content\">\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\"\n                    aria-labelledby=\"form-tabtitle\"\n                >\n                    <form name=\"current\" class=\"modal_content_layout layout\">\n                        <input type=\"hidden\" name=\"type_\" value=\"price_study_work\" />\n                        <div class=\"modal_content\">\n                            <div class=\"message-container\"></div>\n                            <div class=\"errors\"></div>\n                            <fieldset>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-12\">\n                                        <div class=\"field-title\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"field-description\"></div>\n                                <div class=\"field-display_details\"></div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-12\">\n                                        <div class=\"field-margin_rate\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-quantity\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-unity\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-tva_id\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-product_id\"></div>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <footer>\n                            <button\n                                class=\"btn btn-primary icon\"\n                                type=\"submit\"\n                                value=\"submit\"\n                            >\n                                <svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n                                Continuer\n                            </button>\n                            <button\n                                class=\"btn icon\"\n                                type=\"reset\"\n                                value=\"submit\"\n                                title=\"Annuler et revenir en arrière\"\n                                aria-label=\"Annuler et revenir en arrière\"\n                            >\n                                Annuler\n                            </button>\n                        </footer>\n                    </form>\n                </div>\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane\"\n                    id=\"catalog-container\"\n                    aria-labelledby=\"catalog-tabtitle\"\n                >\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/workform/templates/WorkForm.mustache":
/*!*******************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/workform/templates/WorkForm.mustache ***!
  \*******************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<section class=\"modal_view size_large\">\n    <div\n        role=\"dialog\"\n        id=\"work-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"work-forms_title\"\n    >\n        <div class=\"modal_layout\">\n            <header>\n                <button\n                    class=\"icon only unstyled close\"\n                    title=\"Fermer cette fenêtre\"\n                    aria-label=\"Fermer cette fenêtre\"\n                        type=\"button\"\n                >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"work-forms_title\">\n                    Modifier un ouvrage\n                </h2>\n            </header>\n            <form name=\"current\" class=\"modal_content_layout layout\">\n                <div class=\"modal_content\">\n                    <div class=\"errors\"></div>\n                    <fieldset>\n                        <div class=\"row form-row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"field-title\"></div>\n                            </div>\n                        </div>\n                        <div class=\"field-description\"></div>\n                        <div class=\"field-display_details\"></div>\n                        <div class=\"row form-row\">\n                            <div class=\"col-md-12\">\n                                <div class=\"field-margin_rate\"></div>\n                            </div>\n                        </div>\n                        <div class=\"row form-row\">\n                            <div class=\"col-md-6\">\n                                <div class=\"field-quantity\"></div>\n                            </div>\n                            <div class=\"col-md-6\">\n                                <div class=\"field-unity\"></div>\n                            </div>\n                        </div>\n                        <div class=\"row form-row\">\n                            <div class=\"col-md-6\">\n                                <div class=\"field-tva_id\"></div>\n                            </div>\n                            <div class=\"col-md-6\">\n                                <div class=\"field-product_id\"></div>\n                            </div>\n                        </div>\n                    </fieldset>\n                </div>\n                <footer>\n                    <button class=\"btn btn-primary icon\" type=\"submit\" value=\"submit\">\n                        <svg>\n                            <use href=\"/static/icons/endi.svg#check\"></use>\n                        </svg>\n                        Valider\n                    </button>\n                    <button class=\"btn\" type=\"reset\" value=\"cancel\">\n                        Annuler\n                    </button>\n                </footer>\n            </form>\n        </div>\n    </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/price_study/views/workform/templates/WorkItemForm.mustache":
/*!***********************************************************************************************!*\
  !*** ./src/task/views/composition/price_study/views/workform/templates/WorkItemForm.mustache ***!
  \***********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <nav>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a\n                            href=\"#form-container\"\n                            aria-controls=\"form-container\"\n                            id=\"form-tabtitle\"\n                            role=\"tab\"\n                            data-toggle=\"tab\"\n                            tabindex=\"-1\"\n                        >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a\n                            href=\"#catalog-container\"\n                            aria-controls=\"catalog-container\"\n                            id=\"catalog-tabtitle\"\n                            role=\"tab\"\n                            tabindex=\"-1\"\n                            data-toggle=\"tab\"\n                        >\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n            </nav>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <div role=\"tabpanel\" class=\"tab-pane\" id=\"catalog-container\" aria-labelledby=\"catalog-tabtitle\"></div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"modal_view size_middle\">\n    <div\n        role=\"dialog\"\n        id=\"product-forms\"\n        aria-modal=\"true\"\n        aria-labelledby=\"product-forms_title\"\n    >\n        <div class=\"modal_layout\">\n            <header>\n                <button\n                    class=\"icon only unstyled close\"\n                    title=\"Fermer cette fenêtre\"\n                    aria-label=\"Fermer cette fenêtre\"\n                    type='button'\n                >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"product-forms_title\">\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":19,"column":20},"end":{"line":19,"column":29}}}) : helper)))
    + "\n                </h2>\n            </header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":12},"end":{"line":51,"column":19}}})) != null ? stack1 : "")
    + "            <div class=\"tab-content\">\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\"\n                    aria-labelledby=\"form-tabtitle\"\n                >\n                    <form class=\"modal_content_layout layout product-form\">\n                        <div class=\"modal_content\">\n                            <div class=\"errors\"></div>\n                            <fieldset>\n                                <div class=\"row form-row\">\n                                    <div class=\"field-order\"></div>\n                                </div>\n                                <div class=\"field-description\"></div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-mode\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-supplier_ht\"></div>\n                                        <div class=\"field-ht\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-12\">\n                                        <div class=\"field-margin_rate\"></div>\n                                    </div>\n                                </div>\n                                <div class=\"field-quantity_inherited\"></div>\n                                <div class=\"row form-row\">\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-work_unit_quantity\"></div>\n                                    </div>\n                                    <div class=\"col-md-6\">\n                                        <div class=\"field-unity\"></div>\n                                    </div>\n                                </div>\n                            </fieldset>\n                        </div>\n                        <footer>\n                            <button\n                                class=\"btn btn-primary\"\n                                type=\"submit\"\n                                value=\"submit\"\n                                title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":97,"column":39},"end":{"line":97,"column":48}}}) : helper)))
    + "\"\n                                aria-label=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":98,"column":44},"end":{"line":98,"column":53}}}) : helper)))
    + "\"\n                            >\n                                <svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n                                Valider\n                            </button>\n                            <button\n                                class=\"btn\"\n                                type=\"reset\"\n                                value=\"submit\"\n                            >\n                                Annuler\n                            </button>\n                        </footer>\n                    </form>\n                </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":113,"column":16},"end":{"line":115,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/templates/ChapterView.mustache":
/*!****************************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/templates/ChapterView.mustache ***!
  \****************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":5,"column":8},"end":{"line":5,"column":17}}}) : helper)))
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        <small>\n          <em>\n            Aucun titre n’a été saisi pour ce chapitre\n          </em>\n        </small>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":16,"column":8},"end":{"line":16,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "        <small>\n          <em>\n            Aucun détail n’a été saisi pour ce chapitre\n          </em>\n        </small>\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <div class=\"table_container\">\n    <div class=\"errors\"></div>\n    <table class=\"hover_table top_align_table collapsible\">\n      <thead>\n        <tr>\n          <td class=\"level_spacer\"></td>\n          <th scope=\"col\" class=\"col_text\" colspan=\"2\">\n            Description\n          </th>\n          <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxes à facturer\">\n            <span class=\"screen-reader-text\">Prix </span>H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes à facturer</span>\n          </th>\n          <th scope=\"col\" class=\"col_number\" title=\"Montant TVA à facturer\">\n            <span class=\"screen-reader-text\">Montant de la </span>TVA<span class=\"screen-reader-text\"> à facturer</span>\n          </th>\n          <th scope=\"col\" class=\"col_number\" title=\"Prix Toutes Taxes comprises\">\n            <span class=\"screen-reader-text\">Prix </span>H<span class=\"screen-reader-text\">outes </span>T<span class=\"screen-reader-text\">axes comprises à facturer</span>\n          </th>\n          <th scop=\"col\" class=\"col_percentage_graphic\" title=\"Facturation\">\n            Facturation\n          </th>\n          <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxes\">\n            <span class=\"screen-reader-text\">Prix </span>H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n          </th>\n        </tr>\n        <tr class=\"row_recap\">\n          <td class=\"level_spacer\"></td>\n          <th\n            scope=\"row\"\n            class=\"col_text\"\n            colspan=\"6\"\n            title=\"Sous-total Hors Taxes\"\n          >\n            Sous-total H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n          </th>\n          <td class=\"col_number\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":86,"column":12},"end":{"line":86,"column":26}}}) : helper))) != null ? stack1 : "")
    + "\n          </td>\n        </tr>\n      </thead>\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"collectionRawHtml") || (depth0 != null ? lookupProperty(depth0,"collectionRawHtml") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"collectionRawHtml","hash":{},"data":data,"loc":{"start":{"line":90,"column":6},"end":{"line":90,"column":29}}}) : helper))) != null ? stack1 : "")
    + "\n    </table>\n  </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"layout flex quotation_item_title\">\n  <div>\n    <h3>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"title") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":4,"column":6},"end":{"line":12,"column":13}}})) != null ? stack1 : "")
    + "    </h3>\n    <p>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":15,"column":6},"end":{"line":23,"column":13}}})) != null ? stack1 : "")
    + "    </p>\n  </div>\n</div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collectionRawHtml") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":0},"end":{"line":93,"column":7}}})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/templates/ProductRawView.mustache":
/*!*******************************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/templates/ProductRawView.mustache ***!
  \*******************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tbody class=\"lines\">\n  <tr class=\"row taskline\">\n    <td class=\"level_spacer\"></td>\n    <td class=\"col_text description\" colspan=\"2\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":5,"column":6},"end":{"line":5,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_ht_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_to_invoice","hash":{},"data":data,"loc":{"start":{"line":8,"column":6},"end":{"line":8,"column":31}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_tva_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_tva_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_tva_to_invoice","hash":{},"data":data,"loc":{"start":{"line":11,"column":6},"end":{"line":11,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ttc_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_ttc_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ttc_to_invoice","hash":{},"data":data,"loc":{"start":{"line":14,"column":6},"end":{"line":14,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_percentage_graphic\">\n      <div class=\"group-slider-container\">\n        <div class=\"billing_slider\">\n          <a href=\"#chapters/"
    + alias4(((helper = (helper = lookupProperty(helpers,"chapter_id") || (depth0 != null ? lookupProperty(depth0,"chapter_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chapter_id","hash":{},"data":data,"loc":{"start":{"line":19,"column":29},"end":{"line":19,"column":43}}}) : helper)))
    + "/products/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":19,"column":53},"end":{"line":19,"column":59}}}) : helper)))
    + "\" title=\"\">\n            <span class=\"billing done\" title=\"Déjà facturé : "
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":20,"column":61},"end":{"line":20,"column":81}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":20,"column":98},"end":{"line":20,"column":118}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">Déjà facturé : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":20,"column":197},"end":{"line":20,"column":217}}}) : helper)))
    + "</span></span>\n            <span class=\"billing todo\" title=\"À facturer : "
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":21,"column":59},"end":{"line":21,"column":73}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":21,"column":90},"end":{"line":21,"column":104}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">A facturer : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":21,"column":181},"end":{"line":21,"column":195}}}) : helper)))
    + "%</span></span>\n            <span class=\"billing remain\" title=\"Restera à facturer : "
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":22,"column":69},"end":{"line":22,"column":85}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":22,"column":102},"end":{"line":22,"column":118}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">Restera à facturer : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":22,"column":203},"end":{"line":22,"column":219}}}) : helper)))
    + "%</span></span>\n          </a>\n        </div>\n      </div>\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":28,"column":6},"end":{"line":28,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n  </tr>\n</tbody>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/templates/WorkItemRawView.mustache":
/*!********************************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/templates/WorkItemRawView.mustache ***!
  \********************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tr class=\"row taskline\">\n  <td class=\"level_spacer\"></td>\n  <td class=\"level_spacer\"></td>\n    <td class=\"col_text description\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":5,"column":6},"end":{"line":5,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_ht_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_to_invoice","hash":{},"data":data,"loc":{"start":{"line":8,"column":6},"end":{"line":8,"column":31}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_tva_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_tva_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_tva_to_invoice","hash":{},"data":data,"loc":{"start":{"line":11,"column":6},"end":{"line":11,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ttc_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_ttc_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ttc_to_invoice","hash":{},"data":data,"loc":{"start":{"line":14,"column":6},"end":{"line":14,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_percentage_graphic\">\n      <div class=\"group-slider-container\">\n        <div class=\"billing_slider\">\n          <a data-action=\"workitem:edit\" data-workid="
    + alias4(((helper = (helper = lookupProperty(helpers,"work_id") || (depth0 != null ? lookupProperty(depth0,"work_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"work_id","hash":{},"data":data,"loc":{"start":{"line":19,"column":53},"end":{"line":19,"column":64}}}) : helper)))
    + " data-id=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":19,"column":74},"end":{"line":19,"column":80}}}) : helper)))
    + "\" href=\"javascript:void(0);\" title=\"\">\n            <span class=\"billing done\" title=\"Déjà facturé : "
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":20,"column":61},"end":{"line":20,"column":81}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":20,"column":98},"end":{"line":20,"column":118}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">Déjà facturé : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":20,"column":197},"end":{"line":20,"column":217}}}) : helper)))
    + "</span></span>\n            <span class=\"billing todo\" title=\"À facturer : "
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":21,"column":59},"end":{"line":21,"column":73}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":21,"column":90},"end":{"line":21,"column":104}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">A facturer : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":21,"column":181},"end":{"line":21,"column":195}}}) : helper)))
    + "%</span></span>\n            <span class=\"billing remain\" title=\"Restera à facturer : "
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":22,"column":69},"end":{"line":22,"column":85}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":22,"column":102},"end":{"line":22,"column":118}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">Restera à facturer : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":22,"column":203},"end":{"line":22,"column":219}}}) : helper)))
    + "%</span></span>\n          </a>\n        </div>\n      </div>\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":28,"column":6},"end":{"line":28,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n  <td class=\"level_spacer\"></td>\n</tr>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/chapter/templates/WorkRawView.mustache":
/*!****************************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/chapter/templates/WorkRawView.mustache ***!
  \****************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <div class=\"group-slider-container\">\n        <div class=\"billing_slider\">\n          <a href=\"#chapters/"
    + alias4(((helper = (helper = lookupProperty(helpers,"chapter_id") || (depth0 != null ? lookupProperty(depth0,"chapter_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chapter_id","hash":{},"data":data,"loc":{"start":{"line":20,"column":29},"end":{"line":20,"column":43}}}) : helper)))
    + "/products/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":20,"column":53},"end":{"line":20,"column":59}}}) : helper)))
    + "\" title=\"Modifier l'avancement de cet ouvrage\">\n            <span class=\"billing done\" title=\"Déjà facturé : "
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":21,"column":61},"end":{"line":21,"column":81}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":21,"column":98},"end":{"line":21,"column":118}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">Déjà facturé : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"already_invoiced") || (depth0 != null ? lookupProperty(depth0,"already_invoiced") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"already_invoiced","hash":{},"data":data,"loc":{"start":{"line":21,"column":197},"end":{"line":21,"column":217}}}) : helper)))
    + "</span></span>\n            <span class=\"billing todo\" title=\"À facturer : "
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":22,"column":59},"end":{"line":22,"column":73}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":22,"column":90},"end":{"line":22,"column":104}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">A facturer : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":22,"column":181},"end":{"line":22,"column":195}}}) : helper)))
    + "%</span></span>\n            <span class=\"billing remain\" title=\"Restera à facturer : "
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":23,"column":69},"end":{"line":23,"column":85}}}) : helper)))
    + "%\" style=\"width: "
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":23,"column":102},"end":{"line":23,"column":118}}}) : helper)))
    + "%;\"><span class=\"screen-reader-text\">Restera à facturer : </span><span class=\"value\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"percent_left") || (depth0 != null ? lookupProperty(depth0,"percent_left") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"percent_left","hash":{},"data":data,"loc":{"start":{"line":23,"column":203},"end":{"line":23,"column":219}}}) : helper)))
    + "%</span></span>\n          </a>\n        </div>\n      </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "      Les produits ont des pourcentages à facturer différents.\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tbody class=\"lines collapse_title in\" aria-expanded=\"true\">\n  <tr class=\"row taskline\">\n    <td class=\"level_spacer\"></td>\n    <td class=\"col_text description\" colspan=\"2\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":5,"column":6},"end":{"line":5,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_ht_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_to_invoice","hash":{},"data":data,"loc":{"start":{"line":8,"column":6},"end":{"line":8,"column":31}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_tva_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_tva_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_tva_to_invoice","hash":{},"data":data,"loc":{"start":{"line":11,"column":6},"end":{"line":11,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ttc_to_invoice") || (depth0 != null ? lookupProperty(depth0,"total_ttc_to_invoice") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ttc_to_invoice","hash":{},"data":data,"loc":{"start":{"line":14,"column":6},"end":{"line":14,"column":32}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n    <td class=\"col_percentage_graphic\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"locked") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":17,"column":6},"end":{"line":29,"column":13}}})) != null ? stack1 : "")
    + "    </td>\n    <td class=\"col_number\">\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":32,"column":6},"end":{"line":32,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n    </td>\n  </tr>\n</tbody>\n<tbody \n  id=\"workitems-"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":37,"column":16},"end":{"line":37,"column":22}}}) : helper)))
    + "\" \n  class=\"collapse_content collapse in\" \n  aria-expanded=\"true\"\">\n  "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"workitemsHtml") || (depth0 != null ? lookupProperty(depth0,"workitemsHtml") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"workitemsHtml","hash":{},"data":data,"loc":{"start":{"line":40,"column":2},"end":{"line":40,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n</tbody>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/product/templates/ProductForm.mustache":
/*!****************************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/product/templates/ProductForm.mustache ***!
  \****************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                        <div class=\"alert alert-info\">\n                            <p>\n                                Montant HT indiqué dans le devis moins la part d’acompte\n                            </p>\n                        </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section id='task_line_form' class=\"modal_view size_middle\">\n    <div role=\"dialog\" id=\"task_line-forms\" aria-modal=\"true\" aria-labelledby=\"task_line-forms_title\">\n        <form>\n            <div class=\"modal_layout\">\n                <header>\n                    <button\n                        type='button'\n                        class=\"icon only unstyled close\"\n                        title=\"Fermer cette fenêtre\"\n                        aria-label=\"Fermer cette fenêtre\"\n                        >\n                        <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                    </button>\n                    <h2>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"popup_title") || (depth0 != null ? lookupProperty(depth0,"popup_title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"popup_title","hash":{},"data":data,"loc":{"start":{"line":14,"column":24},"end":{"line":14,"column":41}}}) : helper)))
    + "</h2>\n                </header>\n                <div class=\"modal_content_layout\">\n                    <div class=\"modal_content\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"deposit_info") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":24},"end":{"line":24,"column":31}}})) != null ? stack1 : "")
    + "                        <div class=\"table_container\">\n                            <table class=\"top_align_table\">\n                                <thead>\n                                    <tr>\n                                        <th scope=\"col\" class=\"col_text\">Intitulé du produit</th>\n                                        <th scope=\"col\" class=\"col_number unity\" title=\"Prix Unitaire Hors Taxes\">\n                                        P<span class=\"screen-reader-text\">rix</span> H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n                                        </th>\n                                        <th scope=\"col\" class=\"col_number tva\" title=\"Taux de TVA\"><span class=\"screen-reader-text\">Taux de </span>TVA</th>\n                                        <th scope=\"col\" class=\"col_number\" title=\"Prix Toutes Taxes Comprises\">T<span class=\"screen-reader-text\">outes </span>T<span class=\"screen-reader-text\">axes </span>C<span class=\"screen-reader-text\">omprises</span></th>\n                                    </tr>\n                                </thead>\n                                <tbody class=\"quotation_item_title\">\n                                    <tr>\n                                        <td class=\"col_text description rich_text\">\n                                            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":40,"column":44},"end":{"line":40,"column":61}}}) : helper))) != null ? stack1 : "")
    + "\n                                        </td>\n                                        <td class=\"col_number\">\n                                            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_to_invoice_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_to_invoice_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_to_invoice_label","hash":{},"data":data,"loc":{"start":{"line":43,"column":44},"end":{"line":43,"column":75}}}) : helper))) != null ? stack1 : "")
    + "\n                                        </td>\n                                        <td class=\"col_number\">\n                                            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_to_invoice_label") || (depth0 != null ? lookupProperty(depth0,"tva_to_invoice_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_to_invoice_label","hash":{},"data":data,"loc":{"start":{"line":46,"column":44},"end":{"line":46,"column":70}}}) : helper))) != null ? stack1 : "")
    + "\n                                        </td>\n                                        <td class=\"col_number\">\n                                            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ttc_to_invoice_label") || (depth0 != null ? lookupProperty(depth0,"total_ttc_to_invoice_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ttc_to_invoice_label","hash":{},"data":data,"loc":{"start":{"line":49,"column":44},"end":{"line":49,"column":76}}}) : helper))) != null ? stack1 : "")
    + "\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                        <fieldset class=\"separate_top content_vertical_padding\">\n                            <div class=\"layout flex three_cols\">\n                                <div class='percent_done'></div>\n                                <div class='current_percent required'></div>\n                                <div class='percent_left'></div>\n                            </div>\n                        </fieldset>\n                    </div>\n                    <footer>\n                        <button\n                            class='btn btn-primary'\n                            type='submit'\n                            value='submit'>\n                            <svg>\n                                <use href=\"/static/icons/endi.svg#check\"></use></svg>\n                            Valider\n                        </button>\n                        <button\n                            class='btn'\n                            type='reset'\n                            value='reset'>\n                            Annuler\n                        </button>\n                    </footer>\n                </div>\n            </div>\n        </form>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/templates/BlockView.mustache":
/*!******************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/templates/BlockView.mustache ***!
  \******************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"separate_block composite\">\n  <div class=\"border_left_block\">\n    <h2 class=\"title\">\n      Liste des produits\n    </h2>\n  </div>\n  <div class=\"group-container products\"></div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/composition/progress_invoicing/views/templates/RootComponent.mustache":
/*!**********************************************************************************************!*\
  !*** ./src/task/views/composition/progress_invoicing/views/templates/RootComponent.mustache ***!
  \**********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='modal-container'>\n</div>\n<div class='main'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/files/templates/FileContentView.mustache":
/*!*****************************************************************!*\
  !*** ./src/task/views/files/templates/FileContentView.mustache ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "      <span class=\"icon status error\" title=\"Ce bloc comporte une erreur\">\n        <svg>\n          <use href=\"/static/icons/endi.svg#exclamation-triangle\"></use>\n        </svg>\n      </span>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "      <span class=\"icon status success\" title=\"Ce bloc est valide\">\n        <svg>\n          <use href=\"/static/icons/endi.svg#check\"></use>\n        </svg>\n      </span>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "      <small>\n        Il manque des fichiers requis\n      </small>\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "    <h3>\n      <span class=\"label required\">\n        Fichiers requis\n        <span class=\"icon required\">\n          <svg>\n            <use href=\"../static/icons/endi.svg#required\"></use>\n          </svg>\n          <span>\n            Requis pour la validation\n          </span>\n        </span>\n      </span>\n    </h3>\n    <div class=\"table_container\">\n      <table class=\"required-files files\">\n        <thead>\n          <tr>\n            <th scope=\"col\" class=\"col_status\" title=\"Statut\">\n              <span class=\"screen-reader-text\">\n                Statut\n              </span>\n            </th>\n            <th scope=\"col\" class=\"col_text\">\n              Type\n            </th>\n            <th scope=\"col\" class=\"col_text\">\n              Nom\n            </th>\n            <th scope=\"col\">\n              <span class=\"screen-reader-text\">\n                Actions\n              </span>\n            </th>\n          </tr>\n        </thead>\n        <tbody></tbody>\n      </table>\n    </div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "      <h3>\n        Fichiers facultatifs\n      </h3>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "      <p><em>Pas de fichier facultatif attaché au document</em></p>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "          <thead>\n            <tr>\n              <th scope=\"col\" class=\"col_text\">\n                Nom\n              </th>\n              <th scope=\"col\">\n                <span class=\"screen-reader-text\">\n                  Actions\n                </span>\n              </th>\n            </tr>\n          </thead>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"title collapse_title\">\n  <a\n    data-target=\"#files-content\"\n    data-toggle=\"collapse\"\n    aria-expanded=\"true\"\n    aria-controls=\"files-content\"\n  >\n    <svg class=\"arrow\">\n      <use href=\"../static/icons/endi.svg#chevron-down\"></use>\n    </svg>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_warning") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":11,"column":4},"end":{"line":23,"column":11}}})) != null ? stack1 : "")
    + "    Fichiers\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_warning") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":25,"column":4},"end":{"line":29,"column":11}}})) != null ? stack1 : "")
    + "  </a>\n</h2>\n<div class=\"collapse_content\">\n  <div id=\"files-content\" class=\"content collapse in\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_requirements") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":34,"column":4},"end":{"line":73,"column":11}}})) != null ? stack1 : "")
    + "    <div class=\"separate_top\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_other_files") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(11, data, 0),"data":data,"loc":{"start":{"line":75,"column":4},"end":{"line":81,"column":11}}})) != null ? stack1 : "")
    + "      <div class=\"table_container\">\n        <table class=\"other-files files\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_other_files") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":84,"column":8},"end":{"line":97,"column":15}}})) != null ? stack1 : "")
    + "          <tbody></tbody>\n        </table>\n      </div>\n    </div>\n    <div class=\"content_vertical_padding align_right\">\n      <button type=\"button\" class=\"btn add-file-btn\">\n        <svg>\n          <use href=\"../static/icons/endi.svg#paperclip\"></use>\n        </svg>\n        Attacher un fichier\n      </button>\n    </div>\n  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/files/templates/FileRequirementView.mustache":
/*!*********************************************************************!*\
  !*** ./src/task/views/files/templates/FileRequirementView.mustache ***!
  \*********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <td class=\"col_status clickable\"\n        title=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":3,"column":42},"end":{"line":3,"column":57}}}) : helper))) != null ? stack1 : "")
    + "\"\n        aria-label=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":4,"column":47},"end":{"line":4,"column":62}}}) : helper))) != null ? stack1 : "")
    + "\"\n    >\n        <span class='icon status "
    + alias4(((helper = (helper = lookupProperty(helpers,"status") || (depth0 != null ? lookupProperty(depth0,"status") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data,"loc":{"start":{"line":6,"column":33},"end":{"line":6,"column":43}}}) : helper)))
    + "'>\n            <svg><use href=\"/static/icons/endi.svg#"
    + alias4(((helper = (helper = lookupProperty(helpers,"status") || (depth0 != null ? lookupProperty(depth0,"status") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data,"loc":{"start":{"line":7,"column":51},"end":{"line":7,"column":63}}}) : helper)))
    + "\"></use></svg>\n        </span>\n    </td>\n    <td class='col_text clickable'\n            title=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":11,"column":46},"end":{"line":11,"column":61}}}) : helper))) != null ? stack1 : "")
    + "\"\n            aria-label=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":12,"column":51},"end":{"line":12,"column":66}}}) : helper))) != null ? stack1 : "")
    + "\"\n    >\n        <a href=\"#\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":14,"column":20},"end":{"line":14,"column":33}}}) : helper))) != null ? stack1 : "")
    + "</a>\n    </td>\n    <td class=\"col_text clickable\"\n        title=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":17,"column":42},"end":{"line":17,"column":57}}}) : helper))) != null ? stack1 : "")
    + "\"\n        aria-label=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":18,"column":47},"end":{"line":18,"column":62}}}) : helper))) != null ? stack1 : "")
    + "\"\n    >\n        <a href=\"#\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":20,"column":20},"end":{"line":20,"column":33}}}) : helper)))
    + "</a>\n    </td>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <td class=\"col_status\">\n        <span class='icon status "
    + alias4(((helper = (helper = lookupProperty(helpers,"status") || (depth0 != null ? lookupProperty(depth0,"status") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data,"loc":{"start":{"line":24,"column":33},"end":{"line":24,"column":43}}}) : helper)))
    + "'>\n            <svg><use href=\"/static/icons/endi.svg#"
    + alias4(((helper = (helper = lookupProperty(helpers,"status") || (depth0 != null ? lookupProperty(depth0,"status") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"status","hash":{},"data":data,"loc":{"start":{"line":25,"column":51},"end":{"line":25,"column":63}}}) : helper)))
    + "\"></use></svg>\n        </span>\n    </td>\n    <td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":28,"column":25},"end":{"line":28,"column":38}}}) : helper))) != null ? stack1 : "")
    + "</td>\n    <td class=\"col_text\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"file_name") || (depth0 != null ? lookupProperty(depth0,"file_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"file_name","hash":{},"data":data,"loc":{"start":{"line":29,"column":25},"end":{"line":29,"column":38}}}) : helper)))
    + "</td>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "<td class=\"col_actions width_three\">\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "<td class=\"col_actions width_two\">\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "    <button type=\"button\" class='btn icon only btn-edit' title=\"Modifier le fichier\" aria-label=\"Modifier le fichier\">\n        <svg><use href=\"/static/icons/endi.svg#pen\"></use></svg>\n    </button>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "    <button type=\"button\" class='btn icon only btn-view' title=\"Voir le fichier\" aria-label=\"Voir le fichier\">\n		<svg><use href=\"/static/icons/endi.svg#eye\"></use></svg>\n	</button>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "	<button type=\"button\" class='btn icon only btn-add' title=\"Ajouter un fichier\" aria-label=\"Ajouter un fichier\">\n		<svg><use href=\"/static/icons/endi.svg#paperclip\"></use></svg>\n	</button>\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "	<button type=\"button\" class='btn icon only btn-validate' title=\"Valider le fichier fourni ?\" aria-label=\"Valider le fichier fourni ?\">\n		<svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n	</button>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_preview") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":30,"column":7}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_valid_link") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":31,"column":0},"end":{"line":35,"column":7}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_edit") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":36,"column":1},"end":{"line":40,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_preview") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":4},"end":{"line":45,"column":8}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_add") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":46,"column":1},"end":{"line":50,"column":8}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_valid_link") : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":51,"column":1},"end":{"line":55,"column":8}}})) != null ? stack1 : "")
    + "</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/files/templates/FileView.mustache":
/*!**********************************************************!*\
  !*** ./src/task/views/files/templates/FileView.mustache ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "clickable";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        title=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":3,"column":42},"end":{"line":3,"column":52}}}) : helper))) != null ? stack1 : "")
    + "\"\n        aria-label=\"Cliquer pour prévisualiser "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":4,"column":47},"end":{"line":4,"column":57}}}) : helper))) != null ? stack1 : "")
    + "\"\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "<a href=\"#\">";
},"7":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <small>("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":10,"column":14},"end":{"line":10,"column":29}}}) : helper)))
    + ")</small>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "</a>";
},"11":function(container,depth0,helpers,partials,data) {
    return "    <button type=\"button\" class='btn icon only btn-view' title=\"Voir le fichier\" aria-label=\"Voir le fichier\">\n      <svg><use href=\"/static/icons/endi.svg#eye\"></use></svg>\n    </button>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class=\"col_text "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_previewable") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":20},"end":{"line":1,"column":58}}})) != null ? stack1 : "")
    + "\"\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_previewable") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":5,"column":11}}})) != null ? stack1 : "")
    + ">\n  "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_previewable") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":2},"end":{"line":7,"column":44}}})) != null ? stack1 : "")
    + "\n    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":8,"column":12}}}) : helper)))
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description_differs") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":4},"end":{"line":11,"column":11}}})) != null ? stack1 : "")
    + "    "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_previewable") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":12,"column":4},"end":{"line":12,"column":38}}})) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_actions width_two\">\n  <button\n    type=\"button\"\n    class=\"btn icon only btn-edit\"\n    title=\"Modifier le fichier\"\n    aria-label=\"Modifier le fichier\"\n  >\n    <svg>\n      <use href=\"/static/icons/endi.svg#pen\"></use>\n    </svg>\n  </button>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_previewable") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":25,"column":2},"end":{"line":29,"column":9}}})) != null ? stack1 : "")
    + "</td>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/payments/templates/PaymentBlockView.mustache":
/*!*********************************************************************!*\
  !*** ./src/task/views/payments/templates/PaymentBlockView.mustache ***!
  \*********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class='title collapse_title'>\n    <a\n        data-target='#payment-content'\n        data-toggle='collapse'\n        aria-expanded=\"false\"\n        aria-controls=\"payment-content\"\n        >\n		<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n        Paiements et acomptes\n    </a>\n</h2>\n<div class='collapse_content'>\n	<div id=\"payment-content\" class='content collapse' aria-expanded=\"false\">\n		<div class='payment_display-container'></div>\n		<div class='payment-deposit-container'></div>\n		<div class='payment_times-container'></div>\n		<div class='payment-lines-container'></div>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/payments/templates/PaymentConditionBlockView.mustache":
/*!******************************************************************************!*\
  !*** ./src/task/views/payments/templates/PaymentConditionBlockView.mustache ***!
  \******************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "<h2 class=\"title collapse_title\">\n	<a\n		data-target=\"#payment-conditions-content\"\n		data-toggle=\"collapse\"\n		aria-expanded=\"false\"\n		aria-controls=\"payment-conditions-content\"\n		>\n		<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n		<span class=\"icon status error\" title=\"Ce bloc comporte une erreur\">\n			<svg><use href=\"/static/icons/endi.svg#exclamation-triangle\"></use></svg>\n		</span>\n		<span class=\"icon status success\" title=\"Ce bloc est valide\">\n			<svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n		</span>\n		Conditions de paiement\n	</a>\n</h2>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "<h2 class=\"title\">Conditions de paiement</h2>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "class=\"collapse_content\"";
},"7":function(container,depth0,helpers,partials,data) {
    return "class='content collapse' aria-expanded=\"false\"";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collapsed") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":21,"column":7}}})) != null ? stack1 : "")
    + "<div "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collapsed") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":5},"end":{"line":22,"column":53}}})) != null ? stack1 : "")
    + ">\n	<div id=\"payment-conditions-content\" "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"collapsed") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":38},"end":{"line":23,"column":108}}})) != null ? stack1 : "")
    + ">\n	<div class='content'>\n		<div class='errors'></div>\n		<div class='predefined-conditions'></div>\n		<div class='conditions'></div>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/payments/templates/PaymentDepositView.mustache":
/*!***********************************************************************!*\
  !*** ./src/task/views/payments/templates/PaymentDepositView.mustache ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "<td class='col_date date'> à la commande </td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text description'>\n    Facture d’acompte\n</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_date") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":6,"column":7}}})) != null ? stack1 : "")
    + "<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"amount_label") || (depth0 != null ? lookupProperty(depth0,"amount_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"amount_label","hash":{},"data":data,"loc":{"start":{"line":7,"column":23},"end":{"line":7,"column":43}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_actions width_two'></td>\n<td class='col_actions row_ordering'></td>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/payments/templates/PaymentLineFormView.mustache":
/*!************************************************************************!*\
  !*** ./src/task/views/payments/templates/PaymentLineFormView.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section id=\"payments_form\" class=\"modal_view size_small\">\n	<form class='form taskline-form'>\n		<div role=\"dialog\" id=\"payments-forms\" aria-modal=\"true\" aria-labelledby=\"payments-forms_title\">\n			<div class=\"modal_layout\">\n				<header>\n					<button class=\"icon only unstyled close\" type='button' title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\" onclick=\"toggleModal('payments_form'); return false;\">\n						<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n					</button>\n					<h2 id=\"payments-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":9,"column":35},"end":{"line":9,"column":46}}}) : helper)))
    + "</h2>\n				</header>\n				<div class=\"modal_content_layout\">\n					<div class=\"modal_content\">\n						<div class='order'></div>\n						<div class='description required'></div>\n						<div class='date required'></div>\n						<div class='amount required'></div>\n					</div>\n					<footer>\n						<button\n							class='btn btn-primary btn-success'\n							type='submit'\n							value='submit'>\n							"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":23,"column":7},"end":{"line":23,"column":18}}}) : helper)))
    + "\n						</button>\n						<button\n							class='btn'\n							type='reset'\n							value='cancel'>\n							Annuler\n						</button>\n					</footer>\n				</div>\n			</div>\n		</div>\n	</form>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/payments/templates/PaymentLineTableView.mustache":
/*!*************************************************************************!*\
  !*** ./src/task/views/payments/templates/PaymentLineTableView.mustache ***!
  \*************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "				<th scope='col' class='col_date date'>Date</th>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "5";
},"5":function(container,depth0,helpers,partials,data) {
    return "4";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class='payment-line-modal-container'></div>\n<div class='table_container'>\n	<table class=\"hover_table\">\n		<thead>\n			<tr>\n				<th scope='col' class='col_text description'>Libellé</th>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_date") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":4},"end":{"line":9,"column":11}}})) != null ? stack1 : "")
    + "				<th scope='col' class='col_number'>Montant</th>\n				<th scope='col' class='col_actions width_two' title='Actions'><span class=\"screen-reader-text\">Actions</span></th>\n				<th scope=\"col\" class=\"col_actions row_ordering\" title=\"Actions (classement)\"><span class=\"screen-reader-text\">Actions (classement)</span></th>\n			</tr>\n		</thead>\n		<tbody class=\"deposit lines\">		\n		</tbody>\n		<tbody class=\"paymentlines lines\">	\n		</tbody>\n		<tfoot>\n			<tr class='row actions'>\n				<td \n					colspan='\n					"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_date") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":23,"column":5},"end":{"line":23,"column":39}}})) != null ? stack1 : "")
    + "\n					'\n					class='col_actions'>\n					<span class='addbutton'></span>\n				</div>\n			</tr>\n		</tfoot>\n	</table>\n</div>\n<div class='modalregion'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/payments/templates/PaymentLineView.mustache":
/*!********************************************************************!*\
  !*** ./src/task/views/payments/templates/PaymentLineView.mustache ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_date date'>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"date") || (depth0 != null ? lookupProperty(depth0,"date") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"date","hash":{},"data":data,"loc":{"start":{"line":5,"column":26},"end":{"line":5,"column":36}}}) : helper)))
    + " </td>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "		<li>\n			<button type='button' class='btn icon only negative delete' title='Supprimer' aria-label='Supprimer'>\n				<svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg>\n			</button>\n		</li>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":4},"end":{"line":33,"column":11}}})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data) {
    return "		<li>\n			<button type='button' class='btn icon only up' title='Déplacer ce produit vers le haut' aria-label='Déplacer ce produit vers le haut'>\n				<svg><use href=\"/static/icons/endi.svg#arrow-up\"></use></svg>\n			</button>\n		</li>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_not_before_last") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":36,"column":4},"end":{"line":42,"column":11}}})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    return "		<li>\n			<button type='button' class='btn icon only down' title='Déplacer ce produit vers le bas' aria-label='Déplacer ce produit vers le bas'>\n				<svg><use href=\"/static/icons/endi.svg#arrow-down\"></use></svg>\n			</button>\n		</li>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text description'>\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_date") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":0},"end":{"line":6,"column":7}}})) != null ? stack1 : "")
    + "<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"amount") || (depth0 != null ? lookupProperty(depth0,"amount") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"amount","hash":{},"data":data,"loc":{"start":{"line":7,"column":23},"end":{"line":7,"column":37}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_actions width_two'>\n	<ul>\n		<li>\n			<button type='button' class='btn icon only edit' title='Modifier' aria-label='Modifier'>\n				<svg><use href=\"/static/icons/endi.svg#pen\"></use></svg>\n			</button>\n		</li>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":15,"column":2},"end":{"line":21,"column":9}}})) != null ? stack1 : "")
    + "	</ul>\n</td>\n<td class='col_actions row_ordering'>\n	<ul>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_first") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":26,"column":4},"end":{"line":34,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_last") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":35,"column":4},"end":{"line":43,"column":11}}})) != null ? stack1 : "")
    + "	</ul>\n</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/related_estimation/templates/RelatedEstimationCollectionView.mustache":
/*!**********************************************************************************************!*\
  !*** ./src/task/views/related_estimation/templates/RelatedEstimationCollectionView.mustache ***!
  \**********************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "    <span class='children'></span>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "<div class='children'></div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4>\n    Devis de référence\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"multiple") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":4},"end":{"line":5,"column":15}}})) != null ? stack1 : "")
    + "</h4>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"multiple") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":0},"end":{"line":9,"column":7}}})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/task/views/related_estimation/templates/RelatedEstimationView.mustache":
/*!************************************************************************************!*\
  !*** ./src/task/views/related_estimation/templates/RelatedEstimationView.mustache ***!
  \************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<a href='"
    + alias4(((helper = (helper = lookupProperty(helpers,"url") || (depth0 != null ? lookupProperty(depth0,"url") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data,"loc":{"start":{"line":1,"column":9},"end":{"line":1,"column":16}}}) : helper)))
    + ".html' class=\"icon\" target=\"_blank\" title=\"Voir ce devis dans une nouvelle fenêtre\" aria-label=\"Voir ce devis dans une nouvelle fenêtre\">\n    <svg><use href=\"/static/icons/endi.svg#file-alt\"></use></svg>\n    "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":3,"column":4},"end":{"line":3,"column":13}}}) : helper)))
    + "\n</a>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/resume/templates/Contributions.mustache":
/*!****************************************************************!*\
  !*** ./src/task/views/resume/templates/Contributions.mustache ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <th scope=\"row\" title=\"Contribution à la CAE basée sur le Total HT\">\n          Contribution à la CAE\n        </th>\n        <td>\n          "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"contribution") || (depth0 != null ? lookupProperty(depth0,"contribution") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"contribution","hash":{},"data":data,"loc":{"start":{"line":20,"column":10},"end":{"line":20,"column":28}}}) : helper))) != null ? stack1 : "")
    + "\n        </td>\n      </tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <th scope=\"row\" title=\"Montant de l'assurance basée sur le Total HT\">\n          Assurance\n        </th>\n        <td>\n          "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"insurance") || (depth0 != null ? lookupProperty(depth0,"insurance") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"insurance","hash":{},"data":data,"loc":{"start":{"line":30,"column":10},"end":{"line":30,"column":25}}}) : helper))) != null ? stack1 : "")
    + "\n        </td>\n      </tr>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4 class=\"content_vertical_padding\">\n  Contributions\n</h4>\n<table class=\"top_align_table\">\n  <tbody>\n    <tr>\n      <th scope=\"row\" title=\"Total des contributions\">\n        Total\n      </th>\n      <td>\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total") || (depth0 != null ? lookupProperty(depth0,"total") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"total","hash":{},"data":data,"loc":{"start":{"line":11,"column":8},"end":{"line":11,"column":19}}}) : helper))) != null ? stack1 : "")
    + "\n      </td>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_contribution") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":14,"column":4},"end":{"line":23,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_insurance") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":4},"end":{"line":33,"column":11}}})) != null ? stack1 : "")
    + "  </tbody>\n</table>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/resume/templates/Details.mustache":
/*!**********************************************************!*\
  !*** ./src/task/views/resume/templates/Details.mustache ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "          ("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"hours") || (depth0 != null ? lookupProperty(depth0,"hours") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"hours","hash":{},"data":data,"loc":{"start":{"line":10,"column":11},"end":{"line":10,"column":20}}}) : helper)))
    + "h)\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4 class=\"content_vertical_padding\">\n  "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":2,"column":2},"end":{"line":2,"column":11}}}) : helper)))
    + "\n</h4>\n<table class=\"top_align_table\">\n  <tbody>\n    <tr>\n      <th scope=\"row\" title=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":7,"column":29},"end":{"line":7,"column":38}}}) : helper)))
    + "\">\n        "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":8,"column":8},"end":{"line":8,"column":17}}}) : helper)))
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_hours") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":8},"end":{"line":11,"column":15}}})) != null ? stack1 : "")
    + "      </th>\n      <td class=\"col_number\">\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"flat_cost") || (depth0 != null ? lookupProperty(depth0,"flat_cost") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"flat_cost","hash":{},"data":data,"loc":{"start":{"line":14,"column":8},"end":{"line":14,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n      </td>\n    </tr>\n    <tr>\n      <th scope=\"row\" title=\"Total des frais généraux\">\n        Frais généraux\n      </th>\n      <td class=\"col_number\">\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"general_overhead") || (depth0 != null ? lookupProperty(depth0,"general_overhead") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"general_overhead","hash":{},"data":data,"loc":{"start":{"line":22,"column":8},"end":{"line":22,"column":30}}}) : helper))) != null ? stack1 : "")
    + "\n      </td>\n    </tr>\n    <tr>\n      <th scope=\"row\" title=\"Total des marges\">\n        Marges\n      </th>\n      <td class=\"col_number\">\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"margin") || (depth0 != null ? lookupProperty(depth0,"margin") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"margin","hash":{},"data":data,"loc":{"start":{"line":30,"column":8},"end":{"line":30,"column":20}}}) : helper))) != null ? stack1 : "")
    + "\n      </td>\n    </tr>\n  </tbody>\n</table>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/resume/templates/ResumeView.mustache":
/*!*************************************************************!*\
  !*** ./src/task/views/resume/templates/ResumeView.mustache ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"total-material\"></div>\n    <div class=\"total-labor\"></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"has_contribution") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":4},"end":{"line":7,"column":11}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "      <div class=\"total-contribution\"></div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"layout flex\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"has_price_study") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":2,"column":2},"end":{"line":8,"column":9}}})) != null ? stack1 : "")
    + "  <div class=\"totals\"></div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/resume/templates/SmallResumeView.mustache":
/*!******************************************************************!*\
  !*** ./src/task/views/resume/templates/SmallResumeView.mustache ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"has_discounts") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":4},"end":{"line":12,"column":11}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <dt> Total TTC avant remise</dt>\n      <dd>\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc_before") || (depth0 != null ? lookupProperty(depth0,"ttc_before") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ttc_before","hash":{},"data":data,"loc":{"start":{"line":6,"column":8},"end":{"line":6,"column":24}}}) : helper))) != null ? stack1 : "")
    + "\n      </dd>\n      <dt>Total HT avant remise</dt>\n      <dd>\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_before") || (depth0 != null ? lookupProperty(depth0,"ht_before") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_before","hash":{},"data":data,"loc":{"start":{"line":10,"column":8},"end":{"line":10,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n      </dd>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_discounts") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":14,"column":4},"end":{"line":22,"column":11}}})) != null ? stack1 : "")
    + "    <dt>\n      Total HT\n    </dt>\n    <dd>\n      "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":27,"column":6},"end":{"line":27,"column":14}}}) : helper))) != null ? stack1 : "")
    + "\n    </dd>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <dt>\n        Total HT avant remise\n        "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"\\n") || (depth0 != null ? lookupProperty(depth0,"\\n") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"\\n","hash":{},"data":data,"loc":{"start":{"line":17,"column":8},"end":{"line":17,"column":16}}}) : helper)))
    + "\n      </dt>\n      <dd>\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_before") || (depth0 != null ? lookupProperty(depth0,"ht_before") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_before","hash":{},"data":data,"loc":{"start":{"line":20,"column":8},"end":{"line":20,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n      </dd>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<dl class=\"dl-horizontal\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":2,"column":2},"end":{"line":29,"column":9}}})) != null ? stack1 : "")
    + "  <dt>\n    Total TTC\n  </dt>\n  <dd>\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc") || (depth0 != null ? lookupProperty(depth0,"ttc") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"ttc","hash":{},"data":data,"loc":{"start":{"line":34,"column":4},"end":{"line":34,"column":13}}}) : helper))) != null ? stack1 : "")
    + "\n  </dd>\n</dl>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/resume/templates/Totals.mustache":
/*!*********************************************************!*\
  !*** ./src/task/views/resume/templates/Totals.mustache ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"has_discounts") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":6},"end":{"line":16,"column":13}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\n          <th scope=\"row\" title=\"Total TTC avant remise\">\n            Total TTC avant remise\n          </th>\n          <td class=\"col_number\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc_before") || (depth0 != null ? lookupProperty(depth0,"ttc_before") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ttc_before","hash":{},"data":data,"loc":{"start":{"line":13,"column":12},"end":{"line":13,"column":28}}}) : helper))) != null ? stack1 : "")
    + "\n          </td>\n        </tr>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_discounts") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":6},"end":{"line":27,"column":13}}})) != null ? stack1 : "")
    + "      <tr>\n        <th scope=\"row\" title=\"Total HT\">\n          Total HT\n        </th>\n        <td class=\"col_number\">\n          "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":33,"column":10},"end":{"line":33,"column":18}}}) : helper))) != null ? stack1 : "")
    + "\n        </td>\n      </tr>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\n          <th scope=\"row\" title=\"Total HT avant remise\">\n            Total HT avant remise\n          </th>\n          <td class=\"col_number\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_before") || (depth0 != null ? lookupProperty(depth0,"ht_before") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ht_before","hash":{},"data":data,"loc":{"start":{"line":24,"column":12},"end":{"line":24,"column":27}}}) : helper))) != null ? stack1 : "")
    + "\n          </td>\n        </tr>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <th scope=\"row\" title=\"Total TVA Taux "
    + ((stack1 = alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0)) != null ? stack1 : "")
    + "\">\n          TVA\n          "
    + ((stack1 = alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0)) != null ? stack1 : "")
    + "\n        </th>\n        <td class=\"col_number\">\n          "
    + ((stack1 = alias1((depth0 != null ? lookupProperty(depth0,"value") : depth0), depth0)) != null ? stack1 : "")
    + "\n        </td>\n      </tr>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h4 class=\"content_vertical_padding\">\n  Totaux\n</h4>\n<table class=\"top_align_table\">\n  <tbody>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"ttc_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":6,"column":4},"end":{"line":36,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"tvas") : depth0),{"name":"each","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":37,"column":4},"end":{"line":47,"column":13}}})) != null ? stack1 : "")
    + "    <tr>\n      <th scope=\"row\" title=\"Total TTC\">\n        TTC\n      </th>\n      <td class=\"col_number\">\n        "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc") || (depth0 != null ? lookupProperty(depth0,"ttc") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"ttc","hash":{},"data":data,"loc":{"start":{"line":53,"column":8},"end":{"line":53,"column":17}}}) : helper))) != null ? stack1 : "")
    + "\n      </td>\n    </tr>\n  </tbody>\n</table>";
},"useData":true});

/***/ }),

/***/ "./src/task/views/templates/CommonView.mustache":
/*!******************************************************!*\
  !*** ./src/task/views/templates/CommonView.mustache ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "			<div class=\"collapsible separate_top content_vertical_padding\">\n				<h4 class='collapse_title'>\n					<a\n						data-target='#common-more'\n						data-toggle='collapse'\n						aria-expanded=\""
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_more_set") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":43,"column":21},"end":{"line":43,"column":64}}})) != null ? stack1 : "")
    + "\"\n						aria-controls=\"common-more\"\n						>\n						<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n						Mentions facultatives\n					</a>\n				</h4>\n				<div class=\"collapse_content\">\n					<div  id=\"common-more\" class='collapse "
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_more_set") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":51,"column":44},"end":{"line":51,"column":72}}})) != null ? stack1 : "")
    + "' aria-expanded=\""
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_more_set") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":51,"column":89},"end":{"line":51,"column":132}}})) != null ? stack1 : "")
    + "\">\n						<div class='mentions'></div>\n					</div>\n				</div>\n			</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "true";
},"4":function(container,depth0,helpers,partials,data) {
    return "false";
},"6":function(container,depth0,helpers,partials,data) {
    return "in";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"title collapse_title\">\n	<a \n		data-target=\"#common-content\" \n		data-toggle=\"collapse\" \n		aria-expanded=\"true\" \n		aria-controls=\"common-content\"\n		>\n		<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n		<span class=\"icon status error\" title=\"Ce bloc comporte une erreur\">\n			<svg><use href=\"/static/icons/endi.svg#exclamation-triangle\"></use></svg>\n		</span>\n		<span class=\"icon status success\" title=\"Ce bloc est valide\">\n			<svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n		</span>\n		En-têtes du document\n	</a>\n</h2>\n<div class=\"collapse_content\">\n	<div id=\"common-content\" class='content collapse in' aria-expanded=\"true\">\n		<div class='errors'></div>\n		<form class='form' name='common' action=\"#\" onSubmit=\"return false;\">\n			<div class='layout flex'>\n				<div class='col-md-6'><div class='date'></div></div>\n				<div class='col-md-6'><div class='validity_duration'></div></div>\n			</div>\n			<div class='layout flex'>\n				<div class='col-md-6'><div class='description'></div></div>\n				<div class='col-md-6'><div class='address'></div></div>\n			</div>\n            <div class='layout flex two_cols'>\n				<div class='start_date vanishable'></div>\n				<div class='end_date vanishable'></div>\n				<div class='first_visit vanishable'></div>\n				<div class='workplace vanishable'></div>\n				<div class='insurance_id vanishable'></div>\n            </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"has_available_mentions") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":37,"column":12},"end":{"line":56,"column":19}}})) != null ? stack1 : "")
    + "		</form>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/templates/DisplayOptionsView.mustache":
/*!**************************************************************!*\
  !*** ./src/task/views/templates/DisplayOptionsView.mustache ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='input-mode-container'></div>\n<div class='display-units-container'></div>\n<div class='display-ttc-container'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/templates/GeneralView.mustache":
/*!*******************************************************!*\
  !*** ./src/task/views/templates/GeneralView.mustache ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "					<div>\n						<div class=\"business_type\">\n							<div class='form-group'>\n								<span class=\"label\">Type d'affaire</span>\n								<span class=\"data\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? lookupProperty(depth0,"business_type") : depth0)) != null ? lookupProperty(stack1,"label") : stack1), depth0))
    + "</span>\n							</div>\n						</div>\n					</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "					<div>\n						<div class='financial_year'></div>\n					</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"title collapse_title\">\n	<a \n		data-target=\"#general-content\" \n		data-toggle=\"collapse\" \n		aria-expanded=\"true\" \n		aria-controls=\"general-content\"\n		>\n		<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n		<span class=\"icon status error\" title=\"Ce bloc comporte une erreur\">\n			<svg><use href=\"/static/icons/endi.svg#exclamation-triangle\"></use></svg>\n		</span>\n		<span class=\"icon status success\" title=\"Ce bloc est valide\">\n			<svg><use href=\"/static/icons/endi.svg#check\"></use></svg>\n		</span>\n		<span class=\"icon status neutral\" title=\"Ces informations internes n’apparaissent pas dans le document final\">\n			<svg><use href=\"/static/icons/endi.svg#eye-slash\"></use></svg>\n		</span>\n		Informations générales \n		<small>N’apparaissent pas dans le document final</small>\n	</a>\n</h2>\n<div class=\"collapse_content\">\n	<div id=\"general-content\" class='content collapse in' aria-expanded=\"true\">\n		<form class='form' name='common' action=\"#\" onSubmit=\"return false;\">\n			<div class=\"layout flex two_cols\">\n				<div>\n					<div>\n						<div class='name'></div>\n					</div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"business_type") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":30,"column":5},"end":{"line":39,"column":12}}})) != null ? stack1 : "")
    + "					<div class='input_mode'></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"financial_year") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":5},"end":{"line":45,"column":12}}})) != null ? stack1 : "")
    + "				</div>\n				<div class=\"status_history hidden-print in_form\">\n				</div>\n			</div>\n		</form>\n		\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/templates/NotesBlockView.mustache":
/*!**********************************************************!*\
  !*** ./src/task/views/templates/NotesBlockView.mustache ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class='title collapse_title'>\n    <a\n        data-target='#notes-content'\n        data-toggle='collapse'\n        aria-expanded=\"false\"\n        aria-controls=\"notes-content\"\n        >\n		<svg class=\"arrow\"><use href=\"/static/icons/endi.svg#chevron-down\"></use></svg>\n        Notes complémentaires\n    </a>\n</h2>\n<div class='collapse_content'>\n	<div id=\"notes-content\" class='content collapse' aria-expanded=\"false\">\n        <div class='notes'></div>\n    </div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/task/views/templates/RootComponent.mustache":
/*!*********************************************************!*\
  !*** ./src/task/views/templates/RootComponent.mustache ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"modalregion\"></div>\n<div id=\"resumeregion\" class=\"separate_top\"></div>\n<div id=\"main-error-region\"></div>\n<div class=\"related-estimation\"></div>\n<div class=\"task-edit\">\n  <div class=\"general-informations private collapsible in\"></div>\n  <div id=\"files\"></div>\n  <div class=\"common-informations collapsible in\"></div>\n  <div class=\"display-options content_double_padding\"></div>\n  <div class=\"composition\"></div>\n  <div class=\"notes collapsible\"></div>\n  <div class=\"payment-conditions collapsible\"></div>\n  <div class=\"payments collapsible\"></div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/widgets/templates/LabelRowWidget.mustache":
/*!*******************************************************!*\
  !*** ./src/widgets/templates/LabelRowWidget.mustache ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "	<th scope=\"row\" class='col_text' colspan=\""
    + container.escapeExpression(container.lambda((depths[1] != null ? lookupProperty(depths[1],"colspan") : depths[1]), depth0))
    + "\">\n		"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":3,"column":2},"end":{"line":3,"column":15}}}) : helper))) != null ? stack1 : "")
    + "\n	</th>\n	<td class='col_number'>\n		"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"value") || (depth0 != null ? lookupProperty(depth0,"value") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data,"loc":{"start":{"line":6,"column":2},"end":{"line":6,"column":15}}}) : helper))) != null ? stack1 : "")
    + "\n	</td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"values") : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":8,"column":9}}})) != null ? stack1 : "");
},"useData":true,"useDepths":true});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"task": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) var result = runtime(__webpack_require__);
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkenDI"] = self["webpackChunkenDI"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendor"], () => (__webpack_require__("./src/task/task.js")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=task.js.map